# Terranium
The Terranium text editor.

## Download
No binaries available... yet.

## Building
Prerequisite: C++ Build Tools for Visual Studio 2019

1. Clone this git repository.
1. Open the Developer Command Prompt for VS 2019.
1. Change into the src directory.
1. Run build.bat from the Developer Command Prompt for VS 2019. Use argument release to generate a release build.