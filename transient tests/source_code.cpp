ShapedTextBucketShapedTextBucketShapedTextBucket#include "draw.h"
vShapedTextBucketShapedTextBucketShapedTextBucket#include "engine.h"
ShapedTextBucketShapedTextBucket#include "cpp_parser.h"
ShapedTextBucketShapedTextBucket#include "scrollbar.h"
ShapedTextBucketShapedTextBucket#include "view.h"
ShapedTextBucketShapedTextBucketShapedTextBucket
ShapedTextBucketShapedTextBucketShapedTextBucket
ShapedTextBucketShapedTextBucketShapedTextBucket#include "view_container.h"

// figure out this 99999999999999999999999, we win a million dollars";
char startupe them as pixels ?  ???????????
#define EM_WIDTHS_PER_MOUSEsdfsdfsdfsdfsdf_SCROLL 1
#define LINES_PER_MOUSE_SCROLL 5

???????????????????????????????
#define LINES_PER_ARROWS_SCROLL 3
dsdfsdfLACK
#define CLR_NORMAL_TEXT CLR_WHITE
#define CLR_TEXT_SELECTION_REGION CLR_GREY30

#define EDITOR_MAX_VIEW 50
#define EDITOR_MAX_TABS 200

#define UNDO_STORAGE_BLOCK_SIZE Kilobytes(12)

char *CPP_file_asscociatons[] = {
    "cpp",
    "cxx",
    "cc",
    "c",
    "h",
    "hh",
    "hpp",
    "hxx",
};

char *plain_text_file_asscociatons[] = {
    "txt",
};

struct EditorSettings
{
    u32 tab_size = 4;
    String font_name = MakeString("Fira Code");
};

EditorSettings editor_settings;

struct Clipboard
{
    UTF8 *copied_contents;
    u32 copied_contents_cap;
    u32 copied_contents_size;
};

//FOR now views will ALWAYS use Font_code to draw
enum FontID
{
    Font_ui,
    Font_code,
    Font_count
};

struct EditorTheme
{
    Font **fonts;
};

struct EditorState;
#define EDITOR_COMMAND_FUNC(name) void name(EditorState *editor, Input *input, PlatformFramebuffer *framebuffer)
typedef EDITOR_COMMAND_FUNC(EditorCommandFunc);

struct KeyMap {
    EditorCommandFunc *key_to_func[PLATFORM_MAX_KEY];
};

//TODO this is too much data!
//     put this into a hash table!
struct KeyMapSet {
    KeyMap ctrl_keymap;
    KeyMap alt_keymap;
    KeyMap shift_keymap;

    KeyMap ctrl_shift_keymap;
    KeyMap shift_alt_keymap;
    KeyMap ctrl_alt_keymap;

    KeyMap all_mod_keymap;
    KeyMap no_mod_keymap;
};

enum DraggingObject{
    Dragging_none,
    Dragging_scrollbar,
    Dragging_view,
};

#define DRAG_SNAP_NEW_VC_BUFFER_PIXELS 100

struct DragState {
    DraggingObject object;
    ViewContainer *vc;

    union {
        Scrollbar *scrollbar;
        LFView *view;
    };
};

//allocate the global storage context
// struct GlobalState
// {
//     MemoryArena 
// }

// Storage storage;
global MemoryArena *global_frame_arena;

//@editor_state
struct EditorState 
{
    MemoryArena total_storage_arena;
    MemoryArena frame_arena;
    
    //TODO really: what is the best place for this so everyone can access it (the below)
    Storage *storage;
    
    Clipboard clipboard;

    LFView *view;
    b32 *free_file_views;
    u32 views_used;
     
    ViewContainer *view_containers;
    u32 view_containers_used;
    u32 max_view_containers;
    
    ViewContainer *root_vc;
    ViewContainer *active_vc;

    EditorTheme *theme;

    //UI STUFF
    DragState drag_state;

    //TODO have more than one keymap
    KeyMapSet keymap_set;

    b32 full_redraw_needed;
    
    //TODO have debug memory and debug state?
#if INTERNAL
    b32 vsync_debug_test;
    b32 vsync_debug_cyan;

    b32 show_performance_info;
#endif
};

inline Font *
GetThemeFont(EditorTheme *theme, FontID id) {
    return theme->fonts[id];
}

inline FontMetrics *
GetThemeFontMetrics(EditorTheme *theme, FontID id) {
    return theme->fonts[id]->metrics;
}

struct ScrollRegion
{
    Recti rect;
    i32 pixdelta;
};


//EditorState g_estate;

internal FileType
Editor_GetFileTypeFromExtension(UTF8 *extension);


