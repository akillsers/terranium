
// TODO check the DPI scaling of these buttons!
const global u32 button_height = 29;
const global u32 button_width = 45;

inline Recti
GetWin10CaptionButtonRect(Tabbar *tabbar, CaptionButtonType type, PlatformFramebuffer *framebuffer)
{
    u32 button_x = framebuffer->width - button_width*type;

    Recti close_button_rect = {};
    close_button_rect.x = button_x;
    close_button_rect.y = 0;
    close_button_rect.height = button_height;
    close_button_rect.width = button_width;

    return close_button_rect;
}

internal void
RedrawCloseButton(Tabbar *tabbar, PlatformFramebuffer *framebuffer)
{
    f32 icon_to_button_length = 0.33333f;

    Recti close_button_rect = GetWin10CaptionButtonRect(tabbar, CaptionButton_close, framebuffer);
    Color close_icon_color;

    if(tabbar->activated == TabbarUIObject_close)
    {
        DrawRectangle(close_button_rect, CLR_CLOSE_BUTTON_DEPRESSED, framebuffer);
    }
    else if(tabbar->hover == TabbarUIObject_close)
    {
        DrawRectangle(close_button_rect, CLR_CLOSE_BUTTON_HOVER, framebuffer);

        close_icon_color = CLR_WHITE;
    }
    else
    {
        DrawRectangle(close_button_rect, CLR_TABBAR_BACKGROUND, framebuffer);
        close_icon_color = CLR_BLACK;
    }
    
    
    Recti close_icon_rect = {};
    close_icon_rect.width = Round((f32)close_button_rect.width * icon_to_button_length);
    close_icon_rect.height = close_icon_rect.width;

    close_icon_rect.x = close_button_rect.x + close_button_rect.width/2 - close_icon_rect.width/2;
    close_icon_rect.y = close_button_rect.y + close_button_rect.height/2 - close_icon_rect.height/2;

    DrawLine(close_icon_rect.x,
             close_icon_rect.y,
             close_icon_rect.x + close_icon_rect.width,
             close_icon_rect.y + close_icon_rect.height,
             close_icon_color, framebuffer);

    DrawLine(close_icon_rect.x + close_icon_rect.width,
             close_icon_rect.y,
             close_icon_rect.x,
             close_icon_rect.y + close_icon_rect.height,
             close_icon_color, framebuffer);
}

internal void
RedrawMiddleButton(Tabbar *tabbar, Input *input, PlatformFramebuffer *framebuffer)
{
    Recti middle_button_rect = GetWin10CaptionButtonRect(tabbar, CaptionButton_middle, framebuffer);

    Color background_color;
    if(tabbar->hover == TabbarUIObject_middle_button)
    {
        if(tabbar->activated)
        {
            background_color = CLR_CAPTION_BUTTON_DEPRESSED;
        }
        else
        {
            background_color = CLR_CAPTION_BUTTON_HOVER;
        }
    }
    else
    {
        background_color = CLR_TABBAR_BACKGROUND;
    }

    DrawRectangle(middle_button_rect, background_color, framebuffer);
    
    if(input->main_window->size_state == Window_restored)
    {
        f32 icon_to_button_length = 0.222222f;

        Recti maximize_icon_rect = {};
        maximize_icon_rect.width = Round((f32)middle_button_rect.width * icon_to_button_length);
        maximize_icon_rect.height = maximize_icon_rect.width;

        maximize_icon_rect.x = middle_button_rect.x + middle_button_rect.width/2 - maximize_icon_rect.width/2;
        maximize_icon_rect.y = middle_button_rect.y + middle_button_rect.height/2 - maximize_icon_rect.height/2;
        DrawRectOutline(maximize_icon_rect, 1, CLR_BLACK, framebuffer);
    }
    else
    {
        f32 container_to_button_length = 0.222222f;
        
        Recti restore_icon_container = {};
        restore_icon_container.width = Round((f32)middle_button_rect.width * container_to_button_length);
        restore_icon_container.height = restore_icon_container.width;
        restore_icon_container.x = middle_button_rect.x + middle_button_rect.width/2 - restore_icon_container.width/2;
        restore_icon_container.y = middle_button_rect.y + middle_button_rect.height/2 - restore_icon_container.height/2;
        
        f32 icon_to_container_length = 0.80f;

        Recti restore_icon_square_1 = {};
        restore_icon_square_1.width = Round((f32)restore_icon_container.width * icon_to_container_length);
        restore_icon_square_1.height = restore_icon_square_1.width;
        restore_icon_square_1.x = restore_icon_container.x;
        restore_icon_square_1.y = restore_icon_container.y + restore_icon_container.height - restore_icon_square_1.height;
        DrawRectOutline(restore_icon_square_1, 1, CLR_BLACK, framebuffer);

        Recti restore_icon_square_2 = {};
        restore_icon_square_2.width = restore_icon_square_1.width;
        restore_icon_square_2.height = restore_icon_square_2.width;
        restore_icon_square_2.x = restore_icon_container.x + restore_icon_container.width - restore_icon_square_1.width;
        restore_icon_square_2.y = restore_icon_container.y;
        DrawRectOutline(restore_icon_square_2, 1, CLR_BLACK, framebuffer);
    }
}

inline void
ActivateTabbarUIObject(Tabbar *tabbar, TabbarUIObject object, void *ptr)
{
    if(!tabbar->activated)
    {
        tabbar->activated = object;
        tabbar->activated_ptr = ptr;
    }
}

internal void
RedrawMinimizeButton(Tabbar *tabbar, PlatformFramebuffer *framebuffer)
{
    Recti minimize_button_rect = GetWin10CaptionButtonRect(tabbar, CaptionButton_minimize, framebuffer);

    Color background_color;
    if(tabbar->activated == TabbarUIObject_minimize)
    {
        background_color = CLR_CAPTION_BUTTON_DEPRESSED;
    }
    else if(tabbar->hover == TabbarUIObject_minimize)
    {
        background_color = CLR_CAPTION_BUTTON_HOVER;
    }
    else
    {
        background_color = CLR_TABBAR_BACKGROUND;
    }

    DrawRectangle(minimize_button_rect, background_color, framebuffer);

    f32 icon_to_button_length = 0.2222f;

    Recti minimize_icon_rect = {};
    minimize_icon_rect.width = minimize_button_rect.width * icon_to_button_length;
    minimize_icon_rect.x = minimize_button_rect.x + minimize_button_rect.width/2 - minimize_icon_rect.width/2;
    minimize_icon_rect.height = 1;
    minimize_icon_rect.y = minimize_button_rect.y + minimize_button_rect.height/2;
    DrawRectangle(minimize_icon_rect, CLR_BLACK, framebuffer);
}

internal void
UpdateCaptionButtons(Tabbar *tabbar, Input *input, PlatformFramebuffer *framebuffer)
{
    Color close_icon_color;

    // TODO this is a lot of unecessary recalculation of the button bboxes
    Recti middle_button_rect = GetWin10CaptionButtonRect(tabbar, CaptionButton_middle, framebuffer);
    Recti minimize_button_rect = GetWin10CaptionButtonRect(tabbar, CaptionButton_minimize, framebuffer);
    Recti close_button_rect = GetWin10CaptionButtonRect(tabbar, CaptionButton_close, framebuffer);
    
    if(PosInRecti(input->mouse_pos, close_button_rect))
    {
        input->non_client_hittest = HTCLOSE;
        tabbar->hover = TabbarUIObject_close;

        if(KeyPressed(input->mouse_buttons[MouseButton_left]))
        {
            ActivateTabbarUIObject(tabbar, TabbarUIObject_close, 0);
        }
    }

    if(PosInRecti(input->mouse_pos, middle_button_rect))
    {
        input->non_client_hittest = HTMAXBUTTON;
        tabbar->hover = TabbarUIObject_middle_button;
            
        if(KeyPressed(input->mouse_buttons[MouseButton_left]))
        {
            ActivateTabbarUIObject(tabbar, TabbarUIObject_middle_button, 0);
        }
    }

    if(PosInRecti(input->mouse_pos, minimize_button_rect))
    {
        input->non_client_hittest = HTMINBUTTON;
        tabbar->hover = TabbarUIObject_minimize;

        if(KeyPressed(input->mouse_buttons[MouseButton_left]))
        {
            ActivateTabbarUIObject(tabbar, TabbarUIObject_minimize, 0);
        }
    }
}

internal void
SwitchToTab(Tabbar *tabbar, Tab *tab)
{
    tabbar->active_tab = tab;
}

internal void
RedrawTab(Tabbar *tabbar, Tab *tab, PlatformFramebuffer *framebuffer)
{
    Color color = tab == (tabbar->active_tab) ? CLR_WHITE : CLR_TABBAR_BACKGROUND;
        
    DrawRectangle(tab->bbox, color, framebuffer);

    u32 x_margin = 10;
    u32 y_margin = 5;

    RenderTextTransform transform = {};
    transform.bbox = {};
    transform.bbox.x = tab->bbox.x + x_margin;
    transform.bbox.y = tab->bbox.y + y_margin;
    transform.bbox.width = tab->bbox.width - x_margin*2;
    transform.bbox.height = tab->bbox.height - y_margin*2;
    transform.vertical_alignment = TextAlign_center;

    DrawRenderText(tab->render_text, transform, framebuffer);
}

inline Recti
GetNewTabRect(Tabbar *tabbar)
{
    Tab *last_tab = tabbar->sentinel_tab->prev;

    Recti new_tab_rect = {};
    new_tab_rect.x = last_tab->bbox.x + last_tab->bbox.width;
    new_tab_rect.y = tabbar->bbox.y;
    new_tab_rect.width = tabbar->bbox.height;
    new_tab_rect.height = tabbar->bbox.height;

    return new_tab_rect;
}

internal void
RedrawNewTabIcon(Tabbar *tabbar, PlatformFramebuffer *framebuffer)
{
    Recti new_tab_rect = GetNewTabRect(tabbar);
    
    u32 x_margin = 12;
    u32 y_margin = 12;
    Recti new_tab_icon_rect = CreateRectInsideRect(new_tab_rect, x_margin, y_margin);
    // DrawRectangle(new_tab_icon_rect, CLR_LBLUE, framebuffer);

    Color background_color;

    if(tabbar->hover == TabbarUIObject_new_tab)
    {
        background_color = CLR_GREY60;
    }
    else
    {
        background_color = CLR_TABBAR_BACKGROUND;
    }
    
    DrawRectangle(new_tab_rect, background_color, framebuffer);
    
    u32 line_thickness = 2;
    
    Recti vertical_line = {};
    vertical_line.x = new_tab_icon_rect.x + new_tab_icon_rect.width/2 - line_thickness/2;
    vertical_line.y = new_tab_icon_rect.y;
    vertical_line.width = line_thickness;
    vertical_line.height = new_tab_icon_rect.height;
    DrawRectangle(vertical_line, CLR_GREY20, framebuffer);

    Recti horizontal_line = {};
    horizontal_line.x = new_tab_icon_rect.x;
    horizontal_line.y = new_tab_icon_rect.y + new_tab_icon_rect.height/2 - line_thickness/2;
    horizontal_line.width = new_tab_icon_rect.width;
    horizontal_line.height = line_thickness;
    DrawRectangle(horizontal_line, CLR_GREY20, framebuffer);
}

internal TabbarInteraction
UpdateAndRenderTabs(Tabbar *tabbar, Input *input, PlatformFramebuffer *framebuffer)
{
    TabbarInteraction result = {};

    for(Tab *tab = tabbar->sentinel_tab->next;
        tab != tabbar->sentinel_tab;
        tab = tab->next)
    {
        if(PosInRecti(input->mouse_pos, tab->bbox))
        {
            if(input->non_client_hittest == HTCAPTION)
            {
                // the tabs shouldn't hittest as a caption area
                // so let's correct that
                input->non_client_hittest = HTCLIENT;
            }

            if(KeyPressed(input->mouse_buttons[MouseButton_left]))
            {
                SwitchToTab(tabbar, tab);

                result.command = TabbarInteraction_switch_to_tab;
                result.new_active_tab = tab;
            }
        }
    }

    Recti new_tab_rect = GetNewTabRect(tabbar);
    if(PosInRecti(input->mouse_pos, new_tab_rect))
    {
        // the tabs shouldn't hittest as a caption area
        // so let's correct that
        input->non_client_hittest = HTCLIENT;
        tabbar->hover = TabbarUIObject_new_tab;

        if(KeyPressed(input->mouse_buttons[MouseButton_left]))
        {
            result.command = TabbarInteraction_create_new_tab;
        }
    }
        
        
    return result;
}

// TODO consider NOT passing input in here?
//      (we need it to determine which middle button to draw)
internal void
RedrawTabbar(Tabbar *tabbar, Input *input, PlatformFramebuffer *framebuffer)
{
    if(tabbar->full_redraw_needed)
    {
        Color color = CLR_TABBAR_BACKGROUND;
        DrawRectangle(tabbar->bbox, color, framebuffer);

        for(Tab *tab = tabbar->sentinel_tab->next;
            tab != tabbar->sentinel_tab;
            tab = tab->next)
        {
            RedrawTab(tabbar, tab, framebuffer);
        }
        

        RedrawNewTabIcon(tabbar, framebuffer);
        tabbar->full_redraw_needed = false;

        RedrawCloseButton(tabbar, framebuffer);
        RedrawMiddleButton(tabbar, input, framebuffer);
        RedrawMinimizeButton(tabbar, framebuffer);
    }
}

internal void
LayoutTabs(Tabbar *tabbar)
{
    if(tabbar->num_tabs > 0)
    {
        u32 available_space = tabbar->bbox.width * 4/5;
        u32 maximum_width = 200;
    
        u32 tab_width = Min(available_space / tabbar->num_tabs, maximum_width);
    
        u32 tab_x = 0;

        u32 tab_height = tabbar->bbox.height;
        u32 tab_y = tabbar->bbox.y;
        for(Tab *tab = tabbar->sentinel_tab->next;
            tab != tabbar->sentinel_tab;
            tab = tab->next)
        {
            tab->bbox = {};
            tab->bbox.x = tab_x;
            tab->bbox.y = tab_y;
            tab->bbox.width = tab_width;
            tab->bbox.height = tab_height;
        
            tab_x += tab_width;
        }
    }

    tabbar->full_redraw_needed = true;
}

internal Tab *
CreateTab(EditorState *editor, Tabbar *tabbar, String string, void *data, MemoryArena *arena)
{
    Tab *tab = PushType(arena, Tab);

    DL_INSERT_AFTER(tabbar->sentinel_tab->prev, tab);

    tab->render_text = CreateRenderText(RenderTextDataType_string, arena);
    tab->render_text->font = GetThemeFont(editor->theme, Font_tab);
    tab->render_text->string = string;
    tab->render_text->text_color = CLR_BLACK;
    tab->data = data;

    tabbar->active_tab = tab;
    tabbar->num_tabs++;
    LayoutTabs(tabbar);

    return tab;
}

internal Tab *
FindTabWithMatchingData(EditorState *editor, Tabbar *tabbar, void *data)
{
    Tab *found_tab = 0;

    for(Tab *tab = tabbar->sentinel_tab->next;
        tab != tabbar->sentinel_tab;
        tab = tab->next)
    {
        if(tab->data == data)
        {
            found_tab = tab;
            break;
        }
    }

    return found_tab;
}

internal void
ChangeToTab(EditorState *editor, Tabbar *tabbar, Tab *tab)
{
    tabbar->active_tab = tab;    
}

internal void
DeleteTab(EditorState *editor, Tabbar *tabbar)
{
    
}

internal TabbarInteraction
UpdateAndRenderTabbar(EditorState *editor, Tabbar *tabbar, Input *input, PlatformFramebuffer *framebuffer)
{
    TabbarInteraction result = {};

    if(PosInRecti(input->mouse_pos, tabbar->bbox))
    {
        input->non_client_hittest = HTCAPTION;
    }

    TabbarUIObject last_hover = tabbar->hover;
    void *last_hover_data = tabbar->hover_ptr;
    b32 last_activated = tabbar->activated;
    
    if(input->mouse_buttons[MouseButton_left].down == 0)
    {
        tabbar->activated = TabbarUIObject_null;
        tabbar->activated_ptr = 0;
    }
    
    tabbar->hover = TabbarUIObject_null;
    UpdateCaptionButtons(tabbar, input, framebuffer);
    result = UpdateAndRenderTabs(tabbar, input, framebuffer);

    if(last_hover != tabbar->hover ||
       tabbar->activated != last_activated)
    {
        if(last_hover == TabbarUIObject_new_tab ||
           tabbar->hover == TabbarUIObject_new_tab)
        {
            RedrawNewTabIcon(tabbar, framebuffer);
        }

        if(last_hover == TabbarUIObject_minimize ||
           tabbar->hover == TabbarUIObject_minimize)
        {
            RedrawMinimizeButton(tabbar, framebuffer);
        }

        if(last_hover == TabbarUIObject_middle_button ||
           tabbar->hover == TabbarUIObject_middle_button)
        {
            RedrawMiddleButton(tabbar, input, framebuffer);
        }

        if(last_hover == TabbarUIObject_close ||
           tabbar->hover == TabbarUIObject_close)
        {
            RedrawCloseButton(tabbar, framebuffer);
        }
    }

    RedrawTabbar(tabbar, input, framebuffer);

    return result;
}

internal void
InitializeTabbar(Tabbar *tabbar, MemoryArena *arena)
{
    tabbar->sentinel_tab = PushType(arena, Tab);
    DL_INIT_SENTINEL(tabbar->sentinel_tab);
    tabbar->full_redraw_needed = true;
}
