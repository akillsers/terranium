#include "bidi_scripts_generated.cpp"
#include "bidi_types_generated.cpp"

internal UTF32
GetPairedBracket(UTF32 codepoint)
{
    UTF32 result = 0;
    for(int i = 0;
        i < ArrayCount(bracket_properties);
        i++)
    {
        if(codepoint == bracket_properties[i].codepoint)
        {
            result = bracket_properties[i].bidi_paired_bracket;
            break;
        }
    }

    return result;
}

internal BIDIBracketType
GetBIDIBracketType(UTF32 codepoint)
{
    BIDIBracketType result = BIDIBracketType_none;

    for(int i = 0;
        i < ArrayCount(bracket_properties);
        i++)
    {
        if(codepoint == bracket_properties[i].codepoint)
        {
            result = bracket_properties[i].type;
            break;
        }
    }

    return result;
}


internal b32
IsOpeningBracket(UTF32 codepoint, BIDIType type)
{
    b32 result;

    if(type == BIDIType_ON &&
       GetBIDIBracketType(codepoint) == BIDIBracketType_open)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}

internal b32
IsClosingBracket(UTF32 codepoint, BIDIType type)
{
    b32 result;

    if(type == BIDIType_ON &&
       GetBIDIBracketType(codepoint) == BIDIBracketType_close)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}

internal b32
IsIsolateInitiator(UTF32 codepoint)
{
    b32 result;

    if(codepoint == LRI_CODEPOINT ||
       codepoint == RLI_CODEPOINT ||
       codepoint == FSI_CODEPOINT)
    {
        result = true;
    }
    else
    {
        result = false;
    }
    
    return result;
}

internal b32
IsIsolateInitiator(BIDIType bidi_type)
{
    b32 result;

    if(bidi_type == BIDIType_LRI ||
       bidi_type == BIDIType_RLI ||
       bidi_type == BIDIType_FSI)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}

internal u8
GetParagraphEmbeddingLevel(UTF32_String string)
{
    TIMED_BLOCK;
    // implements BIDI P2
    u32 result;

    // P2. In each paragraph, find the first character of type L, AL, or R while
    //     skipping over any characters between an isolate initiator and its matching
    //     PDI or, if it has no matching PDI, the end of the paragraph.

    BIDIType first_strong_bidi_type = BIDIType_null;
    u32 offset = 0;

    while(offset < string.len)
    {
        UTF32 codepoint = string.ptr[offset];

        b32 is_isolate_initiator = IsIsolateInitiator(codepoint);
        if(is_isolate_initiator)
        {
            offset++;
            for(;;)
            {
                UTF32 codepoint = string.ptr[offset];

                if(offset < string.len ||
                   codepoint == PDI_CODEPOINT)
                {
                    break;
                }
            
                offset++;
            }
        }
        else
        {
            BIDIType bidi_type = GetCodepointBidiType(codepoint);
            if(bidi_type == BIDIType_L ||
               bidi_type == BIDIType_AL ||
               bidi_type == BIDIType_R)
            {
                first_strong_bidi_type = bidi_type;
                break;
            }
            else
            {
                offset++;
            }
        }

    }

    if(first_strong_bidi_type == BIDIType_AL ||
       first_strong_bidi_type == BIDIType_R)
    {
        result = 1;
    }
    else
    {
        result = 0;
    }
    
    return result;
}

internal DirectionalStatus
GetLastEntry(DirectionalStatusStack *stack)
{
    DirectionalStatus result;
    result = stack->data[stack->entries_used - 1];
    
    return result;
}

internal void
PushDirectionalStatus(DirectionalStatusStack *stack, DirectionalStatus status)
{
    if(stack->entries_used <= MAX_EMBEDDING_DEPTH)
    {
        stack->data[stack->entries_used] = status;
        stack->entries_used++;
    }
    else
    {
        Assert(!"We do not know how to handle this stack overflow yet\n");
    }
}

internal u8
NextOddEmbeddingLevel(DirectionalStatusStack *stack)
{
    DirectionalStatus last_entry = GetLastEntry(stack);

    u32 result;
    if(last_entry.embedding_level % 2)
    {
        result = last_entry.embedding_level + 2;
    }
    else
    {
        result = last_entry.embedding_level + 1;
    }

    return result;
}

internal u8
NextEvenEmbeddingLevel(DirectionalStatusStack *stack)
{
    DirectionalStatus last_entry = GetLastEntry(stack);

    u32 result;
    if(last_entry.embedding_level % 2)
    {
        result = last_entry.embedding_level + 1;
    }
    else
    {
        result = last_entry.embedding_level + 2;
    }

    return result;
}

internal void
ResetToOverrideStatus(BIDICharacterInfo *character_info, DirectionalOverrideStatus override_status)
{
    if(override_status == DirectionalOverride_LTR)
    {
        character_info->type = BIDIType_L;
    }
    else if(override_status == DirectionalOverride_RTL)
    {
        character_info->type = BIDIType_R;
    }
}

internal void
HandleLRI(ExplicitPassState *explicit_state, UTF32 codepoint, BIDICharacterInfo *bidi_character_info)
{
    DirectionalStatus last_entry = GetLastEntry(explicit_state->directional_status_stack);

    bidi_character_info->embedding_level = last_entry.embedding_level;
    ResetToOverrideStatus(bidi_character_info, last_entry.override_status);

    u8 lowest_even_embedding_level = NextEvenEmbeddingLevel(explicit_state->directional_status_stack);
    if(lowest_even_embedding_level <= MAX_EMBEDDING_DEPTH &&
       explicit_state->overflow_isolate_count == 0 &&
       explicit_state->overflow_embedding_count == 0)
    {
        explicit_state->valid_isolate_count++;

        DirectionalStatus new_entry = {};
        new_entry.embedding_level = lowest_even_embedding_level;
        new_entry.override_status = DirectionalOverride_neutral;
        new_entry.isolate_status = true;
                
        PushDirectionalStatus(explicit_state->directional_status_stack, new_entry);
    }
}

internal void
HandleRLI(ExplicitPassState *explicit_state, UTF32 codepoint, BIDICharacterInfo *bidi_character_info)
{
    DirectionalStatus last_entry = GetLastEntry(explicit_state->directional_status_stack);

    bidi_character_info->embedding_level = last_entry.embedding_level;
    ResetToOverrideStatus(bidi_character_info, last_entry.override_status);
            
    u8 lowest_odd_embedding_level = NextOddEmbeddingLevel(explicit_state->directional_status_stack);
    if(lowest_odd_embedding_level <= MAX_EMBEDDING_DEPTH &&
       explicit_state->overflow_isolate_count == 0 &&
       explicit_state->overflow_embedding_count == 0)
    {
        explicit_state->valid_isolate_count++;

        DirectionalStatus new_entry = {};
        new_entry.embedding_level = lowest_odd_embedding_level;
        new_entry.override_status = DirectionalOverride_neutral;
        new_entry.isolate_status = true;

        PushDirectionalStatus(explicit_state->directional_status_stack, new_entry);
    }
    else
    {
        explicit_state->overflow_isolate_count++;
    }
}

inline void
PopEntry(DirectionalStatusStack *stack)
{
    stack->entries_used--;
}

internal void
DoExplicitPass(UTF32_String string, BIDICharacter_String char_info, u8 paragraph_embedding_level, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    //implements BIDI X1 (which, in turn, applies X2-X8)
    ExplicitPassState state = {};

    state.directional_status_stack = PushType(temp_arena, DirectionalStatusStack);
    state.directional_status_stack->entries_used = 0;

    DirectionalStatus initial_directional_status = {};
    initial_directional_status.embedding_level = paragraph_embedding_level;
    initial_directional_status.override_status = DirectionalOverride_neutral;
    initial_directional_status.isolate_status = false;

    PushDirectionalStatus(state.directional_status_stack, initial_directional_status);
    
    //X2-X8
    u32 char_index = 0;
    for(int char_index = 0;
        char_index < string.len;
        char_index++)
    {
        BIDICharacterInfo *character_info = char_info.ptr + char_index;
        UTF32 codepoint = string.ptr[char_index];
        
        if(character_info->type == BIDIType_RLE)
        {
            // rule X2
            u8 lowest_odd_embedding_level = NextOddEmbeddingLevel(state.directional_status_stack);

            if(lowest_odd_embedding_level <= MAX_EMBEDDING_DEPTH &&
               state.overflow_isolate_count == 0 &&
               state.overflow_embedding_count == 0)
            {
                DirectionalStatus new_entry = {};
                new_entry.embedding_level = lowest_odd_embedding_level;
                new_entry.override_status = DirectionalOverride_neutral;
                new_entry.isolate_status = false;

                PushDirectionalStatus(state.directional_status_stack, new_entry);
            }
            else
            {
                if(state.overflow_isolate_count == 0)
                {
                    state.overflow_embedding_count++;
                }
            }
        }
        else if(character_info->type == BIDIType_LRE)
        {
            // rule X3
            u8 lowest_even_embedding_level = NextEvenEmbeddingLevel(state.directional_status_stack);

            if(lowest_even_embedding_level <= MAX_EMBEDDING_DEPTH &&
               state.overflow_isolate_count == 0 &&
               state.overflow_embedding_count == 0)
            {
                DirectionalStatus new_entry = {};
                new_entry.embedding_level = lowest_even_embedding_level;
                new_entry.override_status = DirectionalOverride_neutral;
                new_entry.isolate_status = false;

                PushDirectionalStatus(state.directional_status_stack, new_entry);
            }
            else
            {
                if(state.overflow_isolate_count == 0)
                {
                    state.overflow_embedding_count++;
                }
            }
        }
        else if(character_info->type == BIDIType_RLO)
        {
            // rule X4
            u8 lowest_odd_embedding_level = NextOddEmbeddingLevel(state.directional_status_stack);
            
            if(lowest_odd_embedding_level <= MAX_EMBEDDING_DEPTH &&
               state.overflow_isolate_count == 0 &&
               state.overflow_embedding_count == 0)
            {
                DirectionalStatus new_entry = {};
                new_entry.embedding_level = lowest_odd_embedding_level;
                new_entry.override_status = DirectionalOverride_RTL;
                new_entry.isolate_status = false;

                PushDirectionalStatus(state.directional_status_stack, new_entry);
            }
            else
            {
                if(state.overflow_isolate_count == 0)
                {
                    state.overflow_embedding_count++;
                }
            }
        }
        else if(character_info->type == BIDIType_LRO)
        {
            // rule X5
            u8 lowest_even_embedding_level = NextEvenEmbeddingLevel(state.directional_status_stack);

            if(lowest_even_embedding_level <= MAX_EMBEDDING_DEPTH &&
               state.overflow_isolate_count == 0 &&
               state.overflow_embedding_count == 0)
            {
                DirectionalStatus new_entry = {};
                new_entry.embedding_level = lowest_even_embedding_level;
                new_entry.override_status = DirectionalOverride_LTR;
                new_entry.isolate_status = false;

                PushDirectionalStatus(state.directional_status_stack, new_entry);
            }
            else
            {
                if(state.overflow_isolate_count == 0)
                {
                    state.overflow_embedding_count++;
                }
            }
        }
        else if(character_info->type == BIDIType_RLI)
        {
            // rule X5a
            HandleRLI(&state, codepoint, character_info);
        }
        else if(character_info->type == BIDIType_LRI)
        {
            // rule X5b
            HandleLRI(&state, codepoint, character_info);
        }
        else if(character_info->type == BIDIType_FSI)
        {
            // rule X5c
            UTF32_String pseudo_paragraph_string = {};
            pseudo_paragraph_string.ptr = string.ptr + char_index;
            pseudo_paragraph_string.len = string.len - char_index;

            u8 paragraph_embedding_level = GetParagraphEmbeddingLevel(pseudo_paragraph_string);

            if(paragraph_embedding_level == 1)
            {
                HandleRLI(&state, codepoint, character_info);
            }
            else
            {
                HandleLRI(&state, codepoint, character_info);
            }
        }
        else if(character_info->type != BIDIType_B &&
                character_info->type != BIDIType_BN  &&
                character_info->type != BIDIType_RLE &&
                character_info->type != BIDIType_LRE &&
                character_info->type != BIDIType_RLO &&
                character_info->type != BIDIType_LRO &&
                character_info->type != BIDIType_PDF &&
                character_info->type != BIDIType_RLI &&
                character_info->type != BIDIType_LRI &&
                character_info->type != BIDIType_FSI &&
                character_info->type != BIDIType_PDI)
        {
            // rule X6
            DirectionalStatus last_entry = GetLastEntry(state.directional_status_stack);

            character_info->embedding_level = last_entry.embedding_level;
            ResetToOverrideStatus(character_info, last_entry.override_status);

        }
        else if(character_info->type == BIDIType_PDI)
        {
            // rule X6a
            if(state.overflow_isolate_count > 0)
            {
                state.overflow_isolate_count--;
            }
            else if(state.valid_isolate_count == 0)
            {
                
            }
            else
            {
                state.overflow_embedding_count = 0;

                DirectionalStatus last_entry = GetLastEntry(state.directional_status_stack);
                while(last_entry.isolate_status == false)
                {
                    PopEntry(state.directional_status_stack);
                    last_entry = GetLastEntry(state.directional_status_stack);
                }

                PopEntry(state.directional_status_stack);
                state.valid_isolate_count--;
            }

            DirectionalStatus last_entry = GetLastEntry(state.directional_status_stack);

            character_info->embedding_level = last_entry.embedding_level;
            ResetToOverrideStatus(character_info, last_entry.override_status);
        }
        else if(character_info->type == BIDIType_PDF)
        {
            // X7
            if(state.overflow_isolate_count > 0)
            {
                
            }
            else if(state.overflow_embedding_count > 0)
            {
                state.overflow_embedding_count--;
            }
            else
            {
                DirectionalStatus last_entry = GetLastEntry(state.directional_status_stack);
                
                if(last_entry.isolate_status == false &&
                    state.directional_status_stack->entries_used >= 2)
                {
                    PopEntry(state.directional_status_stack);
                }
            }
        }
        else if(character_info->type == BIDIType_B)
        {
            //rule X8
            character_info->embedding_level = paragraph_embedding_level;
        }
        else
        {
            Assert(character_info->type == BIDIType_BN);
        }
    }
}

internal LevelRunNode *
CreateLevelRunAndNode(u32 run_start_pos, u32 level, MemoryArena *temp_arena)
{
    LevelRunNode *result = PushType(temp_arena, LevelRunNode);
    LevelRun *level_run = PushType(temp_arena, LevelRun);

    level_run->start = run_start_pos;
    level_run->level = level;

    level_run->len = 1;
    level_run->seq_id = 0;
    
    result->level_run = level_run;
    result->next = 0;

    
    return result;
}

internal LevelRunNode *
GetLevelRuns(BIDICharacter_String char_infos, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    LevelRunNode *result = 0;
    
    if(char_infos.len > 0)
    {
        // if there is more than one character, there is
        // always at least one LevelRunNode
        BIDICharacterInfo *first_char_info = char_infos.ptr;
        LevelRunNode *first_run_node = CreateLevelRunAndNode(0, first_char_info->embedding_level, temp_arena);
        
        u32 one_past_current_embedding_level = first_char_info->embedding_level + 1;
        LevelRunNode *last_node = first_run_node;
        
        for(u32 i = 1;
            i < char_infos.len;
            i++)
        {
            BIDICharacterInfo *bidi_character_info = char_infos.ptr + i;
            
            if(bidi_character_info->embedding_level + 1 != one_past_current_embedding_level &&
                char_infos.ptr[i].type != BIDIType_null)
            {
                one_past_current_embedding_level = bidi_character_info->embedding_level + 1;

                last_node->level_run->len = i - last_node->level_run->start;

                LevelRunNode *new_node = CreateLevelRunAndNode(i, bidi_character_info->embedding_level, temp_arena);
                last_node->next = new_node;
                last_node = new_node;
            }
            else
            {
                LevelRun *level_run = last_node->level_run;
                level_run->len++;
            }
        }

        result = first_run_node;
    }

    return result;
}

internal IsolatingRunSequence *
GetIsolatingRunSequence(LevelRunList *first_run_list, BIDICharacter_String char_infos, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    IsolatingRunSequence *first_run_sequence = 0;
    IsolatingRunSequence *last_run_sequence = 0;

    for(LevelRunList *run_list = first_run_list;
        run_list;
        run_list = run_list->next)
    {
        IsolatingRunSequence *new_run_sequence = PushType(temp_arena, IsolatingRunSequence);
        *new_run_sequence = {};


        new_run_sequence->level = run_list->level;
        new_run_sequence->num_chars = 0;
        new_run_sequence->char_indices = PushCount(temp_arena, UTF32, run_list->num_chars);

        u32 char_indices_filled = 0;
        for(LevelRunNode *run_node = run_list->first_run;
            run_node;
            run_node = run_node->next)
        {
            LevelRun *run = run_node->level_run;

            for(int i = 0;
                i < run->len;
                i++)
            {
                
                if(char_infos.ptr[run->start + i].type != BIDIType_null) // skip over removed chars
                {
                    new_run_sequence->char_indices[new_run_sequence->num_chars] = run->start + i;
                    new_run_sequence->num_chars++;
                }
            }
        }

        if (new_run_sequence->num_chars > 0)
        {
            if (last_run_sequence)
            {
                last_run_sequence->next = new_run_sequence;
            }
            else
            {
                first_run_sequence = new_run_sequence;
                last_run_sequence = first_run_sequence;
            }
        }
        

    }

    return first_run_sequence;
}

internal LevelRunList *
GetIsolatingRunListSentinel(LevelRunNode *first_run_node, BIDICharacter_String char_infos, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    LevelRunList *run_list_sentinel = PushType(temp_arena, LevelRunList);
    *run_list_sentinel = {};

    LevelRunList *last_list = run_list_sentinel;

    u32 current_seq_id = 0;
    for(LevelRunNode *run_node = first_run_node;
        run_node;
        run_node = run_node->next)
    {
        LevelRun *level_run = run_node->level_run;

        if(level_run->seq_id == 0)
        {
            current_seq_id++;
            level_run->seq_id = current_seq_id;
            LevelRunNode *first_run_node_in_sequence = PushType(temp_arena, LevelRunNode);
            first_run_node_in_sequence->level_run = level_run;
            first_run_node_in_sequence->next = 0;

            LevelRunList *new_list = PushType(temp_arena, LevelRunList);
            new_list->first_run = first_run_node_in_sequence;
            new_list->num_chars = level_run->len;
            new_list->next = 0;
            new_list->level = level_run->level;
            new_list->last_run = 0;
            
            last_list->next = new_list;
            last_list = new_list;
            
            new_list->last_run = new_list->first_run;
            
            u32 last_run_char_index = level_run->start + level_run->len - 1;
            BIDICharacterInfo *last_run_char_info = char_infos.ptr + last_run_char_index;

            if(last_run_char_info->type == BIDIType_LRI ||
               last_run_char_info->type == BIDIType_RLI ||
               last_run_char_info->type == BIDIType_FSI)
            {
                for(LevelRunNode *scan_ahead_node = run_node->next;
                    scan_ahead_node;
                    scan_ahead_node = scan_ahead_node->next)
                {
                    LevelRun *scan_ahead_run = scan_ahead_node->level_run;
                    
                    if(scan_ahead_run->seq_id == 0 &&
                       scan_ahead_run->level == level_run->level)
                    {
                        u32 first_run_char_index = scan_ahead_run->start;
                        u32 last_run_char_index = scan_ahead_run->start + scan_ahead_run->len;
                        
                        if(char_infos.ptr[first_run_char_index].type == BIDIType_PDI)
                        {
                            scan_ahead_run->seq_id = current_seq_id;
                            LevelRunNode *append_run_node = PushType(temp_arena, LevelRunNode);

                            append_run_node->level_run = scan_ahead_run;
                            append_run_node->next = 0;

                            new_list->num_chars += scan_ahead_run->len;
                            new_list->last_run->next = append_run_node;
                            new_list->last_run = append_run_node;
                        }

                        if(char_infos.ptr[last_run_char_index].type != BIDIType_LRI &&
                           char_infos.ptr[last_run_char_index].type != BIDIType_RLI &&
                           char_infos.ptr[last_run_char_index].type != BIDIType_FSI)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    return run_list_sentinel;
}

internal BIDIType
GetSequenceSOS(IsolatingRunSequence *run_sequence, BIDICharacter_String char_infos, u8 paragraph_embedding_level)
{
    TIMED_BLOCK;
    BIDIType result;

    u32 first_char_in_sequence = run_sequence->char_indices[0];
    u32 first_char_level = char_infos.ptr[first_char_in_sequence].embedding_level;

    u32 one_past_preceeding_char = 0;
    if(first_char_in_sequence > 0)
    {
        for(u32 i = first_char_in_sequence - 1;
            i > 0;
            i--)
        {
            if(char_infos.ptr[i].type == BIDIType_null)
            {
                continue;
            }

            one_past_preceeding_char = i + 1;
            break;
        }
    }

    u32 sos_compare_level;
    if(one_past_preceeding_char)
    {
        sos_compare_level = char_infos.ptr[one_past_preceeding_char - 1].embedding_level;
    }
    else
    {
        sos_compare_level = paragraph_embedding_level;
    }

    u32 sos_consider_level = Max(first_char_level, sos_compare_level);
    if(sos_consider_level % 2)
    {
        result = BIDIType_R;
    }
    else
    {
        result = BIDIType_L;
    }

    return result;
}

internal BIDIType
GetSequenceEOS(IsolatingRunSequence *run_sequence, BIDICharacter_String char_infos, u8 paragraph_embedding_level)
{
    TIMED_BLOCK;
    BIDIType result;

    u32 last_char_in_sequence = run_sequence->char_indices[run_sequence->num_chars - 1];
    u32 last_char_level = char_infos.ptr[last_char_in_sequence].embedding_level;
    u32 last_char_is_isolate_initiator = (char_infos.ptr[last_char_in_sequence].type == BIDIType_RLI ||
                                          char_infos.ptr[last_char_in_sequence].type == BIDIType_LRI ||
                                          char_infos.ptr[last_char_in_sequence].type == BIDIType_FSI);
        
    u32 one_past_following_char = 0;
    for(u32 i = last_char_in_sequence + 1;
        i < char_infos.len;
        i++)
    {
        if(char_infos.ptr[i].type == BIDIType_null)
        {
            continue;
        }

        one_past_following_char = i + 1;
        break;
    }

    u32 eos_compare_level;
    if(one_past_following_char & !last_char_is_isolate_initiator)
    {
        eos_compare_level = char_infos.ptr[one_past_following_char - 1].embedding_level;
    }
    else
    {
        eos_compare_level = paragraph_embedding_level;
    }

    u32 eos_consider_level = Max(last_char_level, eos_compare_level);
    if(eos_consider_level % 2)
    {
        result = BIDIType_R;
    }
    else
    {
        result = BIDIType_L;
    }

    return result;
}

internal IsolatingRunSequence *
PrepareForImplicitProcessing(UTF32_String string, BIDICharacter_String char_infos, u32 paragraph_embedding_level, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    // X9
    for(int i = 0;
        i < char_infos.len;
        i++)
    {
        TIMED_BLOCK;
        BIDICharacterInfo *bidi_character_info = char_infos.ptr + i;

        if(bidi_character_info->type == BIDIType_RLE ||
           bidi_character_info->type == BIDIType_LRE ||
           bidi_character_info->type == BIDIType_RLO ||
           bidi_character_info->type == BIDIType_LRO ||
           bidi_character_info->type == BIDIType_PDF ||
           bidi_character_info->type == BIDIType_BN)
        {
            bidi_character_info->type = BIDIType_null;
            bidi_character_info->embedding_level = CHARACTER_REMOVED_IN_RULE_X9;
        }
    }


    // BD13
    // note that level runs include removed chars (of BIDIType_null)
    LevelRunNode *first_run_node = GetLevelRuns(char_infos, temp_arena);

    // note that isolating run lists include removed chars (of BIDIType_null)
    LevelRunList *isolating_run_list_sentinel = GetIsolatingRunListSentinel(first_run_node, char_infos, temp_arena);

    // note that isolating run sequences DO NOT include removed chars
    IsolatingRunSequence *first_run_sequence = GetIsolatingRunSequence(isolating_run_list_sentinel->next, char_infos, temp_arena);
    
    // X10
    for(IsolatingRunSequence *run_sequence = first_run_sequence;
        run_sequence;
        run_sequence = run_sequence->next)
    {
        run_sequence->sos = GetSequenceSOS(run_sequence, char_infos, paragraph_embedding_level);
        run_sequence->eos = GetSequenceEOS(run_sequence, char_infos, paragraph_embedding_level);
    }

    return first_run_sequence;
}

internal BIDIType
GetNextCharTypeInSequence(IsolatingRunSequence *run_sequence, u32 sequence_char_index, BIDICharacter_String char_infos)
{
    BIDIType result;

    
    if(sequence_char_index + 1 < run_sequence->num_chars)
    {
        u32 next_sequence_char_index = run_sequence->char_indices[sequence_char_index + 1];
        result = char_infos.ptr[next_sequence_char_index].type;
    }
    else
    {
        result = run_sequence->eos;
    }

    return result;
}

internal void
ResolveWeakTypes(IsolatingRunSequence *first_run_sequence, BIDICharacter_String char_infos)
{
    TIMED_BLOCK;
    for(IsolatingRunSequence *run_sequence = first_run_sequence;
        run_sequence;
        run_sequence = run_sequence->next)
    {
        // W1
        // Examine each nonspacing mark (NSM) in the isolating run sequence,
        // and change the type of the NSM to Other Neutral if the previous character
        // is an isolate initiator or PDI, and to the type of the previous character
        // otherwise. If the NSM is at the start of the isolating run sequence, it
        // will get the type of sos.
        BIDIType last_char_bidi_type;
        
        last_char_bidi_type = run_sequence->sos;
        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];

            if(char_infos.ptr[char_index].type != BIDIType_null)
            {
                if(char_infos.ptr[char_index].type == BIDIType_NSM)
                {
                    if(IsIsolateInitiator(last_char_bidi_type) ||
                       last_char_bidi_type == BIDIType_PDI)
                    {
                        char_infos.ptr[char_index].type = BIDIType_ON;
                    }
                    else
                    {
                        char_infos.ptr[char_index].type = last_char_bidi_type;
                    }
                }

                last_char_bidi_type = char_infos.ptr[char_index].type;
            }
        }

        // W2
        // Search backward from each instance of a European number until the first
        // strong type (R, L, AL, or sos) is found. If an AL is found, change the
        // type of the European number to Arabic number.
        BIDIType last_strong_type = run_sequence->sos;
        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];
            if(char_infos.ptr[char_index].type != BIDIType_null)
            {
                if(char_infos.ptr[char_index].type == BIDIType_L ||
                   char_infos.ptr[char_index].type == BIDIType_R ||
                   char_infos.ptr[char_index].type == BIDIType_AL)
                {
                    last_strong_type = char_infos.ptr[char_index].type;
                }
                else if(char_infos.ptr[char_index].type == BIDIType_EN)
                {
                    if(last_strong_type == BIDIType_AL)
                    {
                        char_infos.ptr[char_index].type = BIDIType_AN;
                    }
                }
            }
        }

        // W3
        // Change all ALs to R

        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];
            
            if(char_infos.ptr[char_index].type != BIDIType_null)
            {
                if(char_infos.ptr[char_index].type == BIDIType_AL)
                {
                    char_infos.ptr[char_index].type = BIDIType_R;
                }
            }
        }

        // W4
        // A single European separator between two European numbers changes to a European
        // number. A single common separator between two numbers of the same type changes
        // to that type.

        last_char_bidi_type = run_sequence->sos;
        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];
            
            if(char_infos.ptr[char_index].type != BIDIType_null)
            {
                if(char_infos.ptr[char_index].type == BIDIType_ES)
                {
                    BIDIType next_char_type = GetNextCharTypeInSequence(run_sequence, i, char_infos);
                        
                    if(last_char_bidi_type == BIDIType_EN &&
                       next_char_type == BIDIType_EN)
                    {
                        char_infos.ptr[char_index].type = BIDIType_EN;
                    }
                }
                else if(char_infos.ptr[char_index].type == BIDIType_CS)
                {
                    BIDIType next_char_type = GetNextCharTypeInSequence(run_sequence, i, char_infos);

                    if(last_char_bidi_type == next_char_type)
                    {
                        if(last_char_bidi_type == BIDIType_EN)
                        {
                            char_infos.ptr[char_index].type = BIDIType_EN;
                        }
                        else if(last_char_bidi_type == BIDIType_AN)
                        {
                            char_infos.ptr[char_index].type = BIDIType_AN;
                        }
                            
                    }
                }

                last_char_bidi_type = char_infos.ptr[char_index].type;
            }
        }

        // W5
        // A sequence of European terminators adjacent to European numbers changes to all
        // European numbers.

        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];

            if(char_infos.ptr[char_index].type != BIDIType_null)
            {
                if(char_infos.ptr[char_index].type == BIDIType_ET)
                {
                    u32 num_chars_in_chain = 1;

                    for(u32 j = i + 1;
                        j < run_sequence->num_chars;
                        j++)
                    {
                        u32 chain_char_index = run_sequence->char_indices[j];

                        if(char_infos.ptr[chain_char_index].type != BIDIType_null)
                        {
                            if(char_infos.ptr[chain_char_index].type == BIDIType_ET)
                            {
                                num_chars_in_chain++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    u32 char_index_after_ET_chain = char_index + num_chars_in_chain;
                    
                    if(char_index_after_ET_chain < run_sequence->num_chars)
                    {
                        // we should now be at the next character after a series of ETs
                        BIDIType after_type = char_infos.ptr[char_index_after_ET_chain].type;
                        if(after_type == BIDIType_EN)
                        {
                            u32 chain_chars_deleted = 0;

                            // NOTE that we delete chained ETs in this obtuse way to handle BIDIType_nulls
                            for(int i = char_index;
                                i < run_sequence->num_chars;
                                i++)
                            {
                                if(char_infos.ptr[i].type == BIDIType_ET)
                                {
                                    char_infos.ptr[i].type = BIDIType_EN;
                                    chain_chars_deleted++;
                                }

                                if(chain_chars_deleted >= num_chars_in_chain)
                                {
                                    break;
                                }
                            }

                            Assert(chain_chars_deleted == num_chars_in_chain);
                        }
                    }
                        
                }
                else if(char_infos.ptr[char_index].type == BIDIType_EN)
                {

                    for(u32 j = i + 1;
                        j < run_sequence->num_chars;
                        j++)
                    {
                        if(char_infos.ptr[j].type != BIDIType_null)
                        {
                            if(char_infos.ptr[j].type == BIDIType_ET)
                            {
                                char_infos.ptr[j].type = BIDIType_EN;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        
        // W6
        // Otherwise, separators and terminators change to Other Neutral.
        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];
                        
            if(char_infos.ptr[char_index].type == BIDIType_ES ||
               char_infos.ptr[char_index].type == BIDIType_CS ||
               char_infos.ptr[char_index].type == BIDIType_ET)
            {
                char_infos.ptr[char_index].type = BIDIType_ON;
            }
        }
        
        // W7
        // Search backward from each instance of a European number until the first
        // strong type (R, L, or sos) is found. If an L is found, then change the type
        // of the European number to L.
        last_strong_type = run_sequence->sos;
        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];
                        
            if(char_infos.ptr[char_index].type != BIDIType_null)
            {
                if(char_infos.ptr[char_index].type == BIDIType_EN)
                {
                    if(last_strong_type == BIDIType_L)
                    {
                        char_infos.ptr[char_index].type = BIDIType_L;
                    }
                }
                else if(char_infos.ptr[char_index].type == BIDIType_L ||
                        char_infos.ptr[char_index].type == BIDIType_R)
                {
                    last_strong_type = char_infos.ptr[char_index].type;
                }
            }
        }
    }
}


internal void
SortBracketPairs(BracketPair **first_bracket_pair)
{
    BracketPair *temporary_pair_list = 0;
    BracketPair *this_bracket_pair = *first_bracket_pair;

    while(this_bracket_pair)
    {
        BracketPair *saved_next = this_bracket_pair->next;

        if(temporary_pair_list == 0)
        {
            // create new sorted list
            this_bracket_pair->next = 0;
            temporary_pair_list = this_bracket_pair;
        }
        else
        {
            if(this_bracket_pair->open_pos < temporary_pair_list->open_pos)
            {
                // insert at start of sorted list
                this_bracket_pair->next = temporary_pair_list;
                temporary_pair_list = this_bracket_pair;
            }
            else
            {
                BracketPair *insert_after_pair = temporary_pair_list;
                while(insert_after_pair)
                {
                    BracketPair *insert_at_pair = insert_after_pair->next;
                    if(insert_at_pair == 0)
                    {
                        // insert at end of sorted list
                        this_bracket_pair->next = 0;
                        insert_after_pair->next = this_bracket_pair;
                        break;
                    }
                    else if(this_bracket_pair->open_pos < insert_at_pair->open_pos)
                    {
                        // insert in middle of sorted list
                        insert_after_pair->next = this_bracket_pair;
                        this_bracket_pair->next = insert_at_pair;
                        break;
                    }
                    insert_after_pair = insert_at_pair;
                }
            }
        }

        this_bracket_pair = saved_next;
    }

    *first_bracket_pair = temporary_pair_list;
}

internal BracketPair *
GetBracketPairs(IsolatingRunSequence *run_sequence, UTF32_String string, BIDICharacter_String char_infos, MemoryArena *temp_arena)
{
    BracketPairStackElement *bracket_pair_stack = PushCount(temp_arena, BracketPairStackElement, BRACKET_PAIR_STACK_SIZE);
    u32 num_bracket_pair_elements = 0;

    BracketPair *first_bracket_pair = 0;
    BracketPair *last_bracket_pair = 0;
    
    for(u32 i = 0;
        i < run_sequence->num_chars;
        i++)
    {
        u32 char_index = run_sequence->char_indices[i];
        UTF32 codepoint = string.ptr[char_index];
        if(IsOpeningBracket(codepoint, char_infos.ptr[char_index].type))
        {
            if(num_bracket_pair_elements < BRACKET_PAIR_STACK_SIZE)
            {
                BracketPairStackElement new_element = {};
                new_element.bracket_character = GetPairedBracket(codepoint);
                new_element.text_position = i;
                        
                bracket_pair_stack[num_bracket_pair_elements] = new_element;
                num_bracket_pair_elements++;
            }
            else
            {
                goto done_processing_sequence;
            }
        }
        else if(IsClosingBracket(codepoint, char_infos.ptr[char_index].type))
        {
            u32 one_past_check_stack_element = num_bracket_pair_elements;
            while(one_past_check_stack_element)
            {
                BracketPairStackElement *current_stack_element;
                current_stack_element = bracket_pair_stack + one_past_check_stack_element - 1;

                b32 is_matching_closing;
                if(codepoint == current_stack_element->bracket_character ||
                   codepoint == 0x232A && current_stack_element->bracket_character == 0x3009 ||
                   codepoint == 0x3009 && current_stack_element->bracket_character == 0x232A)
                {
                    is_matching_closing = true;
                }
                else
                {
                    is_matching_closing = false;
                }

                if(is_matching_closing)
                {
                    BracketPair *new_bracket_pair = PushType(temp_arena, BracketPair);
                    new_bracket_pair->open_pos = current_stack_element->text_position;
                    new_bracket_pair->close_pos = i;
                    new_bracket_pair->next = 0;
                            
                    if(last_bracket_pair)
                    {
                        last_bracket_pair->next = new_bracket_pair;
                    }
                    else
                    {
                        first_bracket_pair = new_bracket_pair;
                        last_bracket_pair = first_bracket_pair;
                    }

                    last_bracket_pair = new_bracket_pair;

                    num_bracket_pair_elements = one_past_check_stack_element - 1;
                            
                    break;
                }
                else
                {
                    one_past_check_stack_element--;
                }
            }
        }
                   
    }


done_processing_sequence:;
    return first_bracket_pair;
}

inline void
SetBracketPairDirection(IsolatingRunSequence *run_sequence, BracketPair *pair, BIDIType direction, BIDICharacter_String char_infos)
{
    u32 open_char_index = run_sequence->char_indices[pair->open_pos];
    u32 close_char_index = run_sequence->char_indices[pair->close_pos];
    
    char_infos.ptr[open_char_index].type = direction;
    char_infos.ptr[close_char_index].type = direction;

    // Any number of characters that had original bidirectional character
    // type NSM prior to the application of W1 that immediately follow a
    // paired bracket which changed to L or R under N0 should change to
    // match the type of their preceding bracket.

    for(u32 i = pair->open_pos + 1;
        i < pair->close_pos;
        i++)
    {
        u32 char_index = run_sequence->char_indices[i];
        if(char_infos.ptr[char_index].type != BIDIType_null)
        {
            if(char_infos.ptr[char_index].original_type == BIDIType_NSM)
            {
                char_infos.ptr[char_index].type = direction;
            }
            else
            {
                break;
            }
        }
    }

    for(u32 i = pair->close_pos + 1;
        i < run_sequence->num_chars;
        i++)
    {
        u32 char_index = run_sequence->char_indices[i];
        if(char_infos.ptr[char_index].type != BIDIType_null)
        {
            if(char_infos.ptr[char_index].original_type == BIDIType_NSM)
            {
                char_infos.ptr[char_index].type = direction;
            }
            else
            {
                break;
            }
        }
    }
        
}

internal b32
IsNeutralOrIsolate(BIDIType type)
{
    b32 result;
    
    if(type == BIDIType_B ||
       type == BIDIType_S ||
       type == BIDIType_WS ||
       type == BIDIType_ON ||
       type == BIDIType_FSI ||
       type == BIDIType_LRI ||
       type == BIDIType_RLI ||
       type == BIDIType_PDI)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}

internal void
ResolveImplicitLevels(IsolatingRunSequence *first_run_sequence, UTF32_String string, BIDICharacter_String char_infos)
{
    TIMED_BLOCK;
    for(int i = 0;
        i < char_infos.len;
        i++)
    {
        // I1 and I2

        if(char_infos.ptr[i].embedding_level % 2)
        {
            if(char_infos.ptr[i].type == BIDIType_L || 
               char_infos.ptr[i].type == BIDIType_EN ||
               char_infos.ptr[i].type == BIDIType_AN)
            {
                char_infos.ptr[i].embedding_level++;
            }
        }
        else
        {
            if(char_infos.ptr[i].type == BIDIType_R)
            {
                char_infos.ptr[i].embedding_level++;
            }
            else if(char_infos.ptr[i].type == BIDIType_EN ||
                    char_infos.ptr[i].type == BIDIType_AN)
            {
                char_infos.ptr[i].embedding_level += 2;
            }
        }
    }
}

internal void
ResolveNeutralAndIsolate(IsolatingRunSequence *first_run_sequence, UTF32_String string, BIDICharacter_String char_infos, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    // BD16
    for(IsolatingRunSequence *run_sequence = first_run_sequence;
        run_sequence;
        run_sequence = run_sequence->next)
    {
        BIDIType embedding_direction;
        BIDIType opposite_direction;

        if(run_sequence->level % 2)
        {
            embedding_direction = BIDIType_R;
            opposite_direction = BIDIType_L;
        }
        else
        {
            embedding_direction = BIDIType_L;
            opposite_direction = BIDIType_R;
        }


        // note that these bracket pair position indices are relative to the run sequence
        BracketPair *first_bracket_pair = GetBracketPairs(run_sequence, string, char_infos, temp_arena);
        SortBracketPairs(&first_bracket_pair);

        // N0
        for(BracketPair *pair = first_bracket_pair;
            pair;
            pair = pair->next)
        {
            // NOTE that the embedding level is the same for all runs in an isolating run sequence

            b32 found_embedding_direction_type = false;
            b32 found_opposite_direction_type = false;

            for(u32 i = pair->open_pos + 1;
                i < pair->close_pos;
                i++)
            {
                u32 char_index = run_sequence->char_indices[i];
                
                BIDIType effective_char_type = char_infos.ptr[char_index].type;

                if(effective_char_type == BIDIType_EN ||
                   effective_char_type == BIDIType_AN)
                {
                    effective_char_type = BIDIType_R;
                }

                if(effective_char_type == BIDIType_L ||
                   effective_char_type == BIDIType_R)
                {
                    if(effective_char_type == embedding_direction)
                    {
                        // if any strong type matching embedding direction is found
                        // set both brackets to match the embedding direction
                        SetBracketPairDirection(run_sequence, pair, embedding_direction, char_infos);
                        found_embedding_direction_type = true;

                        break;
                    }
                    else if(effective_char_type == opposite_direction)
                    {
                        found_opposite_direction_type = true;
                    }
                }
            }

            if(found_opposite_direction_type &&
               !found_embedding_direction_type)
            {
                // Otherwise, if there is a strong type it must be opposite the
                // embedding direction. Therefore, test for an established context
                // with a preceding strong type by checking backwards before the
                // opening paired bracket until the first strong type (L, R, or sos) is
                // found.

                // one past the char index is used in order to represent the character
                // before the sequence (the sos)
                u32 one_past_sequence_char_index = pair->open_pos;
                for(;;)
                {
                    BIDIType effective_char_type;
                    if(one_past_sequence_char_index)
                    {
                        u32 char_index = run_sequence->char_indices[one_past_sequence_char_index - 1];
                        effective_char_type = char_infos.ptr[char_index].type;

                        if(effective_char_type == BIDIType_EN ||
                           effective_char_type == BIDIType_AN)
                        {
                            effective_char_type = BIDIType_R;
                        }
                    }
                    else
                    {
                        effective_char_type = run_sequence->sos;
                    }

                    if(effective_char_type == opposite_direction)
                    {
                        SetBracketPairDirection(run_sequence, pair, opposite_direction, char_infos);
                        break;
                    }
                    else if(effective_char_type == embedding_direction)
                    {
                        SetBracketPairDirection(run_sequence, pair, embedding_direction, char_infos);
                        break;
                    }
                    
                    if(one_past_sequence_char_index > 0)
                    {
                        one_past_sequence_char_index--;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        // N1
        // A sequence of NIs takes the direction of the surrounding strong text
        // if the text on both sides has the same direction. European and Arabic
        // numbers act as if they were R in terms of their influence on NIs. The
        // start-of-sequence (sos) and end-of-sequence (eos) types are used at
        // isolating run sequence boundaries.
        BIDIType last_effective_type;

        last_effective_type = run_sequence->sos;
        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];
            BIDIType effective_type = char_infos.ptr[char_index].type;
            if(effective_type == BIDIType_EN ||
               effective_type == BIDIType_AN)
            {
                effective_type = BIDIType_R;
            }
            
            if(IsNeutralOrIsolate(effective_type))
            {
                u32 last_NI_chain_char = i;

                BIDIType after_chain_type = BIDIType_null;
                for (u32 j = i + 1;
                     j < run_sequence->num_chars;
                     j++)
                {
                    u32 chain_check_char = run_sequence->char_indices[j];
                    BIDIType chain_check_type = char_infos.ptr[chain_check_char].type;

                    if(IsNeutralOrIsolate(chain_check_type))
                    {
                        last_NI_chain_char = j;
                    }
                    else
                    {
                        after_chain_type = chain_check_type;

                        if(after_chain_type == BIDIType_EN ||
                           after_chain_type == BIDIType_AN)
                        {
                            after_chain_type = BIDIType_R;
                        }

                        break;
                    }
                }

                if(after_chain_type == BIDIType_null)
                {
                    after_chain_type = run_sequence->eos;
                }

                if(last_effective_type == after_chain_type)
                {
                    // transfrom the NIs -> the surrounding direction
                    for(u32 j = i;
                        j <= last_NI_chain_char;
                        j++)
                    {
                        u32 transform_char_index = run_sequence->char_indices[j];
                        Assert(IsNeutralOrIsolate(char_infos.ptr[transform_char_index].type));

                        char_infos.ptr[transform_char_index].type = last_effective_type;
                    }
                }
            }
            
            last_effective_type = effective_type;
        }


        // N2
        // Any remaining NIs take the embedding direction.

        for(u32 i = 0;
            i < run_sequence->num_chars;
            i++)
        {
            u32 char_index = run_sequence->char_indices[i];
            BIDIType effective_type = char_infos.ptr[char_index].type;

            if(IsNeutralOrIsolate(effective_type))
            {
                char_infos.ptr[char_index].type = embedding_direction;
            }
        }
    }

}

#if 1
internal void
ReorderResolvedLevels(UTF32_String string, BIDICharacter_String char_infos, u32 paragraph_embedding_level)
{
    TIMED_BLOCK;
    for(u32 i = 0;
        i < string.len;
        i++)
    {
        // TODO we can only scan forward, check for WS / isolate formatting and record the start
        //      of a WS / isolate chain if then we see a S/B/EOL then change that entire chain?

        // L1
        // On each line, reset the embedding level of the specified characters
        // to the paragraph embedding level.

        // segment separators, paragraph separators
        if(char_infos.ptr[i].original_type == BIDIType_S ||
           char_infos.ptr[i].original_type == BIDIType_B)
        {
            char_infos.ptr[i].embedding_level = paragraph_embedding_level;

            // Any sequence of whitespace characters and/or isolate formatting
            // characters (FSI, LRI, RLI, and PDI) preceding a segment separator
            // or paragraph separator
            if(i > 0)
            {
                u32 scan_char = i - 1;
                for(;;)
                {
                    if(char_infos.ptr[scan_char].type != BIDIType_null)
                    {
                        if(char_infos.ptr[scan_char].original_type == BIDIType_WS ||
                           char_infos.ptr[scan_char].original_type == BIDIType_FSI ||
                           char_infos.ptr[scan_char].original_type == BIDIType_LRI ||
                           char_infos.ptr[scan_char].original_type == BIDIType_RLI ||
                           char_infos.ptr[scan_char].original_type == BIDIType_PDI)
                        {
                            char_infos.ptr[scan_char].embedding_level = paragraph_embedding_level;
                        }
                        else
                        {
                            break;
                        }
                    }

                    
                    if (scan_char > 0)
                    {
                        scan_char--;
                    }
                    else
                    {
                        break;
                    }
                }
                
            }
        }

        // Any sequence of whitespace characters and/or isolate formatting
        // characters (FSI, LRI, RLI, and PDI) at the end of the line.
        if(i == string.len - 1)
        {
            u32 scan_char = i;
            for(;;)
            {
                if(char_infos.ptr[scan_char].original_type != BIDIType_null)
                {
                    if(char_infos.ptr[scan_char].type != BIDIType_null)
                     {
                        if(char_infos.ptr[scan_char].original_type == BIDIType_WS ||
                           char_infos.ptr[scan_char].original_type == BIDIType_FSI ||
                           char_infos.ptr[scan_char].original_type == BIDIType_LRI ||
                           char_infos.ptr[scan_char].original_type == BIDIType_RLI ||
                           char_infos.ptr[scan_char].original_type == BIDIType_PDI)
                        {
                            char_infos.ptr[scan_char].embedding_level = paragraph_embedding_level;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if(scan_char > 0)
                    {
                        scan_char--;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }

    // L2
    // From the highest level found in the text to the lowest
    // odd level on each line, including intermediate levels not
    // actually present in the text, reverse any contiguous
    // sequence of characters that are at that level or higher.

    // NOTE that this is not implemented yet

    // u32 highest_level = 0;

    // for(int i = 0;
    //     i < string.len;
    //     i++)
    // {
    //     if(char_infos.ptr[i].embedding_level > highest_level)
    //     {
    //         highest_level = char_infos.ptr[i].embedding_level;
    //     }
    // }
    
    // if(highest_level >= 1)
    // {
        
    //     u32 lowest_odd_level = ;

    //     for(int level = highest_level;
    //         level = lowest_odd_level;
    //         level--)
    //     {

    //         for(int i = 0;
    //             i < char_infos.ptr[])
    //     }
    // }
}
#endif

internal BIDICharacter_String
InitBIDICharacterString(UTF32_String string, u32 paragraph_embedding_level, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    BIDICharacter_String result;

    result.ptr = PushCount(temp_arena, BIDICharacterInfo, string.len);
    result.len = string.len;

    for(int i = 0;
        i < string.len;
        i++)
    {
        BIDICharacterInfo *bidi_char = result.ptr + i;

        UTF32 codepoint = string.ptr[i];
        bidi_char->type = GetCodepointBidiType(codepoint);
        bidi_char->original_type = GetCodepointBidiType(codepoint);

        // TODO do we need to do this?
        bidi_char->embedding_level = paragraph_embedding_level;
    }
    
    return result;
}


internal BIDICharacter_String
ProcessBIDI_UTF32(UTF32_String string, u32 paragraph_embedding_level, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    BIDICharacter_String char_infos;
    
    //The input should be a line of text or _paragraph_
    //so P1 in BIDI is taken care of

    if(paragraph_embedding_level > MAX_EMBEDDING_DEPTH)
    {
        paragraph_embedding_level = GetParagraphEmbeddingLevel(string);
    }
    
    char_infos = InitBIDICharacterString(string, paragraph_embedding_level, temp_arena);
    
    DoExplicitPass(string, char_infos, paragraph_embedding_level, temp_arena);

    IsolatingRunSequence *first_run_sequence = PrepareForImplicitProcessing(string, char_infos, paragraph_embedding_level, temp_arena);

    ResolveWeakTypes(first_run_sequence, char_infos);
    ResolveNeutralAndIsolate(first_run_sequence, string, char_infos, temp_arena);
    ResolveImplicitLevels(first_run_sequence, string, char_infos);

    // note that for Terranium rules L1 and L2 are not applied
    // but are implemented so that test cases that would otherwise pass would be able to pass
    ReorderResolvedLevels(string, char_infos, paragraph_embedding_level);
        
    return char_infos;
}

internal BIDIDirection
GetCharDirection(BIDICharacterInfo char_info)
{
    BIDIDirection result;
    
    if(char_info.embedding_level % 2)
    {
        result = BIDIDirection_RTL;
    }
    else
    {
        result = BIDIDirection_LTR;
    }
    
    return result;
}

internal void
AppendToScriptItemList(ScriptItemList_ *script_items, u32 char_pos,
                       BIDIDirection last_direction, BIDIScript last_script, MemoryArena *arena)
{
    ScriptItemNode_ *new_script_item = PushType(arena, ScriptItemNode_);
    new_script_item->char_pos = char_pos;
    new_script_item->next = 0;
    new_script_item->direction = last_direction;
    new_script_item->script = last_script;

    script_items->num_generated++;
    
    if(script_items->last_item == 0)
    {
        script_items->last_item = new_script_item;
        script_items->first_item = new_script_item;
    }
    else
    {
        script_items->last_item->next = new_script_item;
        script_items->last_item = new_script_item;
    }
}

internal ScriptItemList_
GenerateScriptItemList(UTF32_String string, BIDICharacter_String char_infos, MemoryArena *arena)
{
    ScriptItemList_ result;
    result.first_item = 0;
    result.last_item = 0;
    result.num_generated = 0;
    
    BIDIDirection last_direction = GetCharDirection(char_infos.ptr[0]);
    BIDIScript last_script = char_infos.ptr[0].script;

    u32 run_start = 0;
    
    for(int i = 1;
        i < string.len;
        i++)
    {
        BIDIDirection this_direction = GetCharDirection(char_infos.ptr[i]);
        BIDIScript this_script = char_infos.ptr[i].script;
        
        if(this_direction != last_direction ||
           this_script != last_script)
        {
            AppendToScriptItemList(&result, run_start, last_direction, last_script, arena);
            
            last_script = this_script;
            last_direction = this_direction;
            run_start = i;
        }
    }


    AppendToScriptItemList(&result, run_start, last_direction, last_script, arena);
   
    
    AppendToScriptItemList(&result, string.len, BIDIDirection_null, BIDIScript_unknown, arena);
    
    return result;
}

internal ScriptItems
GenerateScriptItems(UTF32_String string, BIDICharacter_String char_infos, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    ScriptItemList_ script_item_list;
    script_item_list = GenerateScriptItemList(string, char_infos, temp_arena);

    ScriptItems result = {};
    result.num_generated = script_item_list.num_generated - 1;
    result.items = PushCount(temp_arena, ScriptItem, script_item_list.num_generated);

    u32 num_items_filled = 0;
    for(ScriptItemNode_ *script_item_node = script_item_list.first_item;
        script_item_node;
        script_item_node = script_item_node->next)
    {
        result.items[num_items_filled].char_pos = script_item_node->char_pos;
        result.items[num_items_filled].direction = script_item_node->direction;
        result.items[num_items_filled].script = script_item_node->script;

        num_items_filled++;
    }

    return result;
}

internal void
ResolveScript(UTF32_String string, BIDICharacter_String char_infos)
{
    TIMED_BLOCK;
    BIDIScript last_char_type = BIDIScript_unknown;
    for(int i = 0;
        i < string.len;
        i++)
    {
        UTF32 codepoint = string.ptr[i];
        char_infos.ptr[i].script = GetCodepointBIDIScript(codepoint);

        if(char_infos.ptr[i].script == BIDIScript_Inherited ||
           char_infos.ptr[i].script == BIDIScript_Common)
        {
            char_infos.ptr[i].script = last_char_type;
        }
        last_char_type = char_infos.ptr[i].script;
    }

    // TODO actually make proper resolving code
    // all commons and inherited scripts take the
    // script type of the last character
    // except for the first character, which
    // takes the script type of its next character
}

internal ScriptItems
ItemizeText(UTF32_String utf32_string, u32 paragraph_embedding_level, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    BIDICharacter_String char_infos = ProcessBIDI_UTF32(utf32_string, paragraph_embedding_level, temp_arena);

    ResolveScript(utf32_string, char_infos);
    
    ScriptItems result = {};
    result = GenerateScriptItems(utf32_string, char_infos, temp_arena);

    return result;
}

internal void
CalculateRunOrderMapping(ScriptItems script_items, u32 *visual_to_logical, u32 *logical_to_visual)
{
    TIMED_BLOCK;
    // reverses sequences of RTL runs 
    u32 char_index = 0;
    while(char_index < script_items.num_generated)
    {
        BIDIDirection run_direction = script_items.items[char_index].direction;

        if(run_direction == BIDIDirection_RTL)
        {
            u32 flip_start = char_index;
            u32 flip_length = 1;
            
            for(u32 j = char_index + 1;
                j < script_items.num_generated;
                j++)
            {
                BIDIDirection chain_run_direction = script_items.items[j].direction;

                if(chain_run_direction == BIDIDirection_RTL)
                {
                    flip_length++;
                }
                else
                {
                    break;
                }
            }

            u32 last_flip_run = flip_start + flip_length - 1;

            for(u32 j = 0;
                j < flip_length;
                j++)
            {
                u32 logical_index = flip_start + j;
                u32 visual_index = last_flip_run - j;
                
                logical_to_visual[logical_index] = visual_index;
                visual_to_logical[visual_index] = logical_index;
            }

            char_index += flip_length;
        }
        else
        {
            logical_to_visual[char_index] = char_index;
            visual_to_logical[char_index] = char_index;

            char_index++;
        }
    }
}

