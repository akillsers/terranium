#define TEXTURE_VERTEX_FVF (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
struct TextureVertex
{
    FLOAT x, y, z, rhw;
    DWORD color;

    FLOAT tu, tv;
};

internal void
_D3D9_GenerateBuffersAndSetState(WIN_WindowData *window)
{
    HRESULT hr = S_OK;

    
    hr = window->d3d9_data->device->CreateVertexBuffer(4 * sizeof(TextureVertex),
                                                       D3DUSAGE_DYNAMIC, //TODO what usage should we use?
                                                       TEXTURE_VERTEX_FVF,
                                                       D3DPOOL_DEFAULT,
                                                       &window->d3d9_data->texture_vbuffer,
                                                       0); //reserved

    hr = window->d3d9_data->device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
    Assert(hr == S_OK);

    hr = window->d3d9_data->device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
    Assert(hr == S_OK);

    hr = window->d3d9_data->device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
    Assert(hr == S_OK);

    hr = window->d3d9_data->device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
    Assert(hr == S_OK);

    hr = window->d3d9_data->device->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
    Assert(hr == S_OK);
	
	D3DCOLOR color = D3DCOLOR_ARGB(255, 20, 123, 0);
	
    window->d3d9_data->device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
    window->d3d9_data->device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
    window->d3d9_data->device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
    window->d3d9_data->device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER);
    window->d3d9_data->device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER);
	window->d3d9_data->device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER);
    window->d3d9_data->device->SetSamplerState(0, D3DSAMP_BORDERCOLOR, color);
	
}

internal void
_D3D9_ResizeScreenTexture(D3D9_Data *d3d9_data, u32 width, u32 height)
{
    HRESULT hr;

    if(d3d9_data->screen_texture.staging)
    {
        d3d9_data->screen_texture.staging->Release();
        d3d9_data->screen_texture.staging = 0;
    }

    if(d3d9_data->screen_texture.draw)
    {
        d3d9_data->screen_texture.draw->Release();
        d3d9_data->screen_texture.draw = 0;
    }

    hr = d3d9_data->device->CreateTexture(width,
                                          height,
                                          1, // TODO is this appropriate
                                          0, //TODO annotate (by commenting) these values
                                          d3d9_data->present_params.BackBufferFormat,
                                          D3DPOOL_SYSTEMMEM,
                                          &d3d9_data->screen_texture.staging,
                                          0);
    Assert(hr == S_OK);


    hr = d3d9_data->device->CreateTexture(width,
                                          height,
                                          1,
                                          0,
                                          d3d9_data->present_params.BackBufferFormat,
                                          D3DPOOL_DEFAULT,
                                          &d3d9_data->screen_texture.draw,
                                          0);
    Assert(hr == S_OK);

    d3d9_data->screen_texture.width = width;
    d3d9_data->screen_texture.height = height;

    D3DCOLOR d3d_color = D3DCOLOR_COLORVALUE(1.0f, 1.0f, 1.0f, 1.0f);
    float height_f32 = (FLOAT)height;
    float width_f32 = (FLOAT)width;
    TextureVertex vertices[] =
        {
            {0.0f, height_f32, 0.0f, 1.0f, d3d_color, 0.0f, 1.0f},
            {0.0f, 0.0f, 0.0f, 1.0f, d3d_color, 0.0f, 0.0f},
            {width_f32, height_f32, 0.0f, 1.0f, d3d_color, 1.0f, 1.0f},
            {width_f32, 0.0f, 0.0f, 1.0f, d3d_color, 1.0f, 0.0f},
        };

    VOID *buffer_data;
    hr = d3d9_data->texture_vbuffer->Lock(0, 0, &buffer_data, D3DLOCK_DISCARD);
    Assert(hr == S_OK);
    CopyMemory(buffer_data, vertices, sizeof(vertices));
    hr = d3d9_data->texture_vbuffer->Unlock();
    Assert(hr == S_OK);
}

internal void
D3D9_UpdateFramebuffer(WIN_WindowData *window)
{
    HRESULT hr;

    window->d3d9_data->device->Clear(0, // num rects
                                     0, //pRects (array of D3DRECTs)
                                     D3DCLEAR_TARGET,
                                     D3DCOLOR_ARGB(0, 0, 0, 0),
                                     0.0f, // depth clear valuebuffer
                                     0); // stencil clear value

    // TODO this doesn't seem right: why do we unlock the rect and then lock it?
    hr = window->d3d9_data->screen_texture.staging->UnlockRect(0);
    Assert(hr == S_OK);

    RECT lock_region = {};
    lock_region.left = 0;
    lock_region.top = 0;
    lock_region.right = window->d3d9_data->screen_texture.width;
    lock_region.bottom = window->d3d9_data->screen_texture.height;

    hr = window->d3d9_data->screen_texture.staging->LockRect(0, // TODO what is this
                                                             &window->d3d9_data->screen_texture.locked_staging,
                                                             &lock_region,
                                                             0);
    Assert(hr == S_OK);

    WIN_UpdateFramebuffer(window,
                          window->d3d9_data->screen_texture.locked_staging.pBits,
                          window->d3d9_data->screen_texture.locked_staging.Pitch,
                          PixelFormat_B8G8R8A8);

    Assert(hr == S_OK);
        
    hr = window->d3d9_data->device->UpdateTexture(window->d3d9_data->screen_texture.staging,
                                                  window->d3d9_data->screen_texture.draw);
    Assert(hr == S_OK);

    hr = window->d3d9_data->device->BeginScene();
    Assert(hr == S_OK);

    window->d3d9_data->device->SetTexture(0, window->d3d9_data->screen_texture.draw);
	Assert(hr == S_OK)
	// d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
    // d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
    // d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

    window->d3d9_data->device->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
    window->d3d9_data->device->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);

    window->d3d9_data->device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
    window->d3d9_data->device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);

    hr = window->d3d9_data->device->SetFVF(TEXTURE_VERTEX_FVF);

    // TODO is this needed everytime?
    hr = window->d3d9_data->device->SetStreamSource(0,
                                                    window->d3d9_data->texture_vbuffer,
                                                    0,
                                                    sizeof(TextureVertex));
    Assert(hr == S_OK);

    hr = window->d3d9_data->device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
    Assert(hr == S_OK);

    hr = window->d3d9_data->device->EndScene();
    Assert(hr == S_OK);
    
}

internal void	
_D3D9_ResetDevice(WIN_WindowData *window)
{
    HRESULT hr;
    DWORD render_state;

    hr = window->d3d9_data->device->ResetEx(&window->d3d9_data->present_params,
                                            0); // fullscreen display mode
    Assert(hr == S_OK);
    _D3D9_GenerateBuffersAndSetState(window);
}

internal void
D3D9_CreateResizeSwapChain(WIN_WindowData *window)
{
	HRESULT hr;
	
	if(!window->d3d9_data)
	{
		window->d3d9_data = PushType(&g_win32.permanent_arena, D3D9_Data);

		window->d3d9_data->present_params.Windowed = TRUE;
		window->d3d9_data->present_params.hDeviceWindow = window->hwnd;
		window->d3d9_data->present_params.SwapEffect = D3DSWAPEFFECT_FLIP;
		window->d3d9_data->present_params.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
		window->d3d9_data->present_params.BackBufferFormat = D3DFMT_A8R8G8B8;

		UINT initial_adapter = D3DADAPTER_DEFAULT;
		if(window->d3d9_data->device)
		{
			window->d3d9_data->device->Release();
			window->d3d9_data->device = 0;
		}

		hr = g_win32.d3d9->context->CreateDeviceEx(initial_adapter,
												   D3DDEVTYPE_HAL,
												   window->hwnd,
												   D3DCREATE_HARDWARE_VERTEXPROCESSING,
												   &window->d3d9_data->present_params,
												   0, // fullscreen display mode
												   &window->d3d9_data->device);
		Assert(hr == S_OK);

		_D3D9_GenerateBuffersAndSetState(window);
		
		Recti client_rect = WIN_GetClientRect(window);
		_D3D9_ResizeScreenTexture(window->d3d9_data, client_rect.width, client_rect.height);
	}
	else
	{
		D3DVIEWPORT9 viewport = {};

		hr = window->d3d9_data->device->GetViewport(&viewport);
		
		Recti client_rect = WIN_GetClientRect(window);
		#if 1
		viewport.X = 0;
		viewport.Y = 0;
		viewport.Width = client_rect.width;
		viewport.Height = client_rect.height;
		hr = window->d3d9_data->device->SetViewport(&viewport);
		Assert(hr == S_OK);
		#endif
		// TODO find out why this is necessary?
		window->d3d9_data->present_params.BackBufferWidth = 0;
		window->d3d9_data->present_params.BackBufferHeight = 0;
		window->d3d9_data->present_params.FullScreen_RefreshRateInHz = 0;

		// TODO this doesn't look right
		HRESULT cooperative_level = window->d3d9_data->device->TestCooperativeLevel();

		if(cooperative_level != D3DERR_DEVICELOST)
		{
			_D3D9_ResetDevice(window);
		}

		_D3D9_ResizeScreenTexture(window->d3d9_data, client_rect.width, client_rect.height);
	}
}

internal void
D3D9_Present(WIN_WindowData *window)
{
    D3D9_UpdateFramebuffer(window);
    
	// TODO what is this for?
    RECT viewport;
    viewport.left = 0;
    viewport.top = 0;
    viewport.right = window->d3d9_data->screen_texture.width;
    viewport.bottom = window->d3d9_data->screen_texture.height;

    HRESULT present_hr = window->d3d9_data->device->PresentEx(0,
                                                              0,
                                                              0, //hwnd override
                                                              0,
                                                              0);

    if(present_hr != S_OK)
    {
        HRESULT device_state = window->d3d9_data->device->CheckDeviceState(window->hwnd);

        switch(device_state)
        {
            case D3DERR_DEVICELOST:
            {
                Assert(false);
            } break;

            case S_PRESENT_OCCLUDED:
            {
                // we will present later
            } break;

            case S_PRESENT_MODE_CHANGED:
            {
                // we neeed to reset the device to something compatible
                _D3D9_ResetDevice(window);
            } break;
            InvalidDefaultCase;
        }
    }
}

internal void
D3D9_Init()
{
    HRESULT hr;

    Assert(g_win32.d3d9 == 0);
    Assert(g_win32.swap_chain_backend == WIN_SwapChain_null);

    g_win32.CreateResizeSwapChain = D3D9_CreateResizeSwapChain;
    g_win32.Present = D3D9_Present;

    g_win32.d3d9 = PushType(&g_win32.permanent_arena, D3D9_DeviceData);
    
    // TODO do we have to support d3d non-ex?
    Direct3DCreate9Ex(D3D_SDK_VERSION, &g_win32.d3d9->context);
}
