#if FULL_WIN_SDK
#include <dwrite.h>
#include <dwrite_1.h>
#else
#include "min_dwrite.h"
#endif

#include "font_dwrite.h"

DWRITE_data g_dwrite;

#define SAFE_RELEASE(interface) {               \
    if(interface != 0)                          \
    {                                           \
        interface->Release();                   \
        interface = 0;                          \
    }                                           \
}                                               \

internal void
DWRITE_FreeFont(Font *font)
{
    SAFE_RELEASE(font->driver_data->dwrite_font);
    SAFE_RELEASE(font->driver_data->dwrite_font_face);
    SAFE_RELEASE(font->driver_data->dwrite_font_face1);
    
    FreeAllBlocks(&font->arena);
}

internal GlyphRenderInfo
DWRITE_CreateGlyphRenderInfo(Font *font, u16 glyph_id, Vec2i subpixel_position, GlyphTextureType texture_type, MemoryArena *temp_arena)
{
    GlyphRenderInfo result = {};

    DWRITE_GlyphAnalysis *glyph_analysis = PushType(temp_arena, DWRITE_GlyphAnalysis);

    DWRITE_GLYPH_RUN glyph_run = {};
    glyph_run.fontFace = font->driver_data->dwrite_font_face;
    glyph_run.fontEmSize = font->metrics->pixel_size; // DIPS (=1/96 inch)
    glyph_run.glyphCount = 1;
    glyph_run.glyphIndices = &glyph_id;

    DWRITE_MATRIX transform = {};
    transform.m11 = 1.0f;
    transform.m22 = 1.0f;
    transform.dx = (f32)subpixel_position.x / SubpixelGranularity;
    transform.dy = (f32)subpixel_position.y / SubpixelGranularity;

    HRESULT hr;
    {
        TIMED_BLOCK;
        hr = g_dwrite.factory->CreateGlyphRunAnalysis(&glyph_run,
            1.0f, // pixels per DIP
            &transform, // transform
            DWRITE_RENDERING_MODE_NATURAL_SYMMETRIC,
            DWRITE_MEASURING_MODE_NATURAL,
            0.0f, // baselineOriginX
            0.0f, // baselineOriginY
            &glyph_analysis->sys);
    }

    glyph_analysis->font = font;
    glyph_analysis->glyph_id = glyph_id;
    Assert(hr == S_OK);
    
    RECT bbox = {};
    hr = glyph_analysis->sys->GetAlphaTextureBounds(DWRITE_TEXTURE_CLEARTYPE_3x1, &bbox);

    glyph_analysis->glyph_id = glyph_id;
    glyph_analysis->subpixel_position = subpixel_position;
    glyph_analysis->texture_type = texture_type;

    result.sys = glyph_analysis;
    
    result.bboxw = bbox.right - bbox.left;
    result.bboxh = bbox.bottom - bbox.top;
    result.xoff = bbox.left;
    result.yoff = bbox.top;

    return result;
}

internal void
DWRITE_RenderGlyphByID(GlyphRenderInfo *render_info, u8 *gen_dest)
{
    TIMED_BLOCK;
    HRESULT hr;
    DWRITE_GlyphAnalysis *glyph_analysis = (DWRITE_GlyphAnalysis *)render_info->sys;
    
    if (render_info->bboxw != 0 &&
        render_info->bboxh != 0)
    {
        u32 generated_size = 3 * render_info->bboxw * render_info->bboxh;
        u8 *generated_pixels = 0;

        {
            TIMED_BLOCK;
            generated_pixels = PushCount(&platform.frame_arena, u8, generated_size);
        }
            

        RECT bbox = {};
        // TODO do we even need to specify the left and top?
        bbox.left = render_info->xoff;
        bbox.top = render_info->yoff;
        bbox.right = render_info->xoff + render_info->bboxw;
        bbox.bottom = render_info->yoff + render_info->bboxh;

        {
            TIMED_BLOCK;
            hr = glyph_analysis->sys->CreateAlphaTexture(DWRITE_TEXTURE_CLEARTYPE_3x1, 
                                                         &bbox, generated_pixels, generated_size);
        }
        Assert(SUCCEEDED(hr));

        u32 glyph_pixels_size = 4 * (render_info->bboxw) * (render_info->bboxh);

        u8 *src_row = generated_pixels;
        u8 *dest_row = gen_dest;
        
        u32 src_stride = 3 * render_info->bboxw;
        u32 dest_stride = 4 * render_info->bboxw;

        if(glyph_analysis->texture_type == GlyphTextureType_subpixel)
        {
            TIMED_BLOCK;
            for(int i = 0;
                i < render_info->bboxh;
                i++)
            {
                u8 *src = src_row;
                u8 *dest = dest_row;

                for(int j = 0;
                    j < render_info->bboxw;
                    j++)
                {
                    dest[0] = src[2]; // blue
                    dest[1] = src[1]; // green
                    dest[2] = src[0]; // red
                    dest[3] = 0;      // alpha

                    dest += 4;
                    src += 3;
                }

                src_row += src_stride;
                dest_row += dest_stride;
            }
        }
        else if(glyph_analysis->texture_type == GlyphTextureType_grayscale)
        {
            TIMED_BLOCK;
            for(int i = 0;
                i < render_info->bboxh;
                i++)
            {
                u8 *src = src_row;
                u8 *dest = dest_row;

                for(int j = 0;
                    j < render_info->bboxw;
                    j++)
                {
                    u8 intensity = (src[2] + src[1] + src[0]) / 3;
                    
                    dest[0] = intensity; // intensity
                    dest[1] = intensity; // green
                    dest[2] = intensity; // red
                    dest[3] = 0;      // alpha

                    dest += 4;
                    src += 3;
                }

                src_row += src_stride;
                dest_row += dest_stride;
            }
        }
        else
        {
            InvalidCodePath;
        }

    }
    else
    {
        // platform.Info("Didn't generate image for glyph_id %u because it is a zero-size glyph.\n", glyph_analysis->glyph_id);   
    }

    glyph_analysis->sys->Release();
}

internal Vec2i
DWRITE_GetMaxGlyphDimensions(Font *font)
{
    // TODO: This requires dwrite_1.h
    // TODO: Check to see how easy it is to extract this information from an OpenType file
    //       for learning purposes

    Vec2i result = {};

    DWRITE_FONT_METRICS1 metrics = {};

    font->driver_data->dwrite_font_face1->GetMetrics(&metrics);

    // TODO: I have no idea if this is done correctly
    result.x = (metrics.glyphBoxRight - metrics.glyphBoxLeft) * font->metrics->height_scaling;
    result.y = (metrics.glyphBoxTop - metrics.glyphBoxBottom) * font->metrics->height_scaling;

    return result;
}

internal Font *
DWRITE_CreateSystemFont(String name, f32 point_size, f32 dpi_scaling)
{
    HRESULT hr;

    Font *font = (Font *)BootstrapPushStruct(Font, offsetof(Font, arena));
    font->driver_data = PushType(&font->arena, FontDriverData);

    WCHAR *name_wchar = WIN_StringToWCHAR(name, &platform.frame_arena);
    
    u32 family_index;
    b32 family_found;
    g_dwrite.system_fonts->FindFamilyName(name_wchar, &family_index, &family_found);

    if(!family_found)
    {
        char error_message[1024];
        snprintf(error_message, sizeof(error_message), "The system could not find %.*s", name.len, name.ptr);

        platform.Error("Font error", error_message);
        DWRITE_FreeFont(font);

        return font;
    }
    else
    {
        IDWriteFontFamily *font_family = 0;

        g_dwrite.system_fonts->GetFontFamily(family_index, &font_family);
        font_family->GetFirstMatchingFont(DWRITE_FONT_WEIGHT_NORMAL,
                                          DWRITE_FONT_STRETCH_NORMAL,
                                          DWRITE_FONT_STYLE_NORMAL,
                                          &font->driver_data->dwrite_font);
        font_family->Release();

        
        font->driver_data->dwrite_font->CreateFontFace(&font->driver_data->dwrite_font_face);

        font->family_name = PushString(&font->arena, name.len);
        CopyStringData(font->family_name, name);
        
        font->metrics = PushType(&font->arena, FontMetrics);
    
        DWRITE_FONT_METRICS dwrite_font_metrics;
        FontMetrics *metrics = font->metrics;
        font->driver_data->dwrite_font->GetMetrics(&dwrite_font_metrics);
    
        f32 DPI = 96 * dpi_scaling;  // TODO figure out how this will work with DPI scaling!!! right now use const dpi = 96
        
        font->metrics->logical_size = point_size / 72.0f; 
        f32 font_pixel_size = font->metrics->logical_size * DPI;
        metrics->height_scaling = font_pixel_size / dwrite_font_metrics.designUnitsPerEm;
        metrics->pixel_size = font_pixel_size;

        hr = font->driver_data->dwrite_font_face->QueryInterface(&font->driver_data->dwrite_font_face1);
        Assert(SUCCEEDED(hr));

        u32 unicode_ranges_count = 0;
        font->driver_data->dwrite_font_face1->GetUnicodeRanges(0, 0, &unicode_ranges_count);
        DWRITE_UNICODE_RANGE *unicode_ranges = PushCount(&platform.frame_arena, DWRITE_UNICODE_RANGE, unicode_ranges_count);
        hr = font->driver_data->dwrite_font_face1->GetUnicodeRanges(unicode_ranges_count, unicode_ranges, &unicode_ranges_count);
        Assert(SUCCEEDED(hr));

        for(int i = 0; i < unicode_ranges_count; i++)
        {
            DWRITE_UNICODE_RANGE *unicode_range = unicode_ranges + i;

            // platform.Info("Range: %u to %u\n", unicode_range->first, unicode_range->last);

            for(u32 i = unicode_range->first; i <= unicode_range->last; i++)
            {
                u16 glyph_id = 0;
                font->driver_data->dwrite_font_face->GetGlyphIndices(&i, 1, &glyph_id);
                font->codepoint[i] = glyph_id;
            }
        }

        font->num_glyphs = font->driver_data->dwrite_font_face->GetGlyphCount();

        metrics->point_size = point_size;
        metrics->ascent = dwrite_font_metrics.ascent * metrics->height_scaling;
        metrics->descent = dwrite_font_metrics.descent * metrics->height_scaling;
        metrics->y_advance = ((f32)(dwrite_font_metrics.ascent + dwrite_font_metrics.descent + dwrite_font_metrics.lineGap) *
                             metrics->height_scaling);
    }

    return font;
}

internal void *
DWRITE_GetFontFace(Font *font)
{
    return font->driver_data->dwrite_font_face;
}

internal int
DWRITE_Init()
{
    HRESULT hr;
    
    font_backend.GetMaxGlyphDimensions = DWRITE_GetMaxGlyphDimensions;
    font_backend.CreateGlyphRenderInfo = DWRITE_CreateGlyphRenderInfo;
    font_backend.RenderGlyphByID = DWRITE_RenderGlyphByID;
    font_backend.CreateSystemFont = DWRITE_CreateSystemFont;
    font_backend.FreeFont = DWRITE_FreeFont;
    font_backend.GetDWriteFontFace = DWRITE_GetFontFace;
    font_backend.font_backend_id = FontBackend_DWrite;
    
    hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory),
                             (IUnknown **) &g_dwrite.factory);

    if(FAILED(hr))
    {
        return -1;
    }

    
    hr = g_dwrite.factory->GetSystemFontCollection(&g_dwrite.system_fonts);
    if(FAILED(hr))
    {
        return -1;
    }

    return 0;
}
