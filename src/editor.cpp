#include "editor.h"

#include "timers.cpp"
#include "storage.cpp"

#include "bidi.cpp"
#include "terranium_harfbuzz.cpp"
#include "engine.cpp"
#include "cpp_parser.cpp"

#include "tokens.cpp"

#include "undo_storage.cpp"

#include "draw.cpp"
#include "compositor.cpp"
#include "scrollbar.cpp"
#include "shaped_text.cpp"
#include "loaded_file.cpp"
#include "render_text.cpp"
#include "textfield.cpp"
#include "tabbar.cpp"

internal String
GetFileNameSubstring(String file_path)
{
    String file_name_substring = {};

    UTF8 *at = file_path.ptr + file_path.len - 1;
    for(;;)
    {
        at--;
        if(*at == '/' || *at == '\\'|| at == file_path.ptr)
        {
            break;
        }
    }

    file_name_substring.ptr = at + 1;
    file_name_substring.len = file_path.len - (at + 1 - file_path.ptr);

    return file_name_substring;
}

inline b32
ModifierMeetsCriteria(b32 modifier_down, ModifierCriteria criteria)
{
    b32 result = false;

    if(criteria == Modifier_require &&
       modifier_down)
    {
        result = true;
    }
    else if(criteria == Modifier_without &&
            !modifier_down)
    {
        result = true;
    }
    else if(criteria == Modifier_any)
    {
        result = true;
    }

    return result;
}

internal u32
CommandIsTriggered(EditorState *editor, Input *input, EditorCommand editor_command)
{
    b32 result = false;

    b32 shift = input->keys[Key_left_shift].down || input->keys[Key_right_shift].down;
    b32 ctrl = input->keys[Key_left_ctrl].down || input->keys[Key_right_ctrl].down;
    b32 alt = input->keys[Key_left_alt].down || input->keys[Key_right_alt].down;
    
    KeyBinding *first_key_binding = editor->key_bindings + editor_command;
    for(KeyBinding *key_binding = first_key_binding;
        key_binding;
        key_binding = key_binding->next)
    {
        b32 shift_criteria_met = ModifierMeetsCriteria(shift, (ModifierCriteria)key_binding->shift_criteria);
        b32 ctrl_criteria_met = ModifierMeetsCriteria(ctrl, (ModifierCriteria)key_binding->ctrl_criteria);
        b32 alt_criteria_met = ModifierMeetsCriteria(alt, (ModifierCriteria)key_binding->alt_criteria);

        if(shift_criteria_met &&
           ctrl_criteria_met &&
           alt_criteria_met)
        {
            result = input->keys[key_binding->key].down_repeats;
            
            if(input->keys[key_binding->key].down_repeats)
            {
                input->keys[key_binding->key].down_repeats--;
                break;
            }
        }
    }

    return result;
}

internal void
Editor_NewFile(EditorState *editor)
{
    String file_path = EmptyString();
    String file_name = EmptyString();
    String new_file_content = EmptyString();
    LoadFileResult load_result = LoadFile(editor, file_path, file_name, new_file_content, &platform.frame_arena);

    Assert(load_result.error == FileLoadError_none);

    Font *font = GetThemeFont(editor->theme, Font_code);
    
    NotImplemented;
}

internal EditorState *
GetEditorStateFromMemory(EditorMemory *memory)
{
    return (EditorState *) memory->permanent_storage;
}

// TODO we only need the framebuffer here to know the size of the window
//      should we really pass the whole daddy in?
internal void
LayoutEditor(EditorState *editor, u32 dpi_scaling, Framebuffer *framebuffer)
{
    i32 tabbar_height = Round(TABBAR_HEIGHT * dpi_scaling);
    Recti tabbar_rect = {0, 0, (i32) framebuffer->width, tabbar_height};
    editor->tabbar->bbox = tabbar_rect;
	
	editor->textbox->bbox = {0, tabbar_height, (i32) framebuffer->width, (i32) (framebuffer->height - tabbar_height)};
}

internal void
Editor_SwitchToLF(EditorState *editor, LoadedFile *lf)
{
    // TODO consider using different render texts for each LF entirely
    //      so that way we don't have to call InvalidateTextCache?
    InvalidateTextCache(editor->textbox->render_text);
    editor->textbox->render_text->buffer = lf->text;
	
    editor->textbox->undo_storage = lf->undo_storage;

	editor->textbox->full_redraw_needed = true;
	
	editor->textbox->render_text->text_color_function = LF_GetSyntaxColor;
	editor->textbox->render_text->get_color_params = lf;
	editor->textbox->insert_text_hook = LFInsertText;
	editor->textbox->insert_text_hook_params = lf;
	
	editor->textbox->delete_text_hook = LFDeleteText;
	editor->textbox->delete_text_hook_params = lf;	

	LoadedFile *old_lf = editor->current_lf;
	
	if(old_lf)
	{
		old_lf->cursor = editor->textbox->cursor;
		old_lf->line = editor->textbox->line;
	}
	
	editor->textbox->cursor = lf->cursor;
	editor->textbox->line = lf->line;
	editor->textbox->mark_activated = false;
	
	editor->current_lf = lf;
}


#define MIN_FONT_SIZE 6
#define MAX_FONT_SIZE 150

#define FONT_SIZE_CHANGE_STEP 2

internal void
Editor_UpdateLFRenderTextFonts(EditorState *editor, Font *font)
{
    for(LoadedFile *lf = editor->loaded_files_sentinel->next;
        lf != editor->loaded_files_sentinel;
        lf = lf->next)
    {
        if(lf->render_text)
        {
            lf->render_text->font = font;
            InvalidateTextCache(lf->render_text);
        }
    }
}

internal void
Editor_ResizeCodeFont(EditorState *editor, Input *input, f32 new_font_size)
{
    Font *font = GetThemeFont(editor->theme, Font_code);

    font_backend.FreeFont(font);
    font_backend.CreateSystemFont(editor_settings.code_font.name, new_font_size, input->dpi_scaling);
    editor->theme->fonts[Font_code] = font;

    Editor_UpdateLFRenderTextFonts(editor, font);
}

/*Editor_OverwriteFile is the function used to save (Ctrl-S) a file*/
/*So think about this function as Editor_SaveFile()*/
internal b32
Editor_OverwriteFile(EditorState *editor, LoadedFile *lf, String file_path)
{
    b32 overwrite_success = false;
    
    PlatformFileHandle fh = platform.CreateNewFile(file_path, FileAccess_read);

    if(fh)
    {
        // TODO don't always save it as a UTF-8 encoded file
        u32 alloc_size = lf->text->total_bytes;
        UTF8 *buffer = PushCount(&platform.frame_arena, UTF8, alloc_size);
        u32 chars_copied = CopyEntireBuffer(lf->text, buffer, alloc_size);

        u32 chars_written = platform.WriteEntireFile(fh, buffer, chars_copied);
        platform.CloseFile(fh);

        FileError file_error = platform.GetLastFileError();
        if(file_error == FileError_no_error)
        {
            overwrite_success = true;
        }
        else
        {
            // TODO more detail here
            platform.Error("File save unsuccessful", "Could not write the file.");
        }
    }
    else
    {
        // TODO more detail here
        //     -say why the file can't be opened
        //     -say what the path is 
        platform.Error("File access unsuccessful", "Could not open the file to save.");
    }

    return overwrite_success;
}

internal void
Editor_CloseTabHook(EditorState *editor, void *data)
{
    NotImplemented;
}

// NOTE:: that the file_name should be contained in the path
internal void
Editor_OpenFileByPath(EditorState *editor, String file_path, String file_name) 
{
    PlatformFileHandle file_handlle = platform.OpenFile(file_path, FileAccess_read);
    
    if(file_handlle)
    {
        u32 file_size = platform.GetFileSize(file_handlle);
        UTF8 *file_buffer = (UTF8 *)malloc(file_size);
        // TODO malloc out of memory check
        u32 bytes_read = platform.ReadFile(file_handlle, file_buffer, file_size);
        String file_buffer_string = MakeString(file_buffer, file_size);
        if(bytes_read == file_size)
        {
            FileLoadError file_load_error = FileLoadError_none;
            LoadFileResult load_result = LoadFile(editor, file_path, file_name,
                                                  file_buffer_string, &platform.frame_arena);
            if(load_result.error == FileLoadError_none)
            {
				LoadedFile *lf = load_result.lf;
				DL_INSERT_AFTER(editor->loaded_files_sentinel->prev, lf);

                Tab *tab = CreateTab(editor, editor->tabbar, lf->file_name, lf, &lf->arena);
                tab->close_tab_hook = Editor_CloseTabHook;

                lf->tab = tab;

                Font *font = GetThemeFont(editor->theme, Font_code);
				
                Editor_SwitchToLF(editor, lf);
			}
            else
            {
                // ERROR:: did not read properly
                // could not load the file in
                platform.Error("Unsucessful file load", "Terranium was unable to load the file into the editor. Maybe the file encoding is unsupported?");
            }
            
        }
        else
        {
            // ERROR:: did not read properly
            platform.Error("Open file was unsuccesful", "Terranium could not read the file.");
            
        }
        free(file_buffer);
        platform.CloseFile(file_handlle);
    }
    else
    {
        // ERROR:: did not acquire file handle
        FileError file_error = platform.GetLastFileError();
        switch(file_error)
        {
            case FileError_in_use:
            {
                platform.Error("File not opened",
                                 "Another program is currently using the file.");
            } break;

            case FileError_nonexistent:
            {
                platform.Error("File not opened",
                                 "The file could not be found.");
            } break;

            case FileError_access_denied:
            {
                platform.Error("Access denied",
                                 "You do not have sufficient privilleges to view the file.");
            } break;
            
            default:
                break;
        }
    }
}

inline KeyBinding
key_binding(CtrlCriteria ctrl_criteria, ShiftCriteria shift_criteria, AltCriteria alt_criteria, PlatformKey key)
{
    KeyBinding result = {};

    result.shift_criteria = shift_criteria;
    result.ctrl_criteria = ctrl_criteria;
    result.alt_criteria = alt_criteria;

    result.key = key;

    return result;
}

internal void
AddBinding(EditorState *editor, EditorCommand command, CtrlCriteria ctrl, ShiftCriteria shift, AltCriteria alt, PlatformKey key)
{
    KeyBinding *last_binding = editor->key_bindings + command;

    // TODO can we express this in a more concise way?
    for(;;)
    {
        if(last_binding->next)
        {
            last_binding = last_binding->next;
        }
        else
        {
            break;
        }
    }
    
    if(last_binding->key == Key_invalid)
    {
        *last_binding = key_binding(ctrl, shift, alt, key);
    }
    else
    {
        last_binding->next = PushType(&editor->total_storage_arena, KeyBinding);
        *last_binding->next = key_binding(ctrl, shift, alt, key);
    }
}

internal void
Editor_DebugInitDefaultBindings(EditorState *editor)
{
    // this should be cleared to zero
    editor->key_bindings = PushCount(&editor->total_storage_arena, KeyBinding, EditorCommand_count);

    AddBinding(editor, EditorCommand_new_file, RequireCtrl, WithoutShift, WithoutAlt, Key_n);
    AddBinding(editor, EditorCommand_open_file, RequireCtrl, WithoutShift, WithoutAlt, Key_o);
    AddBinding(editor, EditorCommand_save_file, RequireCtrl, WithoutShift, WithoutAlt, Key_s);

    AddBinding(editor, TextFieldCommand_next_character, WithoutCtrl, WithoutShift, WithoutAlt, Key_right);
    AddBinding(editor, TextFieldCommand_next_character_extend, WithoutCtrl, RequireShift, WithoutAlt, Key_right);
    AddBinding(editor, TextFieldCommand_previous_character, WithoutCtrl, WithoutShift, WithoutAlt, Key_left);
    AddBinding(editor, TextFieldCommand_previous_character_extend, WithoutCtrl, RequireShift, WithoutAlt, Key_left);
    AddBinding(editor, TextFieldCommand_previous_word, RequireCtrl, WithoutShift, WithoutAlt, Key_left);
    AddBinding(editor, TextFieldCommand_previous_word_extend, RequireCtrl, RequireShift, WithoutAlt, Key_left);
    AddBinding(editor, TextFieldCommand_next_word, RequireCtrl, WithoutShift, WithoutAlt, Key_right);
    AddBinding(editor, TextFieldCommand_next_word_extend, RequireCtrl, RequireShift, WithoutAlt, Key_right);
    
    AddBinding(editor, TextFieldCommand_previous_line, WithoutCtrl, WithoutShift, WithoutAlt, Key_up);
    AddBinding(editor, TextFieldCommand_previous_line_extend, WithoutCtrl, RequireShift, WithoutAlt, Key_up);
    AddBinding(editor, TextFieldCommand_next_line, WithoutCtrl, WithoutShift, WithoutAlt, Key_down);
    AddBinding(editor, TextFieldCommand_next_line_extend, WithoutCtrl, RequireShift, WithoutAlt, Key_down);

    AddBinding(editor, TextFieldCommand_line_start, WithoutCtrl, WithoutShift, WithoutAlt, Key_home);
    AddBinding(editor, TextFieldCommand_line_end, WithoutCtrl, WithoutShift, WithoutAlt, Key_end);
    AddBinding(editor, TextFieldCommand_line_start_extend, WithoutCtrl, RequireShift, WithoutAlt, Key_home);
    AddBinding(editor, TextFieldCommand_line_end_extend, WithoutCtrl, RequireShift, WithoutAlt, Key_end);

    AddBinding(editor, TextFieldCommand_text_start, RequireCtrl, WithoutShift, WithoutAlt, Key_home);
    AddBinding(editor, TextFieldCommand_text_end, RequireCtrl, WithoutShift, WithoutAlt, Key_end);
    AddBinding(editor, TextFieldCommand_text_start_extend, RequireCtrl, RequireShift, WithoutAlt, Key_home);
    AddBinding(editor, TextFieldCommand_text_end_extend, RequireCtrl, RequireShift, WithoutAlt, Key_end);

    AddBinding(editor, TextFieldCommand_previous_empty_line, RequireCtrl, WithoutShift, WithoutAlt, Key_up);
    AddBinding(editor, TextFieldCommand_previous_empty_line_extend, RequireCtrl, RequireShift, WithoutAlt, Key_up);
    AddBinding(editor, TextFieldCommand_next_empty_line, RequireCtrl, WithoutShift, WithoutAlt, Key_down);
    AddBinding(editor, TextFieldCommand_next_empty_line_extend, RequireCtrl, RequireShift, WithoutAlt, Key_down);

    AddBinding(editor, TextFieldCommand_forward_delete_character, WithoutCtrl, WithoutShift, WithoutAlt, Key_delete);
    AddBinding(editor, TextFieldCommand_forward_delete_word, RequireCtrl, WithoutShift, WithoutAlt, Key_delete);
    AddBinding(editor, TextFieldCommand_backward_delete_character, WithoutCtrl, WithoutShift, WithoutAlt, Key_backspace);
    AddBinding(editor, TextFieldCommand_backward_delete_word, RequireCtrl, WithoutShift, WithoutAlt, Key_backspace);

    AddBinding(editor, TextFieldCommand_set_mark, RequireCtrl, AnyShift, WithoutAlt, Key_space);
    AddBinding(editor, TextFieldCommand_newline, WithoutCtrl, AnyShift, WithoutAlt, Key_enter);

    AddBinding(editor, TextFieldCommand_page_up, WithoutCtrl, WithoutShift, WithoutAlt, Key_page_up);
    AddBinding(editor, TextFieldCommand_page_down, WithoutCtrl, WithoutShift, WithoutAlt, Key_page_down);

    AddBinding(editor, TextFieldCommand_set_mark, RequireCtrl, WithoutShift, WithoutAlt, Key_k);
    AddBinding(editor, TextFieldCommand_cut, RequireCtrl, WithoutShift, WithoutAlt, Key_x);
    AddBinding(editor, TextFieldCommand_copy, RequireCtrl, WithoutShift, WithoutAlt, Key_c);
    AddBinding(editor, TextFieldCommand_copy, RequireCtrl, WithoutShift, WithoutAlt, Key_insert);
    AddBinding(editor, TextFieldCommand_paste, RequireCtrl, WithoutShift, WithoutAlt, Key_v);
    AddBinding(editor, TextFieldCommand_paste, WithoutCtrl, RequireShift, WithoutAlt, Key_insert);

    AddBinding(editor, TextFieldCommand_undo, RequireCtrl, WithoutShift, WithoutAlt, Key_z);
    AddBinding(editor, TextFieldCommand_redo, RequireCtrl, RequireShift, WithoutAlt, Key_z);
    AddBinding(editor, TextFieldCommand_redo, RequireCtrl, WithoutShift, WithoutAlt, Key_y);

    AddBinding(editor, TabbarCommand_close_tab, RequireCtrl, WithoutShift, WithoutAlt, Key_w);
}

internal Framebuffer
sGetFramebufferSubRect(Framebuffer *framebuffer, u32 x, u32 y, u32 width, u32 height)
{
    Framebuffer result;
    result.width = width;
    result.height = height;
    result.stride = framebuffer->stride;
    result.bytespp = framebuffer->bytespp;
    result.pixels = (u8 *)framebuffer->pixels + x*framebuffer->bytespp + y*framebuffer->stride;
    return result;
}

#if INTERNAL
internal void
DisplayCounters()
{
    if(DEBUG_hit_count && DEBUG_counter)
    {
        platform.Info("<---------------------->\n");
        platform.Info("section at %s -> line %u\n", DEBUG_counter_file, DEBUG_counter_line);
        platform.Info("%I64u cycles  (hit %I64u times.) \n", DEBUG_counter, DEBUG_hit_count);
        u32 cycles_per_hit = DEBUG_counter / DEBUG_hit_count;
        platform.Info("%I64u cycles/hit! \n", cycles_per_hit);
        platform.Info("<---------------------->\n");

        DEBUG_hit_count = 0;
        DEBUG_counter = 0;
    }

}
#endif

internal void
Editor_Startup(EditorMemory *memory, Input *input, Framebuffer *framebuffer)
{
    EditorState *editor = BootstrapPushStruct(EditorState, offsetof(EditorState, total_storage_arena));
    memory->permanent_storage = editor;
    
    editor->theme = PushType(&editor->total_storage_arena, EditorTheme);
    editor->theme->fonts = PushCount(&editor->total_storage_arena, Font *, Font_count);

    Font *code_font = font_backend.CreateSystemFont(editor_settings.code_font.name, editor_settings.code_font.point_size, input->dpi_scaling);
    Font *tab_font = font_backend.CreateSystemFont(editor_settings.tab_font.name, editor_settings.tab_font.point_size, input->dpi_scaling);
    Font *ui_font = font_backend.CreateSystemFont(editor_settings.code_font.name, editor_settings.tab_font.point_size, input->dpi_scaling);
    editor->theme->fonts[Font_ui] = ui_font;
    editor->theme->fonts[Font_code] = code_font;
    editor->theme->fonts[Font_tab] = tab_font;

    editor->tabbar = PushType(&editor->total_storage_arena, Tabbar);
    InitializeTabbar(editor->tabbar, &editor->total_storage_arena);
	
	
    Editor_DebugInitDefaultBindings(editor);
	editor->compositor = CreateCompositor(&editor->total_storage_arena);
	
    
	editor->loaded_files_sentinel = PushType(&editor->total_storage_arena, LoadedFile);
    DL_INIT_SENTINEL(editor->loaded_files_sentinel);

    editor->textbox = PushType(&editor->total_storage_arena, TextField);
	InitTextField(editor, editor->textbox, 0, code_font, recti(0,0,0,0), 3);
	
	LayoutEditor(editor, input->dpi_scaling, framebuffer);

    
    PlatformCommandLine arguments = platform.RetrieveCommandLine();

    for(u32 i = 1;
        i < (arguments.num_args - 1);
        i++)
    {
        String argument = arguments.args[i];

        if(argument.ptr[0] == '-')
        {
            String command = OffsetString(argument, 1);

            if(StringMatchesCString(command, "config"))
            {
                platform.Info("Config flag triggered!");
            }
            else if(StringMatchesCString(command, "help"))
            {
                platform.Info("Displaying help!");
            }
            else if(StringMatchesCString(command, "version"))
            {
                platform.Info("Terranium version %u.%u", TERRANIUM_VERSION_MAJOR, TERRANIUM_VERSION_MINOR);
            }
            else
            {
                platform.Error("Unknown command line flag!", "There is an unrecognized command line flag!");
            }
        }
        else
        {
            String file_name_string = GetFileNameSubstring(argument);
            Editor_OpenFileByPath(editor, argument, file_name_string);
        }
    }
}

internal void
Editor_SaveFile(EditorState *editor)
{
    NotImplemented;
}

internal void
Editor_OpenFile(EditorState *editor)
{
    // NOTE:: file_path is in utf8
    // TODO figure out what the max file path is and enforece it
    // TODO test with small numbers

    // TODO unify file errors sometimes the platform returns the error like over here
    //      and in file errors a global var is set
    FileDialogResult dialog = platform.FileDialog(FileDialogType_open);
    
    if(dialog.error)
    {
        switch(dialog.error)
        {
            case FileDialogError_buffer_too_small:
            {
                // TODO specify the max file path size and state the path specified
                // NOTE:: when working on this TODO note that the max file path is also restricted by our Windows
                //       platform layer's transient storage size
                platform.Error("Open file was unsuccesful.", "The path specified was too large.");
            } break;

            case FileDialogError_invalid_filename:
            {
                platform.Error("Open file was unsuccesful.", "Invalid file name.");
            } break;

            case FileDialogError_unknown:
            {
                platform.Error("Open file was unsuccesful.", "Unknown error with the system dialog.");
            } break;

            case FileDialogError_cancelled:
            {
                
            } break;
                
            InvalidDefaultCase;
        }
    }
    else
    {
        for(i32 i = 0; i < dialog.num_files; ++i)
        {
            LoadedFile *existing_lf = 0;

            for(LoadedFile *lf = editor->loaded_files_sentinel->next;
                lf != editor->loaded_files_sentinel;
                lf = lf->next)
            {
                if(StringsAreEqual(lf->file_path, dialog.file_paths[i]))
                {
                    existing_lf = lf;
                }
            }

            if(existing_lf)
            {
                Editor_SwitchToLF(editor, existing_lf);
                SwitchToTab(editor->tabbar, existing_lf->tab);
            }
            else
            {
                Editor_OpenFileByPath(editor, dialog.file_paths[i], dialog.file_names[i]);
            }
        }
    }
}

internal void
Editor_CaptureMouse(EditorState *editor, void *ui_component)
{
    editor->mouse_capture = ui_component;
}

internal void
Editor_ReleaseMouse(EditorState *editor)
{
    editor->mouse_capture = 0;
}

internal void
Editor_SetFocus(EditorState *editor, void *item, i32 z_index, UIDefocusHandler handler)
{
    if(editor->focused_ui_item &&
       item != editor->focused_ui_item)
    {
        editor->defocus_handler(editor->focused_ui_item);
    }

    editor->focused_ui_item = item;
    editor->focused_z_index = z_index;
    editor->defocus_handler = handler;
}

inline b32
Editor_NoOpenFiles(EditorState *editor)
{
	return (editor->loaded_files_sentinel->next == editor->loaded_files_sentinel);
}

internal void
Editor_UpdateAndRender(EditorMemory *memory, Input *input, Framebuffer *framebuffer)
{
    if(!memory->is_initialized)
    {
        Editor_Startup(memory, input, framebuffer);
        memory->is_initialized = true;
    }

    EditorState *editor = GetEditorStateFromMemory(memory);
    
    if(input->main_window->resized)
    {
		// TODO does the first resize trigger this?
		LayoutEditor(editor, input->dpi_scaling, framebuffer);
		LayoutTabs(editor->tabbar);
    }

    if(input->dropped.type != DroppedItemType_nothing)
    {
        for(LinkedString *string = input->dropped.string; string; string = string->next)
        {
            // TODO pass open file by path the extension offset
            Editor_OpenFileByPath(editor, string->data, string->data);
        }
    }

    if(KeyPressed(input->keys[Key_f6]))
    {
        SortTimingRecords();
        PrintTimingRecords();
        ResetTimingRecords();
    }

	if(input->main_window->size_state != Window_minimized)
    {
		if(Editor_NoOpenFiles(editor))
		{
			
		}
		else
		{
			UpdateAndRenderTextField(editor, editor->textbox, input);
		}
        
        TabbarInteraction interaction = UpdateAndRenderTabbar(editor, editor->tabbar, input, framebuffer);
	    
		if(interaction.command == TabbarInteraction_switch_to_tab)
        {
            LoadedFile *desired_lf = (LoadedFile *) interaction.new_active_tab->data;
            
            Editor_SwitchToLF(editor, desired_lf);
        }
        else if(interaction.command == TabbarInteraction_create_new_tab)
        {
            Editor_OpenFile(editor);
        }

        CompositeAndRender(editor->compositor, framebuffer);
    }

    while(CommandIsTriggered(editor, input, EditorCommand_save_file))
    {
        Editor_SaveFile(editor);
    }
	
    while(CommandIsTriggered(editor, input, EditorCommand_new_file))
    {
        
    }
    while(CommandIsTriggered(editor, input, EditorCommand_open_file))
    {
        Editor_OpenFile(editor);
    }
}