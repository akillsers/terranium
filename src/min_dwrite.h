#define DWRITE_DECLARE_INTERFACE(x) DECLSPEC_UUID(x) DECLSPEC_NOVTABLE
#define DWRITE_EXPORT __declspec(dllimport) WINAPI
interface IDWriteLocalizedStrings;
interface IDWriteFontFile;
interface IDWriteGeometrySink;
interface IDWriteRenderingParams;
interface IDWriteFontCollectionLoader;
interface IDWriteFontFileLoader;
interface IDWriteTextFormat;
interface IDWriteTypography;
interface IDWriteTextLayout;
interface IDWriteInlineObject;
interface IDWriteAnalyzer;
interface IDWriteBitmapRenderTarget;
interface IDWriteTextAnalyzer;
interface IDWriteNumberSubstitution;

struct DWRITE_FONT_METRICS1;
struct DWRITE_CARET_METRICS;
struct DWRITE_OUTLINE_THRESHOLD;
struct DWRITE_GLYPH_OFFSET;
struct DWRITE_PIXEL_GEOMETRY;
struct DWRITE_NUMBER_SUBSTITUTION_METHOD;

//forward declarations
interface IDWriteFontFace;
interface IDWriteFontCollection;
interface IDWriteFont;

enum DWRITE_FACTORY_TYPE
{
    /// <summary>
    /// Shared factory allow for re-use of cached font data across multiple in process components.
    /// Such factories also take advantage of cross process font caching components for better performance.
    /// </summary>
    DWRITE_FACTORY_TYPE_SHARED,

    /// <summary>
    /// Objects created from the isolated factory do not interact with internal DirectWrite state from other components.
    /// </summary>
    DWRITE_FACTORY_TYPE_ISOLATED
};

struct DWRITE_UNICODE_RANGE
{
    /// <summary>
    /// The first codepoint in the Unicode range.
    /// </summary>
    UINT32 first;

    /// <summary>
    /// The last codepoint in the Unicode range.
    /// </summary>
    UINT32 last;
};

enum DWRITE_TEXTURE_TYPE
{
    /// <summary>
    /// Specifies an alpha texture for aliased text rendering (i.e., bi-level, where each pixel is either fully opaque or fully transparent),
    /// with one byte per pixel.
    /// </summary>
    DWRITE_TEXTURE_ALIASED_1x1,

    /// <summary>
    /// Specifies an alpha texture for ClearType text rendering, with three bytes per pixel in the horizontal dimension and 
    /// one byte per pixel in the vertical dimension.
    /// </summary>
    DWRITE_TEXTURE_CLEARTYPE_3x1
};

struct DWRITE_GLYPH_RUN
{
    /// <summary>
    /// The physical font face to draw with.
    /// </summary>
    _Notnull_ IDWriteFontFace* fontFace;

    /// <summary>
    /// Logical size of the font in DIPs, not points (equals 1/96 inch).
    /// </summary>
    FLOAT fontEmSize;

    /// <summary>
    /// The number of glyphs.
    /// </summary>
    UINT32 glyphCount;

    /// <summary>
    /// The indices to render.
    /// </summary>    
    _Field_size_(glyphCount) UINT16 const* glyphIndices;

    /// <summary>
    /// Glyph advance widths.
    /// </summary>
    _Field_size_opt_(glyphCount) FLOAT const* glyphAdvances;

    /// <summary>
    /// Glyph offsets.
    /// </summary>
    _Field_size_opt_(glyphCount) DWRITE_GLYPH_OFFSET const* glyphOffsets;

    /// <summary>
    /// If true, specifies that glyphs are rotated 90 degrees to the left and
    /// vertical metrics are used. Vertical writing is achieved by specifying
    /// isSideways = true and rotating the entire run 90 degrees to the right
    /// via a rotate transform.
    /// </summary>
    BOOL isSideways;

    /// <summary>
    /// The implicit resolved bidi level of the run. Odd levels indicate
    /// right-to-left languages like Hebrew and Arabic, while even levels
    /// indicate left-to-right languages like English and Japanese (when
    /// written horizontally). For right-to-left languages, the text origin
    /// is on the right, and text should be drawn to the left.
    /// </summary>
    UINT32 bidiLevel;
};

enum DWRITE_RENDERING_MODE
{
    /// <summary>
    /// Specifies that the rendering mode is determined automatically based on the font and size.
    /// </summary>
    DWRITE_RENDERING_MODE_DEFAULT,

    /// <summary>
    /// Specifies that no antialiasing is performed. Each pixel is either set to the foreground 
    /// color of the text or retains the color of the background.
    /// </summary>
    DWRITE_RENDERING_MODE_ALIASED,

    /// <summary>
    /// Specifies that antialiasing is performed in the horizontal direction and the appearance
    /// of glyphs is layout-compatible with GDI using CLEARTYPE_QUALITY. Use DWRITE_MEASURING_MODE_GDI_CLASSIC 
    /// to get glyph advances. The antialiasing may be either ClearType or grayscale depending on
    /// the text antialiasing mode.
    /// </summary>
    DWRITE_RENDERING_MODE_GDI_CLASSIC,

    /// <summary>
    /// Specifies that antialiasing is performed in the horizontal direction and the appearance
    /// of glyphs is layout-compatible with GDI using CLEARTYPE_NATURAL_QUALITY. Glyph advances
    /// are close to the font design advances, but are still rounded to whole pixels. Use
    /// DWRITE_MEASURING_MODE_GDI_NATURAL to get glyph advances. The antialiasing may be either
    /// ClearType or grayscale depending on the text antialiasing mode.
    /// </summary>
    DWRITE_RENDERING_MODE_GDI_NATURAL,

    /// <summary>
    /// Specifies that antialiasing is performed in the horizontal direction. This rendering
    /// mode allows glyphs to be positioned with subpixel precision and is therefore suitable
    /// for natural (i.e., resolution-independent) layout. The antialiasing may be either
    /// ClearType or grayscale depending on the text antialiasing mode.
    /// </summary>
    DWRITE_RENDERING_MODE_NATURAL,

    /// <summary>
    /// Similar to natural mode except that antialiasing is performed in both the horizontal
    /// and vertical directions. This is typically used at larger sizes to make curves and
    /// diagonal lines look smoother. The antialiasing may be either ClearType or grayscale
    /// depending on the text antialiasing mode.
    /// </summary>
    DWRITE_RENDERING_MODE_NATURAL_SYMMETRIC,

    /// <summary>
    /// Specifies that rendering should bypass the rasterizer and use the outlines directly. 
    /// This is typically used at very large sizes.
    /// </summary>
    DWRITE_RENDERING_MODE_OUTLINE,

    // The following names are obsolete, but are kept as aliases to avoid breaking existing code.
    // Each of these rendering modes may result in either ClearType or grayscale antialiasing 
    // depending on the DWRITE_TEXT_ANTIALIASING_MODE.
    DWRITE_RENDERING_MODE_CLEARTYPE_GDI_CLASSIC         = DWRITE_RENDERING_MODE_GDI_CLASSIC,
    DWRITE_RENDERING_MODE_CLEARTYPE_GDI_NATURAL         = DWRITE_RENDERING_MODE_GDI_NATURAL,
    DWRITE_RENDERING_MODE_CLEARTYPE_NATURAL             = DWRITE_RENDERING_MODE_NATURAL,
    DWRITE_RENDERING_MODE_CLEARTYPE_NATURAL_SYMMETRIC   = DWRITE_RENDERING_MODE_NATURAL_SYMMETRIC
};


struct DWRITE_MATRIX
{
    /// <summary>
    /// Horizontal scaling / cosine of rotation
    /// </summary>
    FLOAT m11;

    /// <summary>
    /// Vertical shear / sine of rotation
    /// </summary>
    FLOAT m12;

    /// <summary>
    /// Horizontal shear / negative sine of rotation
    /// </summary>
    FLOAT m21;

    /// <summary>
    /// Vertical scaling / cosine of rotation
    /// </summary>
    FLOAT m22;

    /// <summary>
    /// Horizontal shift (always orthogonal regardless of rotation)
    /// </summary>
    FLOAT dx;

    /// <summary>
    /// Vertical shift (always orthogonal regardless of rotation)
    /// </summary>
    FLOAT dy;
};

struct DWRITE_GLYPH_OFFSET
{
    /// <summary>
    /// Offset in the advance direction of the run. A positive advance offset moves the glyph to the right
    /// (in pre-transform coordinates) if the run is left-to-right or to the left if the run is right-to-left.
    /// </summary>
    FLOAT advanceOffset;

    /// <summary>
    /// Offset in the ascent direction, i.e., the direction ascenders point. A positive ascender offset moves
    /// the glyph up (in pre-transform coordinates).
    /// </summary>
    FLOAT ascenderOffset;
};

typedef enum DWRITE_MEASURING_MODE
{
    /// <summary>
    /// Text is measured using glyph ideal metrics whose values are independent to the current display resolution.
    /// </summary>
    DWRITE_MEASURING_MODE_NATURAL,

    /// <summary>
    /// Text is measured using glyph display compatible metrics whose values tuned for the current display resolution.
    /// </summary>
    DWRITE_MEASURING_MODE_GDI_CLASSIC,

    /// <summary>
    // Text is measured using the same glyph display metrics as text measured by GDI using a font
    // created with CLEARTYPE_NATURAL_QUALITY.
    /// </summary>
    DWRITE_MEASURING_MODE_GDI_NATURAL

} DWRITE_MEASURING_MODE;

struct DWRITE_GLYPH_METRICS
{
    /// <summary>
    /// Specifies the X offset from the glyph origin to the left edge of the black box.
    /// The glyph origin is the current horizontal writing position.
    /// A negative value means the black box extends to the left of the origin (often true for lowercase italic 'f').
    /// </summary>
    INT32 leftSideBearing;

    /// <summary>
    /// Specifies the X offset from the origin of the current glyph to the origin of the next glyph when writing horizontally.
    /// </summary>
    UINT32 advanceWidth;

    /// <summary>
    /// Specifies the X offset from the right edge of the black box to the origin of the next glyph when writing horizontally.
    /// The value is negative when the right edge of the black box overhangs the layout box.
    /// </summary>
    INT32 rightSideBearing;

    /// <summary>
    /// Specifies the vertical offset from the vertical origin to the top of the black box.
    /// Thus, a positive value adds whitespace whereas a negative value means the glyph overhangs the top of the layout box.
    /// </summary>
    INT32 topSideBearing;

    /// <summary>
    /// Specifies the Y offset from the vertical origin of the current glyph to the vertical origin of the next glyph when writing vertically.
    /// (Note that the term "origin" by itself denotes the horizontal origin. The vertical origin is different.
    /// Its Y coordinate is specified by verticalOriginY value,
    /// and its X coordinate is half the advanceWidth to the right of the horizontal origin).
    /// </summary>
    UINT32 advanceHeight;

    /// <summary>
    /// Specifies the vertical distance from the black box's bottom edge to the advance height.
    /// Positive when the bottom edge of the black box is within the layout box.
    /// Negative when the bottom edge of black box overhangs the layout box.
    /// </summary>
    INT32 bottomSideBearing;

    /// <summary>
    /// Specifies the Y coordinate of a glyph's vertical origin, in the font's design coordinate system.
    /// The y coordinate of a glyph's vertical origin is the sum of the glyph's top side bearing
    /// and the top (i.e. yMax) of the glyph's bounding box.
    /// </summary>
    INT32 verticalOriginY;
};


struct DWRITE_FONT_METRICS
{
    /// <summary>
    /// The number of font design units per em unit.
    /// Font files use their own coordinate system of font design units.
    /// A font design unit is the smallest measurable unit in the em square,
    /// an imaginary square that is used to size and align glyphs.
    /// The concept of em square is used as a reference scale factor when defining font size and device transformation semantics.
    /// The size of one em square is also commonly used to compute the paragraph indentation value.
    /// </summary>
    UINT16 designUnitsPerEm;

    /// <summary>
    /// Ascent value of the font face in font design units.
    /// Ascent is the distance from the top of font character alignment box to English baseline.
    /// </summary>
    UINT16 ascent;

    /// <summary>
    /// Descent value of the font face in font design units.
    /// Descent is the distance from the bottom of font character alignment box to English baseline.
    /// </summary>
    UINT16 descent;

    /// <summary>
    /// Line gap in font design units.
    /// Recommended additional white space to add between lines to improve legibility. The recommended line spacing 
    /// (baseline-to-baseline distance) is thus the sum of ascent, descent, and lineGap. The line gap is usually 
    /// positive or zero but can be negative, in which case the recommended line spacing is less than the height
    /// of the character alignment box.
    /// </summary>
    INT16 lineGap;

    /// <summary>
    /// Cap height value of the font face in font design units.
    /// Cap height is the distance from English baseline to the top of a typical English capital.
    /// Capital "H" is often used as a reference character for the purpose of calculating the cap height value.
    /// </summary>
    UINT16 capHeight;

    /// <summary>
    /// x-height value of the font face in font design units.
    /// x-height is the distance from English baseline to the top of lowercase letter "x", or a similar lowercase character.
    /// </summary>
    UINT16 xHeight;

    /// <summary>
    /// The underline position value of the font face in font design units.
    /// Underline position is the position of underline relative to the English baseline.
    /// The value is usually made negative in order to place the underline below the baseline.
    /// </summary>
    INT16 underlinePosition;

    /// <summary>
    /// The suggested underline thickness value of the font face in font design units.
    /// </summary>
    UINT16 underlineThickness;

    /// <summary>
    /// The strikethrough position value of the font face in font design units.
    /// Strikethrough position is the position of strikethrough relative to the English baseline.
    /// The value is usually made positive in order to place the strikethrough above the baseline.
    /// </summary>
    INT16 strikethroughPosition;

    /// <summary>
    /// The suggested strikethrough thickness value of the font face in font design units.
    /// </summary>
    UINT16 strikethroughThickness;
};


enum DWRITE_FONT_SIMULATIONS
{
    /// <summary>
    /// No simulations are performed.
    /// </summary>
    DWRITE_FONT_SIMULATIONS_NONE    = 0x0000,

    /// <summary>
    /// Algorithmic emboldening is performed.
    /// </summary>
    DWRITE_FONT_SIMULATIONS_BOLD    = 0x0001,

    /// <summary>
    /// Algorithmic italicization is performed.
    /// </summary>
    DWRITE_FONT_SIMULATIONS_OBLIQUE = 0x0002
};

enum DWRITE_INFORMATIONAL_STRING_ID
{
    /// <summary>
    /// Unspecified name ID.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_NONE,

    /// <summary>
    /// Copyright notice provided by the font.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_COPYRIGHT_NOTICE,

    /// <summary>
    /// String containing a version number.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_VERSION_STRINGS,

    /// <summary>
    /// Trademark information provided by the font.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_TRADEMARK,

    /// <summary>
    /// Name of the font manufacturer.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_MANUFACTURER,

    /// <summary>
    /// Name of the font designer.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_DESIGNER,

    /// <summary>
    /// URL of font designer (with protocol, e.g., http://, ftp://).
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_DESIGNER_URL,

    /// <summary>
    /// Description of the font. Can contain revision information, usage recommendations, history, features, etc.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_DESCRIPTION,

    /// <summary>
    /// URL of font vendor (with protocol, e.g., http://, ftp://). If a unique serial number is embedded in the URL, it can be used to register the font.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_FONT_VENDOR_URL,

    /// <summary>
    /// Description of how the font may be legally used, or different example scenarios for licensed use. This field should be written in plain language, not legalese.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_LICENSE_DESCRIPTION,

    /// <summary>
    /// URL where additional licensing information can be found.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_LICENSE_INFO_URL,

    /// <summary>
    /// GDI-compatible family name. Because GDI allows a maximum of four fonts per family, fonts in the same family may have different GDI-compatible family names
    /// (e.g., "Arial", "Arial Narrow", "Arial Black").
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_WIN32_FAMILY_NAMES,

    /// <summary>
    /// GDI-compatible subfamily name.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_WIN32_SUBFAMILY_NAMES,

    /// <summary>
    /// Typographic family name preferred by the designer. This enables font designers to group more than four fonts in a single family without losing compatibility with
    /// GDI. This name is typically only present if it differs from the GDI-compatible family name.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_TYPOGRAPHIC_FAMILY_NAMES,

    /// <summary>
    /// Typographic subfamily name preferred by the designer. This name is typically only present if it differs from the GDI-compatible subfamily name. 
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_TYPOGRAPHIC_SUBFAMILY_NAMES,

    /// <summary>
    /// Sample text. This can be the font name or any other text that the designer thinks is the best example to display the font in.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_SAMPLE_TEXT,

    /// <summary>
    /// The full name of the font, e.g. "Arial Bold", from name id 4 in the name table.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_FULL_NAME,

    /// <summary>
    /// The postscript name of the font, e.g. "GillSans-Bold" from name id 6 in the name table.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_POSTSCRIPT_NAME,

    /// <summary>
    /// The postscript CID findfont name, from name id 20 in the name table.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_POSTSCRIPT_CID_NAME,

    /// <summary>
    /// Family name for the weight-stretch-style model.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_WEIGHT_STRETCH_STYLE_FAMILY_NAME,

    /// <summary>
    /// Script/language tag to identify the scripts or languages that the font was
    /// primarily designed to support. See DWRITE_FONT_PROPERTY_ID_DESIGN_SCRIPT_LANGUAGE_TAG
    /// for a longer description.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_DESIGN_SCRIPT_LANGUAGE_TAG,

    /// <summary>
    /// Script/language tag to identify the scripts or languages that the font declares
    /// it is able to support.
    /// </summary>
    DWRITE_INFORMATIONAL_STRING_SUPPORTED_SCRIPT_LANGUAGE_TAG,

    // Obsolete aliases kept to avoid breaking existing code.
    DWRITE_INFORMATIONAL_STRING_PREFERRED_FAMILY_NAMES = DWRITE_INFORMATIONAL_STRING_TYPOGRAPHIC_FAMILY_NAMES,
    DWRITE_INFORMATIONAL_STRING_PREFERRED_SUBFAMILY_NAMES = DWRITE_INFORMATIONAL_STRING_TYPOGRAPHIC_SUBFAMILY_NAMES,
    DWRITE_INFORMATIONAL_STRING_WWS_FAMILY_NAME = DWRITE_INFORMATIONAL_STRING_WEIGHT_STRETCH_STYLE_FAMILY_NAME,
};

enum DWRITE_FONT_WEIGHT
{
    /// Predefined font weight : Thin (100).
    DWRITE_FONT_WEIGHT_THIN = 100,

    /// Predefined font weight : Extra-light (200).
    DWRITE_FONT_WEIGHT_EXTRA_LIGHT = 200,

    /// Predefined font weight : Ultra-light (200).
    DWRITE_FONT_WEIGHT_ULTRA_LIGHT = 200,

    /// Predefined font weight : Light (300).
    DWRITE_FONT_WEIGHT_LIGHT = 300,

    /// Predefined font weight : Semi-light (350).
    DWRITE_FONT_WEIGHT_SEMI_LIGHT = 350,

    /// Predefined font weight : Normal (400).
    DWRITE_FONT_WEIGHT_NORMAL = 400,

    /// Predefined font weight : Regular (400).
    DWRITE_FONT_WEIGHT_REGULAR = 400,

    /// Predefined font weight : Medium (500).
    DWRITE_FONT_WEIGHT_MEDIUM = 500,

    /// Predefined font weight : Demi-bold (600).
    DWRITE_FONT_WEIGHT_DEMI_BOLD = 600,

    /// Predefined font weight : Semi-bold (600).
    DWRITE_FONT_WEIGHT_SEMI_BOLD = 600,

    /// Predefined font weight : Bold (700).
    DWRITE_FONT_WEIGHT_BOLD = 700,

    /// Predefined font weight : Extra-bold (800).
    DWRITE_FONT_WEIGHT_EXTRA_BOLD = 800,

    /// Predefined font weight : Ultra-bold (800).
    DWRITE_FONT_WEIGHT_ULTRA_BOLD = 800,

    /// Predefined font weight : Black (900).
    DWRITE_FONT_WEIGHT_BLACK = 900,

    /// Predefined font weight : Heavy (900).
    DWRITE_FONT_WEIGHT_HEAVY = 900,

    /// Predefined font weight : Extra-black (950).
    DWRITE_FONT_WEIGHT_EXTRA_BLACK = 950,

    /// Predefined font weight : Ultra-black (950).
    DWRITE_FONT_WEIGHT_ULTRA_BLACK = 950
};

enum DWRITE_FONT_STRETCH
{
    /// <summary>
    /// Predefined font stretch : Not known (0).
    /// </summary>
    DWRITE_FONT_STRETCH_UNDEFINED = 0,

    /// <summary>
    /// Predefined font stretch : Ultra-condensed (1).
    /// </summary>
    DWRITE_FONT_STRETCH_ULTRA_CONDENSED = 1,

    /// <summary>
    /// Predefined font stretch : Extra-condensed (2).
    /// </summary>
    DWRITE_FONT_STRETCH_EXTRA_CONDENSED = 2,

    /// <summary>
    /// Predefined font stretch : Condensed (3).
    /// </summary>
    DWRITE_FONT_STRETCH_CONDENSED = 3,

    /// <summary>
    /// Predefined font stretch : Semi-condensed (4).
    /// </summary>
    DWRITE_FONT_STRETCH_SEMI_CONDENSED = 4,

    /// <summary>
    /// Predefined font stretch : Normal (5).
    /// </summary>
    DWRITE_FONT_STRETCH_NORMAL = 5,

    /// <summary>
    /// Predefined font stretch : Medium (5).
    /// </summary>
    DWRITE_FONT_STRETCH_MEDIUM = 5,

    /// <summary>
    /// Predefined font stretch : Semi-expanded (6).
    /// </summary>
    DWRITE_FONT_STRETCH_SEMI_EXPANDED = 6,

    /// <summary>
    /// Predefined font stretch : Expanded (7).
    /// </summary>
    DWRITE_FONT_STRETCH_EXPANDED = 7,

    /// <summary>
    /// Predefined font stretch : Extra-expanded (8).
    /// </summary>
    DWRITE_FONT_STRETCH_EXTRA_EXPANDED = 8,

    /// <summary>
    /// Predefined font stretch : Ultra-expanded (9).
    /// </summary>
    DWRITE_FONT_STRETCH_ULTRA_EXPANDED = 9
};

enum DWRITE_FONT_STYLE
{
    /// <summary>
    /// Font slope style : Normal.
    /// </summary>
    DWRITE_FONT_STYLE_NORMAL,

    /// <summary>
    /// Font slope style : Oblique.
    /// </summary>
    DWRITE_FONT_STYLE_OBLIQUE,

    /// <summary>
    /// Font slope style : Italic.
    /// </summary>
    DWRITE_FONT_STYLE_ITALIC

};

enum DWRITE_FONT_FACE_TYPE
{
    /// <summary>
    /// OpenType font face with CFF outlines.
    /// </summary>
    DWRITE_FONT_FACE_TYPE_CFF,

    /// <summary>
    /// OpenType font face with TrueType outlines.
    /// </summary>
    DWRITE_FONT_FACE_TYPE_TRUETYPE,

    /// <summary>
    /// OpenType font face that is a part of a TrueType or CFF collection.
    /// </summary>
    DWRITE_FONT_FACE_TYPE_OPENTYPE_COLLECTION,

    /// <summary>
    /// A Type 1 font face.
    /// </summary>
    DWRITE_FONT_FACE_TYPE_TYPE1,

    /// <summary>
    /// A vector .FON format font face.
    /// </summary>
    DWRITE_FONT_FACE_TYPE_VECTOR,

    /// <summary>
    /// A bitmap .FON format font face.
    /// </summary>
    DWRITE_FONT_FACE_TYPE_BITMAP,

    /// <summary>
    /// Font face type is not recognized by the DirectWrite font system.
    /// </summary>
    DWRITE_FONT_FACE_TYPE_UNKNOWN,

    /// <summary>
    /// The font data includes only the CFF table from an OpenType CFF font.
    /// This font face type can be used only for embedded fonts (i.e., custom
    /// font file loaders) and the resulting font face object supports only the
    /// minimum functionality necessary to render glyphs.
    /// </summary>
    DWRITE_FONT_FACE_TYPE_RAW_CFF,

    // The following name is obsolete, but kept as an alias to avoid breaking existing code.
    DWRITE_FONT_FACE_TYPE_TRUETYPE_COLLECTION = DWRITE_FONT_FACE_TYPE_OPENTYPE_COLLECTION,
};

interface DWRITE_DECLARE_INTERFACE("5f49804d-7024-4d43-bfa9-d25984f53849") IDWriteFontFace : public IUnknown
{
    /// <summary>
    /// Obtains the file format type of a font face.
    /// </summary>
    STDMETHOD_(DWRITE_FONT_FACE_TYPE, GetType)() PURE;

    /// <summary>
    /// Obtains the font files representing a font face.
    /// </summary>
    /// <param name="numberOfFiles">The number of files representing the font face.</param>
    /// <param name="fontFiles">User provided array that stores pointers to font files representing the font face.
    /// This parameter can be NULL if the user is only interested in the number of files representing the font face.
    /// This API increments reference count of the font file pointers returned according to COM conventions, and the client
    /// should release them when finished.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetFiles)(
        _Inout_ UINT32* numberOfFiles,
        _Out_writes_opt_(*numberOfFiles) IDWriteFontFile** fontFiles
        ) PURE;

    /// <summary>
    /// Obtains the zero-based index of the font face in its font file or files. If the font files contain a single face,
    /// the return value is zero.
    /// </summary>
    STDMETHOD_(UINT32, GetIndex)() PURE;

    /// <summary>
    /// Obtains the algorithmic style simulation flags of a font face.
    /// </summary>
    STDMETHOD_(DWRITE_FONT_SIMULATIONS, GetSimulations)() PURE;

    /// <summary>
    /// Determines whether the font is a symbol font.
    /// </summary>
    STDMETHOD_(BOOL, IsSymbolFont)() PURE;

    /// <summary>
    /// Obtains design units and common metrics for the font face.
    /// These metrics are applicable to all the glyphs within a fontface and are used by applications for layout calculations.
    /// </summary>
    /// <param name="fontFaceMetrics">Points to a DWRITE_FONT_METRICS structure to fill in.
    /// The metrics returned by this function are in font design units.</param>
    STDMETHOD_(void, GetMetrics)(
        _Out_ DWRITE_FONT_METRICS* fontFaceMetrics
        ) PURE;

    /// <summary>
    /// Obtains the number of glyphs in the font face.
    /// </summary>
    STDMETHOD_(UINT16, GetGlyphCount)() PURE;

    /// <summary>
    /// Obtains ideal glyph metrics in font design units. Design glyphs metrics are used for glyph positioning.
    /// </summary>
    /// <param name="glyphIndices">An array of glyph indices to compute the metrics for.</param>
    /// <param name="glyphCount">The number of elements in the glyphIndices array.</param>
    /// <param name="glyphMetrics">Array of DWRITE_GLYPH_METRICS structures filled by this function.
    /// The metrics returned by this function are in font design units.</param>
    /// <param name="isSideways">Indicates whether the font is being used in a sideways run.
    /// This can affect the glyph metrics if the font has oblique simulation
    /// because sideways oblique simulation differs from non-sideways oblique simulation.</param>
    /// <returns>
    /// Standard HRESULT error code. If any of the input glyph indices are outside of the valid glyph index range
    /// for the current font face, E_INVALIDARG will be returned.
    /// </returns>
    STDMETHOD(GetDesignGlyphMetrics)(
        _In_reads_(glyphCount) UINT16 const* glyphIndices,
        UINT32 glyphCount,
        _Out_writes_(glyphCount) DWRITE_GLYPH_METRICS* glyphMetrics,
        BOOL isSideways = FALSE
        ) PURE;

    /// <summary>
    /// Returns the nominal mapping of UTF-32 Unicode code points to glyph indices as defined by the font 'cmap' table.
    /// Note that this mapping is primarily provided for line layout engines built on top of the physical font API.
    /// Because of OpenType glyph substitution and line layout character substitution, the nominal conversion does not always correspond
    /// to how a Unicode string will map to glyph indices when rendering using a particular font face.
    /// Also, note that Unicode Variation Selectors provide for alternate mappings for character to glyph.
    /// This call will always return the default variant.
    /// </summary>
    /// <param name="codePoints">An array of UTF-32 code points to obtain nominal glyph indices from.</param>
    /// <param name="codePointCount">The number of elements in the codePoints array.</param>
    /// <param name="glyphIndices">Array of nominal glyph indices filled by this function.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetGlyphIndices)(
        _In_reads_(codePointCount) UINT32 const* codePoints,
        UINT32 codePointCount,
        _Out_writes_(codePointCount) UINT16* glyphIndices
        ) PURE;
 
    /// <summary>
    /// Finds the specified OpenType font table if it exists and returns a pointer to it.
    /// The function accesses the underlying font data via the IDWriteFontFileStream interface
    /// implemented by the font file loader.
    /// </summary>
    /// <param name="openTypeTableTag">Four character tag of table to find.
    ///     Use the DWRITE_MAKE_OPENTYPE_TAG() macro to create it.
    ///     Unlike GDI, it does not support the special TTCF and null tags to access the whole font.</param>
    /// <param name="tableData">
    ///     Pointer to base of table in memory.
    ///     The pointer is only valid so long as the FontFace used to get the font table still exists
    ///     (not any other FontFace, even if it actually refers to the same physical font).
    /// </param>
    /// <param name="tableSize">Byte size of table.</param>
    /// <param name="tableContext">
    ///     Opaque context which must be freed by calling ReleaseFontTable.
    ///     The context actually comes from the lower level IDWriteFontFileStream,
    ///     which may be implemented by the application or DWrite itself.
    ///     It is possible for a NULL tableContext to be returned, especially if
    ///     the implementation directly memory maps the whole file.
    ///     Nevertheless, always release it later, and do not use it as a test for function success.
    ///     The same table can be queried multiple times,
    ///     but each returned context can be different, so release each separately.
    /// </param>
    /// <param name="exists">True if table exists.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// If a table can not be found, the function will not return an error, but the size will be 0, table NULL, and exists = FALSE.
    /// The context does not need to be freed if the table was not found.
    /// </returns>
    /// <remarks>
    /// The context for the same tag may be different for each call,
    /// so each one must be held and released separately.
    /// </remarks>
    STDMETHOD(TryGetFontTable)(
        _In_ UINT32 openTypeTableTag,
        _Outptr_result_bytebuffer_(*tableSize) const void** tableData,
        _Out_ UINT32* tableSize,
        _Out_ void** tableContext,
        _Out_ BOOL* exists
        ) PURE;

    /// <summary>
    /// Releases the table obtained earlier from TryGetFontTable.
    /// </summary>
    /// <param name="tableContext">Opaque context from TryGetFontTable.</param>
    STDMETHOD_(void, ReleaseFontTable)(
        _In_ void* tableContext
        ) PURE;

    /// <summary>
    /// Computes the outline of a run of glyphs by calling back to the outline sink interface.
    /// </summary>
    /// <param name="emSize">Logical size of the font in DIP units. A DIP ("device-independent pixel") equals 1/96 inch.</param>
    /// <param name="glyphIndices">Array of glyph indices.</param>
    /// <param name="glyphAdvances">Optional array of glyph advances in DIPs.</param>
    /// <param name="glyphOffsets">Optional array of glyph offsets.</param>
    /// <param name="glyphCount">Number of glyphs.</param>
    /// <param name="isSideways">If true, specifies that glyphs are rotated 90 degrees to the left and vertical metrics are used.
    /// A client can render a vertical run by specifying isSideways = true and rotating the resulting geometry 90 degrees to the
    /// right using a transform.</param>
    /// <param name="isRightToLeft">If true, specifies that the advance direction is right to left. By default, the advance direction
    /// is left to right.</param>
    /// <param name="geometrySink">Interface the function calls back to draw each element of the geometry.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetGlyphRunOutline)(
        FLOAT emSize,
        _In_reads_(glyphCount) UINT16 const* glyphIndices,
        _In_reads_opt_(glyphCount) FLOAT const* glyphAdvances,
        _In_reads_opt_(glyphCount) DWRITE_GLYPH_OFFSET const* glyphOffsets,
        UINT32 glyphCount,
        BOOL isSideways,
        BOOL isRightToLeft,
        _In_ IDWriteGeometrySink* geometrySink
        ) PURE;

    /// <summary>
    /// Determines the recommended rendering mode for the font given the specified size and rendering parameters.
    /// </summary>
    /// <param name="emSize">Logical size of the font in DIP units. A DIP ("device-independent pixel") equals 1/96 inch.</param>
    /// <param name="pixelsPerDip">Number of physical pixels per DIP. For example, if the DPI of the rendering surface is 96 this 
    /// value is 1.0f. If the DPI is 120, this value is 120.0f/96.</param>
    /// <param name="measuringMode">Specifies measuring mode that will be used for glyphs in the font.
    /// Renderer implementations may choose different rendering modes for given measuring modes, but
    /// best results are seen when the corresponding modes match:
    /// DWRITE_RENDERING_MODE_CLEARTYPE_NATURAL for DWRITE_MEASURING_MODE_NATURAL
    /// DWRITE_RENDERING_MODE_CLEARTYPE_GDI_CLASSIC for DWRITE_MEASURING_MODE_GDI_CLASSIC
    /// DWRITE_RENDERING_MODE_CLEARTYPE_GDI_NATURAL for DWRITE_MEASURING_MODE_GDI_NATURAL
    /// </param>
    /// <param name="renderingParams">Rendering parameters object. This parameter is necessary in case the rendering parameters 
    /// object overrides the rendering mode.</param>
    /// <param name="renderingMode">Receives the recommended rendering mode to use.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetRecommendedRenderingMode)(
        FLOAT emSize,
        FLOAT pixelsPerDip,
        DWRITE_MEASURING_MODE measuringMode,
        IDWriteRenderingParams* renderingParams,
        _Out_ DWRITE_RENDERING_MODE* renderingMode
        ) PURE;

    /// <summary>
    /// Obtains design units and common metrics for the font face.
    /// These metrics are applicable to all the glyphs within a fontface and are used by applications for layout calculations.
    /// </summary>
    /// <param name="emSize">Logical size of the font in DIP units. A DIP ("device-independent pixel") equals 1/96 inch.</param>
    /// <param name="pixelsPerDip">Number of physical pixels per DIP. For example, if the DPI of the rendering surface is 96 this 
    /// value is 1.0f. If the DPI is 120, this value is 120.0f/96.</param>
    /// <param name="transform">Optional transform applied to the glyphs and their positions. This transform is applied after the
    /// scaling specified by the font size and pixelsPerDip.</param>
    /// <param name="fontFaceMetrics">Points to a DWRITE_FONT_METRICS structure to fill in.
    /// The metrics returned by this function are in font design units.</param>
    STDMETHOD(GetGdiCompatibleMetrics)(
        FLOAT emSize,
        FLOAT pixelsPerDip,
        _In_opt_ DWRITE_MATRIX const* transform,
        _Out_ DWRITE_FONT_METRICS* fontFaceMetrics
        ) PURE;

    /// <summary>
    /// Obtains glyph metrics in font design units with the return values compatible with what GDI would produce.
    /// Glyphs metrics are used for positioning of individual glyphs.
    /// </summary>
    /// <param name="emSize">Logical size of the font in DIP units. A DIP ("device-independent pixel") equals 1/96 inch.</param>
    /// <param name="pixelsPerDip">Number of physical pixels per DIP. For example, if the DPI of the rendering surface is 96 this 
    /// value is 1.0f. If the DPI is 120, this value is 120.0f/96.</param>
    /// <param name="transform">Optional transform applied to the glyphs and their positions. This transform is applied after the
    /// scaling specified by the font size and pixelsPerDip.</param>
    /// <param name="useGdiNatural">
    /// When set to FALSE, the metrics are the same as the metrics of GDI aliased text.
    /// When set to TRUE, the metrics are the same as the metrics of text measured by GDI using a font
    /// created with CLEARTYPE_NATURAL_QUALITY.
    /// </param>
    /// <param name="glyphIndices">An array of glyph indices to compute the metrics for.</param>
    /// <param name="glyphCount">The number of elements in the glyphIndices array.</param>
    /// <param name="glyphMetrics">Array of DWRITE_GLYPH_METRICS structures filled by this function.
    /// The metrics returned by this function are in font design units.</param>
    /// <param name="isSideways">Indicates whether the font is being used in a sideways run.
    /// This can affect the glyph metrics if the font has oblique simulation
    /// because sideways oblique simulation differs from non-sideways oblique simulation.</param>
    /// <returns>
    /// Standard HRESULT error code. If any of the input glyph indices are outside of the valid glyph index range
    /// for the current font face, E_INVALIDARG will be returned.
    /// </returns>
    STDMETHOD(GetGdiCompatibleGlyphMetrics)(
        FLOAT emSize,
        FLOAT pixelsPerDip,
        _In_opt_ DWRITE_MATRIX const* transform,
        BOOL useGdiNatural,
        _In_reads_(glyphCount) UINT16 const* glyphIndices,
        UINT32 glyphCount,
        _Out_writes_(glyphCount) DWRITE_GLYPH_METRICS* glyphMetrics,
        BOOL isSideways = FALSE
        ) PURE;
};


interface DWRITE_DECLARE_INTERFACE("1a0d8438-1d97-4ec1-aef9-a2fb86ed6acb") IDWriteFontList : public IUnknown
{
    /// <summary>
    /// Gets the font collection that contains the fonts.
    /// </summary>
    /// <param name="fontCollection">Receives a pointer to the font collection object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetFontCollection)(
        _COM_Outptr_ IDWriteFontCollection** fontCollection
        ) PURE;

    /// <summary>
    /// Gets the number of fonts in the font list.
    /// </summary>
    STDMETHOD_(UINT32, GetFontCount)() PURE;

    /// <summary>
    /// Gets a font given its zero-based index.
    /// </summary>
    /// <param name="index">Zero-based index of the font in the font list.</param>
    /// <param name="font">Receives a pointer to the newly created font object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetFont)(
        UINT32 index, 
        _COM_Outptr_ IDWriteFont** font
        ) PURE;
};

interface DWRITE_DECLARE_INTERFACE("da20d8ef-812a-4c43-9802-62ec4abd7add") IDWriteFontFamily : public IDWriteFontList
{
    /// <summary>
    /// Creates a localized strings object that contains the family names for the font family, indexed by locale name.
    /// </summary>
    /// <param name="names">Receives a pointer to the newly created localized strings object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetFamilyNames)(
        _COM_Outptr_ IDWriteLocalizedStrings** names
        ) PURE;

    /// <summary>
    /// Gets the font that best matches the specified properties.
    /// </summary>
    /// <param name="weight">Requested font weight.</param>
    /// <param name="stretch">Requested font stretch.</param>
    /// <param name="style">Requested font style.</param>
    /// <param name="matchingFont">Receives a pointer to the newly created font object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetFirstMatchingFont)(
        DWRITE_FONT_WEIGHT  weight,
        DWRITE_FONT_STRETCH stretch,
        DWRITE_FONT_STYLE   style,
        _COM_Outptr_ IDWriteFont** matchingFont
        ) PURE;

    /// <summary>
    /// Gets a list of fonts in the font family ranked in order of how well they match the specified properties.
    /// </summary>
    /// <param name="weight">Requested font weight.</param>
    /// <param name="stretch">Requested font stretch.</param>
    /// <param name="style">Requested font style.</param>
    /// <param name="matchingFonts">Receives a pointer to the newly created font list object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetMatchingFonts)(
        DWRITE_FONT_WEIGHT      weight,
        DWRITE_FONT_STRETCH     stretch,
        DWRITE_FONT_STYLE       style,
        _COM_Outptr_ IDWriteFontList** matchingFonts
        ) PURE;
};


interface DWRITE_DECLARE_INTERFACE("a71efdb4-9fdb-4838-ad90-cfc3be8c3daf") IDWriteFontFace1 : public IDWriteFontFace
{
    /// <summary>
    /// Gets common metrics for the font in design units.
    /// These metrics are applicable to all the glyphs within a font,
    /// and are used by applications for layout calculations.
    /// </summary>
    /// <param name="fontMetrics">Metrics structure to fill in.</param>
    STDMETHOD_(void, GetMetrics)(
        _Out_ DWRITE_FONT_METRICS1* fontMetrics
        ) PURE;

    /// <summary>
    /// Gets common metrics for the font in design units.
    /// These metrics are applicable to all the glyphs within a font,
    /// and are used by applications for layout calculations.
    /// </summary>
    /// <param name="emSize">Logical size of the font in DIP units. A DIP
    ///     ("device-independent pixel") equals 1/96 inch.</param>
    /// <param name="pixelsPerDip">Number of physical pixels per DIP. For
    ///     example, if the DPI of the rendering surface is 96 this value is
    ///     1.0f. If the DPI is 120, this value is 120.0f/96.</param>
    /// <param name="transform">Optional transform applied to the glyphs and
    ///     their positions. This transform is applied after the scaling
    ///     specified by the font size and pixelsPerDip.</param>
    /// <param name="fontMetrics">Font metrics structure to fill in.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetGdiCompatibleMetrics)(
        FLOAT emSize,
        FLOAT pixelsPerDip,
        _In_opt_ DWRITE_MATRIX const* transform,
        _Out_ DWRITE_FONT_METRICS1* fontMetrics
        ) PURE;

    /// <summary>
    /// Gets caret metrics for the font in design units. These are used by
    /// text editors for drawing the correct caret placement/slant.
    /// </summary>
    /// <param name="caretMetrics">Metrics structure to fill in.</param>
    STDMETHOD_(void, GetCaretMetrics)(
        _Out_ DWRITE_CARET_METRICS* caretMetrics
        ) PURE;

    /// <summary>
    /// Returns the list of character ranges supported by the font, which is
    /// useful for scenarios like character picking, glyph display, and
    /// efficient font selection lookup. This is similar to GDI's
    /// GetFontUnicodeRanges, except that it returns the full Unicode range,
    /// not just 16-bit UCS-2.
    /// </summary>
    /// <param name="maxRangeCount">Maximum number of character ranges passed
    ///     in from the client.</param>
    /// <param name="unicodeRanges">Array of character ranges.</param>
    /// <param name="actualRangeCount">Actual number of character ranges,
    ///     regardless of the maximum count.</param>
    /// <remarks>
    /// These ranges are from the cmap, not the OS/2::ulCodePageRange1.
    /// </remarks>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetUnicodeRanges)(
        UINT32 maxRangeCount,
        _Out_writes_to_opt_(maxRangeCount, *actualRangeCount) DWRITE_UNICODE_RANGE* unicodeRanges,
        _Out_ UINT32* actualRangeCount
        ) PURE;

    /// <summary>
    /// Returns true if the font is monospaced, meaning its characters are the
    /// same fixed-pitch width (non-proportional).
    /// </summary>
    STDMETHOD_(BOOL, IsMonospacedFont)() PURE;

    /// <summary>
    /// Returns the advances in design units for a sequences of glyphs.
    /// </summary>
    /// <param name="glyphCount">Number of glyphs to retrieve advances for.</param>
    /// <param name="glyphIndices">Array of glyph id's to retrieve advances for.</param>
    /// <param name="glyphAdvances">Returned advances in font design units for
    ///     each glyph.</param>
    /// <param name="isSideways">Retrieve the glyph's vertical advance height
    ///     rather than horizontal advance widths.</param>
    /// <remarks>
    /// This is equivalent to calling GetGlyphMetrics and using only the
    /// advance width/height.
    /// </remarks>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetDesignGlyphAdvances)(
        UINT32 glyphCount,
        _In_reads_(glyphCount) UINT16 const* glyphIndices,
        _Out_writes_(glyphCount) INT32* glyphAdvances,
        BOOL isSideways = FALSE
        ) PURE;

    /// <summary>
    /// Returns the pixel-aligned advances for a sequences of glyphs, the same
    /// as GetGdiCompatibleGlyphMetrics would return.
    /// </summary>
    /// <param name="emSize">Logical size of the font in DIP units. A DIP
    ///     ("device-independent pixel") equals 1/96 inch.</param>
    /// <param name="pixelsPerDip">Number of physical pixels per DIP. For
    ///     example, if the DPI of the rendering surface is 96 this value is
    ///     1.0f. If the DPI is 120, this value is 120.0f/96.</param>
    /// <param name="transform">Optional transform applied to the glyphs and
    ///     their positions. This transform is applied after the scaling
    ///     specified by the font size and pixelsPerDip.</param>
    /// <param name="useGdiNatural">When FALSE, the metrics are the same as
    ///     GDI aliased text (DWRITE_MEASURING_MODE_GDI_CLASSIC). When TRUE,
    ///     the metrics are the same as those measured by GDI using a font
    ///     using CLEARTYPE_NATURAL_QUALITY (DWRITE_MEASURING_MODE_GDI_NATURAL).</param>
    /// <param name="isSideways">Retrieve the glyph's vertical advances rather
    ///     than horizontal advances.</param>
    /// <param name="glyphCount">Total glyphs to retrieve adjustments for.</param>
    /// <param name="glyphIndices">Array of glyph id's to retrieve advances.</param>
    /// <param name="glyphAdvances">Returned advances in font design units for
    ///     each glyph.</param>
    /// <remarks>
    /// This is equivalent to calling GetGdiCompatibleGlyphMetrics and using only
    /// the advance width/height. Like GetGdiCompatibleGlyphMetrics, these are in
    /// design units, meaning they must be scaled down by
    /// DWRITE_FONT_METRICS::designUnitsPerEm.
    /// </remarks>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetGdiCompatibleGlyphAdvances)(
        FLOAT emSize,
        FLOAT pixelsPerDip,
        _In_opt_ DWRITE_MATRIX const* transform,
        BOOL useGdiNatural,
        BOOL isSideways,
        UINT32 glyphCount,
        _In_reads_(glyphCount) UINT16 const* glyphIndices,
        _Out_writes_(glyphCount) INT32* glyphAdvances
        ) PURE;

    /// <summary>
    /// Retrieves the kerning pair adjustments from the font's kern table.
    /// </summary>
    /// <param name="glyphCount">Number of glyphs to retrieve adjustments for.</param>
    /// <param name="glyphIndices">Array of glyph id's to retrieve adjustments
    ///     for.</param>
    /// <param name="glyphAdvanceAdjustments">Returned advances in font design units for
    ///     each glyph. The last glyph adjustment is zero.</param>
    /// <remarks>
    /// This is not a direct replacement for GDI's character based
    /// GetKerningPairs, but it serves the same role, without the client
    /// needing to cache them locally. It also uses glyph id's directly
    /// rather than UCS-2 characters (how the kern table actually stores
    /// them) which avoids glyph collapse and ambiguity, such as the dash
    /// and hyphen, or space and non-breaking space.
    /// </remarks>
    /// <remarks>
    /// Newer fonts may have only GPOS kerning instead of the legacy pair
    /// table kerning. Such fonts, like Gabriola, will only return 0's for
    /// adjustments. This function does not virtualize and flatten these
    /// GPOS entries into kerning pairs.
    /// </remarks>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetKerningPairAdjustments)(
        UINT32 glyphCount,
        _In_reads_(glyphCount) UINT16 const* glyphIndices,
        _Out_writes_(glyphCount) INT32* glyphAdvanceAdjustments
        ) PURE;

    /// <summary>
    /// Returns whether or not the font supports pair-kerning.
    /// </summary>
    /// <remarks>
    /// If the font does not support pair table kerning, there is no need to
    /// call GetKerningPairAdjustments (it would be all zeroes).
    /// </remarks>
    /// <returns>
    /// Whether the font supports kerning pairs.
    /// </returns>
    STDMETHOD_(BOOL, HasKerningPairs)() PURE;

    /// <summary>
    /// Determines the recommended text rendering mode to be used based on the
    /// font, size, world transform, and measuring mode.
    /// </summary>
    /// <param name="fontEmSize">Logical font size in DIPs.</param>
    /// <param name="dpiX">Number of pixels per logical inch in the horizontal direction.</param>
    /// <param name="dpiY">Number of pixels per logical inch in the vertical direction.</param>
    /// <param name="transform">Specifies the world transform.</param>
    /// <param name="outlineThreshold">Specifies the quality of the graphics system's outline rendering,
    /// affects the size threshold above which outline rendering is used.</param>
    /// <param name="measuringMode">Specifies the method used to measure during text layout. For proper
    /// glyph spacing, the function returns a rendering mode that is compatible with the specified 
    /// measuring mode.</param>
    /// <param name="renderingMode">Receives the recommended rendering mode.</param>
    /// <remarks>
    /// This method should be used to determine the actual rendering mode in cases where the rendering 
    /// mode of the rendering params object is DWRITE_RENDERING_MODE_DEFAULT.
    /// </remarks>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetRecommendedRenderingMode)(
        FLOAT fontEmSize,
        FLOAT dpiX,
        FLOAT dpiY,
        _In_opt_ DWRITE_MATRIX const* transform,
        BOOL isSideways,
        DWRITE_OUTLINE_THRESHOLD outlineThreshold,
        DWRITE_MEASURING_MODE measuringMode,
        _Out_ DWRITE_RENDERING_MODE* renderingMode
        ) PURE;

    /// <summary>
    /// Retrieves the vertical forms of the nominal glyphs retrieved from
    /// GetGlyphIndices, using the font's 'vert' table. This is used in
    /// CJK vertical layout so the correct characters are shown.
    /// </summary>
    /// <param name="glyphCount">Number of glyphs to retrieve.</param>
    /// <param name="nominalGlyphIndices">Original glyph indices from cmap.</param>
    /// <param name="verticalGlyphIndices">The vertical form of glyph indices.</param>
    /// <remarks>
    /// Call GetGlyphIndices to get the nominal glyph indices, followed by
    /// calling this to remap the to the substituted forms, when the run
    /// is sideways, and the font has vertical glyph variants.
    /// </remarks>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetVerticalGlyphVariants)(
        UINT32 glyphCount,
        _In_reads_(glyphCount) UINT16 const* nominalGlyphIndices,
        _Out_writes_(glyphCount) UINT16* verticalGlyphIndices
        ) PURE;

    /// <summary>
    /// Returns whether or not the font has any vertical glyph variants.
    /// </summary>
    /// <remarks>
    /// For OpenType fonts, this will return true if the font contains a 'vert'
    /// feature.
    /// </remarks>
    /// <returns>
    /// True if the font contains vertical glyph variants.
    /// </returns>
    STDMETHOD_(BOOL, HasVerticalGlyphVariants)() PURE;

    using IDWriteFontFace::GetMetrics;
    using IDWriteFontFace::GetGdiCompatibleMetrics;
    using IDWriteFontFace::GetRecommendedRenderingMode;
};


/// <summary>
/// The IDWriteFont interface represents a physical font in a font collection.
/// </summary>
interface DWRITE_DECLARE_INTERFACE("acd16696-8c14-4f5d-877e-fe3fc1d32737") IDWriteFont : public IUnknown
{
    /// <summary>
    /// Gets the font family to which the specified font belongs.
    /// </summary>
    /// <param name="fontFamily">Receives a pointer to the font family object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetFontFamily)(
        _COM_Outptr_ IDWriteFontFamily** fontFamily
        ) PURE;

    /// <summary>
    /// Gets the weight of the specified font.
    /// </summary>
    STDMETHOD_(DWRITE_FONT_WEIGHT, GetWeight)() PURE;

    /// <summary>
    /// Gets the stretch (aka. width) of the specified font.
    /// </summary>
    STDMETHOD_(DWRITE_FONT_STRETCH, GetStretch)() PURE;

    /// <summary>
    /// Gets the style (aka. slope) of the specified font.
    /// </summary>
    STDMETHOD_(DWRITE_FONT_STYLE, GetStyle)() PURE;

    /// <summary>
    /// Returns TRUE if the font is a symbol font or FALSE if not.
    /// </summary>
    STDMETHOD_(BOOL, IsSymbolFont)() PURE;

    /// <summary>
    /// Gets a localized strings collection containing the face names for the font (e.g., Regular or Bold), indexed by locale name.
    /// </summary>
    /// <param name="names">Receives a pointer to the newly created localized strings object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetFaceNames)(
        _COM_Outptr_ IDWriteLocalizedStrings** names
        ) PURE;

    /// <summary>
    /// Gets a localized strings collection containing the specified informational strings, indexed by locale name.
    /// </summary>
    /// <param name="informationalStringID">Identifies the string to get.</param>
    /// <param name="informationalStrings">Receives a pointer to the newly created localized strings object.</param>
    /// <param name="exists">Receives the value TRUE if the font contains the specified string ID or FALSE if not.</param>
    /// <returns>
    /// Standard HRESULT error code. If the font does not contain the specified string, the return value is S_OK but 
    /// informationalStrings receives a NULL pointer and exists receives the value FALSE.
    /// </returns>
    STDMETHOD(GetInformationalStrings)(
        DWRITE_INFORMATIONAL_STRING_ID informationalStringID,
        _COM_Outptr_result_maybenull_ IDWriteLocalizedStrings** informationalStrings,
        _Out_ BOOL* exists
        ) PURE;

    /// <summary>
    /// Gets a value that indicates what simulation are applied to the specified font.
    /// </summary>
    STDMETHOD_(DWRITE_FONT_SIMULATIONS, GetSimulations)() PURE;

    /// <summary>
    /// Gets the metrics for the font.
    /// </summary>
    /// <param name="fontMetrics">Receives the font metrics.</param>
    STDMETHOD_(void, GetMetrics)(
        _Out_ DWRITE_FONT_METRICS* fontMetrics
        ) PURE;

    /// <summary>
    /// Determines whether the font supports the specified character.
    /// </summary>
    /// <param name="unicodeValue">Unicode (UCS-4) character value.</param>
    /// <param name="exists">Receives the value TRUE if the font supports the specified character or FALSE if not.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(HasCharacter)(
        UINT32 unicodeValue,
        _Out_ BOOL* exists
        ) PURE;

    /// <summary>
    /// Creates a font face object for the font.
    /// </summary>
    /// <param name="fontFace">Receives a pointer to the newly created font face object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateFontFace)(
        _COM_Outptr_ IDWriteFontFace** fontFace
        ) PURE;
};


interface DWRITE_DECLARE_INTERFACE("a84cee02-3eea-4eee-a827-87c1a02a0fcc") IDWriteFontCollection : public IUnknown
{
    /// <summary>
    /// Gets the number of font families in the collection.
    /// </summary>
    STDMETHOD_(UINT32, GetFontFamilyCount)() PURE;

    /// <summary>
    /// Creates a font family object given a zero-based font family index.
    /// </summary>
    /// <param name="index">Zero-based index of the font family.</param>
    /// <param name="fontFamily">Receives a pointer the newly created font family object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetFontFamily)(
        UINT32 index,
        _COM_Outptr_ IDWriteFontFamily** fontFamily
        ) PURE;

    /// <summary>
    /// Finds the font family with the specified family name.
    /// </summary>
    /// <param name="familyName">Name of the font family. The name is not case-sensitive but must otherwise exactly match a family name in the collection.</param>
    /// <param name="index">Receives the zero-based index of the matching font family if the family name was found or UINT_MAX otherwise.</param>
    /// <param name="exists">Receives TRUE if the family name exists or FALSE otherwise.</param>
    /// <returns>
    /// Standard HRESULT error code. If the specified family name does not exist, the return value is S_OK, but *index is UINT_MAX and *exists is FALSE.
    /// </returns>
    STDMETHOD(FindFamilyName)(
        _In_z_ WCHAR const* familyName,
        _Out_ UINT32* index,
        _Out_ BOOL* exists
        ) PURE;

    /// <summary>
    /// Gets the font object that corresponds to the same physical font as the specified font face object. The specified physical font must belong 
    /// to the font collection.
    /// </summary>
    /// <param name="fontFace">Font face object that specifies the physical font.</param>
    /// <param name="font">Receives a pointer to the newly created font object if successful or NULL otherwise.</param>
    /// <returns>
    /// Standard HRESULT error code. If the specified physical font is not part of the font collection the return value is DWRITE_E_NOFONT.
    /// </returns>
    STDMETHOD(GetFontFromFontFace)(
        _In_ IDWriteFontFace* fontFace,
        _COM_Outptr_ IDWriteFont** font
        ) PURE;
};

interface DWRITE_DECLARE_INTERFACE("7d97dbf7-e085-42d4-81e3-6a883bded118") IDWriteGlyphRunAnalysis : public IUnknown
{
    /// <summary>
    /// Gets the bounding rectangle of the physical pixels affected by the glyph run.
    /// </summary>
    /// <param name="textureType">Specifies the type of texture requested. If a bi-level texture is requested, the
    /// bounding rectangle includes only bi-level glyphs. Otherwise, the bounding rectangle includes only anti-aliased
    /// glyphs.</param>
    /// <param name="textureBounds">Receives the bounding rectangle, or an empty rectangle if there are no glyphs
    /// if the specified type.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetAlphaTextureBounds)(
        DWRITE_TEXTURE_TYPE textureType,
        _Out_ RECT* textureBounds
        ) PURE;

    /// <summary>
    /// Creates an alpha texture of the specified type.
    /// </summary>
    /// <param name="textureType">Specifies the type of texture requested. If a bi-level texture is requested, the
    /// texture contains only bi-level glyphs. Otherwise, the texture contains only anti-aliased glyphs.</param>
    /// <param name="textureBounds">Specifies the bounding rectangle of the texture, which can be different than
    /// the bounding rectangle returned by GetAlphaTextureBounds.</param>
    /// <param name="alphaValues">Receives the array of alpha values.</param>
    /// <param name="bufferSize">Size of the alphaValues array. The minimum size depends on the dimensions of the
    /// rectangle and the type of texture requested.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateAlphaTexture)(
        DWRITE_TEXTURE_TYPE textureType,
        _In_ RECT const* textureBounds,
        _Out_writes_bytes_(bufferSize) BYTE* alphaValues,
        UINT32 bufferSize
        ) PURE;

    /// <summary>
    /// Gets properties required for ClearType blending.
    /// </summary>
    /// <param name="renderingParams">Rendering parameters object. In most cases, the values returned in the output
    /// parameters are based on the properties of this object. The exception is if a GDI-compatible rendering mode
    /// is specified.</param>
    /// <param name="blendGamma">Receives the gamma value to use for gamma correction.</param>
    /// <param name="blendEnhancedContrast">Receives the enhanced contrast value.</param>
    /// <param name="blendClearTypeLevel">Receives the ClearType level.</param>
    STDMETHOD(GetAlphaBlendParams)(
        _In_ IDWriteRenderingParams* renderingParams,
        _Out_ FLOAT* blendGamma,
        _Out_ FLOAT* blendEnhancedContrast,
        _Out_ FLOAT* blendClearTypeLevel
        ) PURE;
};
interface DWRITE_DECLARE_INTERFACE("1edd9491-9853-4299-898f-6432983b6f3a") IDWriteGdiInterop : public IUnknown
{
    /// <summary>
    /// Creates a font object that matches the properties specified by the LOGFONT structure
    /// in the system font collection (GetSystemFontCollection).
    /// </summary>
    /// <param name="logFont">Structure containing a GDI-compatible font description.</param>
    /// <param name="font">Receives a newly created font object if successful, or NULL in case of error.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateFontFromLOGFONT)(
        _In_ LOGFONTW const* logFont,
        _COM_Outptr_ IDWriteFont** font
        ) PURE;

    /// <summary>
    /// Initializes a LOGFONT structure based on the GDI-compatible properties of the specified font.
    /// </summary>
    /// <param name="font">Specifies a font.</param>
    /// <param name="logFont">Structure that receives a GDI-compatible font description.</param>
    /// <param name="isSystemFont">Contains TRUE if the specified font object is part of the system font collection
    /// or FALSE otherwise.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(ConvertFontToLOGFONT)(
        _In_ IDWriteFont* font,
        _Out_ LOGFONTW* logFont,
        _Out_ BOOL* isSystemFont
        ) PURE;

    /// <summary>
    /// Initializes a LOGFONT structure based on the GDI-compatible properties of the specified font.
    /// </summary>
    /// <param name="font">Specifies a font face.</param>
    /// <param name="logFont">Structure that receives a GDI-compatible font description.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(ConvertFontFaceToLOGFONT)(
        _In_ IDWriteFontFace* font,
        _Out_ LOGFONTW* logFont
        ) PURE;

    /// <summary>
    /// Creates a font face object that corresponds to the currently selected HFONT.
    /// </summary>
    /// <param name="hdc">Handle to a device context into which a font has been selected. It is assumed that the client
    /// has already performed font mapping and that the font selected into the DC is the actual font that would be used 
    /// for rendering glyphs.</param>
    /// <param name="fontFace">Contains the newly created font face object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateFontFaceFromHdc)(
        HDC hdc,
        _COM_Outptr_ IDWriteFontFace** fontFace
        ) PURE;

    /// <summary>
    /// Creates an object that encapsulates a bitmap and memory DC which can be used for rendering glyphs.
    /// </summary>
    /// <param name="hdc">Optional device context used to create a compatible memory DC.</param>
    /// <param name="width">Width of the bitmap.</param>
    /// <param name="height">Height of the bitmap.</param>
    /// <param name="renderTarget">Receives a pointer to the newly created render target.</param>
    STDMETHOD(CreateBitmapRenderTarget)(
        _In_opt_ HDC hdc,
        UINT32 width,
        UINT32 height,
        _COM_Outptr_ IDWriteBitmapRenderTarget** renderTarget
        ) PURE;
};

interface DWRITE_DECLARE_INTERFACE("b859ee5a-d838-4b5b-a2e8-1adc7d93db48") IDWriteFactory : public IUnknown
{
    /// <summary>
    /// Gets a font collection representing the set of installed fonts.
    /// </summary>
    /// <param name="fontCollection">Receives a pointer to the system font collection object, or NULL in case of failure.</param>
    /// <param name="checkForUpdates">If this parameter is nonzero, the function performs an immediate check for changes to the set of
    /// installed fonts. If this parameter is FALSE, the function will still detect changes if the font cache service is running, but
    /// there may be some latency. For example, an application might specify TRUE if it has itself just installed a font and wants to 
    /// be sure the font collection contains that font.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetSystemFontCollection)(
        _COM_Outptr_ IDWriteFontCollection** fontCollection,
        BOOL checkForUpdates = FALSE
        ) PURE;

    /// <summary>
    /// Creates a font collection using a custom font collection loader.
    /// </summary>
    /// <param name="collectionLoader">Application-defined font collection loader, which must have been previously
    /// registered using RegisterFontCollectionLoader.</param>
    /// <param name="collectionKey">Key used by the loader to identify a collection of font files.</param>
    /// <param name="collectionKeySize">Size in bytes of the collection key.</param>
    /// <param name="fontCollection">Receives a pointer to the system font collection object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateCustomFontCollection)(
        _In_ IDWriteFontCollectionLoader* collectionLoader,
        _In_reads_bytes_(collectionKeySize) void const* collectionKey,
        UINT32 collectionKeySize,
        _COM_Outptr_ IDWriteFontCollection** fontCollection
        ) PURE;

    /// <summary>
    /// Registers a custom font collection loader with the factory object.
    /// </summary>
    /// <param name="fontCollectionLoader">Application-defined font collection loader.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(RegisterFontCollectionLoader)(
        _In_ IDWriteFontCollectionLoader* fontCollectionLoader
        ) PURE;

    /// <summary>
    /// Unregisters a custom font collection loader that was previously registered using RegisterFontCollectionLoader.
    /// </summary>
    /// <param name="fontCollectionLoader">Application-defined font collection loader.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(UnregisterFontCollectionLoader)(
        _In_ IDWriteFontCollectionLoader* fontCollectionLoader
        ) PURE;

    /// <summary>
    /// CreateFontFileReference creates a font file reference object from a local font file.
    /// </summary>
    /// <param name="filePath">Absolute file path. Subsequent operations on the constructed object may fail
    /// if the user provided filePath doesn't correspond to a valid file on the disk.</param>
    /// <param name="lastWriteTime">Last modified time of the input file path. If the parameter is omitted,
    /// the function will access the font file to obtain its last write time, so the clients are encouraged to specify this value
    /// to avoid extra disk access. Subsequent operations on the constructed object may fail
    /// if the user provided lastWriteTime doesn't match the file on the disk.</param>
    /// <param name="fontFile">Contains newly created font file reference object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateFontFileReference)(
        _In_z_ WCHAR const* filePath,
        _In_opt_ FILETIME const* lastWriteTime,
        _COM_Outptr_ IDWriteFontFile** fontFile
        ) PURE;

    /// <summary>
    /// CreateCustomFontFileReference creates a reference to an application specific font file resource.
    /// This function enables an application or a document to use a font without having to install it on the system.
    /// The fontFileReferenceKey has to be unique only in the scope of the fontFileLoader used in this call.
    /// </summary>
    /// <param name="fontFileReferenceKey">Font file reference key that uniquely identifies the font file resource
    /// during the lifetime of fontFileLoader.</param>
    /// <param name="fontFileReferenceKeySize">Size of font file reference key in bytes.</param>
    /// <param name="fontFileLoader">Font file loader that will be used by the font system to load data from the file identified by
    /// fontFileReferenceKey.</param>
    /// <param name="fontFile">Contains the newly created font file object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    /// <remarks>
    /// This function is provided for cases when an application or a document needs to use a font
    /// without having to install it on the system. fontFileReferenceKey has to be unique only in the scope
    /// of the fontFileLoader used in this call.
    /// </remarks>
    STDMETHOD(CreateCustomFontFileReference)(
        _In_reads_bytes_(fontFileReferenceKeySize) void const* fontFileReferenceKey,
        UINT32 fontFileReferenceKeySize,
        _In_ IDWriteFontFileLoader* fontFileLoader,
        _COM_Outptr_ IDWriteFontFile** fontFile
        ) PURE;

    /// <summary>
    /// Creates a font face object.
    /// </summary>
    /// <param name="fontFaceType">The file format of the font face.</param>
    /// <param name="numberOfFiles">The number of font files required to represent the font face.</param>
    /// <param name="fontFiles">Font files representing the font face. Since IDWriteFontFace maintains its own references
    /// to the input font file objects, it's OK to release them after this call.</param>
    /// <param name="faceIndex">The zero based index of a font face in cases when the font files contain a collection of font faces.
    /// If the font files contain a single face, this value should be zero.</param>
    /// <param name="fontFaceSimulationFlags">Font face simulation flags for algorithmic emboldening and italicization.</param>
    /// <param name="fontFace">Contains the newly created font face object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateFontFace)(
        DWRITE_FONT_FACE_TYPE fontFaceType,
        UINT32 numberOfFiles,
        _In_reads_(numberOfFiles) IDWriteFontFile* const* fontFiles,
        UINT32 faceIndex,
        DWRITE_FONT_SIMULATIONS fontFaceSimulationFlags,
        _COM_Outptr_ IDWriteFontFace** fontFace
        ) PURE;

    /// <summary>
    /// Creates a rendering parameters object with default settings for the primary monitor.
    /// </summary>
    /// <param name="renderingParams">Holds the newly created rendering parameters object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateRenderingParams)(
        _COM_Outptr_ IDWriteRenderingParams** renderingParams
        ) PURE;

    /// <summary>
    /// Creates a rendering parameters object with default settings for the specified monitor.
    /// </summary>
    /// <param name="monitor">The monitor to read the default values from.</param>
    /// <param name="renderingParams">Holds the newly created rendering parameters object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateMonitorRenderingParams)(
        HMONITOR monitor,
        _COM_Outptr_ IDWriteRenderingParams** renderingParams
        ) PURE;

    /// <summary>
    /// Creates a rendering parameters object with the specified properties.
    /// </summary>
    /// <param name="gamma">The gamma value used for gamma correction, which must be greater than zero and cannot exceed 256.</param>
    /// <param name="enhancedContrast">The amount of contrast enhancement, zero or greater.</param>
    /// <param name="clearTypeLevel">The degree of ClearType level, from 0.0f (no ClearType) to 1.0f (full ClearType).</param>
    /// <param name="pixelGeometry">The geometry of a device pixel.</param>
    /// <param name="renderingMode">Method of rendering glyphs. In most cases, this should be DWRITE_RENDERING_MODE_DEFAULT to automatically use an appropriate mode.</param>
    /// <param name="renderingParams">Holds the newly created rendering parameters object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateCustomRenderingParams)(
        FLOAT gamma,
        FLOAT enhancedContrast,
        FLOAT clearTypeLevel,
        DWRITE_PIXEL_GEOMETRY pixelGeometry,
        DWRITE_RENDERING_MODE renderingMode,
        _COM_Outptr_ IDWriteRenderingParams** renderingParams
        ) PURE;

    /// <summary>
    /// Registers a font file loader with DirectWrite.
    /// </summary>
    /// <param name="fontFileLoader">Pointer to the implementation of the IDWriteFontFileLoader for a particular file resource type.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    /// <remarks>
    /// This function registers a font file loader with DirectWrite.
    /// Font file loader interface handles loading font file resources of a particular type from a key.
    /// The font file loader interface is recommended to be implemented by a singleton object.
    /// A given instance can only be registered once.
    /// Succeeding attempts will return an error that it has already been registered.
    /// IMPORTANT: font file loader implementations must not register themselves with DirectWrite
    /// inside their constructors and must not unregister themselves in their destructors, because
    /// registration and unregistration operations increment and decrement the object reference count respectively.
    /// Instead, registration and unregistration of font file loaders with DirectWrite should be performed
    /// outside of the font file loader implementation as a separate step.
    /// </remarks>
    STDMETHOD(RegisterFontFileLoader)(
        _In_ IDWriteFontFileLoader* fontFileLoader
        ) PURE;

    /// <summary>
    /// Unregisters a font file loader that was previously registered with the DirectWrite font system using RegisterFontFileLoader.
    /// </summary>
    /// <param name="fontFileLoader">Pointer to the file loader that was previously registered with the DirectWrite font system using RegisterFontFileLoader.</param>
    /// <returns>
    /// This function will succeed if the user loader is requested to be removed.
    /// It will fail if the pointer to the file loader identifies a standard DirectWrite loader,
    /// or a loader that is never registered or has already been unregistered.
    /// </returns>
    /// <remarks>
    /// This function unregisters font file loader callbacks with the DirectWrite font system.
    /// The font file loader interface is recommended to be implemented by a singleton object.
    /// IMPORTANT: font file loader implementations must not register themselves with DirectWrite
    /// inside their constructors and must not unregister themselves in their destructors, because
    /// registration and unregistration operations increment and decrement the object reference count respectively.
    /// Instead, registration and unregistration of font file loaders with DirectWrite should be performed
    /// outside of the font file loader implementation as a separate step.
    /// </remarks>
    STDMETHOD(UnregisterFontFileLoader)(
        _In_ IDWriteFontFileLoader* fontFileLoader
        ) PURE;

    /// <summary>
    /// Create a text format object used for text layout.
    /// </summary>
    /// <param name="fontFamilyName">Name of the font family</param>
    /// <param name="fontCollection">Font collection. NULL indicates the system font collection.</param>
    /// <param name="fontWeight">Font weight</param>
    /// <param name="fontStyle">Font style</param>
    /// <param name="fontStretch">Font stretch</param>
    /// <param name="fontSize">Logical size of the font in DIP units. A DIP ("device-independent pixel") equals 1/96 inch.</param>
    /// <param name="localeName">Locale name</param>
    /// <param name="textFormat">Contains newly created text format object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    /// <remarks>
    /// If fontCollection is nullptr, the system font collection is used, grouped by typographic family name
    /// (DWRITE_FONT_FAMILY_MODEL_WEIGHT_STRETCH_STYLE) without downloadable fonts.
    /// </remarks>
    STDMETHOD(CreateTextFormat)(
        _In_z_ WCHAR const* fontFamilyName,
        _In_opt_ IDWriteFontCollection* fontCollection,
        DWRITE_FONT_WEIGHT fontWeight,
        DWRITE_FONT_STYLE fontStyle,
        DWRITE_FONT_STRETCH fontStretch,
        FLOAT fontSize,
        _In_z_ WCHAR const* localeName,
        _COM_Outptr_ IDWriteTextFormat** textFormat
        ) PURE;

    /// <summary>
    /// Create a typography object used in conjunction with text format for text layout.
    /// </summary>
    /// <param name="typography">Contains newly created typography object, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateTypography)(
        _COM_Outptr_ IDWriteTypography** typography
        ) PURE;

    /// <summary>
    /// Create an object used for interoperability with GDI.
    /// </summary>
    /// <param name="gdiInterop">Receives the GDI interop object if successful, or NULL in case of failure.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(GetGdiInterop)(
        _COM_Outptr_ IDWriteGdiInterop** gdiInterop
        ) PURE;

    /// <summary>
    /// CreateTextLayout takes a string, format, and associated constraints
    /// and produces an object representing the fully analyzed
    /// and formatted result.
    /// </summary>
    /// <param name="string">The string to layout.</param>
    /// <param name="stringLength">The length of the string.</param>
    /// <param name="textFormat">The format to apply to the string.</param>
    /// <param name="maxWidth">Width of the layout box.</param>
    /// <param name="maxHeight">Height of the layout box.</param>
    /// <param name="textLayout">The resultant object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateTextLayout)(
        _In_reads_(stringLength) WCHAR const* string,
        UINT32 stringLength,
        _In_ IDWriteTextFormat* textFormat,
        FLOAT maxWidth,
        FLOAT maxHeight,
        _COM_Outptr_ IDWriteTextLayout** textLayout
        ) PURE;

    /// <summary>
    /// CreateGdiCompatibleTextLayout takes a string, format, and associated constraints
    /// and produces and object representing the result formatted for a particular display resolution
    /// and measuring mode. The resulting text layout should only be used for the intended resolution,
    /// and for cases where text scalability is desired, CreateTextLayout should be used instead.
    /// </summary>
    /// <param name="string">The string to layout.</param>
    /// <param name="stringLength">The length of the string.</param>
    /// <param name="textFormat">The format to apply to the string.</param>
    /// <param name="layoutWidth">Width of the layout box.</param>
    /// <param name="layoutHeight">Height of the layout box.</param>
    /// <param name="pixelsPerDip">Number of physical pixels per DIP. For example, if rendering onto a 96 DPI device then pixelsPerDip
    /// is 1. If rendering onto a 120 DPI device then pixelsPerDip is 120/96.</param>
    /// <param name="transform">Optional transform applied to the glyphs and their positions. This transform is applied after the
    /// scaling specified the font size and pixelsPerDip.</param>
    /// <param name="useGdiNatural">
    /// When set to FALSE, instructs the text layout to use the same metrics as GDI aliased text.
    /// When set to TRUE, instructs the text layout to use the same metrics as text measured by GDI using a font
    /// created with CLEARTYPE_NATURAL_QUALITY.
    /// </param>
    /// <param name="textLayout">The resultant object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateGdiCompatibleTextLayout)(
        _In_reads_(stringLength) WCHAR const* string,
        UINT32 stringLength,
        _In_ IDWriteTextFormat* textFormat,
        FLOAT layoutWidth,
        FLOAT layoutHeight,
        FLOAT pixelsPerDip,
        _In_opt_ DWRITE_MATRIX const* transform,
        BOOL useGdiNatural,
        _COM_Outptr_ IDWriteTextLayout** textLayout
        ) PURE;

    /// <summary>
    /// The application may call this function to create an inline object for trimming, using an ellipsis as the omission sign.
    /// The ellipsis will be created using the current settings of the format, including base font, style, and any effects.
    /// Alternate omission signs can be created by the application by implementing IDWriteInlineObject.
    /// </summary>
    /// <param name="textFormat">Text format used as a template for the omission sign.</param>
    /// <param name="trimmingSign">Created omission sign.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateEllipsisTrimmingSign)(
        _In_ IDWriteTextFormat* textFormat,
        _COM_Outptr_ IDWriteInlineObject** trimmingSign
        ) PURE;

    /// <summary>
    /// Return an interface to perform text analysis with.
    /// </summary>
    /// <param name="textAnalyzer">The resultant object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateTextAnalyzer)(
        _COM_Outptr_ IDWriteTextAnalyzer** textAnalyzer
        ) PURE;

    /// <summary>
    /// Creates a number substitution object using a locale name,
    /// substitution method, and whether to ignore user overrides (uses NLS
    /// defaults for the given culture instead).
    /// </summary>
    /// <param name="substitutionMethod">Method of number substitution to use.</param>
    /// <param name="localeName">Which locale to obtain the digits from.</param>
    /// <param name="ignoreUserOverride">Ignore the user's settings and use the locale defaults</param>
    /// <param name="numberSubstitution">Receives a pointer to the newly created object.</param>
    STDMETHOD(CreateNumberSubstitution)(
        _In_ DWRITE_NUMBER_SUBSTITUTION_METHOD substitutionMethod,
        _In_z_ WCHAR const* localeName,
        _In_ BOOL ignoreUserOverride,
        _COM_Outptr_ IDWriteNumberSubstitution** numberSubstitution
        ) PURE;

    /// <summary>
    /// Creates a glyph run analysis object, which encapsulates information
    /// used to render a glyph run.
    /// </summary>
    /// <param name="glyphRun">Structure specifying the properties of the glyph run.</param>
    /// <param name="pixelsPerDip">Number of physical pixels per DIP. For example, if rendering onto a 96 DPI bitmap then pixelsPerDip
    /// is 1. If rendering onto a 120 DPI bitmap then pixelsPerDip is 120/96.</param>
    /// <param name="transform">Optional transform applied to the glyphs and their positions. This transform is applied after the
    /// scaling specified by the emSize and pixelsPerDip.</param>
    /// <param name="renderingMode">Specifies the rendering mode, which must be one of the raster rendering modes (i.e., not default
    /// and not outline).</param>
    /// <param name="measuringMode">Specifies the method to measure glyphs.</param>
    /// <param name="baselineOriginX">Horizontal position of the baseline origin, in DIPs.</param>
    /// <param name="baselineOriginY">Vertical position of the baseline origin, in DIPs.</param>
    /// <param name="glyphRunAnalysis">Receives a pointer to the newly created object.</param>
    /// <returns>
    /// Standard HRESULT error code.
    /// </returns>
    STDMETHOD(CreateGlyphRunAnalysis)(
        _In_ DWRITE_GLYPH_RUN const* glyphRun,
        FLOAT pixelsPerDip,
        _In_opt_ DWRITE_MATRIX const* transform,
        DWRITE_RENDERING_MODE renderingMode,
        DWRITE_MEASURING_MODE measuringMode,
        FLOAT baselineOriginX,
        FLOAT baselineOriginY,
        _COM_Outptr_ IDWriteGlyphRunAnalysis** glyphRunAnalysis
        ) PURE;

}; // interface IDWriteFactory

extern "C" HRESULT DWRITE_EXPORT DWriteCreateFactory(
    _In_ DWRITE_FACTORY_TYPE factoryType,
    _In_ REFIID iid,
    _COM_Outptr_ IUnknown **factory
    );
