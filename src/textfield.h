#define FIELD_LEFT_MARGIN 10
#define FIELD_TOP_MARGIN 0
#define SCROLL_TWEEN_TIME 0.1

#if INTERNAL
struct DebugDrawCount
{
    Cursor char_pos;
    u32 count;
};
#endif

struct TextField
{
    // TODO right now this is no good we don't want each text field
    //      to have its own arena!
    MemoryArena arena;

    CompositionLayer *layer;
    Framebuffer *framebuffer;
    u32 z_index;
    b32 is_active;
    Recti bbox;
    
    b32 allow_multiline;

    u32 line;
    Cursor cursor; // alternatively called caret

    b32 mark_activated;
    u32 mark_line;
    Cursor mark;

    b32 full_redraw_needed = true;
    
    RenderText *render_text;
    TextUndoStorage *undo_storage;
    
    b32 showing_vertical_scrollbar;
    b32 showing_horizontal_scrollbar;
    Scrollbar vertical_scrollbar;
    Scrollbar horizontal_scrollbar;

    i32 top_pixel;
    f32 top_pixel_t;
    i32 top_pixel_from;
    i32 top_pixel_dest;

    i32 left_pixel;
    f32 left_pixel_t;
    i32 left_pixel_from;
    i32 left_pixel_dest;

    u32 one_past_saved_cursor_x;

    LineEndingType line_endings;

    Color back_color;
    
    // TODO remove the CursorRange return value from delete_text_hook and insert_text_hook
    //      unless we need a minimal performance gain?
    void *delete_text_hook_params;
    CursorRange (*delete_text_hook) (Cursor cursor, u32 line, u32 num_chars, u32 lines_changed, void *params);

    // TODO can we combine these two hooks?
    void *insert_text_hook_params;
    CursorRange (*insert_text_hook) (Cursor cursor, u32 num_chars, u32 num_lines, void *params);

#if INTERNAL
    b32 redraw_occurred;
    b32 show_redraw_visual;
    b32 redraw_visual_line;    
    
#define MAX_DEBUG_DRAW_COUNT 50000
    u32 num_draw_count;
    DebugDrawCount draw_count[MAX_DEBUG_DRAW_COUNT];
    
    u32 last_num_draw_count;
    DebugDrawCount last_draw_count[MAX_DEBUG_DRAW_COUNT];
#endif
};

struct DeleteCharacterInfo
{
    u32 num_chars;
    UTF8 *delete_text;
    u32 lines_changed;
};

struct SelectionRange
{
    Cursor min_cursor;
    Cursor max_cursor;

    u32 min_line;
    u32 max_line;
};

internal void RedrawTextField(TextField* field);