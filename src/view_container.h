struct ViewLink
{
    LFView *view;
    ViewLink *next;
    ViewLink *prev;
};

enum PaneType
{
    Pane_single,
    Pane_double
};

struct Pane
{
    Recti bbox;
    Recti content_bbox; // the pane_bbox - border thickness

    Pane *parent;
    PaneType type;

    union
    {
        struct
        {   // single view
            // singly linked list

            MemoryArena arena;
            ViewLink *free_view_links;
            
            ViewLink sentinel_view_link;
            ViewLink *active_view_link;
        };

        // TODO potentially wasted space since the splitter Pane takes up less useful space
        struct
        {   // double view
            b32 split_horizontally;
            f32 split_percentage;

            Pane *first;
            Pane *second;
        };
        
    };
};
