#define SCROLLBAR_THICKNESS 16
#define SCROLL_BAR_MIN_LENGTH 24

#define CLR_SCROLLBAR_THUMB CLR_GREY50
#define CLR_SCROLLBAR_CONTAINER CLR_GREY20

#define SCROLLBAR_REPEAT_DELAY_SECONDS 0.4
#define SCROLLBAR_REPEAT_HERTZ 30

enum ScrollbarOrientation
{
    ScrollbarOrientation_horizontal,
    ScrollbarOrientation_vertical,
};

enum ScrollbarOperationMode
{
    ScrollbarOperationMode_none,
    ScrollbarOperationMode_dragging_thumb,
    ScrollbarOperationMode_paging_to_cursor,
};

struct Scrollbar
{
    ScrollbarOrientation orientation;
    Recti container;
    
    i32 thumb_pos;
    i32 thumb_length;

    ScrollbarOperationMode operation_mode;
    union {
        i32 drag_offset;
    };

    f64 seconds_since_dragged;
    u32 repeats_commited;
    u32 total_repeats_needed;
};
