internal i32
Round(f32 value)
{
    int ret;
    ret = (value > 0) ? (int)(value + 0.5f) : (int)(value - 0.5f);
    return ret;
}

internal u32
Round_u32(f32 value)
{
    int ret;
    ret = (int) (value + 0.5f);
    return ret;
}

internal f32
Absolute(f32 value)
{
    // TODO no branching
    return (value > 0 ? value : -value);
}

internal i32
Absolute(i32 value)
{
    // TODO no branching
    return (value > 0 ? value : -value);
}

internal f32
GetDecimalPart(f32 value)
{
    f32 result;
    // TODO might be bugged because f32 value might be able to able to be bigger than range of i32...
    result = value - (i32)value;
    return result;
}

#include <math.h>
// TODO this sucks and doesn't cover all the cases
internal u32
RoundUp(f32 value)
{
    u32 rounded_down = (u32)value;
    u32 result = rounded_down + 1;
    return result;
}

// TODO make a suckless version of floor and ceiling
internal f32
Floor(f32 value)
{
    f32 result = floorf(value);
    return result;
}

internal f32
Ceiling(f32 value)
{
    f32 result = ceilf(value);
    return result;
}

internal f32
SquareRoot(f32 value)
{
    f32 result = sqrtf(value);
    return result;
}

internal f32
Lerp(f32 from, f32 to, f32 t)
{
    return from + (to-from)*t;
}

internal i32
Lerp(i32 from, i32 to, f32 t)
{
    return from + Round((f32)(to-from)*t);
}
internal f32
Length(Vec2 operand)
{
    f32 result = SquareRoot(operand.x*operand.x + operand.y*operand.y);
    return result;
}

internal f32
Length(Vec2 first, Vec2 second)
{
    f32 result;

    Vec2 difference_vector = second - first;
    result = Length(difference_vector);
    
    return result;
}

