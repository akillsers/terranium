inline Color
rgb(f32 x, f32 y, f32 z)
{
    Color ret;
    ret.x = x;
    ret.y = y;
    ret.z = z;
    return ret;
}

inline Color
rgb_u32(u32 r, u32 g, u32 b)
{
    Color ret;
    ret.r = r / 255.0f;
    ret.g = g / 255.0f;
    ret.b = b / 255.0f;
    return ret;
}

// TODO this ClipRectangle function may contain bugs!!
inline Recti
ClipRectangle(Recti rect, Recti container)
{
    Recti result = rect;

    if(rect.x >= container.x + container.width ||
       rect.y >= container.y + container.height ||
       rect.x + rect.width < container.x ||
       rect.y + rect.height < container.y ||
       rect.width < 0 ||
       rect.height < 0)
    {
        result.width = 0;
        result.height = 0;
    }
    else
    {
        if(rect.x < container.x)
        {
            result.width -= container.x - result.x;
            result.x = container.x;
        }
        if(rect.y < container.y)
        {
            result.height -= container.y - result.y;
            result.y = container.y;
        }

        if(rect.x + rect.width > container.x + container.width)
        {
            result.width = container.x + container.width - result.x;
        }
        if(rect.y + rect.height > container.y + container.height)
        {
            result.height = container.y + container.height - result.y;
        }
    }

    
    return result;
}

inline Recti
ClipToFramebuffer(Recti rect, Framebuffer *fb)
{
    Recti result = rect;

    if (rect.x > (i32)fb->width || rect.y > (i32)fb->height)
    {
        result.width = 0;
        result.height = 0;
    }
    if (rect.x < 0)
    {
        result.width += result.x;
        result.x = 0;
    }
    if (rect.y < 0)
    {
        result.height += result.y;
        result.y = 0;
    }
    if (rect.width <= 0 || rect.height <= 0)
    {
        result.width = 0;
        result.height = 0;
    }

    if (rect.width + rect.x > fb->width)
    {
        result.width = fb->width - result.x;
    }

    if (rect.height + rect.y > fb->height)
    {
        result.height = fb->height - result.y;
    }

    return result;
}

inline u32
GetPackedU32Color(Color color, f32 alpha)
{
    u32 blue = (u32)Round(color.b * 255);
    u32 green = (u32)Round(color.g * 255);
    u32 red = (u32)Round(color.r * 255);
    u32 alpha_u32 = (u32)Round(alpha * 255);

    u32 result = 0;
    result |= blue << 0;
    result |= green << 8;
    result |= red << 16;
    result |= alpha_u32 << 24;

    return result;
}

struct ScanlineWorkData
{
    u32 color_combined;
    __m128i color_laned;
    u32 *pixels;
    u32 width;
    u32 row_id;
};

internal void
DrawScanline(void *param)
{
    ScanlineWorkData *data = (ScanlineWorkData *)param;

    u32 *pixel = data->pixels;
    
    int j = 0;

    // use sse if 16-byte aligned
    if(!((uintptr_t)pixel & 15))
    {
        for(; j < (i32)data->width - 4; j += 4)
        {
            _mm_stream_si128((__m128i *) pixel, data->color_laned);
            pixel += 4;
        }
    }

    for(; j < data->width; j++)
    {
        *pixel++ = data->color_combined;
    }
}

inline Color
LerpColor(Color first, Color second, f32 t)
{
    Color result = {};
    result.r = first.r * (1 - t) + second.r * t;
    result.g = first.r * (1 - t) + second.g * t;
    result.b = first.r * (1 - t) + second.b * t;

    return result;
}

inline Color
GetPixelColor(u32 pixel)
{
    Color result = {};
    result.r = (pixel >> 16) & 0xFF;
    result.g = (pixel >> 8) & 0xFF;
    result.b = pixel & 0xFF;
    return result;

}

internal void
DrawDot_(i32 x, i32 y, Vec3 color, Framebuffer *framebuffer)
{
    // TODO it might be inefficient to do this check everytime!
    u32 *pixel = (u32 *)((u8 *)framebuffer->pixels +
                         x*framebuffer->bytespp +
                         y*framebuffer->stride);

    u32 packed_u32_color = GetPackedU32Color(color, 1.0f);
    *pixel = packed_u32_color;
}

internal void
DrawCircle(Vec2 center, f32 radius, Color color, Framebuffer *framebuffer)
{
    u32 left = Floor(center.x - radius);
    u32 right = Ceiling(center.x + radius);
    u32 min_row = Floor(center.y - radius);
    u32 max_row = Ceiling(center.y + radius);

    u8 *row = ((u8 *)framebuffer->pixels +
               left * framebuffer->bytespp+
               min_row * framebuffer->stride);

    for(int i = min_row;
        i <= max_row;
        i++)
    {

        u32 *pixel = (u32 *)row;

        int j = left;

        for(int j = left;
            j <= right;
            j++)
        {
            Vec2 pixel_pos = vec2(j, i);
            f32 distance_to_radius = Length(pixel_pos, center);
            f32 distance_to_circumference = distance_to_radius - radius;

            
            Color original_color = GetPixelColor(pixel[0]);
            f32 t = Clamp(distance_to_circumference, 0.0f, 1.0f);
            // Color draw_color = LerpColor(color, original_color, t);

            u32 packed_color = GetPackedU32Color(original_color, 1.0f);
            *pixel = packed_color; 

            pixel++;
        }

        row += framebuffer->stride;
    }
}

internal void
DrawLine(i32 x0, i32 y0, i32 x1, i32 y1, Vec3 color, Framebuffer *framebuffer)
{
    // TODO RESEARCH I DON'T even know what Brasenham's Line Drawing algorithm is!!!
    b32 steep = false;

    if(Absolute(x1 - x0) < Absolute(y1 - y0))
    {
        // TODO make a swap function or macro
        i32 tempx0 = x0;
        x0 = y0;
        y0 = tempx0;

        i32 tempx1 = x1;
        x1 = y1;
        y1 = tempx1;
        steep = true;
    }

    if(x0 > x1)
    {
        i32 tempx = x0;
        x0 = x1;
        x1 = tempx;

        i32 tempy = y0;
        y0 = y1;
        y1 = tempy;
    }

    i32 dx = x1 - x0;
    i32 dy = y1 - y0;

    f32 derror = Absolute((f32)dy/(f32)dx);
    f32 error = 0;
    i32 y = y0;

    for(i32 x = x0;
        x <= x1;
        x++)
    {
        if(steep)
        {
            //set y x to be the color
            DrawDot_(y, x, color, framebuffer);
        }
        else
        {
            //set x y to be the color
            DrawDot_(x, y, color, framebuffer);
        }

        error += derror;
        if(error > 0.5)
        {
            y += (y1>y0 ? 1: -1);
            error -= 1.0f;
        }
    }

    Recti rect = {};
    rect.x = x0;
    rect.width = dx;
    rect.y = y0;
    rect.height = dy;
    AddDirtyRect(framebuffer, rect);
}


internal void
DrawRectangle(Recti rect, Color color, Framebuffer *framebuffer, Recti clip_rect = {})
{
    TIMED_BLOCK;
    Assert(framebuffer->bytespp);

    Recti clipped = {};
    
    if(clip_rect.width != 0 &&
       clip_rect.height != 0)
    {
        clipped = ClipRectangle(rect, clip_rect);
    }
    else
    {
        clipped = ClipToFramebuffer(rect, framebuffer);
    }

    if(clipped.width && clipped.height)
    {
        u8 *row = (u8 *)framebuffer->pixels +
            clipped.x * framebuffer->bytespp +
            clipped.y * framebuffer->stride;

        u32 color_combined = GetPackedU32Color(color, 1.0f);
        __m128i color_laned = _mm_set1_epi32(color_combined);

        if(clipped.width < 4)
        {
            for(int i = 0; i < clipped.height; i++)
            {
                u32 *pixel = (u32 *)row;
                for(int j = 0; j < clipped.width; j++)
                {
                    *pixel++ = color_combined;
                }

                row += framebuffer->stride;
            }
        }
        else
        {
            // NOTE: This assumes that framebuffer->stride is a multiple of 16 bytes
            u32 *aligned_row = (u32 *)((((umm)row) + 15) & ~15);
            u32 pixel_offset = aligned_row - (u32 *)row;

            u32 remaining = clipped.width - pixel_offset;
            u32 num_128_byte_fills = remaining >> 5;
            remaining &= 31;
            u32 num_32_byte_fills = remaining >> 3;
            remaining &= 7;
            u32 num_16_byte_fills = remaining >> 2;
            remaining &= 3;
            
            for(int i = 0; i < clipped.height; i++)
            {
                u32 *pixel = (u32 *)row;

                u32 num_128_byte_fills_left = num_128_byte_fills;
                u32 num_32_byte_fills_left = num_32_byte_fills;
                u32 num_16_byte_fills_left = num_16_byte_fills;
                u32 num_leftover_pixels_left = remaining;

                // Do a possibly unaligned (movaps) 16-byte (4 pixels)
                // fill at the start
                _mm_storeu_ps((float *)pixel, _mm_castsi128_ps(color_laned));
                
                pixel += pixel_offset;

                // Assert(((umm)pixel & 15) == 0);

                while(num_128_byte_fills_left)
                {
                    // Copy 128 bytes (32 pixels) at a time
                    _mm_stream_si128((__m128i *) pixel, color_laned);
                    _mm_stream_si128(((__m128i *) pixel) + 1, color_laned);
                    _mm_stream_si128(((__m128i *) pixel) + 2, color_laned);
                    _mm_stream_si128(((__m128i *) pixel) + 3, color_laned);
                    _mm_stream_si128(((__m128i *) pixel) + 4, color_laned);
                    _mm_stream_si128(((__m128i *) pixel) + 5, color_laned);
                    _mm_stream_si128(((__m128i *) pixel) + 6, color_laned);
                    _mm_stream_si128(((__m128i *) pixel) + 7, color_laned);

                    pixel += 32;

                    num_128_byte_fills_left--;
                }

                while(num_32_byte_fills_left)
                {
                    // Copy 32 bytes (8 pixels) at a time
                    _mm_stream_si128((__m128i *) pixel, color_laned);
                    _mm_stream_si128(((__m128i *) pixel) + 1, color_laned);

                    pixel += 8;

                    num_32_byte_fills_left--;
                }

                
                while(num_16_byte_fills_left)
                {
                    // Copy 16 bytes (4 pixels) at a time
                    _mm_stream_si128((__m128i *) pixel, color_laned);

                    pixel += 4;

                    num_16_byte_fills_left--;
                }

                while(num_leftover_pixels_left)
                {
                    *pixel++ = color_combined;

                    num_leftover_pixels_left--;
                }

                row += framebuffer->stride;
            }
        }
        AddDirtyRect(framebuffer, clipped);
    }

}

inline void
DrawRectangle(i32 x, i32 y, i32 width, i32 height, Vec3 color, Framebuffer *framebuffer)
{
    Recti rect = {x, y, width, height};
    DrawRectangle(rect, color, framebuffer);
}

inline void
DrawRectOutline(Recti rect, i32 thickness, Color color, Framebuffer *fb)
{
    if(rect.width > 0 &&
       rect.height > 0)
    {
        DrawRectangle(rect.x, rect.y, thickness, rect.height, color, fb);
        DrawRectangle(rect.x, rect.y, rect.width, thickness, color, fb);
        DrawRectangle(rect.x + rect.width - thickness, rect.y, thickness, rect.height, color, fb);
        DrawRectangle(rect.x, rect.y + rect.height - thickness, rect.width, thickness, color, fb);
    }
}

inline u32
GlyphHash(u16 glyph_id, Vec2i subpixel_position, GlyphTextureType texture_type)
{
    // TODO: Better hash function
    u32 hash_value = 23 * glyph_id + 3 * texture_type + 7 * (u32)subpixel_position.x + 19 * (u32)subpixel_position.y;
    hash_value = 0;
    return hash_value;
}

internal RenderedGlyph *
_AddGlyphToCache(GlyphCache *cache, u16 glyph_id, Vec2i subpixel_position, GlyphTextureType texture_type)
{
    RenderedGlyph *result = 0;

    if(cache == 0)
    {
        result = PushType(&platform.frame_arena, RenderedGlyph);
    }
    else
    {
        if(cache->free->next != cache->free)
        {
            RenderedGlyphLink *result_link = cache->free->next;
            result_link->next->prev = result_link->prev;
            result_link->prev->next = result_link->next;
            result_link->prev = 0;
            result_link->next = 0;

            result = (RenderedGlyph *)result_link;
        }
        else
        {
            Assert(cache->lru->next != cache->lru);

            result = (RenderedGlyph *)((u8 *)cache->lru->next - offsetof(RenderedGlyph, lru_link));

            result->is_valid = false;
            result->acquire_count = 0;

            RenderedGlyphLink *result_link = cache->lru->next;
            DL_REMOVE(result_link);
        }

        DL_INSERT_BEFORE(cache->lru, &result->lru_link);

        u32 hash_value = GlyphHash(glyph_id, subpixel_position, texture_type);
        u32 hash_slot = hash_value % ArrayCount(cache->table);

        RenderedGlyphLink *hash_slot_sentinel = cache->table[hash_slot];
        DL_INSERT_AFTER(hash_slot_sentinel, &result->link);

        result->glyph_id = glyph_id;
        result->subpixel_position = subpixel_position;
        result->texture_type = texture_type;
        result->is_valid = false;
    }

    return result;
}

struct GetGlyphResult
{
    RenderedGlyph *glyph;
    u32 acquire_order;
};

internal GetGlyphResult
GetGlyphFromCache(GlyphCache *cache, u16 glyph_id, Vec2i subpixel_position, GlyphTextureType texture_type)
{
    GetGlyphResult result = {};

    if(cache == 0) { return result; }

    AcquireTicketLock(&cache->ticket_lock);

    u32 hash_value = GlyphHash(glyph_id, subpixel_position, texture_type);
    u32 hash_slot = hash_value % ArrayCount(cache->table);

    for(RenderedGlyphLink *candidate = cache->table[hash_slot]->next;
        candidate != cache->table[hash_slot];
        candidate = candidate->next)
    {
        RenderedGlyph *glyph= (RenderedGlyph *)candidate;
        if(glyph->glyph_id == glyph_id &&
           glyph->subpixel_position == subpixel_position &&
           glyph->texture_type == texture_type)
        {
            result.glyph = glyph;
            break;
        }

    }

    if(result.glyph == 0)
    {
        result.glyph = _AddGlyphToCache(cache, glyph_id, subpixel_position, texture_type);
    
        Assert(result.glyph->acquire_count == 0);
    }

    result.acquire_order = result.glyph->acquire_count++;

    ReleaseTicketLock(&cache->ticket_lock);

    return result;
}

internal void
DrawGlyphDirty(Font *font, u16 glyph_id, GlyphOffset position, Vec3 color, f32 alpha, GlyphTextureType texture_type, Framebuffer *framebuffer, MemoryArena *temp_arena, Recti clip_rect = {})
{
    TIMED_BLOCK;
    Vec2i pixel_position = IntegerPartOnly(position);
    Vec2i subpixel_position = FractionalPartOnly(position);
    
    if(font->cache == 0)
    {
        // Allocate and initialize the font cache
        font->cache = PushType(&font->arena, GlyphCache);
        GlyphCache *cache = font->cache;

        u32 cache_size = Megabytes(10);

        Vec2i max_glyph_dimensions = font_backend.GetMaxGlyphDimensions(font);
        cache->max_elem_width = max_glyph_dimensions.x;
        cache->max_elem_height = max_glyph_dimensions.y;

        u32 elem_size = cache->max_elem_width * cache->max_elem_height * 4;

        u32 num_elems = cache_size / elem_size;
        
        cache->free = PushType(&font->arena, RenderedGlyphLink);
        cache->lru = PushType(&font->arena, RenderedGlyphLink);

        DL_INIT_SENTINEL(cache->free);
        DL_INIT_SENTINEL(cache->lru);

        u32 data_offset = 0;
        cache->base = PushCount(&font->arena, u8, elem_size * num_elems);

        RenderedGlyph *rendered_glyphs = PushCount(&font->arena, RenderedGlyph, num_elems);
        RenderedGlyphLink *prev = cache->free;
        for(u32 i = 1; i < num_elems; ++i)
        {
            RenderedGlyph *at = &rendered_glyphs[i];
            
            at->pixels= cache->base + data_offset;
            data_offset += elem_size;

            DL_INSERT_AFTER(prev, (RenderedGlyphLink *)at);
            prev = (RenderedGlyphLink *)at;
        }

        RenderedGlyph *last = &rendered_glyphs[num_elems - 1];
        cache->free->prev = (RenderedGlyphLink *)last;

        // TODO: We might not want all these sentinels?
        for(u32 i = 0; i < num_elems; ++i)
        {
            cache->table[i] = PushType(&font->arena, RenderedGlyphLink);
            DL_INIT_SENTINEL(cache->table[i]);
        }
    }

    RenderedGlyph *glyph = 0;

    GetGlyphResult get_glyph_result = GetGlyphFromCache(font->cache, glyph_id, subpixel_position, texture_type);
    glyph = get_glyph_result.glyph;

    if(!glyph->is_valid)
    {
        if(get_glyph_result.acquire_order != 0)
        {
            while(glyph->is_valid == false);
        }
        GlyphRenderInfo render_info = font_backend.CreateGlyphRenderInfo(font, glyph_id, subpixel_position, texture_type, temp_arena);
        // glyph = AddGlyphToCache(font->cache, glyph_id, subpixel_position, texture_type);
        
        // TODO: Memory fence?

        glyph->is_valid = true;

        Assert(glyph);

        glyph->xoff = render_info.xoff;
        glyph->yoff = render_info.yoff;
        glyph->bboxw = render_info.bboxw;
        glyph->bboxh = render_info.bboxh;

        font_backend.RenderGlyphByID(&render_info, glyph->pixels);
    }
    
    Recti dest_rect = {};
    dest_rect.x = pixel_position.x + glyph->xoff;
    dest_rect.y = pixel_position.y + glyph->yoff;
    dest_rect.width = glyph->bboxw;
    dest_rect.height = glyph->bboxh;

    if(clip_rect.width == 0 && clip_rect.height == 0)
    {
        clip_rect = recti(0, 0, framebuffer->width, framebuffer->height);
    }

    Recti clipped_dest_rect = {};
    clipped_dest_rect = ClipRectangle(dest_rect, clip_rect);
    // DrawRectangle(dest_rect, color, framebuffer);
#if 1
    u8 *row = (u8 *)framebuffer->pixels +
        clipped_dest_rect.x * framebuffer->bytespp +
        clipped_dest_rect.y * framebuffer->stride;
    
    u32 glyph_pixels_stride = glyph->bboxw * 4;
    
    u32 src_offset_x = clipped_dest_rect.x - dest_rect.x;
    u32 src_offset_y = clipped_dest_rect.y - dest_rect.y;

    u8 *src_row = glyph->pixels +
        src_offset_x * 4 +
        src_offset_y * glyph_pixels_stride;
    
    for(int i = 0; i < clipped_dest_rect.height; i++)
    {
        TIMED_BLOCK;
        u8 *src_alpha = src_row;
        u8 *dest_pixel = row;

        for(int j = 0; j < clipped_dest_rect.width; j++)
        {
           // TODO: optimize
            dest_pixel[0] = (255 - src_alpha[0]) * alpha / 255 * dest_pixel[0] + color.b * src_alpha[0] * alpha; // blue
            dest_pixel[1] = (255 - src_alpha[1]) * alpha / 255 * dest_pixel[1] + color.g * src_alpha[1] * alpha; // green
            dest_pixel[2] = (255 - src_alpha[2]) * alpha / 255 * dest_pixel[2] + color.r * src_alpha[2] * alpha; // red


            // dest_pixel[3] = dest_pixel[3]; // leave alpha the same

            src_alpha += 4;
            dest_pixel += 4;
        }

        row += framebuffer->stride;
        src_row += glyph_pixels_stride;
    }
#endif
    AddDirtyRect_NoAsserts(framebuffer, dest_rect);
}

// TODO: remove
#if 0
// #pragma optimize("git", on)
// #pragma optimize("", on)
internal void
DrawGlyph(Glyph *glyph, Vec2i dest_pos, Recti src_rect, Vec3 color, f32 alpha,
          GlyphTextureType texture_type, Framebuffer *framebuffer)
{
    Recti dest_rect = {};
    dest_rect.x = dest_pos.x;
    dest_rect.y = dest_pos.y;
    dest_rect.width = src_rect.width;
    dest_rect.height = src_rect.height;
    
    Assert(RectInRect(dest_rect, recti(0, 0, framebuffer->width, framebuffer->height)));
    
    u8 *row = (u8 *)framebuffer->pixels +
        dest_rect.x*framebuffer->bytespp +
        dest_rect.y*framebuffer->stride;

    u32 glyph_pixels_stride = glyph->bboxw * 4;

    u8 *src = 0;
    if(texture_type == GlyphTextureType_subpixel)
    {
        src = glyph->subpixel_pixels;
    }
    else if(texture_type == GlyphTextureType_grayscale)
    {
        src = glyph->grayscale_pixels;
    }
    else
    {
        InvalidCodePath;
    }
    
    u8 *src_row = src +
        src_rect.x * 4 +
        src_rect.y * glyph_pixels_stride;

    for(int i = 0; i < dest_rect.height; i++)
    {
        u8 *src_alpha = src_row;
        u8 *dest_pixel = row;

        for(int j = 0; j < dest_rect.width; j++)
        {
			/* Efficient but broken
            dest_pixel[0] = ((255 - src_alpha[0]*alpha) * dest_pixel[0] + color.b * src_alpha[0]*alpha) / 255; // blue
            dest_pixel[1] = ((255 - src_alpha[1]*alpha) * dest_pixel[1] + color.g * src_alpha[1]*alpha) / 255; // green
            dest_pixel[2] = ((255 - src_alpha[2]*alpha) * dest_pixel[2] + color.r * src_alpha[2]*alpha) / 255; // red
            */
				
			// TODO: optimize
			dest_pixel[0] = (255 - src_alpha[0])*alpha/255 * dest_pixel[0] + color.b * src_alpha[0]*alpha; // blue
            dest_pixel[1] = (255 - src_alpha[1])*alpha/255 * dest_pixel[1] + color.g * src_alpha[1]*alpha; // green
            dest_pixel[2] = (255 - src_alpha[2])*alpha/255 * dest_pixel[2] + color.r * src_alpha[2]*alpha; // red
                

			// dest_pixel[3] = dest_pixel[3]; // leave alpha the same
            
            src_alpha += 4;
            dest_pixel += 4;
        }
        
        row += framebuffer->stride;
        src_row += glyph_pixels_stride;
    }
}
// #pragma optimize("", on)
#endif