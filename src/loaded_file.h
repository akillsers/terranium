struct LoadedFile
{
    MemoryArena arena;
    TextEncoding encoding;

    TokenStore token_store_sentinel;
    u32 total_tokens;

    AnchorLink anchor_link_sentinel;

    String file_path;
    String file_name;     // the file name is a substring of file path

    FileType file_type;
    LineEndingType line_endings;
    
    TextBuffer *text;
    TextUndoStorage *undo_storage;
    
	// TODO: We may want to take this out and put it in a 
	//       view structure
	Cursor cursor;
	u32 line;
	
    RenderText *render_text;

    //TODO should this be extracted out?
    Tab *tab; 
	
	LoadedFile *next;
	LoadedFile *prev;
};

// TODO for now
#define LF_AFTER LF_AFTER_GAP_BYTE_OFFSET
// #define LF_LENGTH LF_BYTE_LENGTH
#define LF_AFTER_LENGTH LF_BYTE_LENGTH_AFTER_GAP


internal u32 CopyLFLine(char *out_line, LoadedFile *lf, u32 line_index, u32 horz_offset, u32 max_line_size);
internal Cursor LinePos(TextBuffer *buffer, u32 line_index);
internal u32 GetLineContainingCharPos(TextBuffer *buffer, Cursor char_pos);
UTF32 GetCodepoint(TextBuffer *lf, Cursor cursor);
