struct ShapedTextMetaInfo
{
    u64 storage_size;
    u32 line;
    b32 is_old_data;
};

struct ShapedTextBucket
{
    u32 one_past_line_number;
    ShapedText *data;
};

struct MultilineShapedTextCache
{
    void *memory;

    CircularBuffer data;
    ShapedTextMetaInfo *last;
    
    ShapedTextBucket *hash_table;
    u64 hash_table_max_elements;
};

enum VerticalTextAlignment
{
    TextAlign_top,
    TextAlign_center,
};

struct RenderTextTransform
{
    Recti bbox;

    i32 top_pixel;
    i32 left_pixel;

    //NOTE, TODO, this text alignment only works for
    //            RenderTextDataType_string
    VerticalTextAlignment vertical_alignment;

};
enum RenderTextDataType
{
    RenderTextDataType_buffer,
    RenderTextDataType_string,
};

struct ShapedTextPrecursor
{
    u32 storage_space_required;
    UTF32_String utf32_line_string;
    
    u32 num_runs;
    EngineShapedRunData **engine_data;
    ScriptItems script_items;
};

struct SingleLineShapedTextCache
{
    MemoryArena arena;
    b32 is_valid;
};

struct RenderText
{
    Font *font;
    
    void *get_color_params;
    Color (*text_color_function) (Cursor cursor, void *params);
    Color text_color;
    
    f32 line_height;

    TicketLock ticket_lock;

    // if the RenderTextDataType is RenderTextDataType_string
    // then multiline is NOT ALLOWED!!!
    RenderTextDataType data_type;
    union
    {
        struct
        {
            String string;
			SingleLineShapedTextCache *single_line_shape_cache;
		};
        struct
        {
            FractionalNumber cached_greatest_line_width;
            b32 cached_greatest_line_width_valid;

            TextBuffer *buffer;
			MultilineShapedTextCache *shape_cache; 
        };
    };
};
