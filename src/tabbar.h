//TODO respect DPI scaling
#define TABBAR_HEIGHT 36

#define CLR_TABBAR_BACKGROUND rgb_u32(190, 190, 190)
#define CLR_CLOSE_BUTTON_HOVER rgb_u32(232, 17, 35)
#define CLR_CAPTION_BUTTON_HOVER rgb_u32(179, 179, 179)

#define CLR_CAPTION_BUTTON_DEPRESSED rgb_u32(170, 170, 170)
#define CLR_CLOSE_BUTTON_DEPRESSED rgb_u32(241, 112, 122)

struct EditorState;
typedef void (*CloseTabHook) (EditorState *editor, void *data);
struct Tab
{
    Recti bbox;
    RenderText *render_text;
    
    void *data;
    CloseTabHook close_tab_hook;
    
    Tab *next;
    Tab *prev;
};


enum TabbarUIObject
{
    TabbarUIObject_null,
    TabbarUIObject_new_tab,
    TabbarUIObject_tab,
    
    TabbarUIObject_minimize,
    TabbarUIObject_middle_button,
    TabbarUIObject_close,
};

enum CaptionButtonType
{
    CaptionButton_null,

    CaptionButton_close = 1,
    CaptionButton_middle = 2,
    CaptionButton_minimize = 3,
};

struct Tabbar
{
    Recti bbox;

    TabbarUIObject hover;
    void *hover_ptr;

    TabbarUIObject activated;
    void *activated_ptr;
    
    u32 num_tabs;
    Tab *sentinel_tab;
    Tab *active_tab;

    
    b32 full_redraw_needed;
};

enum TabbarCommand
{
    Tabbar_none,
    TabbarInteraction_switch_to_tab,
    TabbarInteraction_create_new_tab,
};

struct TabbarInteraction
{
    TabbarCommand command;
    union
    {
        Tab *new_active_tab;
    };
};
