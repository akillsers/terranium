// Counts availible = 1,800,000,000 per second
//                   30,000,000 frame 
// #pragma optimize("git", on)
// TODO turn on the warning that's like NO if theres a = in an if statement

#ifdef _WIN32
#define PLATFORM_WINDOWS 1
#elif __APPLE__
#define PLATFORM_MACOS 1
#elif __linux__
#define PLATFORM_LINUX 1
#elif __unix__
#define PLATFORM_UNIX 1
#elif defined(_POSIX_VERSION)
#define PLATFORM_POSIX 1
#else
    #error "Unknown platform"
#endif

#include <memory.h>    // for memcpy and memmove
#include <stdio.h>     // for snprintf
#include <stddef.h>    // for offsetof
#include <string.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef signed char i8;
typedef short i16;
typedef int i32;
typedef long long i64;

typedef i32 b32;
typedef i8 b8;
typedef float f32;
typedef double f64;
typedef unsigned char uchar;

typedef uintptr_t umm;
typedef u32 UTF32;
typedef u16 UTF16;
typedef u8 UTF8;

#define Kilobytes(value) (value * 1024)
#define Megabytes(value) (Kilobytes(value) * 1024)
#define Gigabytes(value) (Megabytes(value) * 1024)
#define ArrayCount(array) (sizeof(array)/sizeof(array[0]))

#define internal static
#define global static
#define local_persist static

#define Min(a, b) ((a) < (b) ? (a) : (b))
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Clamp(value, min, max) Min(Max((value), (min)), (max))
#define Align16(value) (((value) + 15) & ~15)
// TODO untested rounding function
#define RoundUpToNearestX(value, to_nearest) ((value + to_nearest - 1 ) / to_nearest)
#define CopyElements(dest, src, num_elements) memcpy((dest), (src), num_elements*sizeof(*dest))

// TODO these DL functions are for doubly linked list that have a sentinel that points to itself
#define DL_REMOVE(item) {                       \
        (item)->next->prev = (item)->prev;      \
        (item)->prev->next = (item)->next;      \
        (item)->prev = 0;                       \
        (item)->next = 0;                       \
}

// TODO this macro doesn't work if it's sentinel->prev, find one that works for that case
#define DL_INSERT_AFTER(after_item, item_to_insert) {   \
        decltype(after_item) after_item_legit = after_item;     \
                                                                \
        (item_to_insert)->prev = (after_item_legit);                      \
        (item_to_insert)->next = (after_item_legit)->next;                \
        (after_item_legit)->next->prev = (item_to_insert);                \
        (after_item_legit)->next = (item_to_insert);                      \
    }

#define DL_INSERT_BEFORE(before_item, item_to_insert){  \
    (item_to_insert)->next = (before_item);                 \
    (item_to_insert)->prev = (before_item)->prev;        \
    (before_item)->prev->next = (item_to_insert);           \
    (before_item)->prev = (item_to_insert);                 \
    }

#define DL_INIT_SENTINEL(sentinel_ptr) {            \
        (sentinel_ptr)->next = (sentinel_ptr);        \
        (sentinel_ptr)->prev = (sentinel_ptr);        \
    }

// move the linked list by transfferring the sentinel setup
#define DL_TRANSFER_SENTINEL_SETUP(dest_sentinel_ptr, src_sentinel_ptr) { \
        (dest_sentinel_ptr)->next = (src_sentinel_ptr)->next;           \
        (dest_sentinel_ptr)->prev = (src_sentinel_ptr)->prev;           \
        (dest_sentinel_ptr)->next->prev = (dest_sentinel_ptr);          \
        (dest_sentinel_ptr)->prev->next = (dest_sentinel_ptr);          \
    }

#if INTERNAL
#define Assert(expression) if(!(expression)) {__debugbreak();}
#define InvalidDefaultCase {default: Assert(false)}
#define InvalidCodePath Assert(false)
#define NotImplemented platform.Error("Terranium Debug", "This feature is not implemented."); \
 Assert(false);
#else
#define Assert(expression)
#define InvalidDefaultCase
#define InvalidCodePath
#define NotImplemented
#endif
// Common additional types
union Vec3
{
    struct
    {
        f32 x;
        f32 y;
        f32 z;
    };
    struct
    {
        f32 r;
        f32 g;
        f32 b;
    };
};

union Vec2
{
    struct
    {
        f32 x;
        f32 y;
    };
    struct
    {
        f32 u;
        f32 v;
    };
};

union Vec2u
{
    struct
    {
        u32 x;
        u32 y;
    };
    struct
    {
        u32 u;
        u32 v;
    };
};

union Vec2i
{
    struct
    {
        i32 x;
        i32 y;
    };
    struct
    {
        i32 u;
        i32 v;
    };
};

struct Rectu
{
    u32 x;
    u32 y;
    u32 width;
    u32 height;
};

struct Recti
{
    i32 x;
    i32 y;

    // these are unsigned because we will never have to deal with negative widths or heights
    i32 width;
    i32 height;
};

internal Recti
recti(i32 x, i32 y, u32 width, u32 height)
{
    Recti result = {x, y, (i32) width, (i32)height};
    return result;
}

internal Vec3
vec3(f32 x, f32 y, f32 z)
{
    Vec3 ret;
    ret.x = x;
    ret.y = y;
    ret.z = z;
    return ret;
}

internal Vec2i
vec2i(i32 x, i32 y)
{
    Vec2i ret;
    ret.x = x;
    ret.y = y;
    return ret;
}

internal Vec2
vec2(f32 x, f32 y)
{
    Vec2 result;
    result.x = x;
    result.y = y;

    return result;
}

Vec2i
operator - (Vec2i left, Vec2i right)
{
    Vec2i ret;
    ret.x = left.x - right.x;
    ret.y = left.y - right.y;
    return ret;
}

Vec2i
operator + (Vec2i left, Vec2i right)
{
    Vec2i ret;
    ret.x = left.x - right.x;
    ret.y = left.y - right.y;
    return ret;
}

Vec2i
operator * (Vec2i left, f32 right)
{
    Vec2i ret;
    ret.x = left.x - right;
    ret.y = left.y - right;
    return ret;
}

Vec2i
operator * (f32 left, Vec2i right)
{
    Vec2i ret;
    ret.x = left - right.x;
    ret.y = left - right.y;
    return ret;
}

Vec2i
operator += (Vec2i &left, Vec2i right)
{
    left = left + right;
    return left;
}

Vec2i
operator -= (Vec2i &left, Vec2i right)
{
    left = left - right;
    return left;
}

Vec2i
operator - (Vec2i a)
{
    Vec2i ret;
    ret.x = -a.x;
    ret.y = -a.y;
    return ret;
}

Vec2i
operator *= (Vec2i &left, f32 right)
{
    left = left * right;
    return left;
}

Vec2
operator - (Vec2 left, Vec2 right)
{
    Vec2 ret;
    ret.x = left.x - right.x;
    ret.y = left.y - right.y;
    return ret;
}

Vec2
operator + (Vec2 left, Vec2 right)
{
    Vec2 ret;
    ret.x = left.x - right.x;
    ret.y = left.y - right.y;
    return ret;
}

Vec2
operator * (Vec2 left, f32 right)
{
    Vec2 ret;
    ret.x = left.x - right;
    ret.y = left.y - right;
    return ret;
}

Vec2
operator * (f32 left, Vec2 right)
{
    Vec2 ret;
    ret.x = left - right.x;
    ret.y = left - right.y;
    return ret;
}

Vec2
operator += (Vec2 &left, Vec2 right)
{
    left = left + right;
    return left;
}

Vec2
operator -= (Vec2 &left, Vec2 right)
{
    left = left - right;
    return left;
}

Vec2
operator - (Vec2 a)
{
    Vec2 ret;
    ret.x = -a.x;
    ret.y = -a.y;
    return ret;
}

Vec2
operator *= (Vec2 &left, f32 right)
{
    left = left * right;
    return left;
}

b32
operator == (Vec2i left, Vec2i right)
{
    return (left.x == right.x && left.y == right.y);
}

struct CircularBuffer
{
    void *base;
    u64 capacity;

    umm len1;
    umm start2;
    umm len2;
};

inline b32
PosInRectu(Vec2i pos, Rectu bounds)
{
    b32 result = false;
    if(pos.x >= bounds.x && pos.y >= bounds.y && pos.x <= (bounds.x + bounds.width) && pos.y <= (bounds.y + bounds.height))
        result = true;
    return result;
}

inline b32
PosInRecti(Vec2i pos, Recti bounds)
{
    b32 result = false;
    if(pos.x >= bounds.x && pos.y >= bounds.y && pos.x <= (bounds.x + bounds.width) && pos.y <= (bounds.y + bounds.height))
        result = true;
    return result;
}

inline b32
RectInRect(Recti child, Recti container)
{
    b32 result = false;
    if(child.x >= 0 && child.y >= 0 &&
       child.x + child.width <= container.width &&
       child.y + child.height <= container.height)
        result = true;
    return result;
}

internal Recti
RectEnclose(Recti first, Recti second)
{
    Recti ret;
    if(first.width == 0 || first.height == 0)
        return second;
    if(second.width == 0 || second.height == 0)
        return first;
    ret.x = Min(first.x, second.x);
    ret.y = Min(first.y, second.y);

    // TODO there gotta be a better way to do this
    i32 first_right_bound = first.x + first.width;
    i32 second_right_bound = second.x + second.width;
    if(first_right_bound > second_right_bound) {
        ret.width = first_right_bound - ret.x;
    } else {
        ret.width = second_right_bound - ret.x;
    }

    i32 first_bottom_bound = first.y + first.height;
    i32 second_bottom_bound = second.y + second.height;
    if(first_bottom_bound > second_bottom_bound) {
        ret.height = first_bottom_bound - ret.y;
    } else {
        ret.height = second_bottom_bound - ret.y;
    }
        
    return ret;
}

internal Recti
CreateRectInsideRect(Recti container, u32 x_margin, u32 y_margin)
{
    Recti result;

    result.x = container.x + x_margin;
    result.y = container.y + y_margin;
    result.width = container.width - x_margin*2;
    result.height = container.height - y_margin*2;

    return result;
}

#define ZeroStruct(structure) ZeroSize((structure), sizeof(*structure));

internal void
ZeroSize(void *data, u32 size)
{
    for(u32 i = 0;
        i < size;
        i++)
    {
        u8 *byte_to_clear = (u8 *)data + i;
        *byte_to_clear = 0;
    }
}

internal Recti
ExpandRect(Recti rect, u32 border)
{
    Recti ret = rect;
    ret.x -= border;
    ret.y -= border;
    ret.width += border*2;
    ret.height += border*2;
    return ret;
}


inline i32
Midpoint(i32 left, i32 right)
{
    i32 ret;
    ret = left + (right - left)/2;
    return ret;
}

internal b32
OnlyFlagSet(i32 bit_field, i32 flag)
{
    b32 result = false;
    if(bit_field & flag &&
       !(bit_field &~flag))
    {
        result = true;
    }
    return result;
}

internal b32
RectIsEmpty(Recti rect)
{
    b32 result;
    result = (rect.width == 0 || rect.height == 0);
    return result;
}

struct Framebuffer
{
    // 0xBBGGRRAA
    // this is a 32bpp BBGGRRAA framebuffer
    void *pixels;
    u32 width;
    u32 height;
    u32 stride;
    u32 bytespp;

    Recti *dirty;
    u32 dirty_rects_used;
    u32 max_dirty_rects;
};

internal void
AddDirtyRect_NoAsserts(Framebuffer *framebuffer, Recti rectangle)
{
    // TODO if there is only one dirty rect and it all the screen consider
    //      not doing anything?
    if(framebuffer->dirty_rects_used < framebuffer->max_dirty_rects)
    {
        framebuffer->dirty[framebuffer->dirty_rects_used] = rectangle;
        framebuffer->dirty_rects_used++;
    }
    else
    {
        framebuffer->dirty[0] = recti(0, 0, framebuffer->width, framebuffer->height);
        framebuffer->dirty_rects_used = 1;
    }
}

inline void
AddDirtyRect(Framebuffer *framebuffer, Recti rectangle)
{
    Assert(RectInRect(rectangle, recti(0, 0, framebuffer->width, framebuffer->height)));

    Assert(rectangle.x >= 0 && rectangle.y >= 0 &&
           rectangle.x + rectangle.width <= framebuffer->width &&
           rectangle.y + rectangle.height <= framebuffer->height);

    AddDirtyRect_NoAsserts(framebuffer, rectangle);
}


#if !FULL_WIN_SDK
#define _INTSIZEOF(n)          ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))

#define va_start(ap, v) ((void)(ap = (va_list)_ADDRESSOF(v) + _INTSIZEOF(v)))
#define va_arg(ap, t)     (*(t*)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)))
#define va_end(ap)        ((void)(ap = (va_list)0))
#endif
