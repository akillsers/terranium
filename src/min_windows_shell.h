#define SHSTDAPI          EXTERN_C DECLSPEC_IMPORT HRESULT STDAPICALLTYPE
#define SHSTDAPI_(type)   extern "C" __declspec(dllimport) type __stdcall
#define WINOLEAPI_(type)   extern "C" __declspec(dllimport) type __stdcall

interface IEnumFORMATETC;
interface IAdviseSink;
interface IEnumSTATDATA;

typedef WCHAR *LPOLESTR;
typedef WORD CLIPFORMAT;

struct IStream;
struct IStorage;

typedef struct tagDVTARGETDEVICE
{
    DWORD tdSize;
    WORD tdDriverNameOffset;
    WORD tdDeviceNameOffset;
    WORD tdPortNameOffset;
    WORD tdExtDevmodeOffset;
    /* [size_is] */ BYTE tdData[ 1 ];
} 	DVTARGETDEVICE;

typedef struct tagFORMATETC
{
    CLIPFORMAT cfFormat;
    /* [unique] */ DVTARGETDEVICE *ptd;
    DWORD dwAspect;
    LONG lindex;
    DWORD tymed;
}FORMATETC;

typedef struct tagSTGMEDIUM
{
    DWORD tymed;
    /* [switch_is][switch_type] */
    union 
    {
        /* [case()] */ HBITMAP hBitmap;
        /* [case()] */ HMETAFILEPICT hMetaFilePict;
        /* [case()] */ HENHMETAFILE hEnhMetaFile;
        /* [case()] */ HGLOBAL hGlobal;
        /* [case()] */ LPOLESTR lpszFileName;
        /* [case()] */ IStream *pstm;
        /* [case()] */ IStorage *pstg;
        /* [default] */  /* Empty union arm */ 
    };
    /* [unique] */ IUnknown *pUnkForRelease;
}STGMEDIUM, *LPSTGMEDIUM;

typedef struct _AppBarData
{
    DWORD cbSize;
    HWND hWnd;
    UINT uCallbackMessage;
    UINT uEdge;
    RECT rc;
    LPARAM lParam; // message specific
} APPBARDATA, *PAPPBARDATA;

#define ABM_NEW           0x00000000
#define ABM_REMOVE        0x00000001
#define ABM_QUERYPOS      0x00000002
#define ABM_SETPOS        0x00000003
#define ABM_GETSTATE      0x00000004
#define ABM_GETTASKBARPOS 0x00000005
#define ABM_ACTIVATE      0x00000006  // lParam == TRUE/FALSE means activate/deactivate
#define ABM_GETAUTOHIDEBAR 0x00000007
#define ABM_SETAUTOHIDEBAR 0x00000008  // this can fail at any time.  MUST check the result
                                        // lParam = TRUE/FALSE  Set/Unset
                                        // uEdge = what edge
#define ABM_WINDOWPOSCHANGED 0x0000009
#define ABM_SETSTATE         0x0000000a
#define ABM_GETAUTOHIDEBAREX    0x0000000b // multimon aware autohide bars
#define ABM_SETAUTOHIDEBAREX    0x0000000c


#define ABN_STATECHANGE    0x0000000
#define ABN_POSCHANGED     0x0000001
#define ABN_FULLSCREENAPP  0x0000002
#define ABN_WINDOWARRANGE  0x0000003 // lParam == TRUE means hide

#define ABS_AUTOHIDE    0x0000001
#define ABS_ALWAYSONTOP 0x0000002

#define ABE_LEFT        0
#define ABE_TOP         1
#define ABE_RIGHT       2
#define ABE_BOTTOM      3

SHSTDAPI_(UINT_PTR) SHAppBarMessage(_In_ DWORD dwMessage, _Inout_ PAPPBARDATA pData);

typedef struct IDropTargetHelper IDropTargetHelper;
typedef struct IDataObject IDataObject;

MIDL_INTERFACE("00000122-0000-0000-C000-000000000046")
IDropTarget : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE DragEnter( 
        /* [unique][in] */ __RPC__in_opt IDataObject *pDataObj,
        /* [in] */ DWORD grfKeyState,
        /* [in] */ POINTL pt,
        /* [out][in] */ __RPC__inout DWORD *pdwEffect) = 0;
        
    virtual HRESULT STDMETHODCALLTYPE DragOver( 
        /* [in] */ DWORD grfKeyState,
        /* [in] */ POINTL pt,
        /* [out][in] */ __RPC__inout DWORD *pdwEffect) = 0;
        
    virtual HRESULT STDMETHODCALLTYPE DragLeave( void) = 0;
        
    virtual HRESULT STDMETHODCALLTYPE Drop( 
        /* [unique][in] */ __RPC__in_opt IDataObject *pDataObj,
        /* [in] */ DWORD grfKeyState,
        /* [in] */ POINTL pt,
        /* [out][in] */ __RPC__inout DWORD *pdwEffect) = 0;
};
typedef IDropTarget *LPDROPTARGET;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0000010e-0000-0000-C000-000000000046")
    IDataObject : public IUnknown
    {
    public:
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE GetData( 
            /* [annotation][unique][in] */ 
            _In_  FORMATETC *pformatetcIn,
            /* [annotation][out] */ 
            _Out_  STGMEDIUM *pmedium) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE GetDataHere( 
            /* [annotation][unique][in] */ 
            _In_  FORMATETC *pformatetc,
            /* [annotation][out][in] */ 
            _Inout_  STGMEDIUM *pmedium) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE QueryGetData( 
            /* [unique][in] */ __RPC__in_opt FORMATETC *pformatetc) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCanonicalFormatEtc( 
            /* [unique][in] */ __RPC__in_opt FORMATETC *pformatectIn,
            /* [out] */ __RPC__out FORMATETC *pformatetcOut) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE SetData( 
            /* [annotation][unique][in] */ 
            _In_  FORMATETC *pformatetc,
            /* [annotation][unique][in] */ 
            _In_  STGMEDIUM *pmedium,
            /* [in] */ BOOL fRelease) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE EnumFormatEtc( 
            /* [in] */ DWORD dwDirection,
            /* [out] */ __RPC__deref_out_opt IEnumFORMATETC **ppenumFormatEtc) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE DAdvise( 
            /* [in] */ __RPC__in FORMATETC *pformatetc,
            /* [in] */ DWORD advf,
            /* [unique][in] */ __RPC__in_opt IAdviseSink *pAdvSink,
            /* [out] */ __RPC__out DWORD *pdwConnection) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE DUnadvise( 
            /* [in] */ DWORD dwConnection) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE EnumDAdvise( 
            /* [out] */ __RPC__deref_out_opt IEnumSTATDATA **ppenumAdvise) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IDataObjectVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            __RPC__in IDataObject * This,
            /* [in] */ __RPC__in REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            __RPC__in IDataObject * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            __RPC__in IDataObject * This);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *GetData )( 
            IDataObject * This,
            /* [annotation][unique][in] */ 
            _In_  FORMATETC *pformatetcIn,
            /* [annotation][out] */ 
            _Out_  STGMEDIUM *pmedium);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *GetDataHere )( 
            IDataObject * This,
            /* [annotation][unique][in] */ 
            _In_  FORMATETC *pformatetc,
            /* [annotation][out][in] */ 
            _Inout_  STGMEDIUM *pmedium);
        
        HRESULT ( STDMETHODCALLTYPE *QueryGetData )( 
            __RPC__in IDataObject * This,
            /* [unique][in] */ __RPC__in_opt FORMATETC *pformatetc);
        
        HRESULT ( STDMETHODCALLTYPE *GetCanonicalFormatEtc )( 
            __RPC__in IDataObject * This,
            /* [unique][in] */ __RPC__in_opt FORMATETC *pformatectIn,
            /* [out] */ __RPC__out FORMATETC *pformatetcOut);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *SetData )( 
            IDataObject * This,
            /* [annotation][unique][in] */ 
            _In_  FORMATETC *pformatetc,
            /* [annotation][unique][in] */ 
            _In_  STGMEDIUM *pmedium,
            /* [in] */ BOOL fRelease);
        
        HRESULT ( STDMETHODCALLTYPE *EnumFormatEtc )( 
            __RPC__in IDataObject * This,
            /* [in] */ DWORD dwDirection,
            /* [out] */ __RPC__deref_out_opt IEnumFORMATETC **ppenumFormatEtc);
        
        HRESULT ( STDMETHODCALLTYPE *DAdvise )( 
            __RPC__in IDataObject * This,
            /* [in] */ __RPC__in FORMATETC *pformatetc,
            /* [in] */ DWORD advf,
            /* [unique][in] */ __RPC__in_opt IAdviseSink *pAdvSink,
            /* [out] */ __RPC__out DWORD *pdwConnection);
        
        HRESULT ( STDMETHODCALLTYPE *DUnadvise )( 
            __RPC__in IDataObject * This,
            /* [in] */ DWORD dwConnection);
        
        HRESULT ( STDMETHODCALLTYPE *EnumDAdvise )( 
            __RPC__in IDataObject * This,
            /* [out] */ __RPC__deref_out_opt IEnumSTATDATA **ppenumAdvise);
        
        END_INTERFACE
    } IDataObjectVtbl;

    interface IDataObject
    {
        CONST_VTBL struct IDataObjectVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDataObject_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDataObject_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDataObject_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDataObject_GetData(This,pformatetcIn,pmedium)	\
    ( (This)->lpVtbl -> GetData(This,pformatetcIn,pmedium) ) 

#define IDataObject_GetDataHere(This,pformatetc,pmedium)	\
    ( (This)->lpVtbl -> GetDataHere(This,pformatetc,pmedium) ) 

#define IDataObject_QueryGetData(This,pformatetc)	\
    ( (This)->lpVtbl -> QueryGetData(This,pformatetc) ) 

#define IDataObject_GetCanonicalFormatEtc(This,pformatectIn,pformatetcOut)	\
    ( (This)->lpVtbl -> GetCanonicalFormatEtc(This,pformatectIn,pformatetcOut) ) 

#define IDataObject_SetData(This,pformatetc,pmedium,fRelease)	\
    ( (This)->lpVtbl -> SetData(This,pformatetc,pmedium,fRelease) ) 

#define IDataObject_EnumFormatEtc(This,dwDirection,ppenumFormatEtc)	\
    ( (This)->lpVtbl -> EnumFormatEtc(This,dwDirection,ppenumFormatEtc) ) 

#define IDataObject_DAdvise(This,pformatetc,advf,pAdvSink,pdwConnection)	\
    ( (This)->lpVtbl -> DAdvise(This,pformatetc,advf,pAdvSink,pdwConnection) ) 

#define IDataObject_DUnadvise(This,dwConnection)	\
    ( (This)->lpVtbl -> DUnadvise(This,dwConnection) ) 

#define IDataObject_EnumDAdvise(This,ppenumAdvise)	\
    ( (This)->lpVtbl -> EnumDAdvise(This,ppenumAdvise) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */

SHSTDAPI_(UINT) DragQueryFileW(_In_ HDROP hDrop, _In_ UINT iFile, _Out_writes_opt_(cch) LPWSTR lpszFile, _In_ UINT cch);
SHSTDAPI_(LPWSTR *)  CommandLineToArgvW(_In_ LPCWSTR lpCmdLine, _Out_ int* pNumArgs);
WINOLEAPI_(void)   ReleaseStgMedium(LPSTGMEDIUM);

extern "C" const IID IID_IDropTargetHelper;
#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4657278B-411B-11D2-839A-00C04FD918D0")
    IDropTargetHelper : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE DragEnter( 
            /* [annotation][in] */ 
            _In_  HWND hwndTarget,
            /* [annotation][in] */ 
            _In_  IDataObject *pDataObject,
            /* [annotation][in] */ 
            _In_  POINT *ppt,
            /* [annotation][in] */ 
            _In_  DWORD dwEffect) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE DragLeave( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE DragOver( 
            /* [annotation][in] */ 
            _In_  POINT *ppt,
            /* [annotation][in] */ 
            _In_  DWORD dwEffect) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Drop( 
            /* [annotation][in] */ 
            _In_  IDataObject *pDataObject,
            /* [annotation][in] */ 
            _In_  POINT *ppt,
            /* [annotation][in] */ 
            _In_  DWORD dwEffect) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Show( 
            /* [annotation][in] */ 
            _In_  BOOL fShow) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IDropTargetHelperVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDropTargetHelper * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDropTargetHelper * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDropTargetHelper * This);
        
        HRESULT ( STDMETHODCALLTYPE *DragEnter )( 
            IDropTargetHelper * This,
            /* [annotation][in] */ 
            _In_  HWND hwndTarget,
            /* [annotation][in] */ 
            _In_  IDataObject *pDataObject,
            /* [annotation][in] */ 
            _In_  POINT *ppt,
            /* [annotation][in] */ 
            _In_  DWORD dwEffect);
        
        HRESULT ( STDMETHODCALLTYPE *DragLeave )( 
            IDropTargetHelper * This);
        
        HRESULT ( STDMETHODCALLTYPE *DragOver )( 
            IDropTargetHelper * This,
            /* [annotation][in] */ 
            _In_  POINT *ppt,
            /* [annotation][in] */ 
            _In_  DWORD dwEffect);
        
        HRESULT ( STDMETHODCALLTYPE *Drop )( 
            IDropTargetHelper * This,
            /* [annotation][in] */ 
            _In_  IDataObject *pDataObject,
            /* [annotation][in] */ 
            _In_  POINT *ppt,
            /* [annotation][in] */ 
            _In_  DWORD dwEffect);
        
        HRESULT ( STDMETHODCALLTYPE *Show )( 
            IDropTargetHelper * This,
            /* [annotation][in] */ 
            _In_  BOOL fShow);
        
        END_INTERFACE
    } IDropTargetHelperVtbl;

    interface IDropTargetHelper
    {
        CONST_VTBL struct IDropTargetHelperVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDropTargetHelper_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDropTargetHelper_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDropTargetHelper_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDropTargetHelper_DragEnter(This,hwndTarget,pDataObject,ppt,dwEffect)	\
    ( (This)->lpVtbl -> DragEnter(This,hwndTarget,pDataObject,ppt,dwEffect) ) 

#define IDropTargetHelper_DragLeave(This)	\
    ( (This)->lpVtbl -> DragLeave(This) ) 

#define IDropTargetHelper_DragOver(This,ppt,dwEffect)	\
    ( (This)->lpVtbl -> DragOver(This,ppt,dwEffect) ) 

#define IDropTargetHelper_Drop(This,pDataObject,ppt,dwEffect)	\
    ( (This)->lpVtbl -> Drop(This,pDataObject,ppt,dwEffect) ) 

#define IDropTargetHelper_Show(This,fShow)	\
    ( (This)->lpVtbl -> Show(This,fShow) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */

// ole2.h
WINOLEAPI RegisterDragDrop(HWND hwnd, LPDROPTARGET pDropTarget);

