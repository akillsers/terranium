internal void
SetScrollbarThumb(Scrollbar *scrollbar, f32 pos_percent, f32 preferred_length_percent)
{
    u32 max_length = scrollbar->orientation == ScrollbarOrientation_vertical ? scrollbar->container.height : scrollbar->container.width;
    i32 preferred_thumb_length = preferred_length_percent * max_length;
    i32 new_thumb_length = Max(SCROLL_BAR_MIN_LENGTH, preferred_thumb_length);

    i32 new_thumb_pos = // track_start + 
        pos_percent * (max_length - new_thumb_length);

    scrollbar->thumb_pos = new_thumb_pos;
    scrollbar->thumb_length = new_thumb_length;
}

internal Recti
CalculateScrollbarThumbRect(Scrollbar *scrollbar)
{
    Recti result;
    
    if(scrollbar->orientation == ScrollbarOrientation_vertical)
    {
        i32 track_start = scrollbar->container.y;
        result = {scrollbar->container.x, track_start + scrollbar->thumb_pos,
                  scrollbar->container.width, scrollbar->thumb_length};
    } else {
        i32 track_start = scrollbar->container.x;
        result = {track_start + scrollbar->thumb_pos, scrollbar->container.y,
                  scrollbar->thumb_length, scrollbar->container.width};
    }
    return result;
}

internal void
PositionScrollbar(Scrollbar *scrollbar, Recti scroll_target_bbox)
{
    if(scrollbar->orientation == ScrollbarOrientation_vertical)
    {
        // NOTE:: scroll_target is what (the document, text field, etc.) this scrollbar scrolls
        scrollbar->container = recti(scroll_target_bbox.x + scroll_target_bbox.width,
                                     scroll_target_bbox.y,
                                     SCROLLBAR_THICKNESS,
                                     scroll_target_bbox.height);
    }
    else
    {
        scrollbar->container = recti(scroll_target_bbox.x,
                                     scroll_target_bbox.y + scroll_target_bbox.height,
                                     scroll_target_bbox.width,
                                     SCROLLBAR_THICKNESS);
        
    }
}

internal u32
GetScrollbarPositionAtPercent(Scrollbar *scrollbar, f32 percent)
{
    u32 result;
    i32 track_start = scrollbar->orientation == ScrollbarOrientation_vertical ? scrollbar->container.y : scrollbar->container.x;
    result = track_start + percent*(scrollbar->container.height - scrollbar->thumb_length);
    return result;
}

internal void
DrawScrollbar(Scrollbar *scrollbar, Framebuffer *pfb)
{
    DrawRectangle(scrollbar->container, CLR_SCROLLBAR_CONTAINER, pfb);

    Recti thumb_rect = CalculateScrollbarThumbRect(scrollbar);
    DrawRectangle(thumb_rect, CLR_SCROLLBAR_THUMB, pfb);
}

internal f32
GetScrollPercent(Scrollbar *scrollbar)
{
    f32 result;
    if(scrollbar->orientation == ScrollbarOrientation_vertical)
    {
        if (scrollbar->container.height == scrollbar->thumb_length)
            result = 0;
        else
            result = (f32)(scrollbar->thumb_pos) / (f32)(scrollbar->container.height - scrollbar->thumb_length);
    }
    else
    {
        if (scrollbar->container.width == scrollbar->thumb_length)
        {
            result = 0;
        }
        else 
        {
            result = (f32)(scrollbar->thumb_pos) / (f32)(scrollbar->container.width - scrollbar->thumb_length);

        }
    }
    return result;
}

internal void
ScrollbarMouseDown(Scrollbar *scrollbar, Input *input)
{
    Assert(PosInRecti(input->mouse_pos, scrollbar->container));

    scrollbar->seconds_since_dragged = 0;
    scrollbar->repeats_commited = 0;
    scrollbar->total_repeats_needed = 0;
    
    Recti thumb_rect = CalculateScrollbarThumbRect(scrollbar);
    if(PosInRecti(input->mouse_pos, thumb_rect))
    {
        scrollbar->operation_mode = ScrollbarOperationMode_dragging_thumb;
        u32 mouse_axis = (scrollbar->orientation == ScrollbarOrientation_vertical) ? input->mouse_pos.y : input->mouse_pos.x;
        scrollbar->drag_offset = mouse_axis - scrollbar->thumb_pos;
    }
    else
    {
        scrollbar->operation_mode = ScrollbarOperationMode_paging_to_cursor;
        scrollbar->total_repeats_needed = 1;
    }

}

internal b32
ScrollbarDragged(Scrollbar *scrollbar, Input *input)
{
    b32 dragged = false;
    if(scrollbar->operation_mode == ScrollbarOperationMode_dragging_thumb)
    {
        i32 mouse_axis = scrollbar->orientation == ScrollbarOrientation_vertical ? input->mouse_pos.y : input->mouse_pos.x;
        scrollbar->thumb_pos = mouse_axis - scrollbar->drag_offset;
        
        i32 min_pos = 0;
        i32 max_pos = scrollbar->orientation == ScrollbarOrientation_vertical ? scrollbar->container.height : scrollbar->container.width;

        if(scrollbar->thumb_pos + scrollbar->thumb_length > max_pos)
        {
            scrollbar->thumb_pos = max_pos - scrollbar->thumb_length;
        }
        else if(scrollbar->thumb_pos < min_pos)
        {
            scrollbar->thumb_pos = min_pos;
        }
        dragged = true;
    }

    return dragged;
}

internal void
ScrollbarMouseButtonHeld(Scrollbar *scrollbar, Input *input)
{
    if(scrollbar->operation_mode == ScrollbarOperationMode_paging_to_cursor)
    {
        scrollbar->seconds_since_dragged += input->dt;
        // TODO RESEARCH what variable in Windows is causing the scrollbar delay to change
        //              is it the same as key delay and repeat rate?
        if(scrollbar->seconds_since_dragged > SCROLLBAR_REPEAT_DELAY_SECONDS)
        {
            // repeat hertz is cycles per second
            // 30 cycles/second
            f32 seconds_since_repeat = (scrollbar->seconds_since_dragged - SCROLLBAR_REPEAT_DELAY_SECONDS); 
            scrollbar->total_repeats_needed = seconds_since_repeat * SCROLLBAR_REPEAT_HERTZ + 1;
        }
    } 

}


internal void
Scrollbar_OnMouseUp(Scrollbar *scrollbar, Vec2i mouse_pos)
{
    scrollbar->operation_mode = ScrollbarOperationMode_none;
}
