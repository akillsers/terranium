#if INTERNAL
struct TimingRecord
{
    u64 line;
    char *file;
    char *func;
    
    volatile u64 cycles;
    volatile u32 hit_count;
};

extern TimingRecord timing_records[];
extern const u32 num_timing_records;

struct TimedBlock
{
    u32 counter;
    u64 start_cycles;
    
    TimedBlock(char *file_name, char *function_name, u64 line_num, u32 counter)
    {
        this->counter = counter;
        
        // if(counter == 49) Assert(false);

        timing_records[counter].line = line_num;
        timing_records[counter].file = file_name;
        timing_records[counter].func = function_name;

        start_cycles = __rdtsc();
    }
    
    ~TimedBlock()
    {
        u64 end_cycles = __rdtsc();
        u64 elapsed = end_cycles - this->start_cycles;
        AtomicIncrement(&timing_records[this->counter].hit_count);

        // Note the truncation
        AtomicAdd((volatile u32 *)&timing_records[this->counter].cycles, elapsed);
    }
};

#define TIMED_BLOCK TimedBlock timed_block##__LINE____FILE__(__FILE__, __FUNCTION__, __LINE__, __COUNTER__)

internal void ResetTimingRecords();
internal void PrintTimingRecords();
#else
#define TIMED_BLOCK
#endif