struct FontDriverData
{
    IDWriteFont *dwrite_font;
    IDWriteFontFace *dwrite_font_face;
    IDWriteFontFace1 *dwrite_font_face1;
};

struct DWRITE_data
{
    IDWriteFactory *factory;
    IDWriteFontCollection *system_fonts;
};

struct DWRITE_GlyphAnalysis
{
    Font *font;
    IDWriteGlyphRunAnalysis *sys;

    u16 glyph_id;
    Vec2i subpixel_position;
    GlyphTextureType texture_type;
};
