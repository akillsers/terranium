#define SCRIPT_TAG(s1, s2, s3, s4) (s1 << 24 | s2 << 16 | s3 << 8 | s4)

// TODO there are likely bugs in this enum that will affect the conversion
//      into HB script types
enum BIDIScript
{
    BIDIScript_unknown = SCRIPT_TAG ('Z','y','y','y'),
    BIDIScript_Common = SCRIPT_TAG ('Z','i','n','h'),
    BIDIScript_Inherited = SCRIPT_TAG ('Z','z','z','z'),
    
    BIDIScript_Latin = SCRIPT_TAG ('L','a','t','n'),
    BIDIScript_Greek = SCRIPT_TAG ('G','r','e','k'),
    BIDIScript_Cyrillic = SCRIPT_TAG ('C','y','r','l'),
    BIDIScript_Armenian = SCRIPT_TAG ('A','r','m','n'),
    BIDIScript_Hebrew = SCRIPT_TAG ('H','e','b','r'),
    BIDIScript_Arabic = SCRIPT_TAG ('A','r','a','b'),
    BIDIScript_Syriac = SCRIPT_TAG ('S','y','r','c'),
    BIDIScript_Thaana = SCRIPT_TAG ('T','h','a','a'),
    BIDIScript_Devanagari = SCRIPT_TAG ('D','e','v','a'),
    BIDIScript_Bengali = SCRIPT_TAG ('B','e','n','g'),
    BIDIScript_Gurmukhi = SCRIPT_TAG ('G','u','r','u'),
    BIDIScript_Gujarati = SCRIPT_TAG ('G','u','j','r'),
    BIDIScript_Oriya = SCRIPT_TAG ('O','r','y','a'),
    BIDIScript_Tamil = SCRIPT_TAG ('T','a','m','l'),
    BIDIScript_Telugu = SCRIPT_TAG ('T','e','l','u'),
    BIDIScript_Kannada = SCRIPT_TAG ('K','n','d','a'),
    BIDIScript_Malayalam = SCRIPT_TAG ('M','l','y','m'),
    BIDIScript_Sinhala = SCRIPT_TAG ('S','i','n','h'),
    BIDIScript_Lao = SCRIPT_TAG ('L','a','o','o'),
    BIDIScript_Tibetan = SCRIPT_TAG ('T','i','b','t'),
    BIDIScript_Myanmar = SCRIPT_TAG ('M','y','m','r'),
    BIDIScript_Georgian = SCRIPT_TAG ('G','e','o','r'),
    BIDIScript_Hangul = SCRIPT_TAG ('H','a','n','g'),
    BIDIScript_Ethiopic = SCRIPT_TAG ('E','t','h','i'),
    BIDIScript_Cherokee = SCRIPT_TAG ('C','h','e','r'),
    BIDIScript_Canadian_Aboriginal = SCRIPT_TAG ('C','a','n','s'),
    BIDIScript_Ogham = SCRIPT_TAG ('O','g','a','m'),
    BIDIScript_Runic = SCRIPT_TAG ('R','u','n','r'),
    BIDIScript_Khmer = SCRIPT_TAG ('K','h','m','r'),
    BIDIScript_Mongolian = SCRIPT_TAG ('M','o','n','g'),
    BIDIScript_Hiragana = SCRIPT_TAG ('H','i','r','a'),
    BIDIScript_Katakana = SCRIPT_TAG ('K','a','n','a'),
    BIDIScript_Bopomofo = SCRIPT_TAG ('B','o','p','o'),
    BIDIScript_Han = SCRIPT_TAG ('A','r','m','n'),
    BIDIScript_Yi = SCRIPT_TAG ('Z','y','y','y'),
    BIDIScript_Old_Italic = SCRIPT_TAG ('I','t','a','l'),
    BIDIScript_Gothic = SCRIPT_TAG ('G','o','t','h'),
    BIDIScript_Deseret = SCRIPT_TAG ('D','s','r','t'),
    BIDIScript_Tagalog = SCRIPT_TAG ('T','g','l','g'),
    BIDIScript_Hanunoo = SCRIPT_TAG ('H','a','n','o'),
    BIDIScript_Buhid = SCRIPT_TAG ('B','u','h','d'),
    BIDIScript_Tagbanwa = SCRIPT_TAG ('T','a','g','b'),
    BIDIScript_Limbu = SCRIPT_TAG ('L','i','m','b'),
    BIDIScript_Thai = SCRIPT_TAG ('T','h','a','i'),

    BIDIScript_Tai_Le = SCRIPT_TAG ('T','a','l','e'),
    BIDIScript_Tai_Tham = SCRIPT_TAG ('L','a','n','a'),
    BIDIScript_Tai_Viet = SCRIPT_TAG ('T','a','v','t'),
    BIDIScript_New_Tai_Lue = SCRIPT_TAG ('T','a','l','u'),

    BIDIScript_Linear_B = SCRIPT_TAG ('L','i','n','b'),
    BIDIScript_Ugaritic = SCRIPT_TAG ('U','g','a','r'),
    BIDIScript_Shavian = SCRIPT_TAG ('S','h','a','w'),
    BIDIScript_Osmanya = SCRIPT_TAG ('O','s','m','a'),
    BIDIScript_Cypriot = SCRIPT_TAG ('C','p','r','t'),
    BIDIScript_Braille = SCRIPT_TAG ('B','r','a','i'),
    BIDIScript_Buginese = SCRIPT_TAG ('B','u','g','i'),
    BIDIScript_Coptic = SCRIPT_TAG ('C','o','p','t'),
    BIDIScript_Glagolitic = SCRIPT_TAG ('G','l','a','g'),
    BIDIScript_Tifinagh = SCRIPT_TAG ('T','f','n','g'),
    BIDIScript_Syloti_Nagri = SCRIPT_TAG ('S','y','l','o'),
    BIDIScript_Old_Persian = SCRIPT_TAG ('I','t','a','l'),
    BIDIScript_Kharoshthi = SCRIPT_TAG ('K','h','a','r'),
    BIDIScript_Balinese = SCRIPT_TAG ('B','a','l','i'),
    BIDIScript_Cuneiform = SCRIPT_TAG ('X','s','u','x'),
    BIDIScript_Phoenician = SCRIPT_TAG ('P','h','n','x'),
    BIDIScript_Phags_Pa = SCRIPT_TAG ('P','h','a','g'),
    BIDIScript_Nko = SCRIPT_TAG ('N','k','o','o'),
    BIDIScript_Sundanese = SCRIPT_TAG ('S','u','n','d'),
    BIDIScript_Lepcha = SCRIPT_TAG ('L','e','p','c'),
    BIDIScript_Ol_Chiki = SCRIPT_TAG ('B','e','n','g'),
    BIDIScript_Vai = SCRIPT_TAG ('T','h','a','i'),
    BIDIScript_Saurashtra = SCRIPT_TAG ('S','a','u','r'),
    BIDIScript_Kayah_Li = SCRIPT_TAG ('K','a','l','i'),
    BIDIScript_Rejang = SCRIPT_TAG ('R','j','n','g'),
    BIDIScript_Lycian = SCRIPT_TAG ('L','y','c','i'),
    BIDIScript_Carian = SCRIPT_TAG ('C','a','r','i'),
    BIDIScript_Lydian = SCRIPT_TAG ('L','y','d','i'),
    BIDIScript_Cham = SCRIPT_TAG ('O','g','a','m'),
    BIDIScript_Avestan = SCRIPT_TAG ('A','v','s','t'),
    BIDIScript_Egyptian_Hieroglyphs = SCRIPT_TAG ('E','g','y','p'),
    BIDIScript_Samaritan = SCRIPT_TAG ('S','a','m','r'),
    BIDIScript_Lisu = SCRIPT_TAG ('L','i','s','u'),
    BIDIScript_Bamum = SCRIPT_TAG ('B','a','m','u'),
    BIDIScript_Javanese = SCRIPT_TAG ('J','a','v','a'),
    BIDIScript_Meetei_Mayek = SCRIPT_TAG ('M','t','e','i'),
    BIDIScript_Imperial_Aramaic = SCRIPT_TAG ('A','r','m','i'),
    BIDIScript_Old_South_Arabian = SCRIPT_TAG ('I','t','a','l'),
    BIDIScript_Inscriptional_Parthian = SCRIPT_TAG ('P','h','l','i'),
    BIDIScript_Inscriptional_Pahlavi = SCRIPT_TAG ('P','h','l','i'),
    BIDIScript_Old_Turkic = SCRIPT_TAG ('I','t','a','l'),
    BIDIScript_Kaithi = SCRIPT_TAG ('K','t','h','i'),
    BIDIScript_Batak = SCRIPT_TAG ('K','a','n','a'),
    BIDIScript_Brahmi = SCRIPT_TAG ('B','r','a','h'),
    BIDIScript_Mandaic = SCRIPT_TAG ('M','a','n','d'),
    BIDIScript_Chakma = SCRIPT_TAG ('C','a','k','m'),
    BIDIScript_Meroitic_Cursive = SCRIPT_TAG ('M','e','r','c'),
    BIDIScript_Meroitic_Hieroglyphs = SCRIPT_TAG ('M','e','r','c'),
    BIDIScript_Miao = SCRIPT_TAG ('P','l','r','d'),
    BIDIScript_Sharada = SCRIPT_TAG ('S','h','r','d'),
    BIDIScript_Sora_Sompeng = SCRIPT_TAG ('S','o','r','a'),
    BIDIScript_Takri = SCRIPT_TAG ('T','a','k','r'),
    BIDIScript_Caucasian_Albanian = SCRIPT_TAG ('A','g','h','b'),
    BIDIScript_Bassa_Vah = SCRIPT_TAG ('B','a','s','s'),
    BIDIScript_Duployan = SCRIPT_TAG ('D','u','p','l'),
    BIDIScript_Elbasan = SCRIPT_TAG ('E','l','b','a'),
    BIDIScript_Grantha = SCRIPT_TAG ('G','r','a','n'),
    BIDIScript_Pahawh_Hmong = SCRIPT_TAG ('H','m','n','g'),
    BIDIScript_Khojki = SCRIPT_TAG ('K','h','o','j'),
    BIDIScript_Linear_A = SCRIPT_TAG ('L','i','n','b'),
    BIDIScript_Mahajani = SCRIPT_TAG ('M','a','h','j'),
    BIDIScript_Manichaean = SCRIPT_TAG ('M','a','n','i'),
    BIDIScript_Mende_Kikakui = SCRIPT_TAG ('M','e','n','d'),
    BIDIScript_Modi = SCRIPT_TAG ('M','o','d','i'),
    BIDIScript_Mro = SCRIPT_TAG ('C','h','e','r'),
    BIDIScript_Old_North_Arabian = SCRIPT_TAG ('I','t','a','l'),
    BIDIScript_Nabataean = SCRIPT_TAG ('N','b','a','t'),
    BIDIScript_Palmyrene = SCRIPT_TAG ('P','a','l','m'),
    BIDIScript_Pau_Cin_Hau = SCRIPT_TAG ('S','a','u','r'),
    BIDIScript_Old_Permic = SCRIPT_TAG ('I','t','a','l'),
    BIDIScript_Psalter_Pahlavi = SCRIPT_TAG ('P','h','l','p'),
    BIDIScript_Siddham = SCRIPT_TAG ('S','i','d','d'),
    BIDIScript_Khudawadi = SCRIPT_TAG ('S','i','n','d'),
    BIDIScript_Tirhuta = SCRIPT_TAG ('T','i','r','h'),
    BIDIScript_Warang_Citi = SCRIPT_TAG ('W','a','r','a'),
    BIDIScript_Ahom = SCRIPT_TAG ('A','h','o','m'),
    BIDIScript_Anatolian_Hieroglyphs = SCRIPT_TAG ('H','l','u','w'),
    BIDIScript_Hatran = SCRIPT_TAG ('H','a','t','r'),
    BIDIScript_Multani = SCRIPT_TAG ('M','u','l','t'),
    BIDIScript_Old_Hungarian = SCRIPT_TAG ('I','t','a','l'),
    BIDIScript_SignWriting = SCRIPT_TAG ('S','g','n','w'),
    BIDIScript_Adlam = SCRIPT_TAG ('A','d','l','m'),
    BIDIScript_Bhaiksuki = SCRIPT_TAG ('B','h','k','s'),
    BIDIScript_Marchen = SCRIPT_TAG ('M','a','r','c'),
    BIDIScript_Newa = SCRIPT_TAG ('N','e','w','a'),
    BIDIScript_Osage = SCRIPT_TAG ('O','s','g','e'),
    BIDIScript_Tangut = SCRIPT_TAG ('T','a','n','g'),
    BIDIScript_Masaram_Gondi = SCRIPT_TAG ('G','o','n','m'),
    BIDIScript_Nushu = SCRIPT_TAG ('N','s','h','u'),
    BIDIScript_Soyombo = SCRIPT_TAG ('S','o','y','o'),
    BIDIScript_Zanabazar_Square = SCRIPT_TAG ('Z','a','n','b'),
    BIDIScript_Dogra = SCRIPT_TAG ('D','o','g','r'),
    BIDIScript_Gunjala_Gondi = SCRIPT_TAG ('G','o','n','g'),
    BIDIScript_Makasar = SCRIPT_TAG ('M','a','k','a'),
    BIDIScript_Medefaidrin = SCRIPT_TAG ('M','e','d','f'),
    BIDIScript_Hanifi_Rohingya = SCRIPT_TAG ('R','o','h','g'),
    BIDIScript_Sogdian = SCRIPT_TAG ('S','o','g','o'),
    BIDIScript_Old_Sogdian = SCRIPT_TAG ('I','t','a','l'),
    BIDIScript_Elymaic = SCRIPT_TAG ('E','l','y','m'),
    BIDIScript_Nandinagari = SCRIPT_TAG ('N','a','n','d'),
    BIDIScript_Nyiakeng_Puachue_Hmong = SCRIPT_TAG ('H','m','n','p'),
    BIDIScript_Wancho = SCRIPT_TAG ('W','c','h','o'),
};
#define CHARACTER_REMOVED_IN_RULE_X9 130
#define BIDI_COMPUTE_PARAGRAPH_EMBEDDING 999

enum BIDIDirection
{
    BIDIDirection_null,
    BIDIDirection_LTR,
    BIDIDirection_RTL,
};

// first, we build a list of script items.
// then, we turn the list into an array



struct ScriptItemNode_
{
    u32 char_pos;
    BIDIDirection direction;
    BIDIScript script;

    ScriptItemNode_ *next;
};

struct ScriptItemList_
{
    ScriptItemNode_ *first_item;
    ScriptItemNode_ *last_item;

    u32 num_generated;
};

struct ScriptItem
{
    u32 char_pos;
    BIDIDirection direction;
    BIDIScript script;
};

struct ScriptItems
{
    ScriptItem *items;
    u32 num_generated;
};

// NOTE:: NI means Neutral or Isolate formatting character (B, S, WS, ON, FSI, LRI, RLI, PDI)
enum BIDIType
{
    BIDIType_null,


    /*STRONG*/
    BIDIType_L,     //Left-to-Right                  LRM, most alphabetic, syllabic, Han ideographs, non-European or non-Arabic digits, ...
    BIDIType_R,     //Right-to-Left                  RLM, Hebrew alphabet, and related punctuation
    BIDIType_AL,    //Right-to-Left Arabic           ALM, Arabic, Thaana, and Syriac alphabets, most punctuation specific to those scripts, ...

    /*WEAK*/
    BIDIType_EN,    //European Number                European digits, Eastern Arabic-Indic digits, ...
    BIDIType_ES,    //European Number Separator      PLUS SIGN, MINUS SIGN
    BIDIType_ET,    //European Number Terminator     DEGREE SIGN, currency symbols, ...
    BIDIType_AN,    //Arabic Number                  Arabic-Indic digits, Arabic decimal and thousands separators, ...
    BIDIType_CS,    //Common Number Separator        COLON, COMMA, FULL STOP, NO-BREAK SPACE, ...
    BIDIType_NSM,   //Nonspacing Mark                Characters with the General_Category values: Mn (Nonspacing_Mark) and Me (Enclosing_Mark)
    BIDIType_BN,    //Boundary Neutral               Default ignorables, non-characters, and control characters, other than those explicitly given other types.

    /*NEUTRAL*/
    BIDIType_B,     //Paragraph Separator            PARAGRAPH SEPARATOR, appropriate Newline Functions, higher-level protocol paragraph determination
    BIDIType_S,     //Segment Separator              Tab
    BIDIType_WS,    //Whitespace                     SPACE, FIGURE SPACE, LINE SEPARATOR, FORM FEED, General Punctuation spaces, ...
    BIDIType_ON,    //Other Neutrals                 All other characters, including OBJECT REPLACEMENT CHARACTER

    /*EXPLICIT FORMATTING*/
    BIDIType_LRE,   //Left-to-Right Embedding        LRE
    BIDIType_LRO,   //Left-to-Right Override         LRO
    BIDIType_RLE,   //Right-to-Left Embedding        RLE
    BIDIType_RLO,   //Right-to-Left Override         RLO
    BIDIType_PDF,   //Pop Directional Format         PDF
    BIDIType_LRI,   //Left-to-Right Isolate          LRI
    BIDIType_RLI,   //Right-to-Left Isolate          RLI
    BIDIType_FSI,   //First Strong Isolate           FSI
    BIDIType_PDI,   //Pop Directional Isolate        PDI
};

#define LRI_CODEPOINT 0x2066  // LEFT-TO-RIGHT ISOLATE	
#define RLI_CODEPOINT 0x2067  // RIGHT-TO-LEFT ISOLATE
#define FSI_CODEPOINT 0x2068  // FIRST STRONG ISOLATE

#define PDI_CODEPOINT 0x2069  // POP DIRECTIONAL ISOLATE

#define LRE_CODEPOINT 0x202A  // LEFT-TO-RIGHT EMBEDDING
#define RLE_CODEPOINT 0x202B  // RIGHT-TO-LEFT EMBEDDING

#define LRO_CODEPOINT 0x202D  // LEFT-TO-RIGHT OVERRIDE
#define RLO_CODEPOINT 0x202E  // RIGHT-TO-LEFT OVERRIDE

#define PDF_CODEPOINT 0x202C  // POP DIRECTIONAL FORMATTING

#define LRM_CODEPOINT 0x200E  // LEFT-TO-RIGHT MARK
#define RLM_CODEPOINT 0x200F  // RIGHT-TO-LEFT MARK
#define ALM_CODEPOINT 0x061C  // ARRABIC LETTE MARK

#define MAX_EMBEDDING_DEPTH 125
#define DIRECTIONAL_STATUS_STACK_NUM_ENTRIES MAX_EMBEDDING_DEPTH + 2

enum DirectionalOverrideStatus
{
    DirectionalOverride_neutral,
    DirectionalOverride_RTL,
    DirectionalOverride_LTR,
};

struct DirectionalStatus
{
    u8 embedding_level;

    DirectionalOverrideStatus override_status;
    b32 isolate_status;
};

struct DirectionalStatusStack
{
    DirectionalStatus data[DIRECTIONAL_STATUS_STACK_NUM_ENTRIES];
    u32 entries_used;
};

struct BIDICharacterInfo
{
    u8 embedding_level;

    BIDIType type;
    BIDIType original_type;

    BIDIScript script;
};

struct BIDICharacter_String
{
    BIDICharacterInfo *ptr;
    u32 len;
};

struct ExplicitPassState
{
    DirectionalStatusStack *directional_status_stack;
    u32 overflow_isolate_count = 0;
    u32 overflow_embedding_count = 0;
    u32 valid_isolate_count = 0;
};

struct LevelRun
{
    u32 start;
    u32 len;

    u8 level;
    u32 seq_id;
};

struct LevelRunNode
{
    LevelRun *level_run;
    LevelRunNode *next;
};

struct IsolatingRunSequence
{
    BIDIType sos;
    BIDIType eos;

    u32 num_chars;
    u32 *char_indices;
    
    u8 level;
    
    IsolatingRunSequence *next;
};

struct LevelRunList
{
    LevelRunNode *first_run;
    LevelRunNode *last_run;

    u32 num_chars;
    u8 level;
    
    LevelRunList *next;
};

// LEGACY::
// previously we had a linked list of runs
// in each IsolatingRunSequence

// struct IsolatingRunSequence
// {
//     LevelRunNode *first_run;
//     LevelRunNode *last_run;
//     IsolatingRunSequence *next;

//     BIDIType sos;
//     BIDIType eos;
// };


enum BIDIBracketType
{
    BIDIBracketType_none,
    BIDIBracketType_open,
    BIDIBracketType_close,
};

struct BIDIBracketProperty
{
    UTF32 codepoint;

    UTF32 bidi_paired_bracket;
    BIDIBracketType type;
};

global BIDIBracketProperty bracket_properties[] =
{
    {0x0028,  0x0029,  BIDIBracketType_open},
    {0x0029,  0x0028,  BIDIBracketType_close},
    {0x005B,  0x005D,  BIDIBracketType_open},
    {0x005D,  0x005B,  BIDIBracketType_close},
    {0x007B,  0x007D,  BIDIBracketType_open},
    {0x007D,  0x007B,  BIDIBracketType_close},
    {0x0F3A,  0x0F3B,  BIDIBracketType_open},
    {0x0F3B,  0x0F3A,  BIDIBracketType_close},
    {0x0F3C,  0x0F3D,  BIDIBracketType_open},
    {0x0F3D,  0x0F3C,  BIDIBracketType_close},
    {0x169B,  0x169C,  BIDIBracketType_open},
    {0x169C,  0x169B,  BIDIBracketType_close},
    {0x2045,  0x2046,  BIDIBracketType_open},
    {0x2046,  0x2045,  BIDIBracketType_close},
    {0x207D,  0x207E,  BIDIBracketType_open},
    {0x207E,  0x207D,  BIDIBracketType_close},
    {0x208D,  0x208E,  BIDIBracketType_open},
    {0x208E,  0x208D,  BIDIBracketType_close},
    {0x2308,  0x2309,  BIDIBracketType_open},
    {0x2309,  0x2308,  BIDIBracketType_close},
    {0x230A,  0x230B,  BIDIBracketType_open},
    {0x230B,  0x230A,  BIDIBracketType_close},
    {0x2329,  0x232A,  BIDIBracketType_open},
    {0x232A,  0x2329,  BIDIBracketType_close},
    {0x2768,  0x2769,  BIDIBracketType_open},
    {0x2769,  0x2768,  BIDIBracketType_close},
    {0x276A,  0x276B,  BIDIBracketType_open},
    {0x276B,  0x276A,  BIDIBracketType_close},
    {0x276C,  0x276D,  BIDIBracketType_open},
    {0x276D,  0x276C,  BIDIBracketType_close},
    {0x276E,  0x276F,  BIDIBracketType_open},
    {0x276F,  0x276E,  BIDIBracketType_close},
    {0x2770,  0x2771,  BIDIBracketType_open},
    {0x2771,  0x2770,  BIDIBracketType_close},
    {0x2772,  0x2773,  BIDIBracketType_open},
    {0x2773,  0x2772,  BIDIBracketType_close},
    {0x2774,  0x2775,  BIDIBracketType_open},
    {0x2775,  0x2774,  BIDIBracketType_close},
    {0x27C5,  0x27C6,  BIDIBracketType_open},
    {0x27C6,  0x27C5,  BIDIBracketType_close},
    {0x27E6,  0x27E7,  BIDIBracketType_open},
    {0x27E7,  0x27E6,  BIDIBracketType_close},
    {0x27E8,  0x27E9,  BIDIBracketType_open},
    {0x27E9,  0x27E8,  BIDIBracketType_close},
    {0x27EA,  0x27EB,  BIDIBracketType_open},
    {0x27EB,  0x27EA,  BIDIBracketType_close},
    {0x27EC,  0x27ED,  BIDIBracketType_open},
    {0x27ED,  0x27EC,  BIDIBracketType_close},
    {0x27EE,  0x27EF,  BIDIBracketType_open},
    {0x27EF,  0x27EE,  BIDIBracketType_close},
    {0x2983,  0x2984,  BIDIBracketType_open},
    {0x2984,  0x2983,  BIDIBracketType_close},
    {0x2985,  0x2986,  BIDIBracketType_open},
    {0x2986,  0x2985,  BIDIBracketType_close},
    {0x2987,  0x2988,  BIDIBracketType_open},
    {0x2988,  0x2987,  BIDIBracketType_close},
    {0x2989,  0x298A,  BIDIBracketType_open},
    {0x298A,  0x2989,  BIDIBracketType_close},
    {0x298B,  0x298C,  BIDIBracketType_open},
    {0x298C,  0x298B,  BIDIBracketType_close},
    {0x298D,  0x2990,  BIDIBracketType_open},
    {0x298E,  0x298F,  BIDIBracketType_close},
    {0x298F,  0x298E,  BIDIBracketType_open},
    {0x2990,  0x298D,  BIDIBracketType_close},
    {0x2991,  0x2992,  BIDIBracketType_open},
    {0x2992,  0x2991,  BIDIBracketType_close},
    {0x2993,  0x2994,  BIDIBracketType_open},
    {0x2994,  0x2993,  BIDIBracketType_close},
    {0x2995,  0x2996,  BIDIBracketType_open},
    {0x2996,  0x2995,  BIDIBracketType_close},
    {0x2997,  0x2998,  BIDIBracketType_open},
    {0x2998,  0x2997,  BIDIBracketType_close},
    {0x29D8,  0x29D9,  BIDIBracketType_open},
    {0x29D9,  0x29D8,  BIDIBracketType_close},
    {0x29DA,  0x29DB,  BIDIBracketType_open},
    {0x29DB,  0x29DA,  BIDIBracketType_close},
    {0x29FC,  0x29FD,  BIDIBracketType_open},
    {0x29FD,  0x29FC,  BIDIBracketType_close},
    {0x2E22,  0x2E23,  BIDIBracketType_open},
    {0x2E23,  0x2E22,  BIDIBracketType_close},
    {0x2E24,  0x2E25,  BIDIBracketType_open},
    {0x2E25,  0x2E24,  BIDIBracketType_close},
    {0x2E26,  0x2E27,  BIDIBracketType_open},
    {0x2E27,  0x2E26,  BIDIBracketType_close},
    {0x2E28,  0x2E29,  BIDIBracketType_open},
    {0x2E29,  0x2E28,  BIDIBracketType_close},
    {0x3008,  0x3009,  BIDIBracketType_open},
    {0x3009,  0x3008,  BIDIBracketType_close},
    {0x300A,  0x300B,  BIDIBracketType_open},
    {0x300B,  0x300A,  BIDIBracketType_close},
    {0x300C,  0x300D,  BIDIBracketType_open},
    {0x300D,  0x300C,  BIDIBracketType_close},
    {0x300E,  0x300F,  BIDIBracketType_open},
    {0x300F,  0x300E,  BIDIBracketType_close},
    {0x3010,  0x3011,  BIDIBracketType_open},
    {0x3011,  0x3010,  BIDIBracketType_close},
    {0x3014,  0x3015,  BIDIBracketType_open},
    {0x3015,  0x3014,  BIDIBracketType_close},
    {0x3016,  0x3017,  BIDIBracketType_open},
    {0x3017,  0x3016,  BIDIBracketType_close},
    {0x3018,  0x3019,  BIDIBracketType_open},
    {0x3019,  0x3018,  BIDIBracketType_close},
    {0x301A,  0x301B,  BIDIBracketType_open},
    {0x301B,  0x301A,  BIDIBracketType_close},
    {0xFE59,  0xFE5A,  BIDIBracketType_open},
    {0xFE5A,  0xFE59,  BIDIBracketType_close},
    {0xFE5B,  0xFE5C,  BIDIBracketType_open},
    {0xFE5C,  0xFE5B,  BIDIBracketType_close},
    {0xFE5D,  0xFE5E,  BIDIBracketType_open},
    {0xFE5E,  0xFE5D,  BIDIBracketType_close},
    {0xFF08,  0xFF09,  BIDIBracketType_open},
    {0xFF09,  0xFF08,  BIDIBracketType_close},
    {0xFF3B,  0xFF3D,  BIDIBracketType_open},
    {0xFF3D,  0xFF3B,  BIDIBracketType_close},
    {0xFF5B,  0xFF5D,  BIDIBracketType_open},
    {0xFF5D,  0xFF5B,  BIDIBracketType_close},
    {0xFF5F,  0xFF60,  BIDIBracketType_open},
    {0xFF60,  0xFF5F,  BIDIBracketType_close},
    {0xFF62,  0xFF63,  BIDIBracketType_open},
    {0xFF63,  0xFF62,  BIDIBracketType_close},
};
#define BRACKET_PAIR_STACK_SIZE 63
struct BracketPairStackElement
{
    UTF32 bracket_character;
    u32 text_position;
};

struct BracketPair
{
    u32 open_pos;
    u32 close_pos;
    BracketPair *next;
};
