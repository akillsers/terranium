//TODO remove
// typedef void (*ScriptOutputCallbackFunction) (EditorMemory *memory, UTF8 *buffer, u32 buffer_size);
// internal void platform.RunScript(char *script_path, ScriptOutputCallbackFunction output_callback);

#define UNICODE 1

#if FULL_WIN_SDK

// #define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <dbghelp.h>

// not included in WIN32_LEAN_AND_MEAN
#include <commdlg.h> 
#include <cderr.h> // common dialog error codes
#include <dwmapi.h>
#include <shlobj.h>
#include <Shobjidl.h> // IDropTargetHelper
#include <knownfolders.h>

#include <VersionHelpers.h> // for testing windows 8.1 or greater (for dxgi frame waitable object)
#include <dxgi1_2.h>
#include <dxgi1_3.h>

#include <d3d11.h>

#include <shellscalingapi.h>
#include <d3d9.h>

#else

#include "min_windows.h"
#include "min_windows_shell.h"

#endif
// From windowsx.h::
#define GET_X_LPARAM(lp)                        ((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp)                        ((int)(short)HIWORD(lp))

#include "terranium_windows_scancodes.h"

// for vertex and fragment shaders
#include "sprite_vs.h"
#include "sprite_ps.h"

// use version 6 of common controls used for tooltips
// #pragma comment(linker,"\"/manifestdependency:type='win32' \
// name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
// processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define WIN_MAXIMUM_JOBS 1024

struct WIN_FramebufferHeader
{
    void *platform_pixels;
    u32 platform_height;
    u32 platform_width;
};

class WIN_DropTarget : public IDropTarget 
{
    // IDropTarget implementation
    HRESULT __stdcall DragEnter(IDataObject *data_object,
                                DWORD key_state,
                                POINTL cursor_position,
                                DWORD *effect) override;
    HRESULT __stdcall DragOver(DWORD key_state,
                               POINTL cursor_position,
                               DWORD *effect) override;
    HRESULT __stdcall DragLeave() override;
    HRESULT __stdcall Drop(IDataObject *data_object,
                           DWORD key_state,
                           POINTL cursor_position,
                           DWORD *effect) override;

    // IUnknown implemntation
    HRESULT __stdcall QueryInterface(const IID &,void **) override;
    ULONG __stdcall AddRef(void) override;
    ULONG __stdcall Release(void) override;
public:
    IDropTargetHelper *drop_target_helper;
    HWND linked_hwnd;
};

struct WIN_ImageDragState
{
    b32 is_dragging;
    HWND window;

    BITMAPINFO bitmap_info;
    //WIN_ImageBuffer image_buffer;
};

struct WIN_TooltipState
{
    HWND tip_hwnd;
    TOOLINFOW toolinfo;
};

typedef LONG (WINAPI *RtlVerifyVersionInfo_Func) (OSVERSIONINFOEXW *, ULONG, ULONGLONG);

struct D3D11_DeviceData
{
    HANDLE frame_latency_waitable_object;
    
    IDXGIFactory2 *factory;
    IDXGIOutput *output;
    ID3D11Device *device;
    ID3D11DeviceContext *device_context;
};

//TODO rename this something other than DEVICE data to avoid confusion?
struct D3D9_DeviceData
{
    IDirect3D9Ex *context;
};

struct D3D11_Data
{
    DXGI_SWAP_CHAIN_DESC1 swap_chain_desc;
    IDXGISwapChain1 *swap_chain;
    ID3D11Texture2D *backbuffer;
    ID3D11RenderTargetView *backbuffer_view;

    ID3D11Texture2D *draw_texture;
    ID3D11Texture2D *staging_texture;
    D3D11_MAPPED_SUBRESOURCE staging_texture_mapped;

    ID3D11BlendState *blend_state;
    ID3D11SamplerState *sampler_state;
    ID3D11ShaderResourceView *texture_view;
    ID3D11PixelShader *pixel_shader;
    ID3D11InputLayout *input_layout;
    ID3D11VertexShader *vertex_shader;
    ID3D11Buffer *vertex_buffer;
    D3D11_VIEWPORT viewport;
};

struct D3D9ScreenTexture
{
    IDirect3DTexture9 *draw;
    IDirect3DTexture9 *staging;
    D3DLOCKED_RECT locked_staging;

    u32 width;
    u32 height;
};

struct D3D9_Data
{
    IDirect3DDevice9Ex *device;
    D3DPRESENT_PARAMETERS present_params;

    LPDIRECT3DVERTEXBUFFER9 vbuffer;
    LPDIRECT3DVERTEXBUFFER9 texture_vbuffer;

    D3D9ScreenTexture screen_texture;
};


enum WIN_SwapChainBackend
{
    WIN_SwapChain_null,
    WIN_SwapChain_d3d11,
    WIN_SwapChain_d3d9,
};

struct WIN_WindowStateContent
{
    i32 restored_x;
    i32 restored_y;
    i32 restored_width;
    i32 restored_height;

    i32 maximized;
};

enum WIN_SpecialDirectory
{
    // on windows this is the "Terranium" folder in %appdata%/local/
    WIN_SpecialDirectory_ApplicationData,
};

struct WIN_WindowData
{
    WIN_WindowData *next;

    HWND hwnd;
    Framebuffer *framebuffer;

    PlatformWindowSizeState size_state;
    b32 resized;
    b32 moved;

    PlatformTitlebarType titlebar_type;
    
    b32 use_native_frame;
    b32 has_focus;
    
	WIN_DropTarget drop_target;    
    union
    {
        D3D11_Data *d3d11_data;
        D3D9_Data *d3d9_data;
    };

};

enum WIN_AppbarEdges
{
    WIN_AppbarEdge_left,
    WIN_AppbarEdge_right,
    WIN_AppbarEdge_top,
    WIN_AppbarEdge_down,
};

struct WIN_WorkEntry
{
    PlatformWorkEntry *entry;
    PlatformWorkQueue *queue;
};

// NOTE: d3d11, as far as my research shows, is unable to support blending
//       the swap chain with the content underneath
//
//       so, we plan to use d3d11 + directcomposition on windows 8+
//       and d3d9ex on windows 7+

struct
{
    MemoryArena frame_arena;
    MemoryArena permanent_arena;
    
    FileError last_file_error;

    UINT_PTR move_timer;

    HHOOK mouse_hook;

    b32 editor_resizing;
    b32 running;
    b32 animating;
    b32 dpi_changed;

    void *main_fiber;
    void *message_fiber;
    
    EditorMemory editor_memory;

    WIN_SwapChainBackend swap_chain_backend;
    union
    {
        D3D11_DeviceData *d3d11;
        D3D9_DeviceData *d3d9;
    };

    void (*CreateResizeSwapChain) (WIN_WindowData *window);
    void (*Present) (WIN_WindowData *window);

    Input input;

    u64 perf_startup; // this is mostly for debug (of multithreading) purposes
    u64 perf_freq;

    void *transient_storage;
    u64 transient_storage_size;

    // reset every frame
    // filled in if a WM_CHAR gives us a surrogate
    UTF16 surrogate;

    WIN_ImageDragState image_drag_state;
    
    CursorIconType cursor_icon;

    PlatformCommandLine command_line;
    
    u32 dpi;
    
    COLORREF inactive_frame_color;
    COLORREF accent_color;
    HKEY dwm_key;
    HANDLE dwm_key_change_event;

    RtlVerifyVersionInfo_Func RtlVerifyVerifyVersionInfo_;

    union
    {
        WIN_WindowData *first_window;
        WIN_WindowData *main_window;
    };
    WIN_WindowData *last_window;
    WIN_DropTarget drop_target;    
        
    PlatformFileHandle window_state;
    
    MemoryArena *thread_arena;

    u32 num_worker_threads;
    HANDLE jobs_available;
    volatile u32 next_entry_to_do;
    volatile u32 next_write_index;
    WIN_WorkEntry jobs[WIN_MAXIMUM_JOBS];
}g_win32;
#define RtlVerifyVersionInfo g_win32.RtlVerifyVerifyVersionInfo_


LPWSTR WIN_cursor_icon_to_IDC[] =
{
    IDC_ARROW,
    IDC_IBEAM,
    IDC_NO,
    
    IDC_SIZEALL,
    IDC_SIZENESW,
    IDC_SIZENWSE,
    IDC_SIZENS,
    IDC_SIZEWE,
};


#if INTERNAL
#define PERF_START(name) u64 PERF_##name = WIN_QueryPerfCount();
#define PERF_END(name) PERF_##name = WIN_QueryPerfCount() - PERF_##name;
#else
#define PERF_START(name)
#define PERF_END(name)
#endif


// forward declarations
inline b32 WIN_IsCustomDrawingTopBorder(WIN_WindowData *window);
internal void WIN_ResizeFramebuffer(WIN_WindowData *window);
inline b32 WIN_IsCustomDrawingSideAndBottomBorders(WIN_WindowData *window);
inline u64 WIN_QueryPerfCount();
internal void WIN_UpdateFramebuffer(WIN_WindowData *window, void *bits, u32 pitch, PixelFormat format);
VOID CALLBACK WIN_DWMNotifyWaitCallback(void *param, BOOLEAN wait_fired);
internal Recti WIN_GetClientRect(WIN_WindowData *window);
