inline MemoryBlockFooter *
GetFooter(MemoryArena *arena)
{
    MemoryBlockFooter *ret =  (MemoryBlockFooter *)((u8 *)arena->base + arena->size);
    return ret;
}

inline b32
FreeLastBlock(MemoryArena *arena)
{
    b32 blocks_are_left = false;

    void *free = arena->base;
    
    MemoryBlockFooter *footer = GetFooter(arena);

    arena->base = footer->base;
    arena->size = footer->size;
    arena->used = footer->used;

    MemoryBlockFooter *new_footer = GetFooter(arena);
    blocks_are_left = (footer->base != 0);

    platform.FreeMemory(free);

    return blocks_are_left;
}

// TODO do we want to combine this MemoryArenaSnapshot with the
//      TemporaryMemory concept? (maybe store a snapshot in tempmem?)

inline void
UndoArenaToSnapshot(MemoryArena *arena, MemoryArenaSnapshot snapshot)
{
    while(arena->base != snapshot.base)
    {
        FreeLastBlock(arena);
    }

    Assert(arena->used >= snapshot.used);
    arena->used = snapshot.used;
}

inline MemoryArenaSnapshot
TakeSnapshot(MemoryArena *arena)
{
    MemoryArenaSnapshot snapshot;

    snapshot.base = arena->base;
    snapshot.used = arena->used;

    return snapshot;
}

internal TemporaryMemory
BeginTemporaryMemory(MemoryArena *arena)
{
    TemporaryMemory result;

    result.arena = arena;
    result.snapshot = TakeSnapshot(arena);
    arena->temp_count++;
    return result;
}

internal void
EndTemporaryMemory(TemporaryMemory temp_mem)
{
    UndoArenaToSnapshot(temp_mem.arena, temp_mem.snapshot);

    Assert(temp_mem.arena->temp_count > 0);
    temp_mem.arena->temp_count--;
}

internal u64
GetTotalMemoryUsed(MemoryArena *arena)
{
    u64 total_used = 0;

    total_used += arena->used;

    MemoryBlockFooter *footer = GetFooter(arena);

    while(footer->base != 0)
    {
        MemoryArena previous_arena = {};
        previous_arena.base = footer->base;
        previous_arena.size = footer->size;
        previous_arena.used = footer->used;

        total_used += previous_arena.used;

        footer = GetFooter(&previous_arena);
    }

    return total_used;
}

internal void *
PushSize(MemoryArena *arena, u64 size, u64 alloc_flags)
{
    TIMED_BLOCK;
    void *result = 0;
    u64 alloc_size;
    
    // aligns the data on 16-byte boundaries
    if(alloc_flags & PushSize_align_16)
    {
        u64 start = Align16(arena->used);
        alloc_size = (start - arena->used) + size;
    }
    else
    {
        alloc_size = size;
    }
    
    if((arena->used + alloc_size) > arena->size)
    {
        if(!(arena->flags & MemoryArena_no_grow))
        {
            if(!arena->min_block_size)
            {
                arena->min_block_size = Megabytes(1);
            }
        
            MemoryBlockFooter save = {};
            save.base = arena->base;
            save.size = arena->size;
            save.used = arena->used;
        
            u64 block_size = Max(size + sizeof(MemoryBlockFooter), arena->min_block_size);
            arena->size = block_size - sizeof(MemoryBlockFooter);
            // TODO handmade hero:: how is platform.AllocateMemory encapsulated? where is Platform defined?
            arena->base = platform.AllocateMemory(block_size);
            arena->used = 0;
            
            MemoryBlockFooter *footer = GetFooter(arena);
            *footer = save;
        }
        else
        {
            Assert(!"Out of memory!");
        }
    }
    
    if(arena->used + alloc_size <= arena->size)
    {
        if(alloc_flags & PushSize_align_16)
        {
            result = (u8 *)arena->base + Align16(arena->used);
        }
        else
        {
            result = (u8 *)arena->base + arena->used;
        }

        arena->used += alloc_size;
    }
    return result;
}

internal void
FreeAllBlocks(MemoryArena *arena)
{
    Assert(arena->base);

    b32 blocks_are_left = true;
    while(blocks_are_left)
    {
        blocks_are_left = FreeLastBlock(arena);
    }
}

internal void
ResetArena(MemoryArena *arena)
{
    if(arena->base)
    {
        FreeAllBlocks(arena);
        arena->used = 0;
    }
}

#define BootstrapPushStruct(struct_name, offset_to_arena)               \
    (struct_name *) BootstrapPushSize(sizeof(struct_name), offset_to_arena)

internal void*
BootstrapPushSize(u64 size, u64 offset_to_arena)
{
    MemoryArena bootstrap = {};
    void *result = PushSize(&bootstrap, size);
    *(MemoryArena *)((u8 *)result + offset_to_arena) = bootstrap;
    return result;
}

#if STRING_LIB_DEFINED
internal String
AllocateString(MemoryArena *arena, u32 size)
{
    String ret;
    ret.ptr = PushCount(arena, UTF8, size);
    ret.len = 0;
    return ret;
}
#endif
