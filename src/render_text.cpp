internal ShapedTextMetaInfo *
GetShapedTextMeta(ShapedText *shaped_text)
{
    ShapedTextMetaInfo *result;
    result = (ShapedTextMetaInfo *)((u8 *)shaped_text - sizeof(ShapedTextMetaInfo));

    return result;
}

inline void
InvalidateShapeData_(ShapedText *shaped_text)
{
    ShapedTextMetaInfo *meta = GetShapedTextMeta(shaped_text);
    meta->is_old_data = true;
}

internal u32
GetLineHash(MultilineShapedTextCache *cache, u32 line)
{
    u32 result;
    result = line % cache->hash_table_max_elements;
    return result;
}

internal ShapedTextBucket *
GetShapedTextBucketHackedly(MultilineShapedTextCache *cache, u32 line)
{
    ShapedTextBucket *result = 0;

    for(int i = 0;
        i < cache->hash_table_max_elements;
        i++)
    {
        ShapedTextBucket *bucket = cache->hash_table + i;
        if(bucket->one_past_line_number == line + 1)
        {
            result = bucket;
            break;
        }
    }

    return result;
}

internal ShapedTextBucket *
GetShapedTextBucket(MultilineShapedTextCache *cache, u32 line, b32 force_create = false)
{
    TIMED_BLOCK;
    ShapedTextBucket *result = 0;
    u32 hash = GetLineHash(cache, line);
    u64 probe_index = hash;

    for(;;)
    {
        ShapedTextBucket *shaped_text_bucket = cache->hash_table + probe_index;
        if(shaped_text_bucket->one_past_line_number == 0)
        {
            // the line was never shaped anyway
            if(force_create)
            {
                shaped_text_bucket->data = 0;
                shaped_text_bucket->one_past_line_number = line + 1;

                result = shaped_text_bucket;
            }
            
            break;
        }
        else if(line + 1 == shaped_text_bucket->one_past_line_number)
        {
            result = shaped_text_bucket;
            break;
        }

        probe_index = (probe_index + 1) % cache->hash_table_max_elements;

        if(probe_index == hash)
        {
            // this hash table is saturated but our line isn't in here
            if(force_create)
            {
                InvalidateShapeData_(shaped_text_bucket->data);
                
                shaped_text_bucket->one_past_line_number = line + 1;
                shaped_text_bucket->data = 0;
                
                result = shaped_text_bucket;
            }

            break;
        }
    }

    return result;
}

internal void
DeleteTextBucket(MultilineShapedTextCache *cache, ShapedTextBucket *delete_bucket)
{
    u32 delete_index = delete_bucket - cache->hash_table;
    delete_bucket->one_past_line_number = 0;
    delete_bucket->data = 0;

    u32 probe_index = (delete_index + 1) % cache->hash_table_max_elements;
    for(;;)
    {
        ShapedTextBucket *bucket = cache->hash_table + probe_index;

        if(bucket->one_past_line_number == 0)
        {
            break;
        }
        
        u32 key_distance = (probe_index - GetLineHash(cache, bucket->one_past_line_number - 1) +
                            cache->hash_table_max_elements) % cache->hash_table_max_elements;

        u32 shift_distance = probe_index - delete_index;
        if(shift_distance <= key_distance)
        {
            bucket->one_past_line_number = 0;
            bucket->data = 0;

            delete_bucket = bucket;
            delete_index = delete_bucket - cache->hash_table;
        }

        probe_index = (probe_index + 1) % cache->hash_table_max_elements;
    }
}


internal b32
InvalidateShapedLine(MultilineShapedTextCache *cache, u32 line)
{
    b32 success = false;
    ShapedTextBucket *shaped_text_bucket = GetShapedTextBucket(cache, line);

    if(shaped_text_bucket)
    {
        Assert(shaped_text_bucket->data);

        InvalidateShapeData_(shaped_text_bucket->data);

        DeleteTextBucket(cache, shaped_text_bucket);
        success = true;
    }

    platform.Info("data on line %u is invalidated (success: %s)\n", line, success ? "true" : "false");

    return success;
}

inline b32
InvalidateShapedLine(RenderText *render_text, u32 line)
{
    return InvalidateShapedLine(render_text->shape_cache, line);
}

internal void
InvalidateTextCache(RenderText *render_text)
{
	if(render_text->shape_cache->memory != 0)
	{
		for(int line = 0;
			line < render_text->buffer->num_lines;
			line++)
		{
			InvalidateShapedLine(render_text, line);
		}	
	}
}


enum EvictRegion
{
    EvictRegion_1 = 0,
    EvictRegion_2 = 1,
};

internal umm
_ShapeCacheEvictItems(MultilineShapedTextCache *cache, umm upper, EvictRegion region)
{
    umm lower = (region == EvictRegion_1) ? 0 : cache->data.start2;

    // NOTE: upper is an absolute offset from the base of the cache
    // platform.Info("_ShapeCacheEvictItems: lower %u, upper %u, region %u\n", lower, upper, region + 1);

    umm region_bytes = (region == EvictRegion_1) ? cache->data.len1 : cache->data.len2;

    umm evict_offset = lower;
    for(;;)
    {
        umm bytes_evicted = evict_offset - lower;
        if(bytes_evicted == region_bytes)
        {
            // We evicted the entire region
            break;
        }

        Assert(bytes_evicted < region_bytes);
        
        ShapedTextMetaInfo *evict_data = (ShapedTextMetaInfo *)((u8 *)cache->data.base + evict_offset);
        u64 evict_length = evict_data->storage_size;
        

        if(evict_data->is_old_data == false)
        {
            // Perform the eviction
            // platform.Info("_ShapeCacheEvictItems: evicting %u\n", evict_data->line);
            b32 success = InvalidateShapedLine(cache, evict_data->line);
            Assert(success);
        }

        evict_offset += evict_length;

        if(evict_offset > upper)
        {
            // We've evicted everything that is necessary;
            break;
        }
    }

    return evict_offset;
}

internal void
ResizeShapeCache(MultilineShapedTextCache *cache, umm size_requested)
{
    _ShapeCacheEvictItems(cache, cache->data.len1, EvictRegion_1);
    _ShapeCacheEvictItems(cache, cache->data.start2 + cache->data.len2, EvictRegion_2);

    if(cache->memory)
    {
        platform.FreeMemory(cache->memory);
    }

    // TODO round up the alloc size to allocation graularity
    //      boundaries to avoid losing space

    cache->hash_table_max_elements = 5000;
    u64 hash_table_bytes = cache->hash_table_max_elements * sizeof(ShapedTextBucket);
    u64 alloc_size = size_requested + hash_table_bytes;

    cache->memory = platform.AllocateMemory(alloc_size);

    cache->hash_table = (ShapedTextBucket *)cache->memory;

    cache->last = 0;
    cache->data.base = (u8 *)cache->memory + hash_table_bytes;
    cache->data.capacity = alloc_size - hash_table_bytes;

    cache->data.len1 = 0;
    cache->data.start2 = 0;
    cache->data.len2 = 0;
}

internal MemoryArena
GetShapedTextStorageArena(MultilineShapedTextCache *cache, u32 storage_space_requested, u32 line)
{
    TIMED_BLOCK;
    u32 storage_space_required = storage_space_requested + sizeof(ShapedTextMetaInfo);
    if(storage_space_required > cache->data.capacity)
    {
        platform.Info("Resizing the shape cache (new size: %u)\n", storage_space_required);
        ResizeShapeCache(cache, storage_space_required);
    }

    u32 alloc_start_offset = 0;

    if(cache->data.capacity - cache->data.len1 >= storage_space_required)
    {
        alloc_start_offset = cache->data.len1;
        cache->data.len1 += storage_space_required;

        // Check for overlap with region 2 and evict items as necessary
        if(cache->data.len2 != 0)
        {
            u32 alloc_end = alloc_start_offset + storage_space_required;
            if(alloc_end > cache->data.start2)
            {
                // TODO: evict items in region 2 as necessary
                umm evict_to = _ShapeCacheEvictItems(cache, alloc_end, EvictRegion_2);

                // and update start2 and len2
                Assert(evict_to <= cache->data.start2 + cache->data.len2);
                cache->data.len2 = cache->data.start2 + cache->data.len2 - evict_to;
                cache->data.start2 = evict_to;
            }
        }
    }
    else
    {
        // Wrap around
        if(cache->data.len2 == 0)
        {
            // TODO: evict items in region 1 as necessary
            //       (from 0 to storage_space_required)
            umm evict_to = _ShapeCacheEvictItems(cache, storage_space_required, EvictRegion_1);

            if(evict_to == cache->data.len1)
            {
                // Everything in region 1 is evicted, so region 2 is empty
            }
            else
            {
                // Set the region 2 to start from the first item in region 1 that wasn't
                // evicted to the end of region 1

                cache->data.start2 = evict_to;
                cache->data.len2 = cache->data.len1 - evict_to;
            }
        }
        else
        {
            if(storage_space_required > cache->data.start2)
            {
                // TODO: evict all of region 1
                _ShapeCacheEvictItems(cache, cache->data.len1, EvictRegion_1);

                //       evict items in region 2 as necessary 
                umm evict_upper_bound = storage_space_required;
                umm evict_to = _ShapeCacheEvictItems(cache, evict_upper_bound, EvictRegion_2);

                //       and update start2 and len2
                Assert(evict_to <= cache->data.start2 + cache->data.len2)
                cache->data.len2 = cache->data.start2 + cache->data.len2 - evict_to;
                cache->data.start2 = evict_to;
            }
            else
            {
                // TODO: evict all of region 2
                _ShapeCacheEvictItems(cache, cache->data.start2 + cache->data.len2, EvictRegion_2);
                
                //       evict region 1 as necessary (from 0 to storage_space_required)
                umm evict_to = _ShapeCacheEvictItems(cache, storage_space_required, EvictRegion_1);

                if(evict_to == cache->data.len1)
                {
                    // Everything in region 1 is evicted, so region 2 is empty
                    cache->data.len2 = 0;
                }
                else
                {
                    // Set region 2 to start from the first item in region 1 that wasn't evicted
                    // to the end of region 1

                    Assert(evict_to <= cache->data.start2 + cache->data.len2);
                    cache->data.start2 = evict_to;
                    cache->data.len2 = cache->data.len1 - evict_to;
                }
            }
        }

        // Region 1 now only stores the new item.
        // Update len1 to reflect this
        cache->data.len1 = storage_space_required;
    }
    
    
    void *alloc_start = (u8 *)cache->data.base + alloc_start_offset;

    ShapedTextMetaInfo *meta_info = (ShapedTextMetaInfo *)alloc_start;
    meta_info->storage_size = storage_space_required;
    meta_info->line = line;
    meta_info->is_old_data = false;
    cache->last = meta_info;

    MemoryArena arena = {};
    arena.base = (u8 *)alloc_start + sizeof(ShapedTextMetaInfo);
    arena.flags |= MemoryArena_no_grow;
    arena.size = storage_space_requested;

    ZeroSize(arena.base, arena.size);

    platform.Info("CACHE for line %u:: (start %I64u, length %I64u) region 1 start 0 len %I64u, region 2 start %I64u len %I64u\n",
          line, alloc_start_offset, storage_space_required, cache->data.len1, cache->data.start2, cache->data.len2);
    
    return arena;
}

internal MemoryArena
GetShapedTextStorageArena(SingleLineShapedTextCache *cache, u32 storage_space_required)
{
    ResetArena(&cache->arena);

    MemoryArena result;
    result.base = PushSize(&cache->arena, storage_space_required);
    result.size = storage_space_required;
    result.used = 0;

    return result;
}

internal ShapedTextPrecursor
StartShapeString(RenderText *render_text, String line_string, Font *font, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    ShapedTextPrecursor result = {};
    
    if(font->shape_engine_font == 0)
    {
        font->shape_engine_font = Harfbuzz_CreateFont(font);
    }

    if(line_string.len == 0)
    {
        result.storage_space_required = sizeof(ShapedText);
    }
    else
    {
        result.utf32_line_string = StringToUTF32(line_string, temp_arena);

        // 300 000 cy
        result.script_items = ItemizeText(result.utf32_line_string, 0 /*paragraph embedding level*/, temp_arena);
    
        result.num_runs = result.script_items.num_generated;
        
        // ShapedText struct (per-text)
        result.storage_space_required += sizeof(ShapedText);

        // visual-to-logical and logical-to-visual mapping arrays (per-text)
        result.storage_space_required += result.num_runs * sizeof(u32);
        result.storage_space_required += result.num_runs * sizeof(u32);
        
        // ShapedTextRun structs (per-run)
        result.storage_space_required += result.num_runs * sizeof(ShapedTextRun);

        result.engine_data = PushCount(temp_arena, EngineShapedRunData *, result.num_runs);

        for(int i = 0;
            i < result.num_runs;
            i++)
        {
            u32 chars_in_run = result.script_items.items[i + 1].char_pos - result.script_items.items[i].char_pos;

            UTF32_String run_string = {};
            run_string.ptr = result.utf32_line_string.ptr + result.script_items.items[i].char_pos;
            run_string.len = result.script_items.items[i + 1].char_pos - result.script_items.items[i].char_pos;
            
            result.engine_data[i] = HarfBuzz_ShapeTextRun(run_string, &result.script_items.items[i],
                                                          render_text->font->shape_engine_font, temp_arena);
            
            u32 num_clusters = HarfBuzz_GetNumClusters(result.engine_data[i]);
            u32 num_glyphs = HarfBuzz_GetNumGlyphs(result.engine_data[i]);

            result.storage_space_required += sizeof(ClusterInfo) * num_clusters;
            result.storage_space_required += sizeof(GlyphInfo) * num_glyphs;
        }

        // character to first glyph array (per-character)
        result.storage_space_required += sizeof(u32) * result.utf32_line_string.len;
    }

    return result;
}

internal ShapedText *
FinishShapeString(ShapedTextPrecursor *precursor, MemoryArena *arena)
{
    TIMED_BLOCK;
    ShapedText *result;
    result = PushType(arena, ShapedText);
    
    result->visual_to_logical = PushCount(arena, u32, precursor->num_runs);
    result->logical_to_visual = PushCount(arena, u32, precursor->num_runs);
        
    CalculateRunOrderMapping(precursor->script_items,
                             result->visual_to_logical,
                             result->logical_to_visual);

    result->num_runs = precursor->num_runs;
    result->runs = PushCount(arena, ShapedTextRun, precursor->num_runs);

    result->num_chars = 0;
    
    for(u32 i = 0;
        i < result->num_runs;
        i++)
    {
        ShapedTextRun *run = result->runs + i;

        run->RTL = (precursor->script_items.items[i].direction == BIDIDirection_RTL);
        run->chars_before_run = result->num_chars;
        run->num_chars = precursor->script_items.items[i + 1].char_pos - precursor->script_items.items[i].char_pos;

        run->num_glyphs = HarfBuzz_GetNumGlyphs(precursor->engine_data[i]);
        run->num_clusters = HarfBuzz_GetNumClusters(precursor->engine_data[i]);
        run->glyph = PushCount(arena, GlyphInfo, run->num_glyphs);
        run->cluster = PushCount(arena, ClusterInfo, run->num_clusters);
        run->character_to_cluster = PushCount(arena, u32, run->num_chars);
            
        HarfBuzz_PopulateRun(precursor->engine_data[i], run);

        result->num_chars += run->num_chars;
    }

    // hard-set any trailing newline characters to have EM advance width
    // TODO: consider doing the hardsetting?
#if 0
    u32 char_index = precursor->utf32_line_string.len - 1;
    if(precursor->utf32_line_string.ptr)
    {
        for(;;)
        {
            UTF32 codepoint = precursor->utf32_line_string.ptr[char_index];

            // TODO handle Unicode line separators??
            if(codepoint == '\n' ||
               codepoint == '\r')
            {
                ShapedTextRun *last_run = result->runs + result->num_runs - 1;
                Assert(last_run->num_chars >= precursor->utf32_line_string.len - char_index);

                u32 char_index_in_run = char_index - last_run->chars_before_run;
                u32 glyph_index = last_run->character_to_first_glyph[char_index_in_run];

                // last_run->total_width -= last_run->glyph[glyph_index].advance;
                // last_run->glyph[glyph_index].advance = font_em_width;
            }
            else
            {
                break;
            }

            if(char_index > 0)
            {
                char_index--;
            }
            else
            {
                break;
            }
        }
    }
#endif
    for(u32 i = 0;
        i < result->num_runs;
        i++)
    {
        HarfBuzz_FreeEngineRunData(precursor->engine_data[i]);
    }

    Assert(result->num_chars == precursor->utf32_line_string.len);
    Assert(arena->used == arena->size);

    return result;
}

internal void
ShapeLine(RenderText *render_text, u32 line, MemoryArena *temp_arena)
{
    TIMED_BLOCK;
    ShapedText *shaped_text = 0;

    // if(bytes_in_line)
    // {
    u32 bytes_in_line = GetNumBytesInLine(render_text->buffer, line);
    UTF8 *line_contents = PushCount(temp_arena, UTF8, bytes_in_line);

    u32 bytes_copied = CopyLineContents(render_text->buffer, line_contents, bytes_in_line, line);
    Assert(bytes_copied == bytes_in_line);

    String line_string = MakeString(line_contents, bytes_in_line);

    ShapedTextPrecursor shaped_text_precursor = StartShapeString(render_text, line_string, render_text->font, temp_arena);

    MemoryArena arena = GetShapedTextStorageArena(render_text->shape_cache,
                                                  shaped_text_precursor.storage_space_required, line); //600 000 cy
    shaped_text = FinishShapeString(&shaped_text_precursor, &arena);
    // }
    // else
    // {
    //     u32 required_size = sizeof(ShapedText);
    //     MemoryArena arena = GetShapedTextStorageArena(render_text->shape_cache, required_size, line);

    //     shaped_text = PushType(&arena, ShapedText);
    //     *shaped_text = {};
    // }

    // TODO what if we run out of space on the hash table?
    ShapedTextBucket *line_bucket = GetShapedTextBucket(render_text->shape_cache, line, true);
    line_bucket->data = shaped_text;
}

internal ShapedText *
GetShapedLine(RenderText *render_text, u32 line, MemoryArena *temp_arena)
{
    TIMED_BLOCK;

    AcquireTicketLock(&render_text->ticket_lock);
    ShapedText *result = 0;

    MultilineShapedTextCache *cache = render_text->shape_cache;

    if(cache->memory == 0)
    {
        u64 alloc_size = Megabytes(10);
        ResizeShapeCache(cache, alloc_size);
    }

    ShapedTextBucket *shaped_text_bucket = GetShapedTextBucket(cache, line);

    if(!shaped_text_bucket)
    {
        ShapeLine(render_text, line, temp_arena);
        shaped_text_bucket = GetShapedTextBucket(cache, line);
    }

    ReleaseTicketLock(&render_text->ticket_lock);

    u32 characters_in_line = GetRawOnePastLastLineChar(render_text->buffer, line) - LinePos(render_text->buffer , line);
    Assert(characters_in_line == shaped_text_bucket->data->num_chars);

    result = shaped_text_bucket->data;
    Assert(result);

    return result;
}

internal ShapedText *
GetShapedText(RenderText *render_text, MemoryArena *temp_arena)
{
    ShapedText *result = {};
    SingleLineShapedTextCache *cache = render_text->single_line_shape_cache;

    if(cache->is_valid)
    {
        result = (ShapedText *)cache->arena.base;
    }
    else
    {
        ShapedTextPrecursor shaped_text_precursor = StartShapeString(render_text, render_text->string, render_text->font, temp_arena);
        MemoryArena arena = GetShapedTextStorageArena(cache, shaped_text_precursor.storage_space_required);
        result = FinishShapeString(&shaped_text_precursor, &arena);

        cache->is_valid = true;
    }

    return result;
}

internal void
ShiftCachedLinesUpward(RenderText *render_text, u32 start_line, u32 shift_distance = 1)
{
    // No shifting required, so we're done.
    if(shift_distance == 0) { return; }
    
    // move all the shaped text data up one line
    u32 one_past_end_line =
        (render_text->buffer->num_lines > shift_distance) ? (render_text->buffer->num_lines - shift_distance) : 0;


    for(u32 line = start_line;
        line < one_past_end_line;
        line++)
    {
        ShapedTextBucket *next_line_bucket = GetShapedTextBucket(render_text->shape_cache, line + shift_distance);

        if(next_line_bucket)
        {
            platform.Info("data on line %u is moved up to line %u\n", line + shift_distance, line);
            ShapedText *shaped_text = next_line_bucket->data;

            ShapedTextMetaInfo *meta = GetShapedTextMeta(shaped_text);
            meta->line -= shift_distance;
            
            ShapedTextBucket *line_bucket = GetShapedTextBucket(render_text->shape_cache, line, true);
            if(line_bucket->data)
            {
                InvalidateShapeData_(line_bucket->data);
            }

            line_bucket->data = shaped_text;

            DeleteTextBucket(render_text->shape_cache, next_line_bucket);
        }
        else
        {
            b32 success = InvalidateShapedLine(render_text->shape_cache, line);
        }
    }

    for(u32 line = one_past_end_line;
        line < render_text->buffer->num_lines;
        line++)
    {
        b32 success = InvalidateShapedLine(render_text->shape_cache, line);
    }
}

internal void
ShiftCachedLinesDownward(RenderText *render_text, u32 min_line, u32 shift_distance = 1)
{
    // No shifting required, so we're done
    if(shift_distance == 0) { return; }

    // move all the shaped text data down one line
    for(u32 line = render_text->buffer->num_lines - 1;
        line > min_line;
        line--)
    {
        ShapedTextBucket *prev_line_bucket;
        if(line >= shift_distance)
        {
            prev_line_bucket = GetShapedTextBucket(render_text->shape_cache, line - shift_distance);
        }
        else
        {
            prev_line_bucket = 0;
        }

        if(prev_line_bucket)
        {
            ShapedTextMetaInfo *meta = GetShapedTextMeta(prev_line_bucket->data);
            meta->line += shift_distance;

            ShapedTextBucket *line_bucket = GetShapedTextBucket(render_text->shape_cache, line, true/*force create*/);
            if(line_bucket->data)
            {
                InvalidateShapeData_(line_bucket->data);
            }

            line_bucket->data = prev_line_bucket->data;

            DeleteTextBucket(render_text->shape_cache, prev_line_bucket);
        }
        else
        {
            InvalidateShapedLine(render_text->shape_cache, line);
        }
    }
}

internal u32
GetLinePixelHeight(RenderText *render_text)
{
    u32 result;
    result = render_text->line_height * render_text->font->metrics->pixel_size;

    return result;
}

internal FractionalNumber
GetLineWidth(ShapedText *shaped_line)
{
    TIMED_BLOCK;

    FractionalNumber line_width = FractionalZero();
    for(int i = 0;
        i < shaped_line->num_runs;
        i++)
    {
        ShapedTextRun *run = shaped_line->runs + i;
        line_width += run->total_width;
    }

    return line_width;
}

internal i32
GetLineY(RenderText *render_text, u32 line, RenderTextTransform transform)
{
    TIMED_BLOCK;
    i32 result;
    if(render_text->data_type == RenderTextDataType_buffer)
    {
        u32 line_height = GetLinePixelHeight(render_text);

        // TODO: Address integer overflow concerns
        i32 complete_lines = transform.top_pixel / line_height;
        i32 complete_lines_pixels = complete_lines * line_height;
        i32 pixels_offset_top = complete_lines_pixels + (transform.top_pixel - complete_lines_pixels);

        i32 pixels_offset_line = line * line_height;

        result = pixels_offset_line - pixels_offset_top;
    }
    else
    {
        if(transform.vertical_alignment == TextAlign_top)
        {
            result = transform.bbox.y;
        }
        else if(transform.vertical_alignment == TextAlign_center)
        {
            f32 center_line = transform.bbox.y + (f32)transform.bbox.height/2;
            result = center_line - (render_text->font->metrics->ascent + render_text->font->metrics->descent)/2;
        }
    }

    return result;

}

internal FractionalNumber
GetGreatestLineWidth(RenderText *render_text, MemoryArena *temp_arena)
{
    FractionalNumber max_line_width = {};

    if(render_text->cached_greatest_line_width_valid)
    {
        // TODO we don't invalidate this cache when this value changes
        max_line_width = render_text->cached_greatest_line_width;
    }
    else
    {
        
        ResetTimingRecords();
        
        for(u32 line = 0;
            line < render_text->buffer->num_lines;
            line++)
        {
            TIMED_BLOCK;
            ShapedText *shaped_line = GetShapedLine(render_text, line, temp_arena);
            FractionalNumber this_line_width = GetLineWidth(shaped_line);

            if(this_line_width > max_line_width)
            {
                max_line_width = this_line_width;
            }
        }
        
        PrintTimingRecords();
        render_text->cached_greatest_line_width = max_line_width;
        render_text->cached_greatest_line_width_valid = true;
    }

    return max_line_width;
}

// TODO: remove
#if 0
inline Recti
GetGlyphRenderBounds(Glyph *glyph, GlyphPosition position)
{
    Recti result;

    result.x = position.x + glyph->xoff; 
    result.y = position.baseline + glyph->yoff;
    result.width = glyph->bboxw;
    result.height = glyph->bboxh;

    return result;
}
#endif

inline Color
GetRenderTextColor(RenderText *render_text, Cursor cursor)
{
    TIMED_BLOCK;
    Color result;
    if(render_text->text_color_function)
    {
        result = render_text->text_color_function(cursor, render_text->get_color_params);
    }
    else
    {
        result = render_text->text_color;
    }

    return result;
}

internal void
DrawRenderTextCharacter_(RenderText *render_text, u32 char_pos, u32 line, RenderTextTransform transform, MemoryArena *temp_arena,
                         Framebuffer *framebuffer, ShapedText *shaped_text = 0)
{
    TIMED_BLOCK;
    
    if(!shaped_text)
    {
        TIMED_BLOCK;
        shaped_text = GetShapedLine(render_text, line, temp_arena);
    }

    Color color;
    {
        TIMED_BLOCK;
        color = GetRenderTextColor(render_text, char_pos);
    }

    u32 char_pos_in_line = char_pos;
    if(render_text->data_type == RenderTextDataType_buffer)
    {
        TIMED_BLOCK;
		u32 line_pos = LinePos(render_text->buffer, line);
		Assert(char_pos >= line_pos);
        char_pos_in_line = char_pos - line_pos;
    }
    
    ShapedTextRun *run = GetRun(shaped_text, char_pos_in_line);
    FractionalNumber width_before_run = GetWidthBeforeRun(shaped_text, run);
    
    ClusterInfo *cluster = &run->cluster[run->character_to_cluster[(char_pos_in_line) - run->chars_before_run]];
    u32 glyph_index = cluster->start_glyph_index;

    i32 line_y = Round(GetLineY(render_text, line, transform));
    u32 num_glyphs_in_cluster = cluster->num_glyphs;
   
    for(int i = 0;
        i < num_glyphs_in_cluster;
        i++)
    {
        u32 glyph_index_in_run = cluster->start_glyph_index + i;
        Assert(glyph_index_in_run < run->num_glyphs);

        GlyphOffset glyph_position_in_run = GetGlyphPositionInShapedRun(run, glyph_index_in_run);
        GlyphOffset position = {};

        i32 document_left_side = transform.bbox.x - transform.left_pixel;
        position.x = I32ToFractional(document_left_side) + glyph_position_in_run.x + width_before_run;
        position.y = I32ToFractional(line_y) + glyph_position_in_run.y + I32ToFractional(render_text->font->metrics->ascent);
                
        i32 view_max_x = transform.bbox.x + transform.bbox.width;

        if(position.x.integer_part <= view_max_x)
        {
            GlyphInfo *glyph_info = run->glyph + glyph_index_in_run;
            DrawGlyphDirty(render_text->font, glyph_info->glyph_id, position, color, 1.0f /*alpha*/, GlyphTextureType_grayscale, framebuffer, temp_arena);
        }
    }
}



internal void
DrawRenderTextLine(RenderText *render_text, RenderTextTransform transform, u32 line, Framebuffer *framebuffer, MemoryArena *temp_arena)
{
    // Cursor start = LinePos(view->lf->text, line) + view->left_char;
    // TODO is there a smarter way to calculate start?
    Cursor start = LinePos(render_text->buffer, line); /*+ view->left_char;*/
    Cursor one_past_line_end = GetOnePastLastLineChar(render_text->buffer, line);
        
    ShapedText *shaped_line = GetShapedLine(render_text, line, temp_arena);
    for(Cursor cursor = start;
        cursor < one_past_line_end;
        cursor++)
    {
        DrawRenderTextCharacter_(render_text, cursor, line, transform, temp_arena, framebuffer, shaped_line);
    }
}

inline f32
GetNumVisibleLines(RenderText *render_text, RenderTextTransform transform)
{
    f32 result = 0.0f;
    u32 line_height = GetLinePixelHeight(render_text);

    result = (f32)transform.bbox.height / line_height;
    return result;
}

internal void
DrawRenderText(RenderText *render_text, RenderTextTransform transform, Framebuffer *framebuffer, MemoryArena *temp_arena)
{
    if(render_text->data_type == RenderTextDataType_buffer)
    {
        u32 line_height = GetLinePixelHeight(render_text);
        u32 first_line = (u32)(transform.top_pixel / line_height);

        u32 max_visible_lines = GetNumVisibleLines(render_text, transform);
        u32 one_past_max_draw_line = Min(first_line + RoundUp(max_visible_lines) + 1, render_text->buffer->num_lines);
        
        for(u32 line = first_line;
            line < one_past_max_draw_line;
            line++)
        {
            DrawRenderTextLine(render_text, transform, line, framebuffer, temp_arena);
        }
    }
    else
    {
        ShapedText *shaped_text = GetShapedText(render_text, temp_arena);
        for(Cursor cursor = 0;
            cursor < render_text->string.len;
            cursor++)
        {
            DrawRenderTextCharacter_(render_text, cursor, 0, transform, temp_arena, framebuffer, shaped_text);
        }
    }
}

internal u32
TextRelativeXToOnePastColumn(ShapedText *shaped_line, u32 x)
{
    u32 one_past_column = 0;

    FractionalNumber width_before_run = {};
    for(int i = 0;
        i < shaped_line->num_runs;
        i++)
    {
        ShapedTextRun *run = shaped_line->runs + shaped_line->visual_to_logical[i];

        if(x >= RoundFractional(width_before_run).integer_part &&
           x < RoundFractional(width_before_run + run->total_width).integer_part)
        {
            FractionalNumber run_relative_x = U32ToFractional(x) - width_before_run;
            u32 character_in_run = GetCharacterInRun(run, run_relative_x);
            u32 chars_before_run = GetCharsBeforeRun(shaped_line, run);
            
            one_past_column = character_in_run + chars_before_run + 1;
            break;
        }

        width_before_run += run->total_width;
    }

    return one_past_column;
}

inline i32
GetLineFromY(RenderText *render_text, i32 y, RenderTextTransform transform)
{
    u32 line = 0;

    if(y > transform.bbox.y - transform.top_pixel)
    {
        u32 line_height = GetLinePixelHeight(render_text);
        u32 document_relative_y = y - transform.bbox.y + transform.top_pixel;
        line = document_relative_y / line_height;
        
        if(line >= render_text->buffer->num_lines)
        {
            line = (render_text->buffer->num_lines > 0) ? render_text->buffer->num_lines - 1 : 0;
        }
    }

    return line;
}


inline u32
GetCursorColumn(RenderText *render_text, u32 line, Cursor cursor)
{
    u32 result;
    result = cursor - LinePos(render_text->buffer, line);

    return result;
}

internal RenderText *
CreateBufferRenderText(TextBuffer *buffer, Font *font, MemoryArena *arena)
{
	RenderText *result = PushType(arena, RenderText);
	
	if(buffer)
	{
		result->buffer = buffer;
	}
	result->shape_cache = PushType(arena, MultilineShapedTextCache);
	
	result->data_type = RenderTextDataType_buffer;
	result->font = font;
	
    result->line_height = 1.5f;

	return result;
}

internal RenderText *
CreateStringRenderText(String string, Font *font, MemoryArena *arena)
{
	RenderText *result = PushType(arena, RenderText);
	
	result->string = string;
	result->single_line_shape_cache = PushType(arena, SingleLineShapedTextCache);

	result->data_type = RenderTextDataType_string;
	result->font = font;
	
    result->line_height = 1.5f;

	return result;
}
