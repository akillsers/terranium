enum GetTextOperationFlags
{
    GetTextOperation_remove = (1 << 1),
};

enum TextOpType
{
    TextOp_invalid,
    TextOp_insert,
    TextOp_delete,
};

enum TextOpFlags
{
    TextOp_append = 0x01,
    TextOp_mark_activated_at_start = 0x02,
    TextOp_mark_activated_at_end = 0x04,
};

struct TextOperation
{
    TextOpType type;
    u64 flags;

    // TODO can we make this struct more compact
    Cursor cursor_before;
    u32 line_before;
    Cursor cursor_after;
    u32 line_after;
    
    Cursor start;
    u32 start_line;
    u32 num_chars;
    u32 num_newlines;

    u32 num_bytes;
    UTF8 *text;
};

struct TextOperationLink
{
    MemoryArenaSnapshot arena_snapshot;
    
    TextOperation operation;
    TextOperationLink *next;
    TextOperationLink *prev;
};

struct TextUndoStorage
{
    MemoryArena arena;
    TextOperationLink sentinel_text_op_link;
    TextOperationLink *next_operation_link;
};
