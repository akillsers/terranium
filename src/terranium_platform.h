#define MIN_WINDOW_WIDTH 400
#define MIN_WINDOW_HEIGHT 300

#if PLATFORM_WINDOWS
#define PLATFORM_NATIVE_LINE_ENDING LineEnding_CRLF
#elif PLATFORM_MACOS
#define PLATFORM_NATIVE_LINE_ENDING LineEnding_CR
#elif PLATFORM_LINUX
#define PLATFORM_NATIVE_LINE_ENDING LineEnding_LF
#else
#error "no platform native line ending could be defined!"
#endif

global char *APPLICATION_NAME = "Terranium";

#if INTERNAL
// NOTE
// Counts availible = 1,800,000,000 per second
//                   30,000,000 frame 

typedef void *PlatformFileHandle;
typedef void *PlatformWindowHandle;

enum PlatformCreateWindowFlags
{
    Window_drop_shadow = 0x1,
    Window_no_frame = 0x2,
    Window_no_taskbar_icon = 0x4,
    Window_always_on_top = 0x8,
    Window_not_focusable = 0x16,
};

u32 DEBUG_hit_count = 0;
u64 DEBUG_counter = 0;
char *DEBUG_counter_file = 0;
u32 DEBUG_counter_line = 0;

#define START_TIMED_SECTION                     \
    u64 section_counter = __rdtsc();            \
    DEBUG_hit_count++;                                        

#define END_TIMED_SECTION section_counter = __rdtsc() - section_counter; \
    DEBUG_counter += section_counter;                                   \
    char *file = __FILE__;                                              \
    u32 DEBUG_line = __LINE__;                                          \
    Assert(DEBUG_counter_file == 0 || DEBUG_counter_file == file);      \
    DEBUG_counter_file = file;                                          \
    DEBUG_counter_line = DEBUG_line;                                          
#endif


// for debug purposes
char *line_ending_strings[] {"CR", "LF", "CR LF",};

struct PlatformClipboardPasteResult
{
    b32 is_valid;
    String string;
};

enum PlatformWindowSizeState
{
    Window_minimized,
    Window_restored,
    Window_maximized,
};


enum LineEndingType
{
    LineEnding_CR,
    LineEnding_LF,
    LineEnding_CRLF,
    LineEnding_count,
};

enum CursorIconType
{
    CursorIcon_arrow,
    CursorIcon_ibeam,
    CursorIcon_no,

    CursorIcon_size_all,
    CursorIcon_size_upward_diagonal,
    CursorIcon_size_downward_diagonal,
    CursorIcon_size_vertical,
    CursorIcon_size_horizontal,
};

// NOTE:: this is currently not used in the public platform_application API
enum PixelFormat
{
    PixelFormat_B8G8R8A8,
    PixelFormat_A8R8G8B8,
};


typedef i32 FileAccess;
enum
{
    FileAccess_read = (1 << 0),
    FileAccess_write = (1 << 1),
};

enum SystemColorItem
{
    SystemColorItem_highlighted_text_background,
    SystemColorItem_highlighted_text_foreground,
    SystemColorItem_highlighted_inactive_text,
};

enum FileError
{
    FileError_no_error,
    FileError_directory_not_found,
    FileError_nonexistent,
    FileError_in_use,
    FileError_access_denied,
    FileError_unknown_err,
    FileError_invalid_handle,
    FileError_already_exists,
};

enum FileDialogType
{
    FileDialogType_save,
    FileDialogType_open,
};

enum FileDialogError
{
    FileDialogError_no_error,
    FileDialogError_cancelled,
    FileDialogError_invalid_filename,
    FileDialogError_buffer_too_small, // TODO: we don't need this error!
    FileDialogError_unknown,
};

struct FileDialogResult
{
    FileDialogError error;
    
    String *file_paths;
    String *file_names;
    u32 num_files;
};

#if INTERNAL
struct PlatformDebugInformation
{
    f64 seconds_for_frame;
    f64 seconds_for_editor_update_and_render;

    f64 seconds_for_render;
    // TODO this doesn't apply to all platforms obviously
    f64 seconds_for_copy_framebuffer;
    f64 seconds_for_present;
    f64 seconds_for_capture_chunk;
};
#endif

struct EditorMemory
{
    b32 is_initialized;
    // NOTE:: the platform keeps track of this pointer variable,
    //       which the editor can use for any purpose
    void *permanent_storage;
#if INTERNAL
    PlatformDebugInformation *debug_info;
#endif
};

struct PlatformCommandLine
{
    String *args;
    u32 num_args;
};


#define PLATFORM_MAX_KEY 0xFF
enum PlatformKey
{
    Key_invalid,
    Key_tab,
    Key_a,   
    Key_b,
    Key_c,
    Key_d,
    Key_e,
    Key_f,
    Key_g,
    Key_h,
    Key_i,
    Key_j,
    Key_k,
    Key_l,
    Key_m,
    Key_n,
    Key_o,
    Key_p,
    Key_q,
    Key_r,
    Key_s,
    Key_t,
    Key_u,
    Key_v,
    Key_w,
    Key_x,
    Key_y,
    Key_z,

    Key_comma,
    Key_period,
    Key_forward_slash,
    Key_semicolon,
    Key_single_quotes,
    Key_left_square_brackets,
    Key_right_square_brackets,
    
    Key_grave_accent,
    Key_space,
    Key_capslock,
    Key_numlock,
    Key_scrolllock,
    
    Key_0,
    Key_1,
    Key_2,
    Key_3,
    Key_4,
    Key_5,
    Key_6,
    Key_7,
    Key_8,
    Key_9,

    Key_minus,
    Key_plus,
    Key_backslash,
    
    Key_up,
    Key_down,
    Key_left,
    Key_right,

    Key_enter,
    Key_backspace,

    Key_left_shift,
    Key_right_shift,
    Key_left_ctrl,
    Key_right_ctrl,
    Key_left_alt,
    Key_right_alt,
    Key_menu,
    Key_left_context,
    Key_right_context,
    
    Key_numpad_0,
    Key_numpad_1,
    Key_numpad_2,
    Key_numpad_3,
    Key_numpad_4,
    Key_numpad_5,
    Key_numpad_6,
    Key_numpad_7,
    Key_numpad_8,
    Key_numpad_9,
    Key_numpad_slash,
    Key_numpad_asterisk,
    Key_numpad_minus,
    Key_numpad_plus,
    Key_numpad_enter,
    Key_numpad_period,

    Key_esc,
    Key_f1,
    Key_f2,
    Key_f3,
    Key_f4,
    Key_f5,
    Key_f6,
    Key_f7,
    Key_f8,
    Key_f9,
    Key_f10,
    Key_f11,
    Key_f12,

    Key_print_screen,
    Key_scroll,
    Key_break,
    Key_insert,
    Key_home,
    Key_end,
    Key_page_up,
    Key_page_down,
    Key_delete,

    Key_media_home,
    Key_media_email,
    Key_media_play,
    Key_media_stop,
    Key_media_prev,
    Key_media_next,
    Key_media_louder,
    Key_media_softer,
    Key_media_mute,

#if INTERNAL
    Key_dbge0, // TODO fake dbg key
    Key_dbge1, // TODO fake dbg key
#endif
    Key_unknown,
    Key_count
};

const char *keyname[256]=
{
    "Invalid",
    "TAB",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",

    ",",
    ".",
    "/",
    ";",
    "'",
    "[",
    "]",

    "`",
    "Space",
    "Caps Lock",
    "Num Lock",
    "Scroll Lock",
    
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",

    "-",
    "+/=",
    "/",
    
    "Up Arrow",
    "Down Arrow",
    "Left Arrow",
    "Right Arrow",

    "Enter",
    "Backspace",
    
    "Left Shift",
    "Right Shift",
    "Left Control",
    "Right Control",
    "Left Alt",
    "Right Alt",
    "Left Menu",
    "Right Menu",
    "Left Context",
    "Right Context",
    "Menu",
    
    "Numpad 0",
    "Numpad 1",
    "Numpad 2",
    "Numpad 3",
    "Numpad 4",
    "Numpad 5",
    "Numpad 6",
    "Numpad 7",
    "Numpad 8",
    "Numpad 9",
    "Numpad /",
    "Numpad *",
    "Numpad -",
    "Numpad +",
    "Numpad Enter",
    "Numpad .",

    "Escape",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12",

    "Print Screen",
    "Scroll Lock",
    "Break",
    "Insert",
    "Home",
    "End",
    "Page Up",
    "Page Down",
    "Delete",

    "Media Home",
    "Media Volume Up",
    "Media Volume Down",
    "Media Mute",

#if INTERNAL
    "Debug E0",
    "Debug E1",
#endif
    "Unsupported key"
};

enum MouseButton
{
    MouseButton_left,
    MouseButton_middle,
    MouseButton_right,
    PLATFORM_MAX_MOUSE_BUTTON,
};

// NOTE:: keys include (mouse) buttons
struct KeyState
{
    b32 down;
    u32 transition_count;
    u32 down_repeats; 
};

enum DroppedItemType
{
    DroppedItemType_nothing,
    DroppedItemType_text,
    DroppedItemType_filename
};

struct DroppedItems
{
    DroppedItemType type;

    // this string is allocated by the platform layer
    LinkedString *string;
};

//TODO these are windows specific... what about other platforms???
#define HTERROR             (-2)
#define HTTRANSPARENT       (-1)
#define HTNOWHERE           0
#define HTCLIENT            1
#define HTCAPTION           2
#define HTSYSMENU           3
#define HTGROWBOX           4
#define HTSIZE              HTGROWBOX
#define HTMENU              5
#define HTHSCROLL           6
#define HTVSCROLL           7
#define HTMINBUTTON         8
#define HTMAXBUTTON         9
#define HTLEFT              10
#define HTRIGHT             11
#define HTTOP               12
#define HTTOPLEFT           13
#define HTTOPRIGHT          14
#define HTBOTTOM            15
#define HTBOTTOMLEFT        16
#define HTBOTTOMRIGHT       17
#define HTBORDER            18
#define HTREDUCE            HTMINBUTTON
#define HTZOOM              HTMAXBUTTON
#define HTSIZEFIRST         HTLEFT
#define HTSIZELAST          HTBOTTOMRIGHT

#define HTOBJECT            19
#define HTCLOSE             20
#define HTHELP              21


// TODO this small limit can be a problem if the user
//      really fast and the program is running really slow
#define MAX_CHAR_TYPED_PER_FRAME 512

enum PlatformTitlebarType
{
    PlatformTitlebar_system_drawn,
    PlatformTitlebar_custom_overlay,
    PlatformTitlebar_custom_drawn,
};

struct PlatformWindowInfo
{
    b32 has_focus;
    b32 resized;
    b32 moved;
    PlatformWindowSizeState size_state;

    Vec2i pos;
    Vec2i size;
    PlatformTitlebarType titlebar_type;
};

struct Input
{
    // TODO For now we are using windows scancodes since why not??
    // TODO do we keep the whole array exposed here
    //     even with the new system of keeping an
    //     array of "interesting" keys?
    f64 dt;
    b32 dpi_changed;
    f32 dpi_scaling;
    
    int wheel_delta;
    KeyState mouse_buttons[PLATFORM_MAX_MOUSE_BUTTON];
    b32 mouse_moved;
    KeyState keys[PLATFORM_MAX_KEY];
    b32 ctrl_down;
    b32 alt_down;
    b32 shift_down;
    
    u32 chars_typed;
    char typed_char[MAX_CHAR_TYPED_PER_FRAME];
    
    //NOTE:: mouse_position is relative to main window
    Vec2i mouse_pos;
    PlatformWindowInfo *main_window;
    
    // there are two things that can be dropped:
    // text and filenames!
    // these are RESET everyframe!
    // TODO should the editor call a function to retrieve this instead?
    // TODO should the edtior manually reset dropped type to DroppedItemType_nothing?
    // (instead of platform doing it each frame)
    DroppedItems dropped;

    // NOTE that this is really output
    i32 non_client_hittest;
};

inline b32
KeyPressed(KeyState ks)
{
    return ((ks.down && ks.transition_count >= 1) || ks.transition_count > 1);
}

inline b32
KeyReleased(KeyState ks)
{
    return (!ks.down &&
            (ks.transition_count > 0 || ks.transition_count > 1));
}

struct PlatformWorkEntry
{
    void (*function) (void *data, MemoryArena *thread_arena);
    void *data;
};

// Make this an opaque struct?
struct PlatformWorkQueue
{
    volatile u32 completed_jobs;
    volatile u32 total_jobs;
};

struct PlatformAPI
{
    // --multi-threading--
    void (*AddWorkQueueEntry) (PlatformWorkQueue *work_queue, PlatformWorkEntry *job);
    void (*CompleteAllWork) (PlatformWorkQueue *work_queue);
    
    // --memory management--
    // NOTE: AllocateMemory() returns a 16-byte aligned address
    // TODO: Check that VirtualAlloc() on Windows does this?
    void *(*AllocateMemory) (u64 size_required);
    void (*FreeMemory) (void *ptr);

    // ---window management---
    PlatformWindowHandle (*OpenWindow) (u64 flags, String name);
    void (*SetTitlebar) (PlatformWindowHandle window, String string);
    Recti (*GetWindowRestoredRect) (PlatformWindowHandle window);
    Framebuffer * (*GetWindowFramebuffer) (PlatformWindowHandle handle);
    void (*SetWindowVisible) (PlatformWindowHandle window_handle, b32 visible);
    b32 (*SetWindowPosition) (PlatformWindowHandle handle, Recti window_position);
    b32 (*SetWindowSizeState) (PlatformWindowHandle handle, PlatformWindowSizeState size_state);
    
    // ---clipboard functions---
    void (*CopyToClipboard) (String string);
    PlatformClipboardPasteResult (*PasteFromClipboard) ();  
    
    // ---file/directory functions---
    FileDialogResult (*FileDialog) (FileDialogType type);
    
    u64 (*GetFileSize) (PlatformFileHandle handle);
    PlatformFileHandle (*OpenFile) (String path, FileAccess access);
    PlatformFileHandle (*OverwriteFile) (String path, FileAccess access);
    PlatformFileHandle (*CreateNewFile) (String path, FileAccess access);
    void (*CloseFile) (PlatformFileHandle handle);
    
    u32 (*ReadFile) (PlatformFileHandle handle, void *buffer, u32 buffer_size);
    u32 (*WriteEntireFile) (PlatformFileHandle handle, void *write_buffer, u32 write_buffer_size);
    
    b32 (*DirectoryExists) (String directory);
    b32 (*MakeDirectory) (String directory);
    
    FileError (*GetLastFileError) ();
    
    // ---misc functions---
    void (*SetCursorIcon) (CursorIconType cursor_icon);
    PlatformCommandLine (*RetrieveCommandLine) ();
    
    
    // ---error output, debug, and logging functions---
    void (*Info) (char *message, ...);
    void (*Error) (char *name, char *message);
    
    MemoryArena frame_arena;
};

global PlatformAPI platform;



