enum MemoryArenaFlags
{
    MemoryArena_no_grow = (1 << 1),
};

struct MemoryArena
{
    void *base;
    u64 size;
    u64 used;

    u32 flags;
    
    u64 min_block_size;
    u32 temp_count;
};

struct MemoryArenaSnapshot
{
    void *base;
    u64 used;
};

struct TemporaryMemory
{
    MemoryArena *arena;
    MemoryArenaSnapshot snapshot;
};

// NOTE: We intentionally avoid using __VA_ARGS__ to implement optional paramaters
//       in order to avoid bugs!
#define PushType(arena, type) (type *)PushSize(arena, sizeof(type))
#define PushTypeFlags(arena, type, flags) (type *)PushSize(arena, sizeof(type), flags)
#define PushCount(arena, type, count) (type *)PushSize(arena, sizeof(type) * count)
#define PushCountFlags(arena, type, count, flags) (type *)PushSize(arena, sizeof(type) * count, flags)

struct MemoryBlockFooter
{
    void *base;
    u64 size;
    u64 used;
};

internal void *PushSize(MemoryArena *arena, u64 size, u64 alloc_flags = 0);

enum AllocFlags
{
    PushSize_align_16 = (1 << 1),
};
