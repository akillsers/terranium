#include <hb.h>

#if PLATFORM_WINDOWS

struct IDWriteFontFace;
#include <hb-directwrite.h>

#else
#error "HarfBuzz face creation for this platform is currently unimplemented!"
#endif

internal hb_face_t *
HarfBuzz_CreateHBFace(Font *font)
{
    hb_face_t *result;

    switch(font_backend.font_backend_id)
    {
        case FontBackend_DWrite:
        {
            IDWriteFontFace *dwrite_font_face = (IDWriteFontFace *) font_backend.GetDWriteFontFace(font);

            result = hb_directwrite_face_create(dwrite_font_face);
        } break;

        InvalidDefaultCase;
    }

    return result;
}

struct ShapeEngineFont
{
    hb_font_t *harfbuzz_font;
    hb_face_t *harfbuzz_face;
};

struct EngineShapedRunData
{
    MemoryArena *arena;

    hb_buffer_t *buffer;

    u32 num_glyphs;
    hb_glyph_info_t *glyph_info;
    hb_glyph_position_t *glyph_pos;

    u32 num_clusters;
    ClusterInfo *clusters;
};

internal hb_direction_t
HarfBuzz_BIDIDirectionToHBDirection(BIDIDirection bidi_direction)
{
    hb_direction_t result;

    if(bidi_direction == BIDIDirection_RTL)
    {
        result = HB_DIRECTION_RTL;
    }
    else
    {
        result = HB_DIRECTION_LTR;
    }

    return result;
}

internal hb_script_t
HarfBuzz_ConvertToHBScript(BIDIScript bidi_script)
{
    hb_script_t result;
    result = (hb_script_t)bidi_script;

    return result;
}

internal ShapeEngineFont *
Harfbuzz_CreateFont(Font *font)
{
    ShapeEngineFont *result;

    result = PushType(&font->arena, ShapeEngineFont);

    result->harfbuzz_face = HarfBuzz_CreateHBFace(font);
    result->harfbuzz_font = hb_font_create(result->harfbuzz_face);

    hb_font_set_scale(result->harfbuzz_font, font->metrics->pixel_size*64, font->metrics->pixel_size*64);
    return result;
}

internal void
Harfbuzz_DestroyFont(Font *font)
{
    hb_face_destroy(font->shape_engine_font->harfbuzz_face);
    hb_font_destroy(font->shape_engine_font->harfbuzz_font);
}


internal EngineShapedRunData *
HarfBuzz_ShapeTextRun(UTF32_String string, ScriptItem *script_item, ShapeEngineFont *font, MemoryArena *arena)
{
    // TODO
    // we don't actually use the arena to allocate the harfbuzz things
    // the arena is only used to allocate the EngineShapedRunData struct

    EngineShapedRunData *result;
    result = PushType(arena, EngineShapedRunData);
    ZeroStruct(result);

    result->arena = arena;

    hb_buffer_t *harfbuzz_buffer;
    harfbuzz_buffer = hb_buffer_create();

    // NOTE the reason we use hb_buffer_add_codepoints is because then the cluster information would
    //      point to refer to inidividual codeoints instead of indices of the UTF8 array, which is
    //      what shaped_text.cpp API expects
    hb_buffer_add_codepoints(harfbuzz_buffer, (hb_codepoint_t *)string.ptr, string.len, 0, string.len);

    hb_direction_t text_direction = HarfBuzz_BIDIDirectionToHBDirection(script_item->direction);
    
    hb_buffer_set_direction(harfbuzz_buffer, text_direction);
    hb_script_t harfbuzz_script = HarfBuzz_ConvertToHBScript(script_item->script);
    hb_buffer_set_script(harfbuzz_buffer, harfbuzz_script);

    hb_language_t harfbuzz_language = hb_language_from_string("en", -1);
    hb_buffer_set_language(harfbuzz_buffer, harfbuzz_language);

    hb_shape(font->harfbuzz_font, harfbuzz_buffer, 0, 0);

    result->buffer = harfbuzz_buffer;

    return result;
}


internal void
HarfBuzz_FreeEngineRunData(EngineShapedRunData *engine_data)
{
    // TODO make harfbuzz use the frame arena allocator so that
    //      there is no need to free engine run data here
    hb_buffer_destroy(engine_data->buffer);
}

internal u32
HarfBuzz_GetNumClusters(EngineShapedRunData *engine_data)
{
    if(engine_data->glyph_info == 0)
    {
        u32 num_glyphs;
        engine_data->glyph_info = hb_buffer_get_glyph_infos(engine_data->buffer, &num_glyphs);
        engine_data->num_glyphs = num_glyphs;
    }

    u32 cluster_count = 0;
    u32 one_past_last_cluster = 0;

    for(u32 i = 0;
        i < engine_data->num_glyphs;
        i++)
    {
        u32 glyph_cluster = engine_data->glyph_info[i].cluster;

        if(glyph_cluster + 1 != one_past_last_cluster)
        {
            cluster_count++;
            one_past_last_cluster = glyph_cluster + 1;
        }
    }
    
    return cluster_count;
}

internal u32
HarfBuzz_GetNumGlyphs(EngineShapedRunData *engine_data)
{
    u32 result = 0;

    if(engine_data->glyph_info == 0)
    {
        engine_data->glyph_info = hb_buffer_get_glyph_infos(engine_data->buffer, &result);
        engine_data->num_glyphs = result;
    }
    else
    {
        result = engine_data->num_glyphs;
    }

    return result;
}

internal void
HarfBuzz_PopulateRun(EngineShapedRunData *engine_data, ShapedTextRun *run)
{
    if(!engine_data->glyph_info)
    {
        u32 num_glyphs = 0;
        engine_data->glyph_info = hb_buffer_get_glyph_infos(engine_data->buffer, &num_glyphs);

        engine_data->num_glyphs = num_glyphs;
    }

    if(!engine_data->glyph_pos)
    {
        engine_data->glyph_pos = hb_buffer_get_glyph_positions(engine_data->buffer, 0);
    }

    for(u32 i = 0;
        i < run->num_glyphs;
        i++)
    {
        GlyphInfo *glyph_info = run->glyph + i;

        glyph_info->glyph_id = engine_data->glyph_info[i].codepoint;

        // TODO: should we use Harfbuzz provided y_advance (if it is actually provided) or the y_advance
        //       in Font struct??
        glyph_info->advance = ImproperI32ToFractional(engine_data->glyph_pos[i].x_advance);
        
        glyph_info->offset.x = ImproperI32ToFractional(engine_data->glyph_pos[i].x_offset);
        glyph_info->offset.y = ImproperI32ToFractional(engine_data->glyph_pos[i].y_offset);
        
        run->total_width += ImproperI32ToFractional(engine_data->glyph_pos[i].x_advance);
    }

    // strategy: for any sequences of continuous cluster values,
    //           put them into one cluster
    u32 glyph_index = 0;
    u32 cluster_index = 0;
    for(;;)
    {
        ClusterInfo *cluster_info = run->cluster + cluster_index;
        cluster_info->start_glyph_index = glyph_index;
        cluster_info->num_glyphs = 1;

        u32 cluster_first_character = engine_data->glyph_info[glyph_index].cluster;
        u32 cluster_character_length = 1;

        for(u32 j = glyph_index + 1;
            j < engine_data->num_glyphs;
            j++)
        {
            u32 check_glyph_character = engine_data->glyph_info[j].cluster;
            if (check_glyph_character == cluster_first_character)
            {
                cluster_info->num_glyphs++;
            }
            else
            {
                u32 next_character_cluster = engine_data->glyph_info[j].cluster;

                // "Each character belongs to the cluster that has the highest
                // cluster value not larger than its initial cluster value."
                if(next_character_cluster > cluster_first_character)
                {
                    // For LTR text, cluster values are monotonically increasing
                    cluster_character_length = next_character_cluster - cluster_first_character;
                }
                else
                {
                    // For RTL text, cluster values are monotonically decreasing.
                    cluster_character_length = cluster_first_character - next_character_cluster;
                }

                break;
            }
        }
        
        for(u32 j = 0;
            j < cluster_character_length;
            j++)
        {
            u32 cluster_character_index = cluster_first_character + j;
            run->character_to_cluster[cluster_character_index] = cluster_index;
        }

        cluster_index++;
        glyph_index += cluster_info->num_glyphs;
        
        if(glyph_index >= engine_data->num_glyphs)
        {
            break;
        }
    }
}
