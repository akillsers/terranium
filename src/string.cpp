#define ZERO_WIDTH_JOINER_CODEPOINT 0x200D
#define UTF8_MAX_ENCODING_LENGTH 4
#define UTF32_PLACEHOLDER_CHARACTER 0xFFFFFFFF
#ifndef STRING_LIB_DEFINED
#define STRING_LIB_DEFINED 1
#else
#error "There is already a string library defined!"
#endif
struct String
{
    // TODO consider holding a char count here to make certain things more convenient?
    //      (we can know how big to allocate storage for converting the string to another format like ASCII or UTF32)
    UTF8 *ptr;
    u32 len;
};

struct UTF32_String
{
    UTF32 *ptr;
    u32 len;
};

struct LinkedString {
    String data;
    LinkedString *next;
};
inline b32
IsLeadingUTF8Byte(UTF8 byte)
{
    return ((byte & 0xC0) != 0x80);
}

// TODO what is that anal retention whitespace character?
inline b32
IsWhitespace(UTF32 c)
{
    return c == ' ' || c == '\r' || c == '\n' || c == '\t';
} 

// internal b32
// StringsAreEqual(UTF8 *first, UTF8 *second)
// {
//     // NOTE both string are null terminated
//     UTF8 *first_at = first;
//     UTF8 *second_at = second;

//     while(*first_at)
//     {
//         if(*first_at != *second_at)
//         {
//             break;
//         }

//         first_at++;
//         second_at++;
//     }
//     return (*first_at == 0 && *second_at == 0);
// }

//"there"

internal b32
StringMatchesCString(String string, char *c_string)
{
    b32 result;
    
    u32 offset = 0;
    while(offset < string.len)
    {
        if(string.ptr[offset] != c_string[offset])
        {
            break;
        }
        
        offset++;
    }
    
    result = (offset == string.len &&
              c_string[0] == 0);
    
    return result;
}

internal b32
StringsAreEqual(String first, String second)
{
    b32 result;
    
    if(first.len != second.len)
    {
        result = false;
    }
    else
    {
        u32 offset = 0;
        while(offset < first.len)
        {
            if(first.ptr[offset] != second.ptr[offset])
            {
                break;
            }
            
            offset++;
        }
        
        result = (offset == first.len);
    }
    
    return result;
}

internal UTF8*
CopyUTF8(UTF8 *dest, UTF8 *src, u32 size)
{
    while(size--)
    {
        *dest++ = *src++;
    }
    return dest;
}


internal String
PushString(MemoryArena *arena, u32 len)
{
    String result = {};
    result.ptr = PushCount(arena, UTF8, len);
    result.len = len;
    return result;
}

#define FromCharArray(c_str) MakeString((UTF8 *)(c_str), ArrayCount(c_str) - 1)

internal u32
CountBytes(char *c_string)
{
    u32 char_count = 0;
    
    char *at = c_string;
    while(*at)
    {
        at++;
    }
    
    return at - c_string;
}

inline String
OffsetString(String string, u32 offset)
{
    String result;
    result.ptr = string.ptr + offset;
    result.len = string.len - offset;
    
    return result;
}

internal String
FromCharPtr(char *c_string)
{
    String result;
    result.ptr = (UTF8 *)c_string;
    result.len = CountBytes(c_string);
    
    return result;
}

internal String
MakeString(UTF8 *ptr, u32 len)
{
    String ret;
    ret.ptr = ptr;
    ret.len = len;
    return ret;
}

internal String
PushJoinedString(String first, String second, MemoryArena *memory_arena)
{
    String result = PushString(memory_arena, first.len + second.len);
    CopyUTF8(result.ptr, first.ptr, first.len);
    CopyUTF8(result.ptr + first.len, second.ptr, second.len);
    
    return result;
}

inline String
PushJoinedString(String first, char *second, MemoryArena *memory_arena)
{
    String second_string = FromCharPtr(second);
    return PushJoinedString(first, second_string, memory_arena);
}

internal void
CopyStringData(String dest, String src)
{
    Assert(dest.len >= src.len);
    memcpy(dest.ptr, src.ptr, src.len);
}

internal String
EmptyString()
{
    String result;
    result.ptr = 0;
    result.len = 0;
    return result;
}

internal u32
CodepointToUTF8(UTF32 codepoint, UTF8 utf8_bytes[4])
{
    u32 num_bytes_used = 0;
    // from en.wikipedia.org/wiki/UTF-8 description
    if(codepoint >= 0x0000 && codepoint <= 0x007F) {              // 2^7 
        num_bytes_used = 1;
        utf8_bytes[0] = codepoint;
    } else if(codepoint >= 0x0080 && codepoint <= 0x07FF) {       // 2^11 
        num_bytes_used = 2;
        utf8_bytes[0] = 0xC0 | (codepoint >> 6);  // 5 bytes of data here
        utf8_bytes[1] = 0x80 | (codepoint & 0x3F); // 6 bytes of data herea
    } else if(codepoint >= 0x0800 && codepoint <= 0xFFFF) {       // 2^16 
        num_bytes_used = 3;
        utf8_bytes[0] = 0xE0 | (codepoint >> 12); // 4 bytes of data here
        utf8_bytes[1] = 0x80 | ((codepoint >> 6) & 0x3F);
        utf8_bytes[2] = 0x80 | (codepoint & 0x3F);
    } else if(codepoint >= 0x10000 && codepoint <= 0x10FFFF) {    // 2^21 
        num_bytes_used = 4;
        utf8_bytes[0] = 0xF0 | (codepoint >> 16);
        utf8_bytes[1] = 0x80 | ((codepoint >> 12) &0x3F);
        utf8_bytes[2] = 0x80 | ((codepoint >> 6) &0x3F);
        utf8_bytes[3] = 0x80 | (codepoint & 0x3F);
    }
    
    return num_bytes_used;
}

internal u32
GetMultibyteLength(UTF8 leading)
{
    u32 ret = 0;
    if((leading & 0xE0) == 0xC0)
        ret = 2;
    else if((leading & 0xF0) == 0xE0)
        ret = 3;
    else if((leading & 0xF8) == 0xF0)
        ret = 4;
    else if((leading & 0x80) == 0)
        ret = 1;
    else
        Assert(false);
    return ret;
}

// assumes that from is a leading UTF8 character byte
internal u32
CountCharacters(String string)
{
    u32 char_count = 0;
    
    UTF8 *at = string.ptr;
    
    for(u32 i = 0;
        i < string.len;
        i++)
    {   
        if(IsLeadingUTF8Byte(*at))
        {
            char_count++;
        }
        at++;
    }
    return char_count;
}

internal UTF8 *
GetByteAtCharOffset(String string, u32 offset)
{
    u32 char_count = 0;
    
    u32 byte_offset = 0;
    while(byte_offset < string.len)
    {
        UTF8 *at = string.ptr + byte_offset;
        
        if(IsLeadingUTF8Byte(*at))
        {
            char_count++;
        }
        
        if(char_count > offset)
        {
            at--;
            break;
        }
        
        byte_offset++;
    }
    
    return string.ptr + byte_offset;
}
internal UTF32
GetCodepointFromUTF8(UTF8 *byte, u32 num_utf8_bytes = 0 /*0 for don't know*/)
{
    u32 codepoint;
    
    if(num_utf8_bytes == 0) {
        num_utf8_bytes = GetMultibyteLength(byte[0]);
    }
    
#if INTERNAL
    else {
        // double check
        UTF8 check_num_utf8_bytes = GetMultibyteLength(byte[0]);
        Assert(num_utf8_bytes == check_num_utf8_bytes);
    }
#endif
    if(num_utf8_bytes == 1)
        codepoint = byte[0];
    else if(num_utf8_bytes == 2) {
        UTF8 second = byte[1];
        codepoint = ((*byte & 0x1F) << 6) | (second & 0x3F);
    } else if(num_utf8_bytes == 3) {
        UTF8 second = byte[1];
        UTF8 third = byte[2];
        codepoint = ((*byte &0xF) << 12) | ((second & 0x3f) << 6) | (third & 0x3f);
    } else if(num_utf8_bytes == 4) {
        UTF8 second = byte[1];
        UTF8 third = byte[2];
        UTF8 fourth = byte[3];
        codepoint = ((*byte &0x7) << 18) | ((second & 0x3f) << 12) | ((third & 0x3f) << 6) | (fourth & 0x3f);
    } else {
        Assert(false);
        codepoint = 0xFFFD;
    }
    
    // *out_num_utf8_bytes = num_utf8_bytes;
    return codepoint;
}

internal UTF32_String
StringToUTF32(String string, MemoryArena *arena)
{
    UTF32_String result = {};
    
    u32 chars_in_string = CountCharacters(string);
    result.ptr = PushCount(arena, UTF32, chars_in_string);
    
    u32 utf8_byte_offset = 0;
    while(utf8_byte_offset < string.len)
    {
        UTF8 *utf8_byte = string.ptr + utf8_byte_offset;
        
        u32 num_utf8_bytes = GetMultibyteLength(*utf8_byte);
        UTF32 codepoint = GetCodepointFromUTF8(utf8_byte, num_utf8_bytes);
        
        result.ptr[result.len] = codepoint;
        
        utf8_byte_offset += num_utf8_bytes;
        result.len++;
    }
    
    Assert(result.len == chars_in_string);
    
    
    return result;
}

internal b32
IsValid(UTF32 codepoint)
{
    b32 result;
    result = (codepoint != UTF32_PLACEHOLDER_CHARACTER);
    return result;
}

inline b32
CaseInsensitiveMatch(char a, char b)
{
    char a_comp = 0;
    if(a >= 'a' && a <= 'z')
        a_comp = a - 0x20;
    else if(a >= 'A' && a <= 'Z')
        a_comp = a + 0x20;
    
    if(a == b)
        return true;
    else if(a_comp && a_comp == b)
        return true;
    return false;
}

internal b32
IsAlphanumeric(UTF32 codepoint) {
    return (codepoint >= 'a' && codepoint <= 'z' ||
            codepoint >= 'A' && codepoint <= 'Z' ||
            codepoint >= '0'  && codepoint <= '9');
}

