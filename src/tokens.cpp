/*
  Please note the following limitations on token stores when calculating things:
  -there can NEVER be a valid TokenStore with no tokens (i.e. store->num_tokens == 0)

*/

inline Cursor
TokenLastCharacter(Token *token)
{
    return token->pos + token->len - 1;
}

inline Cursor
TokenOnePastLastCharacter(Token *token)
{
    return token->pos + token->len;
}

internal CursorRange
GetTokenStoreRange(TokenStore *store)
{
    Token *first_token = store->tokens;
    Token *last_token = store->tokens + store->num_tokens - 1;
    
    CursorRange result;
    result.min = first_token->pos;
    result.max = TokenLastCharacter(last_token);
    return result;
}

internal TokenStore *
FindTokenStoreContainingCursor(LoadedFile *lf, Cursor cursor)
{
    TokenStore *result = 0;
    TokenStore *first_store = lf->token_store_sentinel.next;
    for(TokenStore *scan_store = first_store;
        scan_store != &lf->token_store_sentinel;
        scan_store = scan_store->next)
    {
        CursorRange range = GetTokenStoreRange(scan_store);
        if(cursor > range.max)
        {
            continue;
        }

        if(cursor >= range.min)
        {
            result = scan_store;
        }
        break;
    }
    return result;
}

internal TokenStore *
FindTokenStoreNearestBackward(LoadedFile *lf, Cursor cursor)
{
    TokenStore *first_store = lf->token_store_sentinel.next;

    TokenStore *closest = 0;
    for(TokenStore *scan_store = first_store;
        scan_store != &lf->token_store_sentinel;
        scan_store = scan_store->next)
    {
        CursorRange range = GetTokenStoreRange(scan_store);
        if(range.max < cursor)
        {
            closest = scan_store;
        }
        else
        {
            break;
        }
    }

    return closest;
}

internal TokenStore *
FindTokenStoreNearestForward(LoadedFile *lf, Cursor cursor)
{
    TokenStore *last_store = lf->token_store_sentinel.prev;
    
    TokenStore *closest = 0;
    for(TokenStore *scan_store = last_store;
        scan_store != &lf->token_store_sentinel;
        scan_store = scan_store->prev)
    {
        CursorRange range = GetTokenStoreRange(scan_store);
        if(cursor < range.min)
        {
            closest = scan_store;
        } 
        else if (cursor > range.min)
        {
            break;
        }

    }
    return closest;
}

inline TokenStore *
GetTokenStoreContainingToken(LoadedFile *lf, Token *token)
{
    // TODO to speed things up, we can look by token pos (since that's sorted)
    TokenStore *result = FindTokenStoreContainingCursor(lf, token->pos);
    return result;
}

internal Token *
GetTokenXBehind(LoadedFile *lf, Token *from, u32 x)
{
    TokenStore *store = FindTokenStoreContainingCursor(lf, from->pos);

	// make sure the token we are referencing is ACTUALLY in a token store
	Assert(store);

    Token *result = from;
    for(int i = 0; i < x; i++) {
        if(result - store->tokens > 0) {
            result--; // the next token is directly contiguous in memory in front of us
        } else {
            store = store->prev;
            
            if(store == &lf->token_store_sentinel)
            {
                result = 0;
                break;
            } else {
                result = store->tokens + store->num_tokens - 1;
            }
        }
    }
    return result;
}

internal u32
CountTokensBetween(LoadedFile *lf, Token *from, Token *to)
{
    u32 result = 0;
    if(from != to) {
        TokenStore *from_store = FindTokenStoreContainingCursor(lf, from->pos);

        TokenStore *store = from_store;
        u32 tokens_counted = 0;
        Token *at_token = from;
        do
        {
            if(at_token - store->tokens + 1 < store->num_tokens)
            {
                at_token++;
            } else {
               Assert(store != &lf->token_store_sentinel);
                store = store->next;
                at_token = store->tokens;
            }

            tokens_counted++;
        } while(at_token != to);
        result = tokens_counted;
    }
    return result;
}

inline Token *
GetTokenAtCursor(LoadedFile *lf, Cursor cursor)
{
    TIMED_BLOCK;
    Token *result = 0;
    if(lf->total_tokens > 0) {
        // TODO also binary search thru all the token stores too

        // Design decision: there should always only be ONE token store that contains cursor (singularity model)
        TokenStore *store = FindTokenStoreContainingCursor(lf, cursor);
        if(store) {
            i64 l = 0;
            i64 r = (store->num_tokens > 0) ? store->num_tokens - 1 : 0;
            i64 mid;
    
            while(r >= l)
            {
                mid = l + (r-l)/2;
                Token *potential_token = store->tokens + mid;
                if(cursor >= potential_token->pos &&
                   cursor < potential_token->pos + potential_token->len) {
                    result = potential_token;
                    break;
                } else if(cursor < potential_token->pos) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            }

        }
    }
    

    return result;
}

inline Token *
GetTokenByIndex(LoadedFile *lf, u32 index)
{
    Token *result = 0;

    TokenStore *store = 0;

    for(TokenStore *scan = lf->token_store_sentinel.next;
        scan != &lf->token_store_sentinel;
        scan = scan->next)
    {
        if(index >= scan->tokens_before_store + scan->num_tokens)
        {
            continue;
        }

        Assert(index >= scan->tokens_before_store &&
               index < scan->tokens_before_store + scan->num_tokens);
        
        store = scan;
        break;
    }

    
    if(store)
    {
        u32 nominal_index = index - store->tokens_before_store;
        Assert(nominal_index < store->num_tokens);
        result = store->tokens + nominal_index;
    }

    return result;
}

inline Token *
GetNextToken(LoadedFile *lf, Token *token, TokenStore **store)
{
    Token *result = 0;

    if(token - (*store)->tokens + 1 < (*store)->num_tokens)
    {
        result = token + 1;
    }
    else
    {
        *store = (*store)->next;

        if(*store != &lf->token_store_sentinel)
        {
            result = (*store)->tokens;
        }
    }

    return result;
}

inline Token *
GetNextTokenOrNearestForward(LoadedFile *lf, Cursor cursor)
{
    Token *token = 0;

    TokenStore *lowest_store = 0;
    lowest_store = lf->token_store_sentinel.next;
    for(TokenStore *store = lf->token_store_sentinel.next->next;
        store != &lf->token_store_sentinel;
        store = store->next)
    {
        Token *first_token = store->tokens;
        Token *last_token = store->tokens + store->num_tokens - 1;
        
        
        
        if(cursor < first_token->pos)
        {
            break;
        }

        lowest_store = store;
    }

    Assert(lowest_store);

    // in other words, highest token whose pos is <= cursor
    Token *cursor_token_or_nearest_backward = 0;
    for(u32 i = 0;
        i < lowest_store->num_tokens;
        i++)
    {
        Token *scan_token = lowest_store->tokens + i;

        if(cursor >= scan_token->pos)
        {
            cursor_token_or_nearest_backward = scan_token;
        }
        else
        {
            break;
        }
    }

    Token *next_token;
    if(cursor_token_or_nearest_backward)
    {
        next_token = GetNextToken(lf, cursor_token_or_nearest_backward, &lowest_store);
    }
    else
    {
        next_token = lowest_store->tokens;
    }

    return next_token;
}

inline Token *
BrokenGetNextTokenOrNearestForward(LoadedFile *lf, Cursor cursor)
{
    Token *token = 0;

    // TODO for greater efficiency these two calls can be combined
    TokenStore *store = FindTokenStoreContainingCursor(lf, cursor);
    if(!store)
    {
        // TODO don't loop thru the linked list twice
        store = FindTokenStoreNearestForward(lf, cursor);
    }

    if(!store)
    {
        TokenStore *first_store = lf->token_store_sentinel.next;
        if(first_store != &lf->token_store_sentinel)
        {
            if(first_store->num_tokens > 0)
            {
                Token *first_token = first_store->tokens;
                if(cursor < first_token->pos)
                {
                    store = first_store;
                    
                }
            }
        }
    }

    if(store)
    {
        for(int i = 0; i < store->num_tokens; i++)
        {
            Token *potential_token = store->tokens + i;
            // NOTE:: this works because tokens are sequentially ordered
            if(cursor < potential_token->pos)
            {
                token = potential_token;
                break;
            }
        }
    }

    return token;
}

inline Token *
GetPreviousTokenOrNearestBackward(LoadedFile *lf, Cursor cursor) 
{
    Token *result = 0;
	// TODO these two calls can be combined into one
    TokenStore *store = FindTokenStoreContainingCursor(lf, cursor);

    if(!store) {
        store = FindTokenStoreNearestBackward(lf, cursor);
    }

    if(store)
    {
        if(lf->total_tokens > 0) 
        {
            for(int i = 0; i < store->num_tokens; i++) 
            {
                Token *check_token = store->tokens + i;
                if(check_token->pos + check_token->len > cursor) 
                {
                    break;
                }
                result = check_token;
            }
        }
    }
    return result;
}

internal u32
GetTokenIndex(LoadedFile *lf, Token *token)
{
    TokenStore *store = GetTokenStoreContainingToken(lf, token);
    Assert(store);
    u32 result;
    result = (token - store->tokens) + store->tokens_before_store;
    return result;
}

internal u32
FirstTokenIndex(LoadedFile *lf, Token *token)
{
    TokenStore *store = GetTokenStoreContainingToken(lf, token);
    Assert(store);
    
    u32 result = (token - store->tokens) + store->tokens_before_store;
    return result;
}


inline void
FreeTokenStore(TokenStore *store)
{
    platform.FreeMemory(store);
}

internal void
_DEBUG_VerifyTokenStoreIntegrity(LoadedFile *lf)
{
    u32 tokens_before_store = 0;
    u32 cursor = 0;
    
    for(TokenStore *store = lf->token_store_sentinel.next;
        store != &lf->token_store_sentinel;
        store = store->next)
    {
        Assert(tokens_before_store == store->tokens_before_store)
        tokens_before_store += store->num_tokens;
    
        for(u32 i = 0; 
            i < store->num_tokens; 
            ++i)
        {
            Assert((store->tokens[i].pos == 0 && cursor == 0) || store->tokens[i].pos > cursor);
            cursor = store->tokens[i].pos + store->tokens[i].len - 1;
        }
    }
    
    // Assert(lf->token_store_sentinel.prev->num_tokens != 0);
    
    Assert(tokens_before_store == lf->total_tokens);
}

#if INTERNAL
#define DEBUG_VerifyTokenStoreIntegrity(lf) _DEBUG_VerifyTokenStoreIntegrity(lf)
#else
#define DEBUG_VerifyTokenStoreIntegrity(lf)
#endif

internal void
DeleteTokensFromStore(LoadedFile *lf, TokenStore *store, u32 delete_index, u32 num_to_delete)
{
    Assert(delete_index >= store->tokens_before_store && 
           delete_index < store->tokens_before_store + store->num_tokens);

    u32 nominal_delete_index = delete_index - store->tokens_before_store;
    memmove(store->tokens + nominal_delete_index,
            store->tokens + nominal_delete_index + num_to_delete,
            (store->num_tokens - (nominal_delete_index + num_to_delete)) * sizeof(Token));
            
    store->num_tokens -= num_to_delete;

    for(TokenStore *shift_store = store->next;
        shift_store != &lf->token_store_sentinel;
        shift_store = shift_store->next)
    {
        shift_store->tokens_before_store -= num_to_delete;
    }

    if(store->num_tokens == 0)
    {
        DL_REMOVE(store);
        FreeTokenStore(store);
    }
}

internal void
DeleteTokens(LoadedFile *lf, Token *first_token, u32 num_to_delete)
{
    if(num_to_delete)
    {
        DEBUG_VerifyTokenStoreIntegrity(lf);
        
        // shift all the tokens backward
        TokenStore *first_store = GetTokenStoreContainingToken(lf, first_token);
        // NOTE:: this needs to be here since the token store might be spontaneously deleted
        TokenStore *second_store = first_store->next;

        Assert((first_token - first_store->tokens) < first_store->num_tokens);
        
        // TODO: Refactor into a do while loop
        
        u32 current_delete_index = (first_token - first_store->tokens) + first_store->tokens_before_store;
        
        u32 num_left_to_delete = num_to_delete;
        
        Assert(current_delete_index >= first_store->tokens_before_store)
        u32 nominal_delete_index = current_delete_index - first_store->tokens_before_store;
        u32 num_to_delete_in_store = Min(num_left_to_delete, first_store->num_tokens - nominal_delete_index);
        DeleteTokensFromStore(lf, first_store, current_delete_index, num_to_delete_in_store);
        num_left_to_delete -= num_to_delete_in_store;
        // current_delete_index += num_to_delete_in_store;
        
        TokenStore *store = second_store;
        while(num_left_to_delete) 
        {
            // NOTE:: this must be here for reason above
            TokenStore *next_store = store->next;

			Assert(current_delete_index >= store->tokens_before_store);
            u32 nominal_delete_index = current_delete_index - store->tokens_before_store;
            u32 num_to_delete_in_store = Min(num_left_to_delete, store->num_tokens - nominal_delete_index);
            DeleteTokensFromStore(lf, store, current_delete_index, num_to_delete_in_store);
                
            num_left_to_delete -= num_to_delete_in_store;
            // current_delete_index += num_to_delete_in_store;
            store = next_store;
        }


        lf->total_tokens -= num_to_delete;
        DEBUG_VerifyTokenStoreIntegrity(lf);
    }
}

internal TokenStore *
AllocateTokenStoreAndSplitContent(LoadedFile *lf, TokenStore *old_store)
{
    u64 alloc_size = Kilobytes(64);
    TokenStore *new_store = (TokenStore *) platform.AllocateMemory(alloc_size);

    u32 tokens_in_old_store = old_store->num_tokens / 2;
    u32 tokens_in_new_store = old_store->num_tokens - tokens_in_old_store;
    
    new_store->tokens_before_store = old_store->tokens_before_store + tokens_in_old_store;
    new_store->tokens = (Token *)((u8 *)new_store + sizeof(TokenStore));
    new_store->max_tokens = (alloc_size - sizeof(TokenStore)) / sizeof(Token) - 200;

    Assert(tokens_in_new_store < new_store->max_tokens);
    old_store->num_tokens = tokens_in_old_store;
    new_store->num_tokens = tokens_in_new_store;

    memcpy(new_store->tokens,
           old_store->tokens,
           tokens_in_new_store * sizeof(Token));
    
    DL_INSERT_AFTER(old_store, new_store);
    return new_store;
}

internal TokenStore *
AllocateTokenStore(LoadedFile *lf, TokenStore *after_store)
{
    u64 alloc_size = Kilobytes(64);
    TokenStore *store = (TokenStore *) platform.AllocateMemory(alloc_size);
    
    if(after_store == 0)
    {
        after_store = &lf->token_store_sentinel;
    }

    store->tokens_before_store = after_store->tokens_before_store + after_store->num_tokens;
    store->tokens = (Token *)((u8 *)store + sizeof(TokenStore));
    store->max_tokens = (alloc_size - sizeof(TokenStore)) / sizeof(Token) - 200;
    store->num_tokens = 0;
    
    
    DL_INSERT_AFTER(after_store, store);
    return store;
}

inline TokenStore *
AllocateAndPushBackTokenStore(LoadedFile *lf)
{
    TokenStore *result = AllocateTokenStore(lf, lf->token_store_sentinel.prev);
    return result;
}

inline u32
AvailableSpace(TokenStore *store)
{
    u32 result;
    result = store->max_tokens - store->num_tokens;
    return result;
}

inline TokenStore *
GetFirstTokenStore(LoadedFile *lf)
{
    return lf->token_store_sentinel.next;
}

inline TokenStore *
GetLastTokenStore(LoadedFile *lf)
{
    return lf->token_store_sentinel.prev;
}

inline Token *
GetLastToken(LoadedFile *lf)
{
    Token *result = 0;
    TokenStore *last_store = GetLastTokenStore(lf);
    
    if(last_store->num_tokens > 0)
    {
        result = last_store->tokens + last_store->num_tokens - 1;
    }
    else
    {
        Assert(last_store == lf->token_store_sentinel.prev);
    }
    
    return result;
}

inline TokenStore *
GetOrAllocateFirstTokenStore(LoadedFile *lf)
{
    TokenStore *result = GetFirstTokenStore(lf);
    if(result == &lf->token_store_sentinel)
    {
        result = AllocateAndPushBackTokenStore(lf);
    }
    return result;
}

internal void
InsertTokensIntoStore(LoadedFile *lf, TokenStore *store, u32 insert_index, Token *tokens, u32 num_to_insert)
{
    Assert(store->num_tokens + num_to_insert <= store->max_tokens);
    u32 nominal_insert_index = insert_index - store->tokens_before_store;
    memmove(store->tokens + nominal_insert_index + num_to_insert,
            store->tokens + nominal_insert_index,
            (store->num_tokens - nominal_insert_index) * sizeof(Token));

    // CopyElements(store->tokens + nominal_insert_index,
    //              tokens,
    //              num_to_insert);
    memcpy(store->tokens + nominal_insert_index,
           tokens,
           num_to_insert * sizeof(Token));
    
    store->num_tokens += num_to_insert;
    lf->total_tokens += num_to_insert;
    
    for(TokenStore *shift_store = store->next;
        shift_store != &lf->token_store_sentinel;
        shift_store = shift_store->next)
    {
        shift_store->tokens_before_store += num_to_insert;
    }
}

internal void
InsertTokens(LoadedFile *lf, Token *insert_after, Token *tokens, u32 num_to_insert)
{
    if(num_to_insert > 0)
    {
        u32 insert_index = 0;
        TokenStore *first_store;
        if(insert_after == 0) 
        {
            first_store = GetOrAllocateFirstTokenStore(lf);
            if(AvailableSpace(first_store) == 0)
            {
                first_store = AllocateTokenStore(lf, 0);
            }
            insert_index = 0;
        } 
        else 
        {
            first_store = FindTokenStoreContainingCursor(lf, insert_after->pos);
            Assert(first_store);
        
            u32 insert_after_position_in_store = insert_after - first_store->tokens;
            Assert(insert_after_position_in_store < first_store->num_tokens);

            u32 insert_after_index = insert_after_position_in_store + first_store->tokens_before_store;
            insert_index = insert_after_index + 1;
        }
        DEBUG_VerifyTokenStoreIntegrity(lf);
        u32 tokens_left_to_insert = num_to_insert;

        u32 num_to_insert_in_store = Min(tokens_left_to_insert, AvailableSpace(first_store));
        InsertTokensIntoStore(lf, first_store, insert_index, tokens, num_to_insert_in_store);

        // TODO compression don't update three variables update just one instead
        tokens += num_to_insert_in_store;
        insert_index += num_to_insert_in_store;
        tokens_left_to_insert -= num_to_insert_in_store;
        DEBUG_VerifyTokenStoreIntegrity(lf);
        if(tokens_left_to_insert > 0)
        {
            TokenStore *new_store = AllocateTokenStore(lf, first_store);
            TokenStore *insert_store = 0;
            
            if(insert_index < new_store->tokens_before_store)
            {
                // move the tokens after the insert point to a new store
                u32 insert_index_in_old_store = insert_index - first_store->tokens_before_store;

                u32 tokens_after_insert = first_store->num_tokens - insert_index_in_old_store;
                memcpy(new_store->tokens,
                       first_store->tokens + insert_index_in_old_store,
                       tokens_after_insert * sizeof(Token));
                new_store->num_tokens = tokens_after_insert;
                
                first_store->num_tokens -= tokens_after_insert;
                new_store->tokens_before_store = first_store->num_tokens + first_store->tokens_before_store;
                
                DEBUG_VerifyTokenStoreIntegrity(lf);
                
                insert_store = first_store;
            } else {
                Assert(insert_index == new_store->tokens_before_store)
                insert_store = new_store;
            }
            
            u32 num_to_insert_in_store = Min(tokens_left_to_insert, AvailableSpace(insert_store));
            InsertTokensIntoStore(lf, insert_store, insert_index, tokens, num_to_insert_in_store);
            tokens += num_to_insert_in_store;
            insert_index += num_to_insert_in_store;
            tokens_left_to_insert -= num_to_insert_in_store;            
            DEBUG_VerifyTokenStoreIntegrity(lf);
        }
        
        TokenStore *previous_store = first_store;
        while(tokens_left_to_insert)
        {
            TokenStore *store = AllocateTokenStore(lf, previous_store);

            u32 num_to_insert_in_store = Min(tokens_left_to_insert, AvailableSpace(store));
            
            InsertTokensIntoStore(lf, store, insert_index, tokens, num_to_insert_in_store);
            
            tokens += num_to_insert_in_store;
            insert_index += num_to_insert_in_store;
            tokens_left_to_insert -= num_to_insert_in_store;
            
            previous_store = store;
            DEBUG_VerifyTokenStoreIntegrity(lf);
        }
    }
    
    DEBUG_VerifyTokenStoreIntegrity(lf);
    // TODO for now, this function only handles inserting tokens into a single store
    // Assert(num_to_insert < store->max_tokens);
    // if(store->num_tokens + num_to_insert >= store->max_tokens)
    // {
        

        // if(num_to_insert < store->max_tokens) {
        //     TokenStore *new_store = AllocateTokenStoreAndSplitContent(lf, store);
        //     if(insert_index >= new_store->tokens_before_store) {
        //         store = new_store;
        //     }
        //     InsertTokensIntoStore(lf, store, tokens, num_to_insert);
        // } else {
            
        //     Assert(!"TODO not implemented: split at insert point and create enough token stores for it!");
        // }
    // }

}

internal void
ShiftTokensAheadCursor(LoadedFile *lf, Cursor cursor, i32 chars_to_shift)
{
    Token *first_shift_token = GetNextTokenOrNearestForward(lf, cursor);

    if(first_shift_token)
    {
        TokenStore *store = FindTokenStoreContainingCursor(lf, first_shift_token->pos);
        Assert(store);

        Token *token = first_shift_token;
        for(;;)
        {
            token->pos += chars_to_shift;

            token = GetNextToken(lf, token, &store);
            if(token == 0)
            {
                break;
            }
        }
    }
}

internal Token *
GetTokenAtCursorOrNearestForward(LoadedFile *lf, Cursor cursor)
{
    Token *ret = 0;
    Token *cursor_token = GetTokenAtCursor(lf, cursor);

    if(cursor_token)
    {
        ret = cursor_token;
    }
    else
    {
        Token *nearest_forward = GetNextTokenOrNearestForward(lf, cursor);
        ret = nearest_forward;
    }
    
    return ret;
}

internal Token *
GetTokenAtCursorOrNearestBackward(LoadedFile *lf, Cursor cursor)
{
    Token *ret = 0;
    Token *cursor_token = GetTokenAtCursor(lf, cursor);

    if(cursor_token)
    {
        ret = cursor_token;
    }
    else
    {
        Token *nearest_backward = GetPreviousTokenOrNearestBackward(lf, cursor);
        ret = nearest_backward;
    }

    return ret;
}

internal void
PushBackTokens(LoadedFile *lf, Token *tokens, u32 num_tokens)
{   
    TokenStore *store = lf->token_store_sentinel.prev;
    u32 tokens_to_push = Min(num_tokens, store->max_tokens - store->num_tokens);

    if(tokens_to_push == 0 && num_tokens) {
        store = AllocateAndPushBackTokenStore(lf);
        tokens_to_push = Min(num_tokens, store->max_tokens - store->num_tokens);
    }

    Assert(store->num_tokens + tokens_to_push <= store->max_tokens);
    memcpy(store->tokens + store->num_tokens,
           tokens,
           tokens_to_push * sizeof(Token));
    store->num_tokens += tokens_to_push;
    lf->total_tokens += tokens_to_push;

    if(num_tokens - tokens_to_push > 0)
    {
        PushBackTokens(lf, tokens + tokens_to_push, num_tokens - tokens_to_push);
    }
}
