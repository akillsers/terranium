#include <intrin.h>
#include "common.h"
#include "math.cpp"
#define UNICODE 1

#include "mutex.h"
#include "storage.h"   // custom memory management
#include "string.cpp"  // a general string libary
#include "terranium_platform.h"


// the editor source code
#include "editor.cpp"

// windows include files
#include "terranium_windows.h"
#include "win_string.cpp"
#include "font_dwrite.cpp"
#include "win_drag_drop.cpp"

#include "terranium_d3d9.cpp"
#include "terranium_d3d11.cpp"

inline WIN_FramebufferHeader *
GetFramebufferHeader(Framebuffer *framebuffer)
{
    return (WIN_FramebufferHeader *)((u8 *)framebuffer - sizeof(WIN_FramebufferHeader));
}

inline void
WIN_PrintWindowState(WIN_WindowStateContent *window_state_content)
{
    char debug[1024];
    snprintf(debug, sizeof(debug),
             "window x %i\n"
             "window y %i\n"
             "window width %i\n"
             "window height %i\n"
             "maximized? %i\n",
             window_state_content->restored_x,
             window_state_content->restored_y,
             window_state_content->restored_width,
             window_state_content->restored_height,
             window_state_content->maximized);

    OutputDebugStringA(debug);
}

inline void
WIN_DoWork(MemoryArena *thread_arena)
{
try_again:
    // Atomically grabs a piece of work to do
    u32 next_entry_to_do = g_win32.next_entry_to_do;
    u32 new_next_entry_to_do = (next_entry_to_do + 1) % WIN_MAXIMUM_JOBS;
    u32 work_index = InterlockedCompareExchange(&g_win32.next_entry_to_do, new_next_entry_to_do, next_entry_to_do);
    if(work_index == next_entry_to_do)
    {
        WIN_WorkEntry *work = &g_win32.jobs[work_index];

        work->entry->function(work->entry->data, thread_arena);
        InterlockedIncrement(&work->queue->completed_jobs);
    }
    else
    {
        goto try_again;
    }
}

internal void
WIN_CompleteAllWork(PlatformWorkQueue *work_queue)
{
    while(work_queue->completed_jobs != work_queue->total_jobs)
    {
        DWORD wait_result = WaitForSingleObject(g_win32.jobs_available, 0);
        if(wait_result == WAIT_OBJECT_0)
        {
            WIN_DoWork(&platform.frame_arena);
        }
    }

    for(int i = 0; i < g_win32.num_worker_threads; i++)
    {
        ResetArena(&g_win32.thread_arena[i]);
    }
}

internal void
WIN_AddWorkQueueEntry(PlatformWorkQueue *work_queue, PlatformWorkEntry *job)
{
    for(;;)
    {
        u32 original_next_write_index = g_win32.next_write_index;
        u32 new_next_write_index = (original_next_write_index + 1) % WIN_MAXIMUM_JOBS;
        
        if(original_next_write_index == (g_win32.next_entry_to_do + WIN_MAXIMUM_JOBS - 1) % WIN_MAXIMUM_JOBS)
        {
            // For now, we just spinlock when the job FIFO is full
            // TODO: do something more sophisticated in this case
            //       to wake up when an entry is done?
            continue;
        }
        
        u32 write_index = InterlockedCompareExchange(&g_win32.next_write_index, new_next_write_index, original_next_write_index);
        if(write_index == original_next_write_index)
        {
            WIN_WorkEntry work_entry = {};
            work_entry.queue = work_queue;
            work_entry.entry = job;
            
            g_win32.jobs[write_index] = work_entry;
            
            BOOL release_success = ReleaseSemaphore(g_win32.jobs_available, 1, NULL);
            Assert(release_success);
            
            InterlockedIncrement(&work_queue->total_jobs);
            break;
        }    
    }    
}

struct WIN_WorkerThreadInfo
{
    u32 thread_id;
};

DWORD WINAPI 
WIN_WorkerThread(LPVOID parameter)
{
    DWORD wait_result;
    
    WIN_WorkerThreadInfo *thread_info = (WIN_WorkerThreadInfo *) parameter;

    for(;;)
    {
        wait_result = WaitForSingleObject(g_win32.jobs_available, INFINITE);
        if(wait_result == WAIT_OBJECT_0)
        {
            MemoryArena *thread_arena = &g_win32.thread_arena[thread_info->thread_id];
            WIN_DoWork(thread_arena);
        }
        else
        {
            // TODO: Logging
            Assert(!"Unexpected wait result.");
        }
    }
}

internal void
WIN_InitMultithreading()
{
    g_win32.jobs_available = CreateSemaphore(NULL /*lpSemaphoreAttributes*/, 
                                             0 /*lInitialCount*/, 
                                             WIN_MAXIMUM_JOBS /*lMaximumCount*/, 
                                             0 /*lpName*/);
    
    // --Create workers--
    
    // TODO: Set this to the actual number of logical processors
    // TODO: Testing check that our logical processor detecting code works on multiple Windows machines.
    u32 num_logical_processors = 4;
    
    WIN_WorkerThreadInfo *thread_infos = PushCount(&g_win32.permanent_arena, WIN_WorkerThreadInfo, num_logical_processors);

    g_win32.num_worker_threads = num_logical_processors;
    g_win32.thread_arena = PushCount(&g_win32.permanent_arena, MemoryArena, g_win32.num_worker_threads);
    for(int i = 0; i < num_logical_processors; ++i)
    {
        DWORD thread_id = 0;
        
        thread_infos[i].thread_id = i;

        CreateThread(NULL,
                     0 /*dwStackSize*/,
                     WIN_WorkerThread,
                     &thread_infos[i],
                     0,
                     &thread_id);
    }
}

internal void
WIN_HandleKeyChange(KeyState *key, b32 new_state)
{

    if(new_state != key->down)
    {
        key->down = new_state;
        key->transition_count++;
    }

    if(new_state)
    {
        key->down_repeats++;
    }
}

internal PlatformWindowInfo
GetPlatformWindowInfo(WIN_WindowData *window)
{
    PlatformWindowInfo window_info;
    
    window_info.resized = window->resized;
    window_info.moved = window->moved;
    window_info.size_state = window->size_state;
    window_info.has_focus = window->has_focus;
    window_info.titlebar_type = window->titlebar_type;
    
	Recti client_rect = WIN_GetClientRect(window);
	
	// TODO factor this into pos() and dim()
	window_info.pos = vec2i(client_rect.x, client_rect.y);
    window_info.size = vec2i(client_rect.width, client_rect.height);

    return window_info;
}

internal Recti
WIN_GetClientRect(WIN_WindowData *window)
{
	Recti result = {};
	
	// this is taken from https://stackoverflow.com/questions/15734528/client-rectangle-coordinates-on-screen
    // TODO can we just use GetWindowRect?
	RECT rect = {};
	GetClientRect(window->hwnd, &rect);
	MapWindowPoints(window->hwnd, HWND_DESKTOP, (LPPOINT) &rect, 2);
	
	result.x = rect.left;
	result.y = rect.top;
	result.width = rect.right - rect.left;
	result.height = rect.bottom - rect.top;

	return result;
}

LRESULT CALLBACK
WIN_MoveLoopMouseHook(int n_code, WPARAM w_param, LPARAM l_param)
{
    // NOTE: we use a mouse hook to get WM_LBUTTONUP messages when the 
    //       the window is in the resize modal loop because they are not
    //       sent to our window proc for some reason

    if(n_code == HC_ACTION &&
       w_param == WM_LBUTTONUP)
    {
        if(g_win32.mouse_hook)
        {
            BOOL success = UnhookWindowsHookEx(g_win32.mouse_hook);
            Assert(success);

            g_win32.mouse_hook = 0;
        }

        KeyState *key = g_win32.input.mouse_buttons + MouseButton_left;
        WIN_HandleKeyChange(key, false);

    }

    return CallNextHookEx(0, n_code, w_param, l_param);
}

// this is shared by implementaions of swap chain backends
internal void
WIN_UpdateFramebuffer(WIN_WindowData *window, void *bits, u32 pitch, PixelFormat format)
{
    Framebuffer *framebuffer = window->framebuffer;
    WIN_FramebufferHeader *header = GetFramebufferHeader(framebuffer);
    
    Recti *dirty_rects = window->framebuffer->dirty;
    u32 num_rects = window->framebuffer->dirty_rects_used;

    for(int k = 0; k < num_rects; k++)
    {
        Recti *rect = dirty_rects + k;
        i32 left = rect->x;
        i32 right = rect->x + rect->width;
        i32 top = rect->y;

        if(WIN_IsCustomDrawingTopBorder(window))
        {
            top += 1;
        }

        if(WIN_IsCustomDrawingSideAndBottomBorders(window))
        {
            u32 size_width = GetSystemMetrics(SM_CXFRAME);
            left += size_width;
            right += size_width;
        }
        
        
       i32 bottom = top + rect->height;

        Assert(framebuffer->bytespp == 4);
        
        u8 *src_row = (u8 *)header->platform_pixels + left*framebuffer->bytespp + top*framebuffer->stride;
        u8 *staging_row = (u8 *)bits + left*framebuffer->bytespp + top*pitch;

        if(format == PixelFormat_B8G8R8A8)
        {
            // HIGH PRIORITY TODO:
            // we need an efficient blit method (look up SDL)
            // blit surface for sse and stuff)
            for(int i = top;
                i < bottom;
                i++)
            {
                u32 *src_pix = (u32 *)src_row;
                u32 *staging_pix = (u32 *)staging_row;

                //NOTE:: the src format is the exact same 32 bit format as the dest format
                //       so just copy u32 by u32
                for(int j = left;
                    j < right;
                    j++)
                {
                    *staging_pix++ = *src_pix++;
                }

                src_row += framebuffer->stride;
                staging_row += pitch;
            }
        }
        else
        {
            Assert(!"Unsupported format!");
        }
    }
}


internal void
WIN_ResizeFramebuffer(WIN_WindowData *window)
{
    Framebuffer *framebuffer = window->framebuffer;
    WIN_FramebufferHeader *header = GetFramebufferHeader(framebuffer);

	Recti client_rect = WIN_GetClientRect(window);

    header->platform_width = client_rect.width;
    header->platform_height = client_rect.height;
    framebuffer->bytespp = 4; 
    framebuffer->stride = Align16(header->platform_width * framebuffer->bytespp);
    u32 mem_size = framebuffer->stride * header->platform_height;

    if(framebuffer->pixels)
    {
        VirtualFree(framebuffer->pixels, 0, MEM_RELEASE);
    }
    // NOTE:: this will be aligned to at least 16k
    header->platform_pixels = VirtualAlloc(0, mem_size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    Assert(header->platform_pixels);

    if(WIN_IsCustomDrawingTopBorder(window))
    {
        Assert(header->platform_height > 1);
        framebuffer->height = header->platform_height - 1;
        framebuffer->pixels = (u8 *)header->platform_pixels + framebuffer->stride;
    }
    else
    {
        framebuffer->height = header->platform_height;
        framebuffer->pixels = header->platform_pixels;
    }

    if(WIN_IsCustomDrawingSideAndBottomBorders(window))
    {
        u32 size_width = GetSystemMetrics(SM_CXFRAME);
        framebuffer->height -= size_width;
        
        framebuffer->pixels = (u8 *)framebuffer->pixels + size_width*framebuffer->bytespp;
        framebuffer->width = header->platform_width - size_width*2;
    }
    else
    {
        framebuffer->width = header->platform_width;
    }

}

internal void *
WIN_AllocateMemory(u64 size_required)
{
    void *allocated_memory = VirtualAlloc(0, size_required, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    return allocated_memory;
}

internal void
WIN_FreeMemory(void *ptr)
{
    VirtualFree(ptr, 0, MEM_RELEASE);
}

internal void
panic(char *message, ...)
{
    char buf[512];
    va_list args;

    va_start(args, message);
    vsnprintf(buf, sizeof(buf), message, args);
    va_end(args);

    MessageBoxA(g_win32.main_window->hwnd, buf, "Fatal error", MB_ICONERROR | MB_OK);
    ExitProcess(0);
}

internal void
WIN_SetCursorIcon(CursorIconType cursor_icon)
{
    if(cursor_icon != g_win32.cursor_icon)
    {
        g_win32.cursor_icon = cursor_icon;
    }
}
internal void
WIN_Info(char *message, ...)
{
    char buf[512];
    va_list args;
    
    va_start(args, message);
    vsnprintf(buf, sizeof(buf), message, args);
    va_end(args);
    OutputDebugStringA(buf);
}

// NOTE that this api currently only retrieves plain text data only
internal void
WIN_CopyToClipboard(String string)
{
    if(OpenClipboard(g_win32.main_window->hwnd))
    {
        EmptyClipboard();
        // string.len * 4 is the maximum size that we require
        // TODO is this too big for GlobalAlloc?? what's the maximum GlobalAlloc and allocate?
        UTF16_String utf16_string = WIN_StringToUTF16(string, &g_win32.frame_arena);
        
        u64 unicode_alloc_size = (utf16_string.count + 1) * sizeof(WCHAR); 
        HGLOBAL unicode_clipboard_memory_handle = GlobalAlloc(GMEM_MOVEABLE, unicode_alloc_size);
        Assert(unicode_clipboard_memory_handle);

        WCHAR *unicode_clipboard_memory_data = (WCHAR *)GlobalLock(unicode_clipboard_memory_handle);
        memcpy(unicode_clipboard_memory_data,
               utf16_string.ptr,
               utf16_string.count * sizeof(WCHAR));

        unicode_clipboard_memory_data[utf16_string.count] = 0;
        
        GlobalUnlock(unicode_clipboard_memory_handle);

        
        SetClipboardData(CF_UNICODETEXT, unicode_clipboard_memory_handle);
        
        ASCII_String ascii_string = WIN_StringToASCII(string, &g_win32.frame_arena);
        u64 ascii_alloc_size = ascii_string.len + 1;
        HGLOBAL ascii_clipboard_memory_handle = GlobalAlloc(GMEM_MOVEABLE, ascii_alloc_size);
        Assert(ascii_clipboard_memory_handle);
        
        u8 *ascii_clipboard_memory_data = (u8 *)GlobalLock(ascii_clipboard_memory_handle);
        memcpy(ascii_clipboard_memory_data,
               ascii_string.ptr,
               ascii_string.len);
        ascii_clipboard_memory_data[ascii_string.len] = 0;
        GlobalUnlock(ascii_clipboard_memory_handle);
        
        SetClipboardData(CF_TEXT, ascii_clipboard_memory_data);

        CloseClipboard();
    }
}

internal PlatformClipboardPasteResult
WIN_PasteFromClipboard() 
{
    PlatformClipboardPasteResult result = {};
    
    HANDLE contents_handle = 0;

    // TODO should we be accepting CF_OEMTEXT or any other plain text formats??
    if(IsClipboardFormatAvailable(CF_UNICODETEXT))
    {
        if(OpenClipboard(g_win32.main_window->hwnd))
        {
            contents_handle = GetClipboardData(CF_UNICODETEXT);
            u8 *raw_data = (u8 *)GlobalLock(contents_handle);

            // TODO remove this arbitrary limitation of 24kb clipboard paste data
            u32 max_buffer_size = Kilobytes(24);
            
            u32 offset = 0;
            u32 bytes_outputted = 0;
            WCHAR *unicode_text = (WCHAR *)raw_data;
            String string = WIN_UTF16ToString(unicode_text, &g_win32.frame_arena);
            result.string.ptr = string.ptr;
            result.string.len = string.len;
             
            GlobalUnlock(contents_handle);

            result.is_valid = true;
            CloseClipboard();
        }
    }
    else if(IsClipboardFormatAvailable(CF_TEXT))
    {
        if(OpenClipboard(g_win32.main_window->hwnd))
        {
            if(IsClipboardFormatAvailable(CF_TEXT))
            {
                contents_handle = GetClipboardData(CF_TEXT);
            } else {
                contents_handle = GetClipboardData(CF_OEMTEXT);
            }
            char *raw_data = (char *)GlobalLock(contents_handle);

            // TODO remove this arbitrary limitation of 24kb clipboard paste data
            u32 max_buffer_size = Kilobytes(24);
            result.string.ptr = PushCount(&g_win32.frame_arena, UTF8, max_buffer_size);

            u32 bytes_outputted = 0;
            CHAR character = *raw_data;
            while(character && bytes_outputted < max_buffer_size)
            {
                result.string.ptr[bytes_outputted] = character;
                bytes_outputted++;
                character = raw_data[bytes_outputted];
            }
            
            result.string.len = bytes_outputted;
            GlobalUnlock(contents_handle);

            result.is_valid = true;
            CloseClipboard();
        }
    }

    return result;
}

internal b32
WIN_DirectoryExists(String directory)
{
    b32 result = false;
    
    WCHAR *directory_wchars = WIN_StringToWCHAR(directory, &g_win32.frame_arena);
    DWORD attributes = GetFileAttributesW(directory_wchars);

    if(attributes != INVALID_FILE_ATTRIBUTES &&
       attributes & FILE_ATTRIBUTE_DIRECTORY)
    {
        result = true;
    }

    return result;
}

internal b32
WIN_CreateDirectory(String path)
{
    WCHAR *path_wchars = WIN_StringToWCHAR(path, &g_win32.frame_arena);
    b32 success = CreateDirectoryW(path_wchars, 0 /*lpSecurityAttributes*/);

    if(!success)
    {
        int last_err = GetLastError();
        if(last_err = ERROR_PATH_NOT_FOUND)
            g_win32.last_file_error = FileError_directory_not_found;
        else if (last_err == ERROR_ACCESS_DENIED)
            g_win32.last_file_error = FileError_access_denied;
        else if(last_err == ERROR_SHARING_VIOLATION)
            g_win32.last_file_error = FileError_in_use;
        else if(last_err == ERROR_ALREADY_EXISTS)
            g_win32.last_file_error = FileError_already_exists;
        else
            g_win32.last_file_error = FileError_unknown_err;
    }

    return success;
}

internal String
WIN_GetSpecialDirectory(WIN_SpecialDirectory special_dir)
{
    String result = {};
    HRESULT hresult;

    WCHAR* returned_path = 0; 
    switch(special_dir)
    {
        case WIN_SpecialDirectory_ApplicationData:
        {
            hresult = SHGetKnownFolderPath(FOLDERID_LocalAppData,
                                           KF_FLAG_DEFAULT,
                                           0, /*access token*/
                                           &returned_path);

        } break;
        
        InvalidDefaultCase;
    }

    if(returned_path)
    {
        String returned_dir_utf8 = WIN_UTF16ToString(returned_path, &g_win32.frame_arena);
        String append = PushJoinedString(FromCharArray("\\"), APPLICATION_NAME, &g_win32.frame_arena);
        result = PushJoinedString(returned_dir_utf8, append, &g_win32.frame_arena);

        CoTaskMemFree(returned_path);
        
        if(!WIN_DirectoryExists(result))
        {
            if(!WIN_CreateDirectory(result))
            {
                Assert(false);
                result = {};
            }
        }
        
    }
    
    return result;
}

internal u32
WIN_ReadFile(PlatformFileHandle handle, void *buffer, u32 buffer_size)
{
    DWORD bytes_read = 0;
    BOOL success = ReadFile(handle, buffer, buffer_size, &bytes_read, 0);
    if(!success)
    {
        // TODO error checking
        int last_err = GetLastError();
        
        if(last_err = ERROR_INVALID_HANDLE)
        {
            last_err = FileError_invalid_handle;
        }
        else
        {
            last_err = FileError_unknown_err;
        }
    }
    return bytes_read;
} 

internal FileError
WIN_GetLastFileError()
{
    FileError ret = g_win32.last_file_error;
    g_win32.last_file_error = FileError_no_error;
    return ret;
}

PlatformFileHandle
_WIN_CreateFile(String path, FileAccess access, DWORD creation_disposition)
{
    WCHAR *wchar_path = WIN_StringToWCHAR(path, &g_win32.frame_arena);

    DWORD dwaccess = 0;
    if(access & FileAccess_read)
        dwaccess |= GENERIC_READ;
    if(access & FileAccess_write)
        dwaccess |= GENERIC_WRITE;

    // TODO use unicode version so that long file paths (> MAX_PATH) are supported
    HANDLE handle = CreateFileW(wchar_path,
                                dwaccess,
                                // as a text editor we can let others edit this file too
                                FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, 
                                0, // lpSecurityAttributes
                                creation_disposition,
                                FILE_ATTRIBUTE_NORMAL,
                                0);// hTemplateFile
    if(handle != INVALID_HANDLE_VALUE)
    {
        return (PlatformFileHandle)handle;
    }
    else
    {
        int last_err = GetLastError();
        if(last_err == ERROR_PATH_NOT_FOUND)
            g_win32.last_file_error = FileError_nonexistent;
        else if(last_err == ERROR_FILE_NOT_FOUND)
            g_win32.last_file_error = FileError_nonexistent;
        else if (last_err == ERROR_ACCESS_DENIED)
            g_win32.last_file_error = FileError_access_denied;
        else if(last_err == ERROR_SHARING_VIOLATION)
            g_win32.last_file_error = FileError_in_use;
        else if(last_err == ERROR_ALREADY_EXISTS)
            g_win32.last_file_error = FileError_already_exists;
        else
            g_win32.last_file_error = FileError_unknown_err;

        // TODO as far as I know 0 can never be valid handle value but that might be really wrong!
        // TODO translate windows HANDLE to our own handles
        return 0;
    }
}

PlatformFileHandle
WIN_OpenFile(String path, FileAccess access)
{
    return _WIN_CreateFile(path, access, OPEN_EXISTING);
}

PlatformFileHandle
WIN_CreateNewFile(String path, FileAccess access)
{
    return _WIN_CreateFile(path, access, CREATE_NEW);
}

PlatformFileHandle
WIN_OverwriteFile(String path, FileAccess access)
{
    return _WIN_CreateFile(path, access, TRUNCATE_EXISTING);
}

internal void
WIN_AttemptToLoadWindowState()
{
    String application_data_directory = WIN_GetSpecialDirectory(WIN_SpecialDirectory_ApplicationData);
    String window_state_path = PushJoinedString(application_data_directory, "\\Window", &platform.frame_arena);

    g_win32.window_state = WIN_OpenFile(window_state_path, FileAccess_read | FileAccess_write);

    if(!g_win32.window_state)
    {
        FileError file_error = WIN_GetLastFileError();
        switch(file_error)
        {
            case FileError_nonexistent:
            {
                g_win32.window_state = WIN_CreateNewFile(window_state_path, FileAccess_read | FileAccess_write);
            } break;
                        
            case FileError_in_use:
            default:
            {
                WIN_Info("Could not access the user state file!\n");
            }
        }
    }    
}

internal Recti
WIN_GetWindowRestoredRect(PlatformWindowHandle handle)
{
    WIN_WindowData *window = 0;

    if(handle)
    {
        window = (WIN_WindowData *)handle;
    }
    else
    {
        window = g_win32.main_window;
    }

    WINDOWPLACEMENT window_placement = {};
    b32 success = GetWindowPlacement(window->hwnd, &window_placement);
    
    Recti result = {};

    if(success)
    {
        result.x = window_placement.rcNormalPosition.left;
        result.y = window_placement.rcNormalPosition.top;
        result.width = window_placement.rcNormalPosition.right - window_placement.rcNormalPosition.left;
        result.height = window_placement.rcNormalPosition.bottom - window_placement.rcNormalPosition.top;
    }
    
    return result;
}

internal b32
WIN_EdgeHasAutoHideAppbar(u32 edge, HMONITOR monitor)
{
    b32 result;
    APPBARDATA taskbar_data = {sizeof(APPBARDATA)};
    taskbar_data.uEdge = edge;

    MONITORINFO monitor_info = {sizeof(MONITORINFO)};
    b32 success = GetMonitorInfoW(monitor, &monitor_info);

    taskbar_data.rc = monitor_info.rcMonitor;

    int before_last_error = GetLastError();
    Assert(before_last_error == 0);
    
    HWND window = (HWND)SHAppBarMessage(ABM_GETAUTOHIDEBAREX, &taskbar_data);
    if(!window)
    {
        int last_error = GetLastError();
        Assert(last_error == 0);
    }

    if(window)
    {
        result = true;
    }
    else
    {
        result = false;
    }
    
    return result;
}


internal void
WIN_CloseFile(PlatformFileHandle handle)
{
    CloseHandle(handle);
}

internal void
WIN_SetTitlebar(PlatformWindowHandle window, String string)
{
    WIN_WindowData *data = 0;
    if(window == 0)
    {
        data = g_win32.main_window;
    }
    else
    {
        data = (WIN_WindowData *)window;
    }

    if(string.len == 0)
    {
        //TODO make this less hardcoded
        SetWindowTextW(data->hwnd, L"Terranium");
    }
    else
    {
        //TODO make this less hardcoded
        String titlebar_append = FromCharArray(" - Terranium");
        String titlebar_string = PushJoinedString(string, titlebar_append, &g_win32.frame_arena);

        WCHAR *wchars = WIN_StringToWCHAR(titlebar_string, &g_win32.frame_arena);
        BOOL success = SetWindowTextW(data->hwnd, wchars);
        
        UpdateWindow(data->hwnd);
    }
}

internal u64
WIN_GetFileSize(PlatformFileHandle handle)
{
    LARGE_INTEGER li = {};
    BOOL success = GetFileSizeEx(handle, &li);
    return li.QuadPart;
}

/*write the entire file and set the size of the file to the write_buffer_size*/
internal u32
WIN_WriteEntireFile(PlatformFileHandle handle, void *write_buffer, u32 write_buffer_size)
{
    DWORD bytes_written = 0;

    SetFilePointer(handle, 0, 0, FILE_BEGIN);
    BOOL success = WriteFile(handle, write_buffer, write_buffer_size, &bytes_written, 0);
    if(success)
    {
        // truncation
        SetEndOfFile(handle);
    }
    else
    {
        int last_err = GetLastError();
        if(last_err = ERROR_INVALID_HANDLE)
        {
            g_win32.last_file_error = FileError_invalid_handle;
        }
        else
        {
            g_win32.last_file_error = FileError_unknown_err;
        }
    }

    return bytes_written; 
}

internal void
WIN_RecordWindowState(WIN_WindowData *window)
{
    if(!g_win32.window_state)
    {
        // TODO don't repeatedly try to create his file every pixel that we resize
        WIN_AttemptToLoadWindowState();
    }
            
    if(g_win32.window_state)
    {
        Recti restored_rect = WIN_GetWindowRestoredRect(window);

        WIN_WindowStateContent window_state = {};
        window_state.restored_x = restored_rect.x;
        window_state.restored_y = restored_rect.y;
        window_state.restored_width = restored_rect.width;
        window_state.restored_height = restored_rect.height;

        window_state.maximized = (window->size_state == Window_maximized) ? 1 : 0;
        WIN_WriteEntireFile(g_win32.window_state, &window_state, sizeof(WIN_WindowStateContent));
    }
}

internal void
WIN_Error(char *name, char *message)
{
    MessageBoxA(g_win32.main_window->hwnd, message, name, MB_ICONERROR);
}

// internal void
// WIN_RunScript(char *script_path, PlatformScriptOutputCallbackFunction output_callback)
// {
//     char command_line[4096];
//     // TODO this relies on cmd to be in the path
//     snprintf(command_line, sizeof(command_line), "\"C:\\WINDOWS\\system32\\cmd.exe\" /C  \"%s\"", script_path);

//     SECURITY_ATTRIBUTES security_attributes = {sizeof(SECURITY_ATTRIBUTES)};
//     security_attributes.bInheritHandle = TRUE;
    
//     BOOL success;
//     // create child process STDOUT pipe
//     HANDLE pipe_read_handle;
//     HANDLE pipe_write_handle;
//     success = CreatePipe(&pipe_read_handle, &pipe_write_handle, &security_attributes, 0);

//     Assert(success);
//     // make the read handle to the pipe for STDOUT not inherited
//     success = SetHandleInformation(pipe_read_handle, HANDLE_FLAG_INHERIT, 0);
//     Assert(success);
    
//     STARTUPINFOA startup_info = {sizeof(STARTUPINFO)};
//     PROCESS_INFORMATION process_information = {};
//     startup_info.hStdOutput = pipe_write_handle;
//     startup_info.hStdError = pipe_write_handle;
//     startup_info.dwFlags |= STARTF_USESTDHANDLES;
    
    
//     success = CreateProcessA(0, // application name
//                              command_line,
//                              0, // process security attributes
//                              0, // thread security attributes
//                              TRUE, // inherit handles?
//                              CREATE_NO_WINDOW, // creation flags
//                              0, // environment block
//                              0, // current directory
//                              &startup_info,
//                              &process_information);
//     int error = GetLastError();
//     Assert(success);
//     // CloseHandle(process_information.hProcess);
//     // CloseHandle(process_information.hThread);
//     CloseHandle(pipe_write_handle);
//     // read from the child's pipe
//     DWORD bytes_read;
//     CHAR buffer[4096];
//     success = false;

//     for(;;)
//     {
//         success = ReadFile(pipe_read_handle, buffer, sizeof(buffer), &bytes_read, 0);
//         if(!success || bytes_read == 0) break;
//         output_callback(&g_win32.editor_memory, (UTF8 *)buffer, bytes_read);
//     }
    
// }

internal b32
WIN_IsWindowsVersionOrGreater(WORD major, WORD minor, WORD service_pack)
{
    if(RtlVerifyVersionInfo == 0)
    {
        HMODULE ntdll = LoadLibraryA("ntdll.dll");
        RtlVerifyVersionInfo = (RtlVerifyVersionInfo_Func) GetProcAddress(ntdll, "RtlVerifyVersionInfo");

        int last_error = GetLastError();
        // FreeLibrary(ntdll);
    }

    OSVERSIONINFOEXW version_info = {sizeof(OSVERSIONINFOEXW)};
    version_info.dwMajorVersion = major;
    version_info.dwMinorVersion = minor;
    version_info.wServicePackMajor = service_pack;
    
    DWORD compare = VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR;
    ULONGLONG condition = 0;
    condition = VerSetConditionMask(condition, VER_MINORVERSION, VER_GREATER_EQUAL);
    condition = VerSetConditionMask(condition, VER_MAJORVERSION, VER_GREATER_EQUAL);
    condition = VerSetConditionMask(condition, VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL);
    
    // HACK:: use RtlVerifyVersionInfo which works without
    //        a properly defined manifest with the right
    //        supportedOS GUID
    b32 result = RtlVerifyVersionInfo(&version_info, compare, condition);

    return (result == ERROR_SUCCESS);
    // return result;
}

#define IsWindows8Point1OrGreater()                                     \
    WIN_IsWindowsVersionOrGreater(HIBYTE(_WIN32_WINNT_WINBLUE),         \
                                  HIBYTE(_WIN32_WINNT_WINBLUE), 0 )

#define IsWindows8OrGreater()                                           \
    WIN_IsWindowsVersionOrGreater(HIBYTE(_WIN32_WINNT_WIN8),            \
                                  HIBYTE(_WIN32_WINNT_WIN8), 0 )

#define IsWindows10OrGreater()                  \
    WIN_IsWindowsVersionOrGreater(10, 0, 0)

inline b32
WIN_IsMaximized(HWND hwnd)
{
    b32 result;
    WINDOWPLACEMENT window_placement = {};
    b32 window_placement_succeeded = GetWindowPlacement(hwnd, &window_placement);
    Assert(window_placement_succeeded);

    result = window_placement.showCmd == SW_MAXIMIZE;
    return result;
}


inline b32
WIN_IsCustomDrawingTopBorder(WIN_WindowData *window)
{
    b32 result;
    result = (window->titlebar_type == PlatformTitlebar_custom_drawn &&
              !WIN_IsMaximized(window->hwnd));

    return result;
}

inline b32
WIN_IsCustomDrawingSideAndBottomBorders(WIN_WindowData *window)
{
    b32 result;
    result = (window->titlebar_type == PlatformTitlebar_custom_drawn &&
              !WIN_IsMaximized(window->hwnd) &&
              !IsWindows10OrGreater() && 0);

    return result;
}

inline u64
WIN_QueryPerfCount()
{
    LARGE_INTEGER perf_count_li;
    QueryPerformanceCounter(&perf_count_li);
    return perf_count_li.QuadPart;
}
// THIS function is also kinda needed/already implemented in platform layer
// TODO no need to implement in both platform and editor layers
internal UTF8 *
WIN_OffsetUTF8(UTF8 *utf8_str, u32 str_byte_size, u32 character_offset)
{
    // IN utf8, chars that start with binary bits 10 are continuation bytes, that are part of a multibyte character
    // we skip those
    UTF8 *result = utf8_str;
    u32 chars_to_go = character_offset;

    int offset = 0;
    for(;;)
    {
        if(chars_to_go <= 0)
        {
            result = utf8_str + offset;
            break;
        }
        if((utf8_str[offset] & 0xc0) != 0x80)
        {
            chars_to_go--;
        }

        ++offset;
        
        if(offset >= str_byte_size)
        {
            result = utf8_str + str_byte_size - 1;
        }
    }
    return result;
}

// TODO file name buffer overun?
// TODO easter egg with the help button
// out_file_name is a substring of out_file_path
internal FileDialogResult
WIN_FileDialog(FileDialogType type)
{
    FileDialogResult result = {};

    // TODO: do more detailed logging on failed HRESULT
    HRESULT hr;

    IFileDialog *dialog = 0;
    IFileOpenDialog *open_dialog = 0;

    if(type == FileDialogType_open)
    {
        hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&open_dialog));
        dialog = (IFileDialog *)open_dialog;
    }
    else
    {
        hr = CoCreateInstance(CLSID_FileSaveDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&dialog));
    }
    
    if(FAILED(hr))
    {
        result.error = FileDialogError_unknown;
        return result;
    }

    DWORD flags;

    hr = dialog->GetOptions(&flags);

    if(FAILED(hr))
    {
        result.error = FileDialogError_unknown;
        dialog->Release();
        return result;
    }

    hr = dialog->SetOptions(flags | FOS_FORCEFILESYSTEM | FOS_ALLOWMULTISELECT);

    if(FAILED(hr))
    {
        result.error = FileDialogError_unknown;
        dialog->Release();
        return result;
    }

    COMDLG_FILTERSPEC filter_spec[] =
    {
        { L"All files", L"*.*" },
        { L"Text documents", L"*.txt" },
        { L"C++ source", L"*.cpp;*.cc;*.hpp;*.h*" },
    };

    hr = dialog->SetFileTypes(ARRAYSIZE(filter_spec), filter_spec);
    
    if(FAILED(hr))
    {
        result.error = FileDialogError_unknown;
        dialog->Release();
        return result;
    }

    u32 file_type_index = 1;
    hr = dialog->SetFileTypeIndex(file_type_index);
    
    if(FAILED(hr))
    {
        result.error = FileDialogError_unknown;
        dialog->Release();
        return result;
    }

    // Set the default file extension to be added to file names
    hr = dialog->SetDefaultExtension(L"txt");

    if(FAILED(hr))
    {
        result.error = FileDialogError_unknown;
        dialog->Release();
        return result;
    }

    hr = dialog->Show(NULL);

    if(FAILED(hr))
    {
        result.error = FileDialogError_unknown;
        dialog->Release();
        return result;
    }

    DWORD num_files_selected = 0;
    IShellItem **shell_items;

    if(type == FileDialogType_open)
    {
        IShellItemArray *shell_item_array = 0;
        hr = open_dialog->GetResults(&shell_item_array);

        if(FAILED(hr))
        {
            result.error = FileDialogError_unknown;
            dialog->Release();
            return result;
        }

        hr = shell_item_array->GetCount(&num_files_selected);

        if(FAILED(hr))
        {
            result.error = FileDialogError_unknown;
            shell_item_array->Release();
            dialog->Release();
            return result;
        }

        shell_items = PushCount(&g_win32.frame_arena, IShellItem *, num_files_selected);

        for(i32 i = 0; i < num_files_selected; ++i)
        {
            hr = shell_item_array->GetItemAt(i, &shell_items[i]);

            if(FAILED(hr))
            {
                result.error = FileDialogError_unknown;

                // TODO: test this code, do we need to release every individual shell item?
                //       or is shell_item_array->Release() good enough
                for(i32 j = 0; j < i; ++j)
                {
                    shell_items[j]->Release();
                }

                shell_item_array->Release();
                dialog->Release();
                return result;
            }
        }

        // Hopefully we are able to release the shell item array now ...
        shell_item_array->Release();
    }
    else
    {
        num_files_selected = 1;
        shell_items = PushType(&g_win32.frame_arena, IShellItem *);
        hr = dialog->GetResult(&shell_items[0]);

        if(FAILED(hr))
        {
            result.error = FileDialogError_unknown;
            dialog->Release();
            return result;
        }
    }
    
    result.num_files = num_files_selected;
    result.file_paths = PushCount(&g_win32.frame_arena, String, result.num_files);
    result.file_names = PushCount(&g_win32.frame_arena, String, result.num_files);

    for(i32 i = 0; i < num_files_selected; ++i)
    {
        IShellItem *shell_item = shell_items[i];
        // TODO: testing: unset FOS_FORCEFILESYSTEM and try to break this by 
        //       selecting non-SFGAO_FILESYSTEM items??
        LPWSTR file_path_wchar = 0;
        LPWSTR file_name_wchar = 0;

        hr = shell_item->GetDisplayName(SIGDN_FILESYSPATH, &file_path_wchar);

        if(FAILED(hr))
        {
            result.error = FileDialogError_unknown;
            goto cleanup;
        }

        hr = shell_item->GetDisplayName(SIGDN_NORMALDISPLAY, &file_name_wchar);

        if(FAILED(hr))
        {
            result.error = FileDialogError_unknown;
            goto cleanup;
        }

        u32 path_size_required = WIN_UTF16toUTF8SizeRequired(file_path_wchar);
        u32 name_size_required = WIN_UTF16toUTF8SizeRequired(file_name_wchar);

        result.file_paths[i] = PushString(&g_win32.frame_arena, path_size_required);
        WIN_UTF16ToUTF8((UTF8*)result.file_paths[i].ptr, path_size_required, file_path_wchar);

        result.file_names[i] = PushString(&g_win32.frame_arena, name_size_required);
        WIN_UTF16ToUTF8((UTF8 *)result.file_names[i].ptr, name_size_required, file_name_wchar);

        // Don't include the null terminator in string length
        if(result.file_paths[i].len > 0) --result.file_paths[i].len;
        if(result.file_names[i].len > 0) --result.file_names[i].len;

        CoTaskMemFree(file_path_wchar);
        CoTaskMemFree(file_name_wchar);
    }

cleanup:
    dialog->Release();
    for(int i = 0; i < num_files_selected; ++i)
    {
        shell_items[i]->Release();
    }

    return result;
}

internal f64
WIN_CalculateSecondsSinceStartup()
{
    f64 ret;
    u64 current_counter = WIN_QueryPerfCount();
    u64 counts_since_startup = current_counter - g_win32.perf_startup;
    ret = (f64)counts_since_startup / g_win32.perf_freq;
    return ret;    
}

internal void
WIN_OpenDWMRegistryKey()
{
    HKEY registry_key = 0;
    LSTATUS status;
    status = RegOpenKeyExW(HKEY_CURRENT_USER, L"Software\\Microsoft\\Windows\\DWM", 0, KEY_READ, &registry_key);
    if(status == ERROR_SUCCESS)
    {
        g_win32.dwm_key = registry_key;
    }
}

internal void
WIN_DWMKeyChanged()
{
    // TODO detect windows version
    b32 obtained_accent_color = false;

    COLORREF accent_color;
    u32 accent_color_size = sizeof(COLORREF);
    LSTATUS status = 0;

    if(IsWindows10OrGreater())
    {
        DWORD type;
        status = RegGetValueW(g_win32.dwm_key, 0, L"AccentColor", RRF_RT_REG_DWORD, (LPDWORD) &type,
                              &accent_color,
                              (LPDWORD) &accent_color_size);

        if(status == ERROR_SUCCESS)
        {
            obtained_accent_color = true;
        }
    }

    if(!obtained_accent_color)
    {
        // the "AccentColor" key is not in the registry, try mixing the
        // ColorizationColor with the color #d9d9d9 according to the percent
        // given in ColorizationColorBalance
        DWORD colorization_color;
        DWORD colorization_balance;
        
        u32 colorization_color_size = sizeof(COLORREF);
        u32 balance_size = sizeof(COLORREF);
        
        DWORD type;
        LSTATUS color_error = RegGetValueW(g_win32.dwm_key,
                                           0,
                                           L"ColorizationColor",
                                           RRF_RT_REG_DWORD,
                                           &type, 
                                           &colorization_color,
                                           (LPDWORD) &colorization_color_size);
        LSTATUS percentage_error = RegGetValueW(g_win32.dwm_key,
                                                0,
                                                L"ColorizationColorBalance",
                                                RRF_RT_REG_DWORD,
                                                &type,
                                                &colorization_balance,
                                                (LPDWORD) &balance_size);

        if(color_error == ERROR_SUCCESS &&
           percentage_error == ERROR_SUCCESS)
        {
            // Windows version 1611: ColorizationColorBalance can be 0xfffffff3
            // change to default 80
            if(colorization_balance > 100)
            {
                colorization_balance = 80;
            }

            COLORREF neutral = RGB(0xd9, 0xd9, 0xd9);

            // for some reason the 
            u32 colorization_red = (colorization_color >> 16) & 0xFF;
            u32 colorization_green = (colorization_color >> 8) & 0xFF;
            u32 colorization_blue = colorization_color & 0xFF;

            f32 colorization_percent = ((f32)colorization_balance/100.0f);
            f32 neutral_percent = 1.0f - colorization_percent;

            f32 neutral_component = 0xd9;
            
            u32 accent_color_red = Round(colorization_percent*colorization_red + neutral_percent*neutral_component);
            u32 accent_color_green = Round(colorization_percent*colorization_green + neutral_percent*neutral_component);
            u32 accent_color_blue = Round(colorization_percent*colorization_blue + neutral_percent*neutral_component);

            accent_color = RGB(accent_color_red, accent_color_green, accent_color_blue);
            obtained_accent_color = true;
        }
    }

    if(!obtained_accent_color)
    {
        accent_color = RGB(0, 0, 0); // black
        obtained_accent_color = true;
    }
    
    g_win32.accent_color = accent_color;

    DWORD filter = (REG_NOTIFY_CHANGE_NAME |
                    REG_NOTIFY_CHANGE_ATTRIBUTES |
                    REG_NOTIFY_CHANGE_LAST_SET |
                    REG_NOTIFY_CHANGE_SECURITY);

    if(!g_win32.dwm_key_change_event)
    {
        g_win32.dwm_key_change_event = CreateEventA(0, 1, 0, 0);
    }
    status = RegNotifyChangeKeyValue(g_win32.dwm_key,
                                     TRUE /*bWatchSubtree*/,
                                     filter,
                                     g_win32.dwm_key_change_event,
                                     TRUE /*fAsynchronous*/);
    Assert(status == ERROR_SUCCESS);
    HANDLE wait_handle = 0;
    BOOL wait_succeeded = RegisterWaitForSingleObject(&wait_handle,
                                                      g_win32.dwm_key_change_event,
                                                      WIN_DWMNotifyWaitCallback,
                                                      0 /*Context*/,
                                                      INFINITE,
                                                      WT_EXECUTEINWAITTHREAD | WT_EXECUTEONLYONCE);
    Assert(wait_succeeded)
}

VOID CALLBACK
WIN_DWMNotifyWaitCallback(void *param, BOOLEAN wait_fired)
{
    if(wait_fired == FALSE)
    {
        WIN_DWMKeyChanged();
    }
}

internal void
WIN_NewInputFrame(Input *input)
{
    // TODO there has to be a faster way to just memzero (perhaps using sse???)
    for(int i = 0; i < ArrayCount(input->keys); i++)
    {
        input->keys[i].transition_count = 0;
        input->keys[i].down_repeats = 0;
    }

    for(int i = 0; i < ArrayCount(input->mouse_buttons); i++)
    {
        input->mouse_buttons[i].transition_count = 0;
        input->mouse_buttons[i].down_repeats = 0;
    }

    input->dropped.type = DroppedItemType_nothing;
    input->chars_typed = 0;
    input->wheel_delta = 0;
    input->mouse_moved = false;
    
    POINT cursor_point;
    GetCursorPos(&cursor_point);

    ScreenToClient(g_win32.main_window->hwnd, &cursor_point);

    if(WIN_IsCustomDrawingTopBorder(g_win32.main_window))
    {
        cursor_point.y -= 1; 
    }

    if(WIN_IsCustomDrawingSideAndBottomBorders(g_win32.main_window))
    {
        u32 size_width = GetSystemMetrics(SM_CXFRAME);

        cursor_point.x += size_width;
    }
    
    *input->main_window = GetPlatformWindowInfo(g_win32.main_window);
    
    g_win32.input.mouse_pos = *(Vec2i *)&cursor_point;
}

inline PlatformKey
GetExtendedKey(PlatformKey key)
{
    PlatformKey result = Key_invalid;

    if(key == Key_enter)
        
        result = Key_numpad_enter;
    else if(key == Key_left_ctrl)
        result = Key_right_ctrl;
        //TODO do we have to account for fake left and right shifts?
        //else if(key == Key_lShift)
        //lfakeshift
        //else if(key == Key_rShift)
    //rfakeshift
    else if(key == Key_forward_slash)
        result = Key_numpad_slash;
    else if(key == Key_numpad_7)
        result = Key_home;
    else if(key == Key_numpad_8)
        result = Key_up;
    else if(key == Key_numpad_9)
        result = Key_page_up;
    else if(key == Key_numpad_4)
        result = Key_left;
    else if(key == Key_numpad_6)
        result = Key_right;
    else if(key == Key_numpad_1)
        result = Key_end;
    else if(key == Key_numpad_2)
        result = Key_down;
    else if(key == Key_numpad_3)
        result = Key_page_down;
    else if(key == Key_numpad_0)
        result = Key_insert;
    else if(key == Key_numpad_period)
        result = Key_delete;
    else if(key == Key_numpad_asterisk)
        result = Key_print_screen;
    else if(key == Key_menu)
        // TODO for some reason,
        //      both the left and right
        //      menu keys generate Key_menu
        result = Key_menu;
    
    return result;
}

internal Framebuffer *
WIN_GetWindowFramebuffer(PlatformWindowHandle handle)
{
    Framebuffer *result;

    WIN_WindowData *window = (WIN_WindowData *)handle;
    result = window->framebuffer;

    return result;
}

internal b32
WIN_SetWindowSizeState(PlatformWindowHandle handle, PlatformWindowSizeState size_state)
{
    WIN_WindowData *window = 0;

    if(handle)
    {
        window = (WIN_WindowData *)handle;
    }
    else
    {
        window = g_win32.main_window;
    }

    int show_command = 0;

    if(size_state == Window_minimized)
    {
        show_command = SW_SHOWMINIMIZED;
    }
    else if(size_state == Window_maximized)
    {
        show_command = SW_SHOWMAXIMIZED;
    }
    else if(size_state = Window_restored)
    {
        show_command = SW_RESTORE;
    }
    
    BOOL success = ShowWindow(window->hwnd, show_command);
        
    return success;
}

internal void
UpdateDWMFrame(WIN_WindowData *window)
{
    MARGINS margins;

    margins.cyTopHeight = 0;
    margins.cxLeftWidth = 0;
    margins.cxRightWidth = 0;
    margins.cyBottomHeight = 0;

    // HRESULT hr = DwmExtendFrameIntoClientArea(window->hwnd, &margins);
    // Assert(SUCCEEDED(hr));
}

internal b32
WIN_SetWindowPosition(PlatformWindowHandle handle, Recti window_position)
{
    WIN_WindowData *window = 0;

    if(handle)
    {
        window = (WIN_WindowData *)handle;
    }
    else
    {
        window = g_win32.main_window;
    }

    POINT absolute_position = {};
    absolute_position.x = window_position.x;
    absolute_position.y = window_position.y;

    if(window != g_win32.main_window)
    {
        ClientToScreen(g_win32.main_window->hwnd, &absolute_position);
    }

    UINT flags = 0;
    if(GetWindowLongW(window->hwnd, GWL_EXSTYLE) & WS_EX_NOACTIVATE)
    {
        flags |= SWP_NOACTIVATE;
    }
    
    BOOL success = SetWindowPos(window->hwnd,
                                HWND_TOP,
                                absolute_position.x,
                                absolute_position.y,
                                window_position.width,
                                window_position.height,
                                flags);
	// TODO is this necessary?
    g_win32.CreateResizeSwapChain(window);
    UpdateDWMFrame(window);
    return success;
}

internal void
WIN_SetWindowVisible(PlatformWindowHandle window_handle, b32 visible)
{
    WIN_WindowData *window = (WIN_WindowData *)window_handle;

    //ShowWindow((HWND)hwnd, SW_SHOWNOACTIVATE);
    if(visible)
    {
        u32 ex_style = GetWindowLongW(window->hwnd, GWL_EXSTYLE);
        if(ex_style & WS_EX_NOACTIVATE)
        {
            ShowWindow((HWND)window->hwnd, SW_SHOWNOACTIVATE);
        }
        else
        {
            ShowWindow((HWND)window->hwnd, SW_SHOW);
        }
    }
    else
    {
        ShowWindow((HWND)window->hwnd, SW_HIDE);
    }
    
}    

internal WIN_WindowData *
GetWindowFromHWND(HWND hwnd)
{
    WIN_WindowData *result = 0;

    // do a linear search for the WIN_WindowData
    // that matches our hwnd
    for(WIN_WindowData *window = g_win32.first_window;
        window;
        window = window->next)
    {
        if(window->hwnd == hwnd)
        {
            result = window;
            break;
        }
    }
    
    return result;
}

LRESULT CALLBACK
WIN_TerraniumWindowProc(HWND   hwnd,
                        UINT   message,
                        WPARAM w_param,
                        LPARAM l_param)
{
    LRESULT ret = 0;
    HRESULT hresult;

    WIN_WindowData *window = GetWindowFromHWND(hwnd);
    if(!window)
    {
        // before calling CreateWindowEx
        // the caller should allocate a
        // WIN_WindowData struct initialized to
        // zero and point g_win32.last_window
        // to it
        Assert(g_win32.last_window &&
               g_win32.last_window->hwnd == 0);

        g_win32.last_window->hwnd = hwnd;
    }
    
    switch(message)
    {
        case WM_SETTEXT:
        {
            ret = DefWindowProcW(hwnd, message, w_param, l_param);
            int e = 0;
        } break;

        case WM_MOUSEWHEEL:
        {
            g_win32.input.wheel_delta = GET_WHEEL_DELTA_WPARAM(w_param);
        } break;

        case WM_SETCURSOR:
        {
            if (LOWORD(l_param) == HTCLIENT)
            {
                HCURSOR cursor = LoadCursor(0, WIN_cursor_icon_to_IDC[g_win32.cursor_icon]);
                SetCursor(cursor);
                
                // TODO do we have to release the loaded cursor?
                ret = true;
            }
            else
            {
                ret = DefWindowProcW(hwnd, message, w_param, l_param);
            }
        } break;

        case WM_KILLFOCUS:
        {
            window->has_focus = false;
            Input *input = &g_win32.input;
            for(int i = 0; i < ArrayCount(input->keys); i++)
            {
                input->keys[i].down = false;
            }
            for(int i = 0; i < ArrayCount(input->mouse_buttons); i++)
            {
                input->mouse_buttons[i].down = false;
            }
        } break;
        case WM_SETFOCUS:
        {
            window->has_focus = true;
            WIN_RecordWindowState(window);
        } break;

        case WM_MOUSEACTIVATE:
        {
            if(GetWindowLongW(window->hwnd, GWL_EXSTYLE) & WS_EX_NOACTIVATE)
            {
                ret = MA_NOACTIVATE;
            }
            else
            {
                ret = DefWindowProcW(window->hwnd, message, l_param, w_param);
            }
        } break;
        
        // TODO handle windows aero glass
        case WM_ACTIVATE:
        {
            if(window->titlebar_type == PlatformTitlebar_custom_overlay)
            {
                UpdateDWMFrame(window);
            }
        } break;

        case WM_NCCALCSIZE:
        {

            // HACK we don't actually test for w_param to be true
            //      as required by the docs because otherwise
            //      the titlebar flashes
            // if(w_param == TRUE)
            // {
            
            if(window->titlebar_type == PlatformTitlebar_system_drawn ||
               w_param != TRUE)
            {
                ret = DefWindowProcW(hwnd, message, w_param, l_param);
            }
            else if(window->titlebar_type == PlatformTitlebar_system_drawn ||
                    window->titlebar_type == PlatformTitlebar_custom_overlay)
            {
                NCCALCSIZE_PARAMS *nc_calc_size_params = (NCCALCSIZE_PARAMS *)l_param;
                RECT *client_rect = nc_calc_size_params->rgrc + 0;

                //NOTE:: the top must be 0 or else windows will draw
                //       a full native titlebar outide the client area

                int x_size_frame = GetSystemMetrics(SM_CXSIZEFRAME);
                int x_padded_border = GetSystemMetrics(SM_CXPADDEDBORDER);
                u32 frame_thickness = Round((f32)x_size_frame + (f32)x_padded_border * g_win32.dpi / 96);

                if(IsWindows10OrGreater())
                {
                    client_rect->left += frame_thickness;
                    client_rect->bottom -= frame_thickness;
                    client_rect->right -= frame_thickness;
                }
                else
                {
                    client_rect->bottom -= 1;
                    client_rect->right -= 1;
                }
                
                b32 maximized = WIN_IsMaximized(hwnd);
                if(maximized)
                {
                    const u32 autohide_appbar_thickness = 2;
                    
                    HMONITOR monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

                    //TODO force an NCCalcSize when the taskbar location changes
                    if(WIN_EdgeHasAutoHideAppbar(ABE_LEFT, monitor))
                    {
                        WIN_Info("TABBAR: LEFT\n");
                        client_rect->left += autohide_appbar_thickness;
                    }
                    else if(WIN_EdgeHasAutoHideAppbar(ABE_TOP, monitor))
                    {
                        WIN_Info("TABBAR: TOP\n");
                        client_rect->top += autohide_appbar_thickness;
                    }
                    else if(WIN_EdgeHasAutoHideAppbar(ABE_RIGHT, monitor))
                    {
                        WIN_Info("TABBAR: RIGHT\n");
                        client_rect->right -= autohide_appbar_thickness;
                    }
                    else if(WIN_EdgeHasAutoHideAppbar(ABE_BOTTOM, monitor))
                    {
                        WIN_Info("TABBAR: BOTTOM\n");
                        client_rect->bottom -= autohide_appbar_thickness;
                    }
                }
                else
                {
                    client_rect->top -= 0;
                    // client_rect->top += 100;
                }

            }

        } break;

        
        case WM_RBUTTONUP:
        {
            // ReleaseCapture();

            KeyState *key = g_win32.input.mouse_buttons + MouseButton_right;
            WIN_HandleKeyChange(key, false);

            if(window->titlebar_type == PlatformTitlebar_system_drawn)
            {
                LRESULT hit_test_result = SendMessageW(hwnd, WM_NCHITTEST, 0, l_param);
                if(hit_test_result == HTCAPTION)
                {
                    //TODO bug with system menu not greying out the proper things
                    //     that should be greyed out
                    HMENU menu = GetSystemMenu(hwnd, TRUE);
                    menu = GetSystemMenu(hwnd, FALSE);

                    POINT point = {};
                    point.x = GET_X_LPARAM(l_param);
                    point.y = GET_Y_LPARAM(l_param);

                    ClientToScreen(hwnd, &point);
                    
                    i32 command = TrackPopupMenu(menu,
                                                 TPM_LEFTBUTTON | TPM_RIGHTBUTTON | TPM_RETURNCMD,
                                                 point.x, point.y,
                                                 0, hwnd, 0);
                    SendMessageW(hwnd, WM_SYSCOMMAND, command, 0);
                }
            }
        } break;
        
        case WM_NCRBUTTONDOWN:
        {
            // HACK:: we have to manually SetCapture to recieve
            //        a WM_RBUTTONDOWN message and responde by
            //        sending the appropriate SysCommands
            //        because DefWindowProcW does not do
            //        properly
            if(window->titlebar_type == PlatformTitlebar_system_drawn &&
                w_param == HTCAPTION)
            {
                // SetCapture(hwnd);
            }
            else
            {
                ret = DefWindowProcW(hwnd, message, w_param, l_param);
            }
        } break;

        case WM_RBUTTONDOWN:
        {
            // SetCapture(hwnd);
 
            KeyState *key = g_win32.input.mouse_buttons + MouseButton_right;
            WIN_HandleKeyChange(key, true);
        } break;

        case WM_NCMOUSEMOVE:
        {
        } break;

        case WM_MOUSEMOVE:
        {
            g_win32.input.mouse_moved = true;
        } break;

        // case WM_ERASEBKGND:
        // {
        //     ret = 0;
        // } break;
        
        case WM_NCLBUTTONDOWN:
        {
            // SetCapture(hwnd);
            g_win32.mouse_hook = SetWindowsHookExA(WH_MOUSE, WIN_MoveLoopMouseHook, 0, GetCurrentThreadId());

            KeyState *key = g_win32.input.mouse_buttons + MouseButton_left;
            WIN_HandleKeyChange(key, true);
            ret = DefWindowProcW(hwnd, message, w_param, l_param);
        } break;

        case WM_LBUTTONDOWN:
        {
            SetCapture(hwnd);

            KeyState *key = g_win32.input.mouse_buttons + MouseButton_left;
            WIN_HandleKeyChange(key, true);
        } break;

        case WM_LBUTTONUP:
        {
            ReleaseCapture();

            // manually handle specific non-client mouse ups instead of deffering to windows.
            // if(window->titlebar_type == PlatformTitlebar_system_drawn)
            // {
            //     LRESULT hit_test_result = SendMessageW(hwnd, WM_NCHITTEST, 0, l_param);
            //     if(hit_test_result == HTCLOSE)
            //     {
            //         SendMessageW(hwnd, WM_SYSCOMMAND, SC_CLOSE, l_param);
            //     }
            //     else if(hit_test_result == HTMINBUTTON)
            //     {
            //         SendMessageW(hwnd, WM_SYSCOMMAND, SC_MINIMIZE, l_param);
            //     }
            //     else if(hit_test_result == HTMAXBUTTON)
            //     {
            //         if(window->size_state == Window_maximized)
            //         {
            //             PostMessageW(hwnd, WM_SYSCOMMAND, SC_RESTORE, l_param);   
            //         }
            //         else if(window->size_state == Window_restored)
            //         {
            //             PostMessageW(hwnd, WM_SYSCOMMAND, SC_MAXIMIZE, l_param);   
            //         }
            //     }
            // }
            
            // i32 xPos = GET_X_LPARAM(l_param);
            // i32 yPos = GET_Y_LPARAM(l_param);

            KeyState *key = g_win32.input.mouse_buttons + MouseButton_left;
            WIN_HandleKeyChange(key, false);
        } break;
        
        case WM_NCLBUTTONUP:
        {
            ReleaseCapture();
            WIN_Info("WM_NCLBUTTONUP\n");
            
            ret = DefWindowProcW(hwnd, message, w_param, l_param);
            KeyState *key = g_win32.input.mouse_buttons + MouseButton_left;
            WIN_HandleKeyChange(key, false);
        }break;

        case WM_NCHITTEST:
            
        {
            if(g_win32.input.non_client_hittest != HTNOWHERE &&
               window->titlebar_type == PlatformTitlebar_system_drawn)
            {
                ret = g_win32.input.non_client_hittest;
            }
            else if(window->titlebar_type == PlatformTitlebar_custom_overlay)
            {
                b32 handled = DwmDefWindowProc(hwnd, message, w_param, l_param, &ret);

                if(!handled)
                {
                    ret = HTCAPTION;
                }
            }
            else
            {
                ret = DefWindowProcW(hwnd, message, w_param, l_param);
            }
        } break;

        case WM_GETMINMAXINFO:
        {
            MINMAXINFO *min_max_info = (MINMAXINFO *)l_param;
            min_max_info->ptMinTrackSize.x = MIN_WINDOW_WIDTH;
            min_max_info->ptMinTrackSize.y = MIN_WINDOW_HEIGHT;
        } break;
        
        case WM_DPICHANGED:
        {
            u32 new_dpi = LOWORD(w_param);
            u32 old_dpi = g_win32.dpi;

            // TODO check if this behavior is correct,
            //      it would be incorrect if one of
            //      GetWindowRect and SetWindowPos
            //      uses client metrics and the other
            //      includes non-client metrics
            RECT window_rect = {};
            GetWindowRect(hwnd, &window_rect);

            u32 old_width = window_rect.right - window_rect.left;
            u32 old_height = window_rect.bottom - window_rect.top;
            u32 old_center = (window_rect.left + window_rect.right) / 2;
            
            u32 new_width = old_width * new_dpi / old_dpi;
            u32 new_height = old_height * new_dpi / old_dpi;

            
            BOOL success = SetWindowPos(hwnd,
                                        HWND_TOP,
                                        old_center - new_width/2,
                                        window_rect.top,
                                        new_width,
                                        new_height,
                                        0);
            Assert(success);

            g_win32.dpi_changed = true;
            g_win32.dpi = new_dpi;
        } break;

        case WM_ENTERSIZEMOVE:
        {
            // g_win32.move_timer = SetTimer(hwnd, 1, USER_TIMER_MINIMUM, (TIMERPROC)0);
        } break;

        case WM_EXITSIZEMOVE:
        {
            // KillTimer(hwnd, g_win32.move_timer);
        } break;
        
        case WM_TIMER:
        {
            SwitchToFiber(g_win32.main_fiber);
        } break;
        
        case WM_SIZE:
        {
            u32 width_param = LOWORD(l_param);
            u32 height_param = HIWORD(l_param);
			
			// TODO check if this is called 2 times on startup
            if(w_param != SIZE_MINIMIZED)
			{
				WIN_ResizeFramebuffer(window);
				g_win32.CreateResizeSwapChain(window);
			}

            if(w_param == SIZE_MAXIMIZED)
            {
                window->size_state = Window_maximized;
            }
            else if(w_param == SIZE_RESTORED)
            {
                window->size_state = Window_restored;
            }
            else if(w_param == SIZE_MINIMIZED)
            {
                window->size_state = Window_minimized;
            }

            if(window == g_win32.main_window)
            {
                WIN_RecordWindowState(window);
            }

            UpdateDWMFrame(window);
            window->resized = true;
        } break;

        case WM_MOVE:
        {
            window->moved = true;

            if(window == g_win32.main_window)
            {
                WIN_RecordWindowState(window);
            }
        } break;
        

        


        case WM_SYSKEYDOWN:
        {
            //TODO we may want to reimplement some more sys key shortcuts!
            if(w_param == VK_F4)
            {
                PostMessageW(hwnd, WM_CLOSE, 0, 0);
            }
        }
        // fall-through
        case WM_KEYDOWN:
        {
            // TODO make sure shift, ctrl and alt are all processed
            u16 scan_code = (l_param >> 16) & 127;

            PlatformKey key = win32_scancode_to_key[scan_code];
            if(l_param & 0x1000000)
            {
                key = GetExtendedKey(key);
            }

            KeyState *key_state = g_win32.input.keys + key;
            
            WIN_HandleKeyChange(key_state, true);
        } break;


        case WM_SYSKEYUP:
        case WM_KEYUP:
        {
            u16 scan_code = (l_param >> 16) & 127;
            PlatformKey key = win32_scancode_to_key[scan_code];
            if(l_param & 0x1000000)
            {
                key = GetExtendedKey(key);
            }
            
            KeyState *key_state = g_win32.input.keys + key;
            WIN_HandleKeyChange(key_state, false);
        }break;

        // case WM_PAINT:
        // {
        //     PAINTSTRUCT ps = {};

        //     HDC hdc = BeginPaint(hwnd, &ps);
        //     FillRect(hdc, &ps.rcPaint, (HBRUSH)GetStockObject(0));
        //     EndPaint(hwnd, &ps);
        // }break;

        case WM_UNICHAR: {
            // TODO handle this cuz some programs will input it this way?
        }break;

        case WM_CHAR:
        {
            // TODO are there any key combinations that have control??
            if(!g_win32.input.keys[Key_left_ctrl].down ||
                g_win32.input.keys[Key_right_ctrl].down)
            {
                char scan_code = (l_param >> 16) & 127; // bits 16-23
                PlatformKey key = win32_scancode_to_key[scan_code];
                if(l_param & 0x1000000)
                {
                    key = GetExtendedKey(key);
                }

                if(key != Key_enter &&
                   key != Key_backspace &&
                   key != Key_esc)
                {
                    WCHAR utf16_char = w_param & 0xFFFF;
                    if(utf16_char >= 0xD800 && utf16_char <= 0xDFFF)
                    {
                        if(g_win32.surrogate)
                        {
                            // TODO what if multiple UTF16 surrogate pairs are being sent a frame??
                            //     will this break or will it be fine
                            UTF16 leading;
                            UTF16 trailing;
                            if(g_win32.surrogate >= 0xD800 && g_win32.surrogate <= 0xDBFF &&
                               utf16_char >= 0xDC00 && utf16_char <= 0xDFFF)
                            {
                                leading = g_win32.surrogate;
                                trailing = utf16_char;
                            }
                            else if(g_win32.surrogate >= 0xDC00 && g_win32.surrogate <= 0xDFFF &&
                                    utf16_char >= 0xD800 && utf16_char <= 0xDBFF)
                            {
                                leading = utf16_char;
                                trailing = g_win32.surrogate;
                            }
                            else
                            {
                                Assert(false);
                            }

                            // from unicode.org/faq/utf_bom.html
#define SURROGATE_OFFSET 0x10000 - (0xD800 << 10) - 0xDC00;
                            UTF32 codepoint = (leading << 10) + trailing + SURROGATE_OFFSET;
                            Input *input = &g_win32.input;
                            if(input->chars_typed < MAX_CHAR_TYPED_PER_FRAME)
                            {
                                input->typed_char[input->chars_typed++] = codepoint;
                            }
                        }
                        else
                        {
                            g_win32.surrogate = utf16_char;
                        }
                    }
                    else
                    {
                        Input *input = &g_win32.input;
                        if(input->chars_typed < MAX_CHAR_TYPED_PER_FRAME) 
                            input->typed_char[input->chars_typed++] = w_param;
                    }
                }
            }
        }break;

        case WM_CLOSE:
        {
            g_win32.running = false;
        }break;

        default:
        {
            ret = DefWindowProcW(hwnd, message, w_param, l_param);
        }break;
    }
    return ret;
}

internal PlatformWindowHandle
WIN_OpenWindow(u64 flags, String name)
{
    PlatformWindowHandle result;

    // TODO combine this string append functionality
    //      with the one in WIN_SetTitlebar?
    char window_class_append[] = " Window Class";
    String window_class_name = PushString(&g_win32.frame_arena, name.len + ArrayCount(window_class_append) - 1);
    CopyUTF8(window_class_name.ptr, name.ptr, name.len);
    CopyUTF8(window_class_name.ptr + name.len, (UTF8 *)window_class_append, ArrayCount(window_class_append) - 1);

    WCHAR *window_class_wchars = WIN_StringToWCHAR(window_class_name, &g_win32.frame_arena);
    WCHAR *window_title_wchars = WIN_StringToWCHAR(name, &g_win32.frame_arena);
    
    WNDCLASSW wc = {};
    wc.style = CS_OWNDC;
    wc.lpszClassName = window_class_wchars;
    wc.lpfnWndProc = WIN_TerraniumWindowProc;
    wc.hInstance = GetModuleHandleW(0);
    wc.hCursor = LoadCursor(0, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

    if(flags & Window_drop_shadow)
    {
        wc.style |= CS_DROPSHADOW;
    }
    RegisterClassW(&wc);

    DWORD style = 0;
    DWORD extended_style = 0;

    if(flags & Window_no_frame)
    {
        style |= WS_POPUP;
    }
    else
    {
        style |= WS_OVERLAPPEDWINDOW;
    }

    if(flags & Window_always_on_top)
    {
        extended_style |= WS_EX_TOPMOST;
    }

    if(flags & Window_no_taskbar_icon)
    {
        extended_style |= WS_EX_TOOLWINDOW;
    }

    if(flags & Window_not_focusable)
    {
        extended_style |= WS_EX_NOACTIVATE;
    }

    WIN_WindowData *new_window = PushType(&g_win32.permanent_arena, WIN_WindowData);
    Assert(g_win32.last_window);
    g_win32.last_window->next = new_window;
    g_win32.last_window = new_window;
    new_window->titlebar_type = PlatformTitlebar_system_drawn;
    
    HWND hwnd = CreateWindowExW(extended_style,
                                wc.lpszClassName,
                                window_title_wchars,
                                style,
                                CW_USEDEFAULT,
                                CW_USEDEFAULT,
                                400,
                                400,
                                0, // hParent
                                0, // hMenu
                                wc.hInstance,
                                0);
    
    return g_win32.last_window;
    
}


internal PlatformCommandLine
WIN_GetCommandLine()
{
    if(g_win32.command_line.num_args == 0)
    {
        LPWSTR command_line = GetCommandLineW();

        int num_args;
        LPWSTR *argv = CommandLineToArgvW(command_line, &num_args);
        
        g_win32.command_line.args = PushCount(&g_win32.permanent_arena, String, num_args);
        g_win32.command_line.num_args = num_args;

        for(u32 i = 0;
            i < num_args;
            i++)
        {
            g_win32.command_line.args[i] = WIN_UTF16ToString(argv[i], &g_win32.permanent_arena);
        }

        // NOTE:: the command line is intentionally not freed
        // LocalFree(argv);
    }

    // TODO on first use, make this allocate a UTF8* stream of argv and
    //      argcs so that we can become platform independent
    return g_win32.command_line;
}

internal void
WIN_ProcessMessages()
{
    for(;;)
    {
        MSG message;
        while(PeekMessageW(&message, g_win32.main_window->hwnd, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&message);
            DispatchMessageW(&message);
        }
        SwitchToFiber(g_win32.main_fiber);
    }
}

internal Color
WIN_GetSystemColor(SystemColorItem color_item)
{
    Color result;

    if(color_item == SystemColorItem_highlighted_inactive_text)
    {
        result = vec3(200, 200, 200);
    }
    else
    {
        int index = 0;

        switch (color_item)
        {
            case SystemColorItem_highlighted_text_background:
            {
                index = COLOR_HIGHLIGHT;
            } break;

            case SystemColorItem_highlighted_text_foreground:
            {
                index = COLOR_HIGHLIGHTTEXT;
            } break;
        }

        if(index)
        {
            DWORD sys_color = GetSysColor(index);
    
            result.r = GetRValue(sys_color);
            result.g = GetGValue(sys_color);
            result.b = GetBValue(sys_color);
        }
        else
        {
            result = INVALID_CLR;
        }
    }
    
    return result;
}

LONG WINAPI
WIN_ExceptionHandler(PEXCEPTION_POINTERS exceptioninfo)
{
    HMODULE dbg_help = 0;
    HANDLE file = 0;

    b32 dump_written = false;
    dbg_help = LoadLibraryA("dbghelp.dll");
    if(dbg_help)
    {
        SYSTEMTIME system_time = {};
        GetSystemTime(&system_time);
    
        char file_name[1024];
        snprintf(file_name, ArrayCount(file_name),
                 "Terranium v%u.%u %u.%u.%u %u.%u.%u.dmp",
                 TERRANIUM_VERSION_MAJOR,
                 TERRANIUM_VERSION_MINOR,
                 system_time.wMonth,
                 system_time.wDay,
                 system_time.wYear,
                 system_time.wHour,
                 system_time.wMinute,
                 system_time.wSecond);
    
        file = CreateFileA(file_name, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
        if(file != INVALID_HANDLE_VALUE)
        {
            MINIDUMP_EXCEPTION_INFORMATION mei;
            mei.ThreadId = GetCurrentThreadId();
            mei.ExceptionPointers = exceptioninfo;
            mei.ClientPointers = FALSE;

            typedef BOOL (WINAPI *LPMINIDUMPWRITEDUMP)
                (HANDLE                            hProcess,
                 DWORD                             ProcessId,
                 HANDLE                            hFile,
                 MINIDUMP_TYPE                     DumpType,
                 PMINIDUMP_EXCEPTION_INFORMATION   ExceptionParam,
                 PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
                 PMINIDUMP_CALLBACK_INFORMATION    CallbackParam);

            LPMINIDUMPWRITEDUMP MiniDumpWriteDump = (LPMINIDUMPWRITEDUMP) GetProcAddress(dbg_help, "MiniDumpWriteDump");
            HANDLE hProcess = GetCurrentProcess();
            DWORD dwProcessId = GetCurrentProcessId();

            dump_written = MiniDumpWriteDump(hProcess, dwProcessId, file, MiniDumpNormal, &mei, 0, 0);

            if(!dump_written)
            {
                CloseHandle(file);
                DeleteFileA(file_name);
            }
        }
    }


    ShowWindow(g_win32.main_window->hwnd, SW_HIDE);

    if(dump_written)
    {
        MessageBoxA(0, "Terranium has crashed. A minidump has been saved in the executable's working directory.",
               "Error", MB_OK);
    }
    else
    {
        // TODO what the heck is the user supposed to do now??
        MessageBoxA(0, "Terranium has crashed. Terranium has failed saving a minidump", "Error", MB_OK);

    }
    return EXCEPTION_EXECUTE_HANDLER;
}

int CALLBACK
WinMain(HINSTANCE instance,
        HINSTANCE ,
        LPSTR     ,
        int       )
{
    HRESULT hresult;

    HMODULE shcore = LoadLibraryA("Shcore.dll");
    if(shcore)
    {
        typedef HRESULT (*SetProcessDpiAwarenessFunc_) (PROCESS_DPI_AWARENESS dpi_awareness);

        SetProcessDpiAwarenessFunc_ SetProcessDpiAwareness =
            (SetProcessDpiAwarenessFunc_) GetProcAddress(shcore, "SetProcessDpiAwareness");
        
        SetProcessDpiAwareness(PROCESS_PER_MONITOR_DPI_AWARE);
        
        // FreeLibrary(shcore);
    }
    else
    {
        int success = SetProcessDPIAware();
        Assert(success);
    }

    g_win32.perf_startup = WIN_QueryPerfCount();

    platform.AllocateMemory = WIN_AllocateMemory;
    platform.FreeMemory = WIN_FreeMemory;

    platform.OpenWindow = WIN_OpenWindow;
    platform.SetTitlebar = WIN_SetTitlebar;
    platform.GetWindowRestoredRect = WIN_GetWindowRestoredRect;
    platform.GetWindowFramebuffer = WIN_GetWindowFramebuffer;
    platform.SetWindowVisible = WIN_SetWindowVisible;
    platform.SetWindowPosition = WIN_SetWindowPosition;
    platform.SetWindowSizeState = WIN_SetWindowSizeState;
    platform.CopyToClipboard = WIN_CopyToClipboard;
    platform.PasteFromClipboard = WIN_PasteFromClipboard;
    platform.FileDialog = WIN_FileDialog;
    platform.GetFileSize = WIN_GetFileSize;
    platform.OpenFile = WIN_OpenFile;
    platform.CreateNewFile = WIN_CreateNewFile;
    platform.OverwriteFile = WIN_OverwriteFile;
    platform.CloseFile = WIN_CloseFile;
    platform.ReadFile = WIN_ReadFile;
    platform.WriteEntireFile = WIN_WriteEntireFile;
    platform.DirectoryExists = WIN_DirectoryExists;
    platform.MakeDirectory = WIN_CreateDirectory;
    platform.GetLastFileError = WIN_GetLastFileError;
    platform.SetCursorIcon = WIN_SetCursorIcon;
    platform.RetrieveCommandLine = WIN_GetCommandLine;
    platform.Info = WIN_Info;
    platform.Error = WIN_Error;
    
    platform.AddWorkQueueEntry = WIN_AddWorkQueueEntry;
    platform.CompleteAllWork = WIN_CompleteAllWork;
    
    SetUnhandledExceptionFilter(WIN_ExceptionHandler);
    
    WNDCLASSW wc = {};
    wc.style = CS_OWNDC;
    wc.lpszClassName = L"Terranium Window Class";
    wc.lpfnWndProc = WIN_TerraniumWindowProc;
    wc.hCursor = LoadCursor(0, IDC_ARROW);
    wc.hInstance = instance;
    wc.hIcon = LoadIcon(instance, MAKEINTRESOURCE(101));
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

    RegisterClassW(&wc);
    g_win32.main_window = PushType(&g_win32.permanent_arena, WIN_WindowData);

    void *internal_framebuffer = PushSize(&g_win32.permanent_arena, sizeof(WIN_FramebufferHeader) + sizeof(Framebuffer));
    g_win32.main_window->framebuffer = (Framebuffer *) ((u8 *)internal_framebuffer + sizeof(WIN_FramebufferHeader));

    DWORD sys_color = GetSysColor(COLOR_HIGHLIGHTTEXT);

    int r = GetRValue(sys_color);
    int g = GetGValue(sys_color);
    int b = GetBValue(sys_color);
    
    // @framer
    g_win32.main_window->titlebar_type = PlatformTitlebar_system_drawn;
    g_win32.last_window = g_win32.main_window;

    if(!g_win32.window_state)
    {
        WIN_AttemptToLoadWindowState();        
    }

    i32 window_x = CW_USEDEFAULT;
    i32 window_y = CW_USEDEFAULT;
    i32 window_width = CW_USEDEFAULT;
    i32 window_height = CW_USEDEFAULT;
    i32 initial_show_command = SW_RESTORE;

    if(g_win32.window_state)
    {
        u64 file_size = WIN_GetFileSize(g_win32.window_state);
        u8 *buffer = (u8 *)PushSize(&g_win32.frame_arena, file_size);

        u32 bytes_read = WIN_ReadFile(g_win32.window_state, buffer, file_size);

        if(bytes_read >= sizeof(WIN_WindowStateContent))
        {
            // TODO unsafe! we do not sanitize this user input
            WIN_WindowStateContent *window_state_content = (WIN_WindowStateContent *)buffer;
            
            window_x = window_state_content->restored_x;
            window_y = window_state_content->restored_y;
            window_width = window_state_content->restored_width;
            window_height = window_state_content->restored_height;
            initial_show_command = (window_state_content->maximized == 1) ? SW_MAXIMIZE : SW_RESTORE;
        }
    }

    g_win32.main_window->hwnd = CreateWindowExW(WS_EX_ACCEPTFILES,
                                                wc.lpszClassName,
                                                L"Terranium",
                                                WS_OVERLAPPEDWINDOW,
                                                window_x,
                                                window_y,
                                                window_width,
                                                window_height,
                                                0, // hParent
                                                0, // hMenu
                                                instance,
                                                0); // lpParam

    if(shcore)
    {
        typedef HRESULT (*GetDpiForMonitorFunc_) (HMONITOR hmonitor, MONITOR_DPI_TYPE dpi_type, UINT *dpiX, UINT *dpiY);
        GetDpiForMonitorFunc_ GetDpiForMonitor = (GetDpiForMonitorFunc_) GetProcAddress(shcore, "GetDpiForMonitor");

        HMONITOR nearest_monitor = MonitorFromWindow(g_win32.main_window->hwnd, MONITOR_DEFAULTTONEAREST);
        u32 horizontal_dpi;
        u32 vertical_dpi;
        hresult = GetDpiForMonitor(nearest_monitor, MDT_EFFECTIVE_DPI, &horizontal_dpi, &vertical_dpi);
        g_win32.dpi = horizontal_dpi;
    }
    else
    {
        u32 horizontal_dpi = 96;
        u32 vertical_dpi = 96;

        HDC hdc = GetDC(0);

        if(hdc)
        {
            horizontal_dpi = GetDeviceCaps(hdc, LOGPIXELSX);
            vertical_dpi = GetDeviceCaps(hdc, LOGPIXELSY);

            ReleaseDC(g_win32.main_window->hwnd, hdc);
        }
        
        g_win32.dpi = horizontal_dpi;
    }

    // must be called before drag drop and clipboard operations
    HRESULT hr = OleInitialize(0/*Reserved*/); 
    Assert(SUCCEEDED(hr));
    
    // TODO pull this out to a general window creation routine
    WIN_InitDragState(g_win32.main_window);
    
    WIN_InitMultithreading();
    
    // TODO robustness: can we make this happen in WM_CREATE or something like that?
    // force a WM_NCCALCSIZE to extend the client area into the window frame

    SetWindowPos(g_win32.main_window->hwnd, NULL, 0, 0, 0, 0,
                 SWP_FRAMECHANGED  | SWP_NOREPOSITION | SWP_NOSIZE | SWP_NOMOVE);



    g_win32.main_fiber = ConvertThreadToFiber(0);
    g_win32.message_fiber = CreateFiber(0, (LPFIBER_START_ROUTINE) WIN_ProcessMessages, 0);
    SwitchToFiber(g_win32.message_fiber);

#if 0
    D3D9_Init();
#else
    D3D11_Init();
#endif
    
    LARGE_INTEGER perf_freqli;
    QueryPerformanceFrequency(&perf_freqli);
    g_win32.perf_freq = perf_freqli.QuadPart; // Performance counter frequency (counts per second)
    
    if(DWRITE_Init() < 0)
    {
        panic("Could not initialize DirectWrite");
    }

#define PERMANENT_STORAGE_SIZE Megabytes(100)
#define TRANSIENT_STORAGE_SIZE Megabytes(100)
#define PLATFORM_MEMORY_SIZE Megabytes(1)
    void *allocated_memory = VirtualAlloc(0, PERMANENT_STORAGE_SIZE + TRANSIENT_STORAGE_SIZE + PLATFORM_MEMORY_SIZE,
                                          MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    if(!allocated_memory)
    {
        OutputDebugStringA("Not enough RAM.");
    }

    g_win32.editor_memory = {};
    g_win32.transient_storage = (u8 *)allocated_memory + PERMANENT_STORAGE_SIZE + TRANSIENT_STORAGE_SIZE;
    g_win32.transient_storage_size = PLATFORM_MEMORY_SIZE;

    g_win32.running = true;
    // TODO what should the first frame second delta logically be??
    u64 last_frame_counter = WIN_QueryPerfCount();

    PlatformDebugInformation last_frame_debug_info = {};
    g_win32.editor_memory.debug_info = &last_frame_debug_info;
    
    g_win32.input.main_window = PushType(&g_win32.permanent_arena, PlatformWindowInfo);
    
    POINT cursor_point;
    GetCursorPos(&cursor_point);

    // TODO can we have this here
    // g_win32.Render();

    g_win32.cursor_icon = CursorIcon_arrow;

    // NOTE the following makes sure that we wait for the swap chain before the first rendering
    g_win32.animating = true;

    ShowWindow(g_win32.main_window->hwnd, initial_show_command);
    if(initial_show_command == SW_MAXIMIZE)
    {
        WINDOWPLACEMENT window_placement = {};
        GetWindowPlacement(g_win32.main_window->hwnd, &window_placement);

        window_placement.rcNormalPosition.left = window_x;
        window_placement.rcNormalPosition.top = window_y;
        window_placement.rcNormalPosition.right = window_x + window_width;
        window_placement.rcNormalPosition.bottom = window_y + window_height;
    }
    else
    {
        Assert(initial_show_command == SW_RESTORE)
    }
    WIN_OpenDWMRegistryKey();
    WIN_DWMKeyChanged();

    u32 num_dirty_rects = 700;
    g_win32.main_window->framebuffer->dirty = PushCount(&g_win32.permanent_arena, Recti, num_dirty_rects);
    g_win32.main_window->framebuffer->max_dirty_rects = num_dirty_rects;
    
    while(g_win32.running)
    {
        u64 this_frame_counter = WIN_QueryPerfCount();
        u64 counter_delta = this_frame_counter - last_frame_counter;
        f64 delta_seconds = (f64) counter_delta / g_win32.perf_freq;
        g_win32.input.dt = delta_seconds;
        last_frame_counter = this_frame_counter;

        WIN_NewInputFrame(&g_win32.input);
		
		for(WIN_WindowData *window = g_win32.first_window;
            window;
            window = window->next)
        {
            window->resized = false;
            window->moved = false;
        }
        
        if(g_win32.dpi_changed)
        {
            g_win32.dpi_changed = false;
        }

        SwitchToFiber(g_win32.message_fiber);


        g_win32.input.ctrl_down = g_win32.input.keys[Key_left_ctrl].down || g_win32.input.keys[Key_right_ctrl].down;
        g_win32.input.alt_down = g_win32.input.keys[Key_left_alt].down || g_win32.input.keys[Key_right_alt].down;
        g_win32.input.shift_down = g_win32.input.keys[Key_left_shift].down || g_win32.input.keys[Key_right_shift].down;

        g_win32.input.dpi_scaling = (f32) g_win32.dpi / 96.0f;
        g_win32.input.dpi_changed = g_win32.dpi_changed;

        g_win32.input.main_window->resized = g_win32.main_window->resized;
        g_win32.input.main_window->moved = g_win32.main_window->moved;
        g_win32.input.main_window->size_state = g_win32.main_window->size_state;
        
        g_win32.input.non_client_hittest = HTNOWHERE;

        PERF_START(editor_frame);
        Editor_UpdateAndRender(&g_win32.editor_memory, &g_win32.input, g_win32.main_window->framebuffer);
        PERF_END(editor_frame);

		#if 0
        // TODO should this be DPI-adjusted?
        i32 top_border_pixels = GetSystemMetrics(SM_CYSIZEFRAME);
        i32 side_buffer_pixels = 14;
        if(g_win32.main_window->titlebar_type == PlatformTitlebar_custom_drawn &&
           g_win32.input.mouse_pos.y < top_border_pixels &&
           g_win32.main_window->size_state == Window_restored &&
           g_win32.input.non_client_hittest != HTCLOSE &&
           g_win32.input.non_client_hittest != HTMAXBUTTON &&
           g_win32.input.non_client_hittest != HTMINBUTTON)
        {
			Recti window_rect = WIN_GetClientRect(g_win32.main_window);
			
            if(g_win32.input.mouse_pos.x < side_buffer_pixels)
            {
                g_win32.input.non_client_hittest = HTTOPLEFT;
            }
            else if(g_win32.input.mouse_pos.x > window_rect.width - side_buffer_pixels)
            {
                g_win32.input.non_client_hittest = HTTOPRIGHT;
            }
            else
            {
                g_win32.input.non_client_hittest = HTTOP;
            }
        }
		#endif

        WIN_FramebufferHeader *header = GetFramebufferHeader(g_win32.main_window->framebuffer);
        u32 *pixel = (u32 *)header->platform_pixels;

        if(WIN_IsCustomDrawingTopBorder(g_win32.main_window))
        {
            u32 inactive_border_color = 0x00AAAAAA;
            COLORREF frame_color = (g_win32.main_window->has_focus) ? (g_win32.accent_color) : inactive_border_color;
            frame_color = RGB(0, 255, 255);
            u8 b = (frame_color >> 16) & 0xFF;
            u8 g = (frame_color >> 8) & 0xFF;
            u8 r = frame_color & 0xFF;
            
            u32 packed_frame_color = (b << 0 | g << 8 | r << 16);

            for(u32 i = 0;
                i < g_win32.main_window->framebuffer->width;
                i++)
            {
                *pixel++ = packed_frame_color;
            }
            Recti top_frame_dirty = {};
            top_frame_dirty.x = 0;
            top_frame_dirty.y = -1;
            top_frame_dirty.width = g_win32.main_window->framebuffer->width;
            top_frame_dirty.height = 1;
            AddDirtyRect_NoAsserts(g_win32.main_window->framebuffer, top_frame_dirty);
        }

        Recti entire = {};
        entire.width = g_win32.main_window->framebuffer->width;
        entire.height = g_win32.main_window->framebuffer->height;
        // DrawRectangle(entire, CLR_RED, &g_win32.main_window->framebuffer);
        AddDirtyRect_NoAsserts(g_win32.main_window->framebuffer, entire);

        ResetArena(&platform.frame_arena);
        ResetArena(&g_win32.frame_arena);

        
        PERF_START(render);
        g_win32.Present(g_win32.main_window);
        g_win32.main_window->framebuffer->dirty_rects_used = 0;
        PERF_END(render);

        last_frame_debug_info.seconds_for_render = (f64)PERF_render / g_win32.perf_freq;
        last_frame_debug_info.seconds_for_editor_update_and_render = (f64)PERF_editor_frame / g_win32.perf_freq;
        last_frame_debug_info.seconds_for_frame = (f64)counter_delta / g_win32.perf_freq;

#if INTERNAL
        DisplayCounters();
#endif

    }
    
    return 0;
}

// TODO: Get rid of using __COUNTER__ to generate unique names all together?
#include "timers_footer.h"