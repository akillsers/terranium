#define TERRANIUM_VERSION_MAJOR 0
#define TERRANIUM_VERSION_MINOR 1


#include "bidi.h"
#include "shaped_text.h"
#include "font.h"
#include "draw.h"
#include "compositor.h"
#include "engine.h"
#include "undo_storage.h"
#include "cpp_parser.h"
#include "scrollbar.h"
#include "render_text.h"
#include "textfield.h"
#include "tabbar.h"
#include "loaded_file.h"
#include "timers.h"

// char startup_path[] = "W:\\Terranium\\transient tests\\isolated.txt";
char startup_path[] = "W:\\Terranium\\transient tests\\unicoder.txt";

// TODO don't define these as lines, define them as pixels???
#define EM_WIDTHS_PER_MOUSE_SCROLL 1
#define LINES_PER_MOUSE_SCROLL 5
#define LINES_PER_ARROWS_SCROLL 3


// TODO what if the line goes longer than this max line length!
#define MAX_LINE_LENGTH 512

#define CLR_POINT_FOREGROUND CLR_BLACK
#define CLR_FOCUSED_POINT_FOREGROUND CLR_BLACK

#define CLR_POINT_BACKGROUND CLR_LBLUE
#define CLR_FOCUSED_POINT_BACKGROUND CLR_ORANGE

#define CLR_BACK CLR_BLACK
#define CLR_NORMAL_TEXT CLR_ORANGE
#define CLR_TEXT_SELECTION_REGION rgb_u32(9, 74, 185)

#define EDITOR_MAX_VIEW 50
#define EDITOR_MAX_TABS 200

#define UNDO_STORAGE_BLOCK_SIZE Kilobytes(12)

char *CPP_file_asscociatons[] =
{
    "cpp",
    "cxx",
    "cc",
    "c",
    "h",
    "hh",
    "hpp",
    "hxx",
};

char *plain_text_file_asscociatons[] =
{
    "txt",
};

struct EditorSettings
{
    u32 tab_size = 4;

    FontDescriptor code_font = {12, FromCharArray("Arial")};
    FontDescriptor tab_font = {12, FromCharArray("Arial")};
};

EditorSettings editor_settings;

struct Clipboard
{
    UTF8 *copied_contents;
    u32 copied_contents_cap;
    u32 copied_contents_size;
};

// FOR now views will ALWAYS use Font_code to draw
enum FontID
{
    Font_tab,
    Font_ui,
    Font_code,
    Font_count
};

struct EditorTheme
{
    Font **fonts;
};

enum ModifierCriteria
{
    Modifier_without,
    Modifier_require,
    Modifier_any
};

enum ShiftCriteria
{
    WithoutShift = Modifier_without,
    RequireShift = Modifier_require,
    AnyShift = Modifier_any,
};

enum CtrlCriteria
{
    WithoutCtrl = Modifier_without,
    RequireCtrl = Modifier_require,
    AnyCtrl = Modifier_any,
};

enum AltCriteria
{
    WithoutAlt = Modifier_without,
    RequireAlt = Modifier_require,
    AnyAlt = Modifier_any,
};

struct KeyBinding
{
    ShiftCriteria shift_criteria;
    CtrlCriteria ctrl_criteria;
    AltCriteria alt_criteria;

    PlatformKey key;
    KeyBinding *next;
};

enum EditorCommand
{
    EditorCommand_invalid,

    EditorCommand_new_file,
    EditorCommand_open_file,
    EditorCommand_save_file,

    TextFieldCommand_next_character,
    TextFieldCommand_next_character_extend,
    TextFieldCommand_previous_character,
    TextFieldCommand_previous_character_extend,
    TextFieldCommand_next_word,
    TextFieldCommand_next_word_extend,
    TextFieldCommand_previous_word,
    TextFieldCommand_previous_word_extend,

    TextFieldCommand_previous_line,
    TextFieldCommand_previous_line_extend,
    TextFieldCommand_next_line,
    TextFieldCommand_next_line_extend,

    TextFieldCommand_line_start,
    TextFieldCommand_line_start_extend,
    TextFieldCommand_line_end,
    TextFieldCommand_line_end_extend,

    TextFieldCommand_text_start,
    TextFieldCommand_text_start_extend,
    TextFieldCommand_text_end,
    TextFieldCommand_text_end_extend,

    TextFieldCommand_previous_empty_line,
    TextFieldCommand_previous_empty_line_extend,
    TextFieldCommand_next_empty_line,
    TextFieldCommand_next_empty_line_extend,

    TextFieldCommand_page_up,
    TextFieldCommand_page_down,

    TextFieldCommand_forward_delete_character,
    TextFieldCommand_backward_delete_character,
    TextFieldCommand_forward_delete_word,
    TextFieldCommand_backward_delete_word,
    TextFieldCommand_set_mark,
    TextFieldCommand_newline,
    TextFieldCommand_kill_line,

    TextFieldCommand_paste,
    TextFieldCommand_cut,
    TextFieldCommand_copy,

    TextFieldCommand_undo,
    TextFieldCommand_redo,

    TabbarCommand_close_tab,
    
    EditorCommand_count,
};

typedef void (*UIDefocusHandler) (void *ui_item);

enum UIItemType
{
    UIItem_null,
    UIItem_TextField,
};

struct UIItem
{
    UIItem *next;
    // TODO make it so that interactable elements can have bboxes of different shape!
    Recti bbox;

    i32 z_index;

    UIItemType type;
    union
    {
        TextField *field;
    };
};
    

struct EditorState 
{
    MemoryArena total_storage_arena;

    PlatformFileHandle user_state;
    Clipboard clipboard;

    LoadedFile *loaded_files_sentinel;

    Compositor *compositor;
    
    Tabbar *tabbar;

    KeyBinding *key_bindings;

    EditorTheme *theme;
    
    // TODO in the future we might have
    //      a general keyboard focus and
    //      when we have more UI elements
    i32 focused_z_index;
    void *focused_ui_item;
    UIDefocusHandler defocus_handler;
    void *mouse_capture;
    
	LoadedFile *current_lf;
    TextField *textbox;
	
    // TODO have more than one keymap

    // TODO have debug memory and debug state?
#if INTERNAL
    b32 vsync_debug_test;
    b32 vsync_debug_cyan;

    b32 show_performance_info;
#endif
};

inline Font *
GetThemeFont(EditorTheme *theme, FontID id)
{
    return theme->fonts[id];
}

inline FontMetrics *
GetThemeFontMetrics(EditorTheme *theme, FontID id)
{
    return theme->fonts[id]->metrics;
}

struct ScrollRegion
{
    Recti rect;
    i32 pixdelta;
};


// EditorState g_estate;

internal FileType Editor_GetFileTypeFromExtension(UTF8 *extension);
internal u32 CommandIsTriggered(EditorState *editor, Input *input, EditorCommand editor_command);

// TODO reevaluate whether PlatformFramebuffer getting passed here is necessary
//      we need it to redraw the active frame outline
internal void Editor_CaptureMouse(EditorState *editor, void *ui_component);
internal void Editor_ReleaseMouse(EditorState *editor);
internal void Editor_SetFocus(EditorState *editor, void *item, i32 z_index, UIDefocusHandler defocus_handler);
