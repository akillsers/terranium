// make the utf16 to utf8 functions consistent in return value and things (String vs UTF8 *)
// converts null terminated utf-16 to utf-8
internal String
WIN_UTF16ToString(WCHAR *src_text, MemoryArena *arena)
{
    String string;
    u32 required_size = WideCharToMultiByte(CP_UTF8,
                                            0, // dwFlags
                                            src_text,
                                            -1, // src string size, -1 for null-terminated
                                            0,
                                            0, // dest size is 0
                                            NULL, // lpDefaultChar
                                            NULL); // lpUsedDefaultChar
    UTF8 *utf8_buffer = PushCount(arena, UTF8, required_size);
    int bytes_written =  WideCharToMultiByte(CP_UTF8,
                                             0, // dwFlags
                                             src_text,
                                             -1, // src string size, -1 for null-terminated
                                             (char *) utf8_buffer,
                                             required_size,
                                             NULL, // lpDefaultChar
                                             NULL); // lpUsedDefaultChar
    Assert(bytes_written == required_size);

    string.ptr = utf8_buffer;
    string.len = bytes_written - 1; // exclude the null terminator
    return string;
}

struct UTF16_String
{
    UTF16 *ptr;
    u32 count;
};

internal UTF16_String
WIN_StringToUTF16(String string, MemoryArena *arena)
{
    UTF16_String result = {};
    int required_buffer_size = MultiByteToWideChar(CP_UTF8,
                                                   0 /*dwFlags*/,
                                                   (char *)string.ptr,
                                                   string.len,
                                                   0,
                                                   0); // 0 to return the buffer size required
    result.ptr = PushCount(arena, UTF16, required_buffer_size);
    result.count = required_buffer_size;
    
    int chars_written = MultiByteToWideChar(CP_UTF8,
                                            0 /*dwFlags*/,
                                            (char *)string.ptr,
                                            string.len,
                                            (LPWSTR)result.ptr,
                                            result.count);
    Assert(chars_written == required_buffer_size);
    return result;
}

internal WCHAR *
WIN_StringToWCHAR(String string, MemoryArena *arena)
{
    int required_buffer_size = MultiByteToWideChar(CP_UTF8,
                                                   0 /*dwFlags*/,
                                                   (char *)string.ptr,
                                                   string.len,
                                                   0,
                                                   0); // 0 to return the buffer size required
    int error_code = GetLastError();
    WCHAR *result = PushCountFlags(arena, WCHAR, required_buffer_size + 1, PushSize_align_16); // + 1 == null terminator
    
    int chars_written = MultiByteToWideChar(CP_UTF8,
                                            0 /*dwFlags*/,
                                            (char *)string.ptr,
                                            string.len, 
                                            result,
                                            required_buffer_size);
    result[required_buffer_size] = 0;
    return result;
}

struct ASCII_String
{
    u8 *ptr;
    u32 len;
};

internal ASCII_String
WIN_StringToASCII(String string, MemoryArena *arena)
{
    ASCII_String result = {};

    // it might be smaller than this if it has multibyte chars
    u64 max_string_size = string.len; 
    result.ptr = PushCount(arena, UTF8, max_string_size);

    u32 at = 0;
    u32 i = 0;
    while(i < string.len) {
        u32 codepoint;
        UTF8 num_utf8_bytes = GetMultibyteLength(string.ptr[i]);
        if(num_utf8_bytes > 1) {
            codepoint = '?';
        } else {
            Assert(num_utf8_bytes == 1);
            codepoint = string.ptr[i];
        }

        result.ptr[at++] = codepoint;
        i += num_utf8_bytes;
    }
    result.len = at;
    return result;
}

// converts null terminated utf-16 to utf-8
internal int
WIN_UTF16ToUTF8(UTF8 *dest, u32 dest_size, WCHAR *src_text)
{
    int bytes_written =  WideCharToMultiByte(CP_UTF8,
                                             0, // dwFlags
                                             src_text,
                                             -1, 
                                             (char *) dest,
                                             dest_size,
                                             NULL, // lpDefaultChar
                                             NULL); // lpUsedDefaultChar
    Assert(bytes_written);
    return bytes_written;
}

// determines the number of bytes required for the UTF8 string obtained from text
internal int
WIN_UTF16toUTF8SizeRequired(WCHAR *text)
{
    int size_required =  WideCharToMultiByte(CP_UTF8,
                                             0, // dwFlags
                                             text,
                                             -1, 
                                             0, // dest
                                             0, // destSize
                                             NULL, // lpDefaultChar
                                             NULL); // lpUsedDefaultChar
                                             
    return size_required;
}

internal int
WIN_UTF8ToUTF16(WCHAR *dest, u32 dest_char_size, UTF8 *src_text)
{
    // TODO is this the required buffer size in chars, like stated in documentation
    //     or of WCHAR
    // TODO consider removing the first part
    int required_buffer_size = MultiByteToWideChar(CP_UTF8,
                                                   0, // dwFlags
                                                   (char *)src_text,
                                                   -1,
                                                   dest,
                                                   0); // 0 to return the buffer size required
    Assert(dest_char_size >= required_buffer_size);
    int chars_written = MultiByteToWideChar(CP_UTF8,
                                            0, // dwFlags
                                            (char *)src_text,
                                            -1,
                                            dest,
                                            dest_char_size);
    Assert(chars_written);
    return chars_written;
}


