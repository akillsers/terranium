internal b32
CPP_IsValidIdentifierContinuation(UTF32 codepoint)
{
    b32 result = false;

    if((codepoint >= 'a' && codepoint <= 'z') ||
       (codepoint >= 'A' && codepoint <= 'Z') ||
       (codepoint == '_') ||
       (codepoint >= '0' && codepoint <= '9'))
    {
        result = true;
    }
    else if(codepoint >= 168)
    {
        /*
          Allowed unicode characters:
          00A8, 00AA, 00AD, 00AF, 00B2-00B5, 00B7-00BA, 00BC-00BE, 00C0-00D6, 00D8-00F6, 00F8-00FF
          0100-167F, 1681-180D, 180F-1FFF
          200B-200D, 202A-202E, 203F-2040, 2054, 2060-206F
          2070-218F, 2460-24FF, 2776-2793, 2C00-2DFF, 2E80-2FFF
          3004-3007, 3021-302F, 3031-303F
          3040-D7FF,
          F900-FD3D, FD40-FDCF, FDF0-FE44, FE47-FFFD
          10000-1FFFD, 20000-2FFFD, 30000-3FFFD, 40000-4FFFD, 50000-5FFFD,
          60000-6FFFD, 70000-7FFFD, 80000-8FFFD, 90000-9FFFD, A0000-AFFFD,
          B0000-BFFFD, C0000-CFFFD, D0000-DFFFD, E0000-EFFFD
        */

        if(codepoint == 0x00A8 || 
           codepoint == 0x00AA || 
           codepoint == 0x00AD || 
           codepoint == 0x00AF || 
           codepoint >= 0x00B2 && codepoint <= 0x00B5 || 
           codepoint >= 0x00B7 && codepoint <= 0x00BA || 
           codepoint >= 0x00BC && codepoint <= 0x00BE || 
           codepoint >= 0x00C0 && codepoint <= 0x00D6 || 
           codepoint >= 0x00D8 && codepoint <= 0x00F6 || 
           codepoint >= 0x00F8 && codepoint <= 0x00FF || 
           codepoint >= 0x0100 && codepoint <= 0x167F || 
           codepoint >= 0x1681 && codepoint <= 0x180D || 
           codepoint >= 0x180F && codepoint <= 0x1FFF || 
           codepoint >= 0x200B && codepoint <= 0x200D || 
           codepoint >= 0x202A && codepoint <= 0x202E || 
           codepoint >= 0x203F && codepoint <= 0x2040 || 
           codepoint == 0x2054 || 
           codepoint >= 0x2060 && codepoint <= 0x206F || 
           codepoint >= 0x2070 && codepoint <= 0x218F || 
           codepoint >= 0x2460 && codepoint <= 0x24FF || 
           codepoint >= 0x2776 && codepoint <= 0x2793 || 
           codepoint >= 0x2C00 && codepoint <= 0x2DFF || 
           codepoint >= 0x2E80 && codepoint <= 0x2FFF || 
           codepoint >= 0x3004 && codepoint <= 0x3007 || 
           codepoint >= 0x3021 && codepoint <= 0x302F || 
           codepoint >= 0x3031 && codepoint <= 0x303F || 
           codepoint >= 0x3040 && codepoint <= 0xD7FF || 
           codepoint >= 0xF900 && codepoint <= 0xFD3D || 
           codepoint >= 0xFD40 && codepoint <= 0xFDCF || 
           codepoint >= 0xFDF0 && codepoint <= 0xFE44 || 
           codepoint >= 0xFE47 && codepoint <= 0xFFFD || 
           codepoint >= 0x10000 && codepoint <= 0x1FFFD || 
           codepoint >= 0x20000 && codepoint <= 0x2FFFD || 
           codepoint >= 0x30000 && codepoint <= 0x3FFFD || 
           codepoint >= 0x40000 && codepoint <= 0x4FFFD || 
           codepoint >= 0x50000 && codepoint <= 0x5FFFD || 
           codepoint >= 0x60000 && codepoint <= 0x6FFFD || 
           codepoint >= 0x70000 && codepoint <= 0x7FFFD || 
           codepoint >= 0x80000 && codepoint <= 0x8FFFD || 
           codepoint >= 0x90000 && codepoint <= 0x9FFFD || 
           codepoint >= 0xA0000 && codepoint <= 0xAFFFD || 
           codepoint >= 0xB0000 && codepoint <= 0xBFFFD || 
           codepoint >= 0xC0000 && codepoint <= 0xCFFFD || 
           codepoint >= 0xD0000 && codepoint <= 0xDFFFD || 
           codepoint >= 0xE0000 && codepoint <= 0xEFFFD)
        {
            result = true;
        }

    }

    
    return result;
}

internal b32
CPP_IsValidIdentifierStart(UTF32 codepoint)
{
    b32 result = CPP_IsValidIdentifierContinuation(codepoint);

    if(codepoint >= '0' && codepoint <= '9')
    {
        result = false;
    } else if(codepoint >= 0x0300 && codepoint <= 0x036F || 
              codepoint >= 0x1DC0 && codepoint <= 0x1DFF || 
              codepoint >= 0x20D0 && codepoint <= 0x20FF || 
              codepoint >= 0xFE20 && codepoint <= 0xFE2F)
    {
        result = false;
    }

    return result;
}

internal void
CPP_IdentifierAppendNormalKeywordSlowly(LoadedFile *lf, CPPToken *token)
{
    // scan the list of "normal" keywords (any keywords that are not preprocessor keywords)
    for(int i = 0; i < ArrayCount(CPP_keywords); i++) {
        char *compare_keyword_at = CPP_keywords[i];
        u32 token_offset = 0;
        while(compare_keyword_at && token_offset < token->len)
        {
            Cursor cursor = token->pos + token_offset;
            UTF32 codepoint = GetCodepoint(lf->text, cursor);
            if(codepoint != *compare_keyword_at)
                break;
            token_offset++;
            compare_keyword_at++;
        }
        if(token_offset == token->len && *compare_keyword_at == 0){
            // we have found the right keyword
            token->type = (CPPTokenType)(token->type | (((i + 1) << 16 ) | CPPToken_is_normal_keyword));
            return;
        }
    }

    // this identifier is not a normal keyword!
    return;
}

struct GetCodepointCache
{
    // we only need to store a letters used because we KNOW everything below letters_used is valid
    u32 letters_used;

    Cursor base_char_pos;
    UTF32 codepoints[20];
};

inline UTF32
CachedGetCodepoint(TextBuffer *buffer, GetCodepointCache *cache, Cursor char_pos)
{
    UTF32 result;
    u32 index_in_cache = char_pos - cache->base_char_pos;
    if(index_in_cache < cache->letters_used)
    {
        result = cache->codepoints[index_in_cache];
    } else
    {
        result = GetCodepoint(buffer, char_pos);
        cache->codepoints[index_in_cache] = result;
        cache->letters_used++;
    }
    return result;
}

internal void
CPP_IdentifierAppendNormalKeyword(LoadedFile *lf, CPPToken *token)
{
    // TODO alglorithm: see if the first letter matches then check
    //     using fast codepoints
    GetCodepointCache codepoint_cache = {};
    codepoint_cache.base_char_pos = token->pos;
    
    for(int i = 0; i < ArrayCount(CPP_keywords); i++)
    {
        char *keyword_at = CPP_keywords[i];
        int letter = 0;
        while(keyword_at && letter < token->len)
        {
            UTF32 codepoint = CachedGetCodepoint(lf->text, &codepoint_cache, token->pos + letter);

            if(codepoint != *keyword_at)
                break;

            keyword_at++;
            letter++;
        }

        if(letter == token->len && *keyword_at == 0)
        {
            token->type = (CPPTokenType)(token->type | (((i + 1) << 16 ) | CPPToken_is_normal_keyword));
            break;
        }
    }
    return;
}

internal CPPTokenType
CPP_IdentifierGetPreprocessorKeyword(TextBuffer *buffer, CPPToken *token)
{
    CPPTokenType ret = CPPToken_invalid;
    // scan the list of preprocessor keywords
    for(int i = 0; i < ArrayCount(CPP_preprocessing_keywords); i++) {
        char *compare_keyword_at = CPP_preprocessing_keywords[i];
        u32 token_offset = 0;
        while(compare_keyword_at && token_offset < token->len) {
            Cursor cursor = token->pos + token_offset;
            UTF32 codepoint = GetCodepoint(buffer, cursor);
            if(codepoint != *compare_keyword_at)
                break;
            token_offset++;
            compare_keyword_at++;
        }
        if(token_offset == token->len && *compare_keyword_at == 0){
            // we have found the right keyword
            ret = (CPPTokenType)(token->type | (((i + 1) << 16 ) | CPPToken_is_preprocessing_directive_keyword));
            break;
        }
    }
    return ret;
}

// handles word alternative token types like 'or', 'and', and 'not_eq'
internal CPPTokenType
CPP_GetAlternativeTokenType(TextBuffer *buffer, CPPToken *token)
{
    CPPTokenType ret = CPPToken_invalid;
    for(int i = 0; i < ArrayCount(CPP_alternative_token_keys); i++) {
        char *compare_alternative_token_at = CPP_alternative_token_keys[i];
        u32 token_offset = 0;

        while(compare_alternative_token_at && token_offset < token->len) {
            Cursor cursor = token->pos + token_offset;
            UTF32 codepoint = GetCodepoint(buffer, cursor);
            if(codepoint != *compare_alternative_token_at)
                break;
            token_offset++;
            compare_alternative_token_at++;
        }
        if(token_offset == token->len && *compare_alternative_token_at == 0){
            // we have found the right keyword
            ret = CPP_alternative_token_values[i];
            break;
        }
    }
    return ret;
}

internal CPPToken
GetQuoteToken(LoadedFile *lf, Tokenizer *tokenizer, char quote_delimiter)
{
    CPPToken ret = {};
    ret.pos = tokenizer->at - 1;

    UTF32 codepoint = GetCodepoint(lf->text, tokenizer->at);
    while(tokenizer->at < lf->text->total_characters)
    {
        if(codepoint == quote_delimiter)
        {
            tokenizer->at++;
            break;
        }
        else if(codepoint == '\\')
        {
            tokenizer->at++;
            if(tokenizer->at >= lf->text->total_characters) {
                break;
            }

            codepoint = GetCodepoint(lf->text, tokenizer->at);
            tokenizer->at++;
        }
        else
        {
            tokenizer->at++;
        }
        codepoint = GetCodepoint(lf->text, tokenizer->at);
    }
    ret.len = tokenizer->at - ret.pos;
    return ret;
}

internal void
CPP_ParseIntegerSuffix(LoadedFile *lf, Tokenizer *tokenizer)
{
    UTF32 codepoint = GetCodepoint(lf->text, tokenizer->at);
    if(codepoint == 'u' || codepoint == 'U')
    {
        tokenizer->at++;
        codepoint = GetCodepoint(lf->text, tokenizer->at);
        if(codepoint == 'l')
        {
            tokenizer->at++;
            codepoint = GetCodepoint(lf->text, tokenizer->at);
            if(codepoint == 'l') { tokenizer->at++; }
        }
        else if(codepoint == 'L')
        {
            tokenizer->at++;
            codepoint = GetCodepoint(lf->text, tokenizer->at);
            if(codepoint == 'L') { tokenizer->at++; }
        }
        else if(codepoint == 'z' || codepoint == 'Z') { tokenizer->at++; }
    }
    else if(codepoint == 'l')
    {
        tokenizer->at++;
        codepoint = GetCodepoint(lf->text, tokenizer->at);
        if(codepoint == 'l') {
            tokenizer->at++;
            codepoint = GetCodepoint(lf->text, tokenizer->at);
            if(codepoint == 'u' || codepoint == 'U') tokenizer->at++;
        }
        else if(codepoint == 'u' || codepoint == 'U') {
            tokenizer->at++;
        }
    }
    else if(codepoint == 'L')
    {
        tokenizer->at++;
        codepoint = GetCodepoint(lf->text, tokenizer->at);
        if(codepoint == 'L') {
            tokenizer->at++;
            codepoint = GetCodepoint(lf->text, tokenizer->at);
            if(codepoint == 'u' || codepoint == 'U') tokenizer->at++;
        }
        else if(codepoint == 'u' || codepoint == 'U')
        {
            tokenizer->at++;
        }
    }
    else if(codepoint == 'z' || codepoint == 'Z')
    {
        tokenizer->at++;
        codepoint = GetCodepoint(lf->text, tokenizer->at);
        if(codepoint == 'u' || codepoint == 'U') { tokenizer->at++; }
    }
}

inline void
ParseHexDigitSequence(LoadedFile *lf, Tokenizer *tokenizer)
{
    UTF32 codepoint;
    while(tokenizer->at < lf->text->total_characters)
    {
        tokenizer->at++;
        codepoint = GetCodepoint(lf->text, tokenizer->at);

        if(codepoint == '\'') { 
            tokenizer->at++; codepoint = GetCodepoint(lf->text, tokenizer->at); 
        }

        if(!(codepoint >= '0' && codepoint <= '9') &&
           !(codepoint >= 'a' && codepoint <= 'f') &&
           !(codepoint >= 'A' && codepoint <= 'F'))
        {
            break;
        }
    }
}

inline void
ParseDecDigitSequence(LoadedFile *lf, Tokenizer *tokenizer)
{
    UTF32 codepoint;
    while(tokenizer->at < lf->text->total_characters)
    {
        tokenizer->at++;
        codepoint = GetCodepoint(lf->text, tokenizer->at);

        if(codepoint == '\'') {
            tokenizer->at++; codepoint = GetCodepoint(lf->text, tokenizer->at);
        }
        if(!(codepoint >= '0' && codepoint <= '9'))
        {
            break;
        }
    }
}

inline void
ParseDecExponent(LoadedFile *lf, Tokenizer *tokenizer)
{
    UTF32 codepoint = GetCodepoint(lf->text, tokenizer->at);
    if(codepoint == 'e' || codepoint == 'E')
    {
        tokenizer->at++;
        if(tokenizer->at < lf->text->total_characters)
        {
            codepoint = GetCodepoint(lf->text, tokenizer->at);
            if(codepoint == '+' || codepoint == '-')
            {
                tokenizer->at++;
                codepoint = GetCodepoint(lf->text, tokenizer->at);
            }

            ParseDecDigitSequence(lf, tokenizer);
        }
    }
}

inline void
ParseHexExponent(LoadedFile *lf, Tokenizer *tokenizer)
{
    UTF32 codepoint = GetCodepoint(lf->text, tokenizer->at);
    if(codepoint == 'p' || codepoint == 'P')
    {
        tokenizer->at++;
        if(tokenizer->at < lf->text->total_characters)
        {
            codepoint = GetCodepoint(lf->text, tokenizer->at);
            if(codepoint == '+' || codepoint == '-')
            {
                tokenizer->at++;
                codepoint = GetCodepoint(lf->text, tokenizer->at);
            }

            ParseHexDigitSequence(lf, tokenizer);
        }
    }
}

inline void
ParseFloatingPointSuffix(LoadedFile *lf, Tokenizer *tokenizer)
{
    UTF32 codepoint = GetCodepoint(lf->text, tokenizer->at);
    if(codepoint == 'f' || codepoint == 'F' || codepoint == 'l' || codepoint == 'L')
    {
        tokenizer->at++;
    }
}

// NOTE:: ignores whitespace
internal CPPToken
GetNextCPPToken(LoadedFile *lf, Tokenizer *tokenizer)
{
    CPPToken ret = {};
    ret.len = 1;
    ret.type = CPPToken_unknown;

    UTF32 codepoint1;
    UTF32 codepoint2;
    while(tokenizer->at < lf->text->total_characters)
    {
        codepoint1 = GetCodepoint(lf->text, tokenizer->at);

        b32 previous_char_was_return = false;
        // TODO any more whitespaces??
        if(codepoint1 == ' ' || codepoint1 == '\v' || codepoint1 == '\t') {
            tokenizer->at++;
        } else if(codepoint1 == '\r') {
            if(previous_char_was_return) {
                tokenizer->relative_line++;
            }
            tokenizer->at++;
            previous_char_was_return = true;
        } else if(codepoint1 == '\n') {
            if(previous_char_was_return) {
                tokenizer->relative_line++;
            } else { 
                tokenizer->relative_line++;
            }
            tokenizer->at++;
        } else {
            if(previous_char_was_return)
                tokenizer->relative_line++;
            break;
        }
    }

    ret.pos = tokenizer->at;
    Cursor start = tokenizer->at;

    if(tokenizer->at >= lf->text->total_characters)
    {
        ret.type = CPPToken_eof;
        return ret;
    }

    codepoint1 = GetCodepoint(lf->text, tokenizer->at);
    codepoint2 = GetCodepoint(lf->text, tokenizer->at + 1);
    if(codepoint1 == '/' &&
       codepoint2 == '/')
    {
        tokenizer->at++; // advances to second slash
        codepoint1 = GetCodepoint(lf->text, tokenizer->at);
        tokenizer->at++;
        while(tokenizer->at < lf->text->total_characters)
        {
            codepoint1 = GetCodepoint(lf->text, tokenizer->at);
            // if(codepoint1 == '\0') // TODO if it hits early break prematurely??
            //     break;
            if(codepoint1 == '\r') {
                codepoint2 = GetCodepoint(lf->text, tokenizer->at);
                if(codepoint2 == '\n') {
                    ret.type = CPPToken_comment;
                    ret.len = tokenizer->at - start;
                    tokenizer->at += 2;
                    tokenizer->relative_line++;
                    return ret;
                } else {
                    ret.type = CPPToken_comment;
                    ret.len = tokenizer->at - start;
                    tokenizer->at++;
                    tokenizer->relative_line++;
                    return ret;
                }
            } else if(codepoint1 == '\n') {
                ret.type = CPPToken_comment;
                ret.len = tokenizer->at - start;
                tokenizer->at++;
                tokenizer->relative_line++;
                return ret;
            }
            tokenizer->at++;
        }

        ret.type = CPPToken_comment;
        ret.len = tokenizer->at - start;
        return ret;
    } else if(codepoint1 == '/' &&
              codepoint2 == '*')
    {
        tokenizer->at++; // advances to asterisk
        codepoint1 = GetCodepoint(lf->text, tokenizer->at);
        codepoint2 = GetCodepoint(lf->text, tokenizer->at + 1);
        while(tokenizer->at < lf->text->total_characters)
        {
            tokenizer->at++;
            codepoint1 = GetCodepoint(lf->text, tokenizer->at);
            codepoint2 = GetCodepoint(lf->text, tokenizer->at + 1);
            if(codepoint1 == '*' &&
               codepoint2 == '/')
            {
               tokenizer->at += 2;
               break;
            }
           
        }
        
        ret.type = CPPToken_comment;
        ret.len = tokenizer->at - start;
        return ret;
    }

    // // TODO optimization all of the characters that are valid c++ are one byte long
    // //     so we don't need GetCodepoint just get the byte at the character address
    // codepoint1 = GetCodepoint(lf, tokenizer->at);
    tokenizer->at++; // moves on to next character;

    // TODO support C++ trigraphs
    switch(codepoint1)
    {
        case ';': {ret.len = 1; ret.type = CPPToken_semicolon;}break;
        case '*': {ret.len = 1; ret.type = CPPToken_asterisk;}break;
        case '/': {ret.len = 1; ret.type = CPPToken_slash; }break;
        case '.': {
            if(codepoint2 >= '0' && codepoint2 <= '9') {
                ParseDecDigitSequence(lf, tokenizer);
                ParseDecExponent(lf, tokenizer); // optional
                ParseFloatingPointSuffix(lf, tokenizer); // optional

                ret.type = CPPToken_number;
                ret.len = tokenizer->at - start;
            }
            else {
                ret.len = 1; ret.type = CPPToken_period;
            }
        }break;
        case ',': {ret.len = 1; ret.type = CPPToken_comma;}break;
        case '\\': {ret.len = 1; ret.type = CPPToken_backslash;}break;
        case '=': {ret.len = 1; ret.type = CPPToken_equal;}break;
        case '+': {ret.len = 1; ret.type = CPPToken_plus;}break;
        case '#': {ret.len = 1; ret.type = CPPToken_preprocessing_start;}break;
        case '{': {ret.len = 1; ret.type = CPPToken_open_brace;}break;
        case '}': {ret.len = 1; ret.type = CPPToken_close_brace;}break;
        case '(': {ret.len = 1; ret.type = CPPToken_open_paren;}break;
        case ')': {ret.len = 1; ret.type = CPPToken_close_paren;}break;
        case '~': {ret.len = 1; ret.type = CPPToken_tilde;}break;
        case '?': {
            if(codepoint2 == '?') {
                // parse trigraphs
                tokenizer->at++;
                codepoint2 = GetCodepoint(lf->text, tokenizer->at);
                tokenizer->at++;
                ret.len = 3;
                switch(codepoint2) {
                    case '=': {
                        ret.type = CPPToken_preprocessing_start;
                    }break;
                    case '/': {
                        ret.type = CPPToken_continuation; 
                    }break;
                    case '\'': {
                        ret.type = CPPToken_xor;
                    }break;
                    case '(': {
                        ret.type = CPPToken_open_square;
                    }break;
                    case ')': {
                        ret.type = CPPToken_close_square;
                    }break;
                    case '!': {
                        ret.type = CPPToken_bitwise_or;
                    }break;
                    case '<': {
                        ret.type = CPPToken_open_brace;
                    }break;
                    case '>': {
                        ret.type = CPPToken_close_brace;
                    }break;
                    case '-': {
                        ret.type = CPPToken_tilde;
                    }break;
                    default: {
                        ret.type = CPPToken_unknown;
                    }break;
                }
            }
            else
            {
                ret.len = 1; ret.type = CPPToken_question;
            }
        }break;
        case '%': {
            if(codepoint2 == '=') {ret.len = 2; ret.type = CPPToken_mod_equal; tokenizer->at++; break;}
            else if(codepoint2 == '>') {ret.len = 2; ret.type = CPPToken_close_brace; tokenizer->at++; break;}
            else if(codepoint2 == ':') {ret.len = 2; ret.type = CPPToken_preprocessing_start; tokenizer->at++; break;}
            else {ret.len = 1; ret.type = CPPToken_mod; break;}
        }break;
        case '&': {
            if(codepoint2 == '&') {ret.len = 2; ret.type = CPPToken_and; tokenizer->at++; break;}
            else if(codepoint2 == '=') {ret.len = 2; ret.type = CPPToken_and_equal; tokenizer->at++; break;}
            else {ret.len = 1; ret.type = CPPToken_bitwise_and; break;}
        }break;
        case '^': {
            if(codepoint2 == '=') {ret.len = 2; ret.type = CPPToken_xor_equal; tokenizer->at++; break;}
            else {ret.len = 1; ret.type = CPPToken_xor; break;}
        }break;
        case '|': {
            if(codepoint2 == '|') {ret.len = 2; ret.type = CPPToken_or; tokenizer->at++; break;}
            else if(codepoint2 == '=') {ret.len = 2; ret.type = CPPToken_or_equal; tokenizer->at++; break;}
            else {ret.len = 1; ret.type = CPPToken_bitwise_or; break;}
        }break;
            case '[': {
                    if(codepoint2 == '[') {ret.len = 2; ret.type = CPPToken_double_open_square; tokenizer->at++; break;}
            else {ret.len = 1; ret.type = CPPToken_open_square; break;}
        }break;
        case ']': {
            if(codepoint2 == ']'){ret.len = 2; ret.type = CPPToken_double_close_square; tokenizer->at++; break;}
            else {ret.len = 1; ret.type = CPPToken_close_square;break;}
        }break;
        case '-': {
            if(codepoint2 == '>'){ret.len = 2; ret.type = CPPToken_arrow; tokenizer->at++; break;}
            else {ret.len = 1; ret.type = CPPToken_minus; break;}
        }break;
        case '>': {
            if(codepoint2 == '='){ret.len = 2; ret.type = CPPToken_greater_than_or_equal; tokenizer->at++; break;}
            else{ret.len = 1; ret.type = CPPToken_greater_than; break;}
        }break;
        case '<': {
            if(codepoint2 == '='){ret.len = 2; ret.type = CPPToken_less_than_or_equal; tokenizer->at++; break;}
            // alternative token
            else if(codepoint2 == '%') {ret.len = 2; ret.type = CPPToken_open_brace; tokenizer->at++; break;}
            else if(codepoint2 == ':') {ret.len = 2; ret.type = CPPToken_open_square; tokenizer->at++; break;}
            else{ret.len = 1; ret.type = CPPToken_less_than; break;}
        }break;
        case '!':{
            if(codepoint2 == '='){ret.len = 2; ret.type = CPPToken_not_equal; tokenizer->at++; break;}
            else{ret.len = 1; ret.type = CPPToken_not; break;};
        }break;

        case ':':
        {
            if(codepoint2 == ':'){ret.len = 2; ret.type = CPPToken_scope; tokenizer->at++; break;}
            else if(codepoint2 == '>'){ret.len = 2; ret.type = CPPToken_close_square; tokenizer->at++; break;}
            else{ret.len = 1; ret.type = CPPToken_colon; break;}; // single colon is invalid
        }break;

        case '\"':
        {
            ret = GetQuoteToken(lf, tokenizer, '\"');
            ret.type = CPPToken_string;
        }break;

        // same quote as above, except changed double quote mark (") to single quote mark (')
        case '\'':
        {
            ret = GetQuoteToken(lf, tokenizer, '\'');
            ret.type = CPPToken_char_literal;
        }break;
        
        default:
        {
            if(codepoint1 >= '1' && codepoint1 <= '9')
            {
                // Decimal literal
                codepoint1 = codepoint2;
                codepoint2 = GetCodepoint(lf->text, tokenizer->at + 1);
                while(tokenizer->at < lf->text->total_characters)
                {
                    if(codepoint1 == '\'' && (codepoint2 >= '0' && codepoint2 <= '9'))
                    { 
                        tokenizer->at += 2; 
                        codepoint1 = GetCodepoint(lf->text, tokenizer->at);
                        codepoint2 = GetCodepoint(lf->text, tokenizer->at + 1);
                    }
                    else if(codepoint1 >= '0' && codepoint1 <= '9')
                    {
                        tokenizer->at++;
                        codepoint1 = codepoint2;
                        codepoint2 = GetCodepoint(lf->text, tokenizer->at + 1);
                    }
                    else break;
                }

                if(codepoint1 == '.' || codepoint1 == 'e' || codepoint1 == 'E')
                {
                    goto parse_possible_floating_point_literal;
                }

                CPP_ParseIntegerSuffix(lf, tokenizer);
                ret.type = CPPToken_number;
                ret.len = tokenizer->at - start;
            }
            else if(codepoint1 == '0' && (codepoint2 == 'x' || codepoint2 == 'X'))
            {
                // Hex literal
                ParseHexDigitSequence(lf, tokenizer);
                codepoint1 = GetCodepoint(lf->text, tokenizer->at);
                if(codepoint1 == '.')
                {
                    // Hex floating point literal
                    ParseHexDigitSequence(lf, tokenizer); // This may not exist
                    ParseHexExponent(lf, tokenizer);
                    ParseFloatingPointSuffix(lf, tokenizer); // Optional
                }
                else if(codepoint1 == 'p' || codepoint1 == 'P')
                {
                    // Hex floating point literal
                    ParseHexExponent(lf, tokenizer);
                    ParseFloatingPointSuffix(lf, tokenizer); // Optional
                }
                else
                {
                    // Hex integer literal
                    CPP_ParseIntegerSuffix(lf, tokenizer);
                }

                ret.type = CPPToken_number;
                ret.len = tokenizer->at - start;
            }
            else if(codepoint1 == '0' && (codepoint2 == 'b' || codepoint2 == 'B'))
            {
                // Binary literal
                while(tokenizer->at < lf->text->total_characters)
                {
                    tokenizer->at++;
                    codepoint1 = GetCodepoint(lf->text, tokenizer->at);
                    if(codepoint1 == '\'') { tokenizer->at++; codepoint1 = GetCodepoint(lf->text, tokenizer->at); }
                    if(codepoint1 != '0' && codepoint1 != '1')
                    {
                        break;
                    }
                }

                CPP_ParseIntegerSuffix(lf, tokenizer);
                ret.type = CPPToken_number;
                ret.len = tokenizer->at - start;
            }
            else if(codepoint1 == '0')
            {
                // Octal literal or decimal floating point literal
                codepoint1 = codepoint2;
                b32 seen_octal_digits = false;
                b32 seen_decimal_digits = false;
                while(tokenizer->at < lf->text->total_characters)
                {
                    if(codepoint1 == '\'') { tokenizer->at++; codepoint1 = GetCodepoint(lf->text, tokenizer->at); }
                    if(codepoint1 >= '0' && codepoint1 <= '7')
                    {
                        // octal literal digits
                        tokenizer->at++;
                        codepoint1 = GetCodepoint(lf->text, tokenizer->at);
                        seen_octal_digits = true;
                    }
                    else if(codepoint1 >= '0' && codepoint1 <= '9')
                    {
                        // decimal literal digits
                        tokenizer->at++;
                        codepoint1 = GetCodepoint(lf->text, tokenizer->at);
                        seen_decimal_digits = true;
                    }
                    else break;
                }
parse_possible_floating_point_literal:
                if(codepoint1 == '.')
                {
                    tokenizer->at++;
                    ParseDecDigitSequence(lf, tokenizer); // optional
                    ParseDecExponent(lf, tokenizer); // optional
                    ParseFloatingPointSuffix(lf, tokenizer); // optional (as always)
                }
                else if(codepoint1 == 'e' || codepoint1 == 'E')
                {
                    ParseDecExponent(lf, tokenizer); // _not_ optional
                    ParseFloatingPointSuffix(lf, tokenizer);
                } 
                else if(!seen_decimal_digits)
                {
                    CPP_ParseIntegerSuffix(lf, tokenizer);
                }
                else
                {
                    ret.type = CPPToken_unknown;
                    ret.len = tokenizer->at - start;
                    break;
                }

                
                ret.type = CPPToken_number;
                ret.len = tokenizer->at - start;
            }
#if 0
            // TODO support all numbers!!
            if(codepoint1 >= '0' && codepoint1 <= '9')
            {
                codepoint1 = codepoint2;
                while(tokenizer->at < lf->text->total_characters)
                {
                    if(codepoint1 >= '0' && codepoint1 <= '9')
                    {
                        tokenizer->at++;
                        codepoint1 = GetCodepoint(lf->text, tokenizer->at);
                        // tokenizer->at[0]++;
                    } else if(codepoint1 == '.' ||
                              codepoint1 == 'f')
                    {
                        tokenizer->at++;
                        codepoint1 = GetCodepoint(lf->text, tokenizer->at);
                        // tokenizer->at[0]++;
                    }
                    else break;
                }

                ret.type = CPPToken_number;
                ret.len = tokenizer->at - start;
            }
#endif
            else if(CPP_IsValidIdentifierStart(codepoint1))
            {
                codepoint1 = codepoint2;
                while(tokenizer->at < lf->text->total_characters)
                {
                    if(CPP_IsValidIdentifierContinuation(codepoint1))
                    {
                        tokenizer->at++;
                        codepoint1 = GetCodepoint(lf->text, tokenizer->at);
                    }
                    else break;
                }

                
                ret.type = CPPToken_identifier;
                ret.len = tokenizer->at - start;
                CPP_IdentifierAppendNormalKeyword(lf, &ret);
            }
        }break;

    }
    return ret;
}

internal b32
ParseCPP(ParseContext *pc, LoadedFile *lf)
{
    Assert(pc->parsed_tokens); 

    b32 parsed_to_eof = false;
    pc->num_tokens_parsed = 0;
    Tokenizer tokenizer = {};
    tokenizer.at = pc->start_pos;
    
    for(;;)
    {
        if(pc->num_tokens_parsed >= pc->max_tokens_to_parse)
        {
            // TODO: what do we do in this case??? Consider getting rid of the max tokens to reparse
            //       concept?
            // error out because we ran out of space
            parsed_to_eof = false;
            break;
        } 

        Token *token = pc->parsed_tokens + pc->num_tokens_parsed;

        *token = *(Token *)&GetNextCPPToken(lf, &tokenizer);

        if((token->pos > pc->end_pos) || // sometimes we might skip newline and get that next token, this prevents overparsing
           token->type == CPPToken_eof)
        {
            parsed_to_eof = true;
            break;
        }

        if((token->type & CPP_TOKEN_BASIC_TYPE) == CPPToken_identifier) {
            Token *last_token = 0;
            Token *last_last_token = 0;
            if(pc->num_tokens_parsed > 0)
            {
                last_token = pc->parsed_tokens + pc->num_tokens_parsed - 1;
            } else
            {
                last_token = pc->last_token;
            }

            if(pc->num_tokens_parsed > 1) {
                last_last_token = pc->parsed_tokens + pc->num_tokens_parsed - 2;
            }
            else if (pc->num_tokens_parsed > 0)
            {
                last_last_token = pc->last_token;
            }
            else
            {
                last_last_token = pc->last_last_token;
            }
            // TODO don't check for preprocessor keyword if it is a normal keyword??
            CPPTokenType preprocessor_keyword_type = CPP_IdentifierGetPreprocessorKeyword(lf->text, token);
            if(preprocessor_keyword_type & CPPToken_is_preprocessing_directive_keyword)
            {
                // for preprocessor keywords to be in a valid position there are two requirements:
                // the last token is a #
                // the last last token is on the last line or non_existent

                // TODO this is inefficient, just pass in the curr_line_index in
                u32 line = GetLineContainingCharPos(lf->text, token->pos);
                u32 line_start = LinePos(lf->text, line);


                if(last_token && 
                   last_token->type == CPPToken_preprocessing_start &&
                   (!last_last_token ||
                    last_last_token->pos < line_start))
                {
                    token->type = (CPPTokenType)((int)token->type | (int)preprocessor_keyword_type);
                }
            }
            else if(last_token && last_token->type == CPPToken_preprocessing_start)
            {
                u32 line = GetLineContainingCharPos(lf->text, token->pos);
                u32 line_start = LinePos(lf->text, line);

                if(last_token->pos >= line_start) {
                    token->type = CPPToken_unknown;
                }
            }
        }

        pc->num_tokens_parsed++;

        if(token->type == CPPToken_preprocessing_start)
        {
            /*Make sure that we don't have to un-preprocessing something*/
            u32 line = GetLineContainingCharPos(lf->text, token->pos);
            Cursor end_of_line = GetLastLineChar(lf->text, line);

            if(pc->end_pos < end_of_line)
            {
                pc->end_pos = end_of_line;
            }
        }

        if(tokenizer.at > pc->end_pos)
        {
            parsed_to_eof = true;
            break;
        }
    }

    pc->parse_finished_at = tokenizer.at;
    return parsed_to_eof;
}
