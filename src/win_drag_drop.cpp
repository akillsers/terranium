internal LinkedString *
GetFilenames(IDataObject *data_object, MemoryArena *arena)
{
    LinkedString *result = {};
    
    STGMEDIUM medium;
    {
        FORMATETC hdrop_etc = {};
        hdrop_etc.cfFormat = CF_HDROP;
        hdrop_etc.dwAspect = DVASPECT_CONTENT;
        hdrop_etc.lindex = -1;
        hdrop_etc.tymed = TYMED_HGLOBAL;

        HRESULT hresult = data_object->GetData(&hdrop_etc, &medium);
        if(SUCCEEDED(hresult))
        {
            HDROP hdrop = (HDROP)GlobalLock(medium.hGlobal);

            u32 num_files = DragQueryFileW(hdrop, 0xFFFFFFFF, 0, 0);

            if(num_files > 0) {
                LinkedString *filename_strings = PushCount(arena, LinkedString, num_files);

                LinkedString *last_string = 0;
                for(int i = 0; i < num_files; i++) {
                    LinkedString *this_string = filename_strings + i;
                    // TODO docs say that this is this returns the required number of characters
                    //     presumably that means the required WCHARS even though some Unicode codepoints
                    //     take two WCHARS
                    // TODO test that theory out
                    u32 string_length = DragQueryFileW(hdrop, i, 0, 0);
                    string_length++; // DragQueryFileW likes copying in a null terminator

                    WCHAR *filename_utf16 = PushCount(arena, WCHAR, string_length);
                    u32 characters_copied = DragQueryFileW(hdrop, i, filename_utf16, string_length);

                    // Assert(string_length == characters_copied);
                    this_string->data = WIN_UTF16ToString(filename_utf16, arena);

                    // next is actaully the previously processed string
                    // but for the purposes of the editor, it doesn't matter
                    // since these aren't sorted in any way
                    this_string->next = last_string;
                    last_string = this_string;
                }
                result = filename_strings + num_files - 1;
            }

            GlobalUnlock(medium.hGlobal);
            ReleaseStgMedium(&medium);
        }
    }
    
    // TODO support CFSTR_FILENAMEA, CFSTR_FILENAMW, and FileDescriptorW
    // FORMATETC format_etc = {};
    // format_etc.cfFormat = CF_HDROP;
    // format_etc.dwAspect = DVASPECT_CONTENT;
    // format_etc.lindex = -1;
    // format_etc.tymed = TYMED_HGLOBAL;
    
    return result;
}

HRESULT WIN_DropTarget::DragLeave()
{
    drop_target_helper->DragLeave();
    return S_OK;
}

HRESULT WIN_DropTarget::Drop(IDataObject *data_object,
                             DWORD key_state,
                             POINTL cursor_position,
                             DWORD *effect)
{
    drop_target_helper->Drop(data_object, (POINT *)&cursor_position, *effect);

    // TODO note that if two items are dropped per frame, only the most recent dropped item will be processed
    
    LinkedString *linked_string = {};
    if(linked_string = GetFilenames(data_object, &g_win32.frame_arena)) {
        DroppedItems *dropped = &g_win32.input.dropped;
        dropped->type = DroppedItemType_filename;
        dropped->string = linked_string;
    } 
    return S_OK;
}

HRESULT WIN_DropTarget::DragOver(DWORD key_state,
                                 POINTL cursor_position,
                                 DWORD *effect)
{
    drop_target_helper->DragOver((POINT *)&cursor_position, *effect);
    return S_OK;
}

HRESULT WIN_DropTarget::DragEnter(IDataObject *data_object,
                                  DWORD key_state,
                                  POINTL cursor_position,
                                  DWORD *effect)
{
    drop_target_helper->DragEnter(linked_hwnd, data_object, (POINT *) &cursor_position, *effect);
    return S_OK;
}

// TODO does correct implementation of this matter?
HRESULT
WIN_DropTarget::QueryInterface(const IID &,void **)
{
    return S_OK;
}

ULONG
WIN_DropTarget::AddRef(void)
{
    return 1;
}

ULONG
WIN_DropTarget::Release(void)
{
    return 1;
}


// {4657278A-411B-11d2-839A-00C04FD918D0}
DEFINE_GUID(CLSID_DragDropHelper,   0x4657278a, 0x411b, 0x11d2, 0x83, 0x9a, 0x0, 0xc0, 0x4f, 0xd9, 0x18, 0xd0);

internal void 
WIN_InitDragState(WIN_WindowData *window)
{
    HRESULT hr;
#if 1
    WIN_DropTarget *drop_target = &g_win32.drop_target;
#else
    WIN_DropTarget *drop_target = &window->drop_target;    
#endif
    
    (*drop_target) = {};
    
    hr = RegisterDragDrop(window->hwnd, drop_target);
    Assert(SUCCEEDED(hr));
    
    hr = CoCreateInstance(CLSID_DragDropHelper, 0, CLSCTX_INPROC_SERVER,
                          IID_IDropTargetHelper,
                          (LPVOID *) &drop_target->drop_target_helper);
    
    drop_target->linked_hwnd = window->hwnd;
    Assert(SUCCEEDED(hr));
    
}

