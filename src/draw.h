typedef Vec3 Color;

#define INVALID_CLR vec3(-1, -1, -1)

#define CLR_MAROON rgb_u32(62, 28, 28)
#define CLR_RED rgb(1.0f, 0.0f, 0.0f)
#define CLR_GREEN rgb(0.0f, 0.6f, 0.0f)
#define CLR_BLUE rgb(0.0f, 0.0f, 0.9f)
#define CLR_CYAN rgb(0.0f, 0.9f, 0.9f)
#define CLR_ORANGE rgb(0.97f, 0.6f, 0.0f)
#define CLR_LBLUE rgb(0.f, 0.6f, 0.90f)
#define CLR_YELLOW rgb(1.0f,1.0f,0.0f)


#define CLR_BLACK rgb(0.0f, 0.0f, 0.0f)
#define CLR_WHITE rgb(1.0f, 1.0f, 1.0f)

#define CLR_GREY10 rgb(0.1f, 0.1f, 0.1f)
#define CLR_GREY20 rgb(0.2f, 0.2f, 0.2f)
#define CLR_GREY30 rgb(0.3f, 0.3f, 0.3f)
#define CLR_GREY40 rgb(0.4f, 0.4f, 0.4f)
#define CLR_GREY50 rgb(0.5f, 0.5f, 0.5f)
#define CLR_GREY60 rgb(0.6f, 0.6f, 0.6f)
#define CLR_GREY70 rgb(0.7f, 0.7f, 0.7f)
#define CLR_GREY80 rgb(0.8f, 0.8f, 0.8f)
#define CLR_GREY90 rgb(0.9f, 0.9f, 0.9f)

#define CLR_MAGENTA rgb(1.0f, 0.0f, 1.0f)
    
inline b32
    IsColorValid(Color color)
{
    b32 result;

    if(color.r >= 0 &&
       color.r <= 255 &&
       color.g >= 0 &&
       color.g <= 255 &&
       color.b >= 0 &&
       color.b <= 255)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}
