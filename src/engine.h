// TODO support ctrl insert and shift insert to copy + paste
typedef u32 BytePos;
typedef u32 Cursor;

/*------------------------------------*\
  structure of the primary storage unit 
  
  LoadedFile struct (serves as header)
  the file path
  the line array
  RawBufSection array (if any space is availible, may also be put in LoadedFile->section_storage_space)
  the raw buf storage (if any space is availible)
\*------------------------------------*/

struct CursorBytePosPair
{
    // TODO
    // any destructive operation to the LF will invalidate the cache
    // even though this should be the case if the destructive operation happened
    // after the cache point
    b32 is_valid;
    u32 check_total_bytes;
    u32 check_total_chars;

    Cursor section_char_pos;
    BytePos section_byte_pos;
};

struct CursorRange
{
    Cursor min;
    Cursor max;
};

struct RawBufSection
{
    // these are the only values
    // that are not relative to data
    // they concern the LoadedFile
    u32 chars_before_section;
    u32 bytes_before_section;

    UTF8 *data;
    u32 num_gap_bytes;
    u32 max_bytes; // includes gap bytes
    u32 num_chars;
    u32 num_bytes; // does not include gap bytes (skips them over)

    // every section has a gap
    // NOTE that these values are relative to data
    u32 bytes_before_gap;
    u32 chars_before_gap;

    CursorBytePosPair cached_pos_pair;
    
    RawBufSection *next;
    RawBufSection *prev;
};

enum TextEncodingType
{
    Encoding_ASCII,
    Encoding_CP1252,
    Encoding_UTF8,
    Encoding_UTF16,
    Encoding_UTF32,
    Encoding_unknown,
};

typedef u32 TextEncodingFlags;
#define TEXT_ENCODING_HAS_UNICODE_BOM (1 << 1)
#define TEXT_ENCODING_IS_BIG_ENDIAN (1 << 2)
#define TEXT_ENCODING_IS_LITTLE_ENDIAN (1 << 3)
struct TextEncoding
{
    TextEncodingType type;
    TextEncodingFlags flags;
};

struct LineStore {
    u32 lines_before_store;
    Cursor *line_array;
    u32 num_lines;
    u32 max_lines;
    
    LineStore *next;
    LineStore *prev;
};

#define SECTION_BYTE_AFTER_GAP(section) (section->data +               \
                                         section->bytes_before_gap +    \
                                         section->num_gap_bytes)
#define SECTION_NUM_BYTES_AFTER_GAP(section) (section->num_bytes - section->bytes_before_gap)
struct LinesCount
{
    u32 endings_of_type[LineEnding_count];
};

typedef u32 TerraniumTokenType;
struct Token
{
    TerraniumTokenType type;
    Cursor pos;
    u32 len;
};

struct TokenStore
{
    u32 tokens_before_store;
    Token *tokens;
    u32 num_tokens;
    u32 max_tokens;

    TokenStore *next;
    TokenStore *prev;
};


struct ParseContext
{
    Cursor start_pos;
    Cursor end_pos;

    // additional information needed to parse certain things:
    // for c++: preprocessor directive keywords (#if, error, define, etc.)
    // for 
    Token *last_token;      // if 0 -> assumed to not exist
    Token *last_last_token; // if 0 -> assumed to not exist

    // --output variables--
    Token *parsed_tokens;
    u32 num_tokens_parsed;
    u32 max_tokens_to_parse;
    Cursor parse_finished_at;
};

enum FileType {
    FileType_unknown,
    FileType_CPP,
    FileType_plain_text,
};

struct LoadedFile;
struct Anchor
{
    b32 activated;
    LoadedFile *lf;
    Cursor *cursor;
    u32 *line;
    // Cursor *cursor;
    // u32 *line;
};

struct AnchorLink
{
    Anchor anchor;
    AnchorLink *next;
    AnchorLink *prev;
};

struct TextBuffer
{
    
    // the LineStore and RawBufSection are both sorted
    LineStore line_store_sentinel;
    u32 num_lines;

    RawBufSection section_sentinel;
    RawBufSection *cached_section;

    // doesn't include gap bytes
    u32 total_bytes; 
    u32 total_characters;
};

#define GAP_BUFFER_ALLOC_SIZE 20
struct InsertTextResult
{
    u32 num_chars;
    u32 num_newlines;
};