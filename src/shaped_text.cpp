inline FractionalNumber
ImproperI32ToFractional(i32 improper)
{
    FractionalNumber result;

    result.integer_part = improper / SubpixelGranularity;
    result.fractional_part = improper % SubpixelGranularity;

    return result;
}

inline FractionalNumber
I32ToFractional(i32 num)
{
    FractionalNumber result = {};
    result.integer_part = num;

    return result;
}

inline FractionalNumber
operator+ (FractionalNumber left, FractionalNumber right)
{
    FractionalNumber result;
    
    i32 fractional_sum = left.fractional_part + right.fractional_part;
    i32 carry_over = fractional_sum / SubpixelGranularity;

    result.integer_part = left.integer_part + right.integer_part + carry_over;
    result.fractional_part = fractional_sum % SubpixelGranularity;
    return result;
}

inline FractionalNumber
operator- (FractionalNumber left, FractionalNumber right)
{
    FractionalNumber result;

    FractionalNumber negated_right = right;
    negated_right.integer_part *= -1;
    negated_right.fractional_part *= -1;

    result = left + negated_right;
    
    return result;
}

inline b32
operator< (FractionalNumber left, FractionalNumber right)
{
    b32 result = false;

    if(left.integer_part < right.integer_part)
    {
        result = true;
    }
    else if(left.integer_part == right.integer_part)
    {
        if(left.fractional_part < right.fractional_part)
        {
            result = true;
        }
    }

    return result;
}

inline b32
operator> (FractionalNumber left, FractionalNumber right)
{
    b32 result = false;

    if(left.integer_part > right.integer_part)
    {
        result = true;
    }
    else if(left.integer_part == right.integer_part)
    {
        if(left.fractional_part > right.fractional_part)
        {
            result = true;
        }
    }

    return result;
}

inline FractionalNumber
operator+= (FractionalNumber &left, FractionalNumber right)
{
    left = left + right;
    return left;
}

inline FractionalNumber
operator-= (FractionalNumber &left, FractionalNumber right)
{
    left = left - right;
    return left;
}

inline FractionalNumber
FractionalZero()
{
    FractionalNumber result = {};
    return result;
}

inline FractionalNumber
RoundFractional(FractionalNumber num)
{
    FractionalNumber result = num;

    if(result.fractional_part < 0 &&
       result.fractional_part <= -SubpixelGranularity / 2)
    {
        --result.integer_part;
    }
    else if(result.fractional_part >= 0 &&
            result.fractional_part >= SubpixelGranularity / 2)
    {
        ++result.integer_part;
    }
    
    result.fractional_part = 0;

    return result;
}

inline f32
FractionalToF32(FractionalNumber num)
{
    f32 result = 0;

    result = num.integer_part + (f32)num.fractional_part / (f32)SubpixelGranularity;

    return result;
}

inline FractionalNumber
U32ToFractional(u32 num)
{
    FractionalNumber result = {};

    // TODO: handle overflow
    result.integer_part = num;

    return result;
}

inline Vec2i
IntegerPartOnly(GlyphOffset offset)
{
    Vec2i result;

    result.x = offset.x.integer_part;
    result.y = offset.y.integer_part;

    return result;
}


inline Vec2i
FractionalPartOnly(GlyphOffset offset)
{
    Vec2i result;

    result.x = offset.x.fractional_part;
    result.y = offset.y.fractional_part;

    return result;
}

internal u32
GlyphToCluster(ShapedTextRun *run, u32 glyph_index)
{
    TIMED_BLOCK;
    u32 result = 0;
#if 0
    // Binary search
    u32 left = 0;
    u32 right = run->num_clusters - 1;
    
    while(left <= right)
    {
        u32 mid = left + (right - left) / 2;
        ClusterInfo *cluster = &run->cluster[mid];

        if(cluster->start_glyph_index + cluster->num_glyphs - 1 < glyph_index)
        {
            left = mid + 1;
        }
        else if(cluster->start_glyph_index > glyph_index)
        {
            right = mid - 1;
        }
        else
        {
            result = mid;
            break;
        }
    }
#else
    // Intepolation-like search
    
    u32 low = 0;
    u32 high = run->num_clusters - 1;
    for(;;)
    {
        ClusterInfo *first_cluster = &run->cluster[low];
        ClusterInfo *last_cluster = &run->cluster[high];
        f32 pos_f32 = low + (high - low + 1) * ((f32)(glyph_index - first_cluster->start_glyph_index) / (last_cluster->start_glyph_index + last_cluster->num_glyphs - first_cluster->start_glyph_index));
        u32 guess_pos = pos_f32;
        Assert(guess_pos <= high && guess_pos >= low);

        ClusterInfo *guess_cluster = &run->cluster[guess_pos];
        if(guess_cluster->start_glyph_index + guess_cluster->num_glyphs - 1 < glyph_index)
        {
            low = guess_pos + 1;
        }
        else if(guess_cluster->start_glyph_index > glyph_index)
        {
            high = guess_pos - 1;
        }
        else
        {
            result = guess_pos;
            break;
        }
    }    
#endif
    return result;
}

internal FractionalNumber
GetClusterWidth(ShapedTextRun *run, u32 cluster_index)
{
    TIMED_BLOCK;
    Assert(cluster_index < run->num_clusters);

    ClusterInfo *cluster = run->cluster + cluster_index;
    
    FractionalNumber total_advance_width = {};
    for(int i = 0; 
        i < cluster->num_glyphs;
        i++)
    {
        GlyphInfo *glyph = run->glyph + (cluster->start_glyph_index + i);
        total_advance_width += glyph->advance;
    }

    return total_advance_width;
}

// TODO: Remove
/*
internal u32
GetCharacterCluster(ShapedTextRun *run, u32 character_index)
{
    TIMED_BLOCK;
    u32 cluster_index = 0;
    Assert(run->num_clusters > 0);
    if(run->num_clusters > 0)
    {
        u32 character_glyph_index = run->character_to_first_glyph[character_index];
        cluster_index = GlyphToCluster(run, character_glyph_index);
    }
    
    return cluster_index;
}
*/

internal FractionalNumber
GetWidthBeforeRun(ShapedText *shaped_text, ShapedTextRun *shaped_text_run)
{
    FractionalNumber result = {};

    b32 found_run = false;
    for(int i = 0;
        i < shaped_text->num_runs;
        i++)
    {
        ShapedTextRun *run = shaped_text->runs + shaped_text->visual_to_logical[i];

        if(run == shaped_text_run)
        {
            found_run = true;
            break;
        }
        result += run->total_width;
            
    }

    Assert(found_run);
    
    return result;
}


internal u32
GetCharsBeforeRun(ShapedText *shaped_text, ShapedTextRun *shaped_text_run)
{
    u32 result = 0;

    u32 chars_before_run = 0;
    for(int i = 0;
        i < shaped_text->num_runs;
        i++)
    {
        ShapedTextRun *run = shaped_text->runs + i;

        if(run == shaped_text_run)
        {
            result = chars_before_run;
            break;
        }
        chars_before_run += run->num_chars;
    }
    return result;
}

internal ShapedTextRun *
GetRun(ShapedText *shaped_text, u32 character_from_text_start)
{
    TIMED_BLOCK;
    ShapedTextRun *result = 0;

    // TODO make binary search
    for(int i = 0;
        i < shaped_text->num_runs;
        i++)
    {
        ShapedTextRun *run = shaped_text->runs + i;

        if(character_from_text_start >= run->chars_before_run &&
           character_from_text_start < run->chars_before_run + run->num_chars)
        {
            result = run;
            break;
        }
    }

    // Assert(result);
    return result;
}

internal GlyphOffset
GetGlyphPositionInShapedRun(ShapedTextRun *run, u32 glyph_index)
{
    Assert(glyph_index < run->num_glyphs);

    GlyphOffset result = {};
    GlyphInfo *glyph = run->glyph + glyph_index;

    for(int i = 0;
        i < glyph_index;
        i++)
    {
        GlyphInfo *preceeding_glyph = run->glyph + i;
        result.x += preceeding_glyph->advance;
    }
    
    result.x += glyph->offset.x;
    // TODO check chromium firefox sources, etc. is this supposed to be negative??
    result.y -= glyph->offset.y; // note that this is the y (top) of the glyph and NOT the baseline

    return result;
}

internal u32
ClusterToFirstCharacterInShapedRun(ShapedTextRun *run, u32 cluster_index)
{
    u32 result = 0;

    // Strategy: Iterate through all the characters and see 
    for(int i = 0;
        i < run->num_chars;
        i++)
    {
        u32 char_cluster = run->character_to_cluster[i];
        if(char_cluster == cluster_index)
        {
            result = i;
            break;
        }
    }

    return result;
}

internal u32
GetCharacterInRun(ShapedTextRun *run, FractionalNumber run_relative_x)
{
    FractionalNumber width_before_cluster = {};
    u32 one_past_cluster = 0;
    
    for(u32 i = 0;
        i < run->num_clusters;
        i++)
    {
        FractionalNumber cluster_width = GetClusterWidth(run, i);

        width_before_cluster += cluster_width;

        if(run_relative_x < width_before_cluster)
        {
            one_past_cluster = i + 1;
            break;
        }
    }

    Assert(one_past_cluster);

    u32 result = ClusterToFirstCharacterInShapedRun(run, one_past_cluster - 1);
    return result;
}

internal FractionalNumber
GetCharacterWidthInShapedRun(ShapedTextRun *run, u32 chars_from_start)
{
    FractionalNumber result = {};

    Assert(chars_from_start < run->num_chars);

    // TODO: There may be multiple characters in a cluster!
    u32 cluster_index = run->character_to_cluster[chars_from_start];
    ClusterInfo *char_cluster = &run->cluster[cluster_index];

    // Loop through all the glyphs in the cluster and sum up the advances
    for(int i = 0;
        i < char_cluster->num_glyphs;
        ++i)
    {
        GlyphInfo *glyph = &run->glyph[char_cluster->start_glyph_index + i];
        result += glyph->advance;
    }

    return result;
}

internal FractionalNumber
GetCharacterXInShapedRun(ShapedTextRun *run, u32 chars_from_start)
{
    FractionalNumber result = {};

    Assert(chars_from_start < run->num_chars);

    u32 cluster_index = run->character_to_cluster[chars_from_start];

    // Sum up the advances of all the glyphs in all the clusters before this one
    for(int i = 0;
        i < cluster_index;
        ++i)
    {
        ClusterInfo *cluster = &run->cluster[i];

        u32 first_glyph = cluster->start_glyph_index;
        u32 last_glyph = first_glyph + cluster->num_glyphs;

        for(int j = first_glyph;
            j < last_glyph;
            ++j)
        {
            GlyphInfo *glyph = &run->glyph[i];
            result += glyph->advance;
        }
    }
    return result;
}

internal FractionalNumber
GetCharacterWidth(ShapedText *shaped_text, u32 chars_from_start)
{
    FractionalNumber result = {};

    Assert(chars_from_start < shaped_text->num_chars);

    ShapedTextRun *run = GetRun(shaped_text, chars_from_start);
    result = GetCharacterWidthInShapedRun(run, chars_from_start - run->chars_before_run);

    return result;
}

internal FractionalNumber
GetCharacterX(ShapedText *shaped_text, u32 chars_from_start)
{
    FractionalNumber result = {};

    if(chars_from_start < shaped_text->num_chars)
    {
        ShapedTextRun *run = GetRun(shaped_text, chars_from_start);
        FractionalNumber width_before_run = GetWidthBeforeRun(shaped_text, run);
        FractionalNumber char_x = GetCharacterXInShapedRun(run, chars_from_start - run->chars_before_run);

        result = width_before_run + char_x;
    }
    
    return result;
}

internal FractionalNumber
GetShapedTextWidth(ShapedText *shaped_text)
{
    FractionalNumber result = {};

    // return the width of the shape text
    for(u32 i = 0;
        i < shaped_text->num_runs;
        i++)
    {
        ShapedTextRun *run = shaped_text->runs + i;
        result += run->total_width;
    }

    return result;
}


inline u32
GetNumCharsInRTLSequenceBackward(ShapedText *shaped_text, ShapedTextRun *last_run)
{
    u32 result = 0;

    Assert(last_run->RTL);

    u32 run_index = last_run - shaped_text->runs;

    for(;;)
    {
        ShapedTextRun *run = shaped_text->runs + run_index;

        if(run->RTL)
        {
            result += run->num_chars;
        }
        else
        {
            break;
        }

        if(run_index == 0)
        {
            break;
        }
        else
        {
            run_index--;
        }
    }

    return result;
}

inline u32
GetNumCharsInRTLSequence(ShapedText *shaped_text, ShapedTextRun *first_run)
{
    u32 result = 0;

    Assert(first_run->RTL);
    u32 run_index = first_run - shaped_text->runs;

    while(run_index < shaped_text->num_runs)
    {
        ShapedTextRun *run = shaped_text->runs + run_index;

        if(run->RTL)
        {
            result += run->num_chars;
        }
        else
        {
            break;
        }
                    
        run_index++;
    }

    return result;
}
