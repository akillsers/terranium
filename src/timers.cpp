#if INTERNAL
internal void
SortTimingRecords()
{
    // Bubble sort the timing records
    for(int i = 0; i < num_timing_records - 1; ++i)
    {
        for(int j = 0; j < num_timing_records - i - 1; j++)
        {
            if(timing_records[j + 1].cycles > timing_records[j].cycles)
            {
                // Swap!
                TimingRecord temp = timing_records[j + 1];
                timing_records[j + 1] = timing_records[j];
                timing_records[j] = temp;
            }
        }
    }
}

internal void 
PrintTimingRecords()
{
    platform.Info("Printing timing records\n");

    for(u32 i = 0; i < num_timing_records; ++i)
    {
        if(timing_records[i].hit_count > 0)
        {
            platform.Info("%s(%s, line %d, id %d): %I64u cycles, %d hits, %I64u cycles/hit\n", timing_records[i].func,
                          strrchr(timing_records[i].file, '\\') + 1,
                          timing_records[i].line,
                          i,
                          timing_records[i].cycles, timing_records[i].hit_count, 
                          timing_records[i].cycles/timing_records[i].hit_count);
        }
    }
}

internal void
ResetTimingRecords()
{
    for(u32 i = 0; i < num_timing_records; ++i)
    {
        timing_records[i].cycles = 0;
        timing_records[i].hit_count = 0;
    }
}
#endif