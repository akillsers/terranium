internal void
BlitBits(void *dest, u32 dest_width, u32 dest_height, u32 dest_stride_bytes, u32 x, u32 y,
         void *src, u32 src_width, u32 src_height, u32 src_stride_bytes, u32 bytes_per_pixel)
{
    u8 *destrow = ((u8 *)dest + x*bytes_per_pixel + y*dest_stride_bytes);
    u8 *srcrow = (u8 *)src;

    u32 max_blit_width = dest_width - x;
    u32 max_blit_height = dest_height - y;
    u32 blit_width = Min(max_blit_width, src_width);
    u32 blit_height = Min(max_blit_height, src_height);
    
    for(int i = 0; i < src_width; i++)
    {
        u8 *destpixel = destrow;
        u8 *srcpixel = srcrow;
        for(int j = 0; j < src_height; j++)
        {
            for(int k = 0; k < bytes_per_pixel; k++)
            {
                *destpixel++ = *srcpixel++;
            }
        }
        destrow += dest_stride_bytes;
        srcrow += src_stride_bytes;
    }
}
