internal Compositor *
CreateCompositor(MemoryArena *arena)
{
    Compositor *result;

    result = PushType(arena, Compositor);
    result->sentinel_layer = PushType(arena, CompositionLayer);
    DL_INIT_SENTINEL(result->sentinel_layer);

    return result;
}

internal CompositionLayer *
AddCompositonLayer(Compositor *compositor, i32 z_index, Framebuffer *framebuffer)
{
    CompositionLayer *insert_before = compositor->sentinel_layer;

    for(CompositionLayer *layer = compositor->sentinel_layer->next;
        layer != compositor->sentinel_layer;
        layer = layer->next)
    {
        if(layer->z_index >= z_index)
        {
            insert_before = layer;
            break;
        }
    }

    // TODO massive: we need to figure out a way to reclaim memory!
    CompositionLayer *new_layer = PushType(&compositor->arena, CompositionLayer);
    new_layer->z_index = z_index;
    new_layer->framebuffer = framebuffer;

    DL_INSERT_BEFORE(insert_before, new_layer);

    return new_layer;
}

internal void
RemoveCompositionLayer(Compositor *compositor, CompositionLayer *layer)
{
	DL_REMOVE(layer);
}

internal void
CompositeAndRender(Compositor *compositor, Framebuffer *framebuffer)
{
    //NOTE::  let's forward-iterate through the composition layers
    //        since they are sorted by increasing z_index
    for(CompositionLayer *layer = compositor->sentinel_layer->next;
        layer != compositor->sentinel_layer;
        layer = layer->next)
    {
        Recti src_rect = {};
        src_rect.x = 0;
        src_rect.y = 0;
        src_rect.width = layer->framebuffer->width;
        src_rect.height = layer->framebuffer->height;

        Recti dest = {};
        dest.x = layer->pos.x; 
        dest.y = layer->pos.y; 
        dest.height = src_rect.height;
        dest.width = src_rect.width;

        dest = ClipToFramebuffer(dest, framebuffer);
        
        u8 *src_row = (u8 *)layer->framebuffer->pixels + 0 + 0;
        u8 *dest_row = (u8 *)framebuffer->pixels +
            dest.x * framebuffer->bytespp +
            dest.y * framebuffer->stride;

        
        for(int i = 0;
            i < dest.height;
            i++)
        {
            u32 *src_pix = (u32 *) src_row;
            u32 *dest_pix = (u32 *) dest_row;

            for(int j = 0;
                j < dest.width;
                j++)
            {
                *dest_pix++ = *src_pix++;
            }
            src_row += layer->framebuffer->stride;
            dest_row += framebuffer->stride;
        }

    }
}


