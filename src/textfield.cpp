inline RenderTextTransform
GetRenderTextTransform(TextField *field)
{
    RenderTextTransform result = {};
    result.bbox = field->bbox;
    result.bbox.x = 0;
    result.bbox.y = 0;
    result.top_pixel = field->top_pixel;
    result.left_pixel = field->left_pixel;
    
    return result;
}

inline Recti
GetClipRect(TextField *field)
{
    Recti clip_rect = {};
    clip_rect.x = 0;
    clip_rect.y = 0;
    clip_rect.width = field->bbox.width;
    clip_rect.height = field->bbox.height;

    return clip_rect;
}

/*
  negative: scroll up
  positive: scroll down
*/
internal void
ScrollTextVertically(TextField *field, f32 line_delta)
{
    f32 line_height = GetLinePixelHeight(field->render_text);
    i32 pixel_delta = line_delta * line_height;

    u32 num_lines = field->render_text->buffer->num_lines;
    u32 max_to = (num_lines > 0) ? ((num_lines - 1) * line_height) : 0;

    if(pixel_delta < 0 &&
       pixel_delta * -1 > field->top_pixel_dest)
    {
        pixel_delta = field->top_pixel_dest * -1;
    }
    else if(pixel_delta > 0 && 
            max_to - field->top_pixel_dest < pixel_delta)
    {
        pixel_delta = max_to - field->top_pixel_dest;
    }

    Assert((i32)field->top_pixel_dest + pixel_delta >= 0);
    u32 new_to = field->top_pixel_dest + pixel_delta;

    if(field->top_pixel_dest != new_to)
    {
        field->top_pixel_from = field->top_pixel;
        field->top_pixel_t = 0;
        field->top_pixel_dest = new_to;
    }
}

/*
  negative: scroll leftward
  positive: scroll rightward
*/
internal void
ScrollTextHorizontally(TextField *field, f32 pixel_delta)
{
    f32 max_to = 0;

    i32 max_line_width = RoundFractional(GetGreatestLineWidth(field->render_text, &platform.frame_arena)).integer_part;
    if(max_line_width > field->bbox.width)
    {
        max_to = max_line_width - field->bbox.width;
    }
    
    f32 new_to = field->left_pixel_dest + pixel_delta;
    if(new_to < 0)
    {
        new_to = 0;
    }
    else if(new_to >= max_to)
    {
        new_to = max_to;
    }

    if(field->left_pixel_dest != new_to)
    {
        field->left_pixel_from = field->left_pixel;
        field->left_pixel_t = 0;
        field->left_pixel_dest = new_to;
    }
}

inline i32
GetMaxLeftPixel(TextField *field)
{
    i32 result;
    i32 max_text_horizontal_pixels = RoundFractional(GetGreatestLineWidth(field->render_text, &platform.frame_arena)).integer_part;

    if(field->bbox.width > max_text_horizontal_pixels)
    {
        // not scrollable horizontally
        result = 0; 
    }
    else
    {
        result = max_text_horizontal_pixels - field->bbox.width;
    }
    return result;
}

internal void
UpdateVerticalScrollbarThumb(TextField *field)
{
    FontMetrics *metrics = field->render_text->font->metrics;
    f32 visible_lines = GetNumVisibleLines(field->render_text, GetRenderTextTransform(field));

    f32 end_buffer_lines = visible_lines - 1;
    f32 end_buffer_pixels = end_buffer_lines * Round(metrics->y_advance);
    f32 file_pixels = field->render_text->buffer->num_lines * Round(metrics->y_advance);
    f32 total_vertical_pixels = file_pixels + end_buffer_pixels;

    f32 y_percent = field->line / field->render_text->buffer->num_lines;

    // for this can we just use field->bbox.height
    f32 visible_pixels = (visible_lines) * Round(metrics->y_advance);
    f32 preferred_height_percent =  (f32)visible_pixels / total_vertical_pixels;

    Scrollbar *scrollbar = &field->vertical_scrollbar;
    SetScrollbarThumb(scrollbar, y_percent, preferred_height_percent);
}

internal void
UpdateHorizontalScrollbarThumb(TextField *field)
{
    i32 total_horizontal_pixels = RoundFractional(GetGreatestLineWidth(field->render_text, &platform.frame_arena)).integer_part;
    f32 max_left_pixel_pixel_offset = GetMaxLeftPixel(field);
        
    f32 current_left_pixel_pixel_offset = field->left_pixel;

    f32 x_percent;
    if(max_left_pixel_pixel_offset)
    {
        x_percent = (f32)current_left_pixel_pixel_offset / max_left_pixel_pixel_offset;
    }
    else 
    {
        x_percent = 0;
    }
        
    f32 preferred_width_percent = field->bbox.width / total_horizontal_pixels;
    if (preferred_width_percent > 1.0f)
    {
        preferred_width_percent = 1.0f;
    }
    Scrollbar *scrollbar = &field->horizontal_scrollbar;
    SetScrollbarThumb(scrollbar, x_percent, preferred_width_percent);
}    
internal void
PositionVerticalScrollbar(TextField *field)
{
    if(field->showing_vertical_scrollbar)
    {
        field->bbox.width = field->bbox.width - SCROLLBAR_THICKNESS - FIELD_LEFT_MARGIN;

        Recti pseudo_text_rect = field->bbox;
        pseudo_text_rect.y = field->bbox.y;
        pseudo_text_rect.height = field->bbox.height;

        PositionScrollbar(&field->vertical_scrollbar, pseudo_text_rect);
        
        UpdateVerticalScrollbarThumb(field);
    }
    else
    {
        field->bbox.width = field->bbox.width;
    }
}
internal void
PositionHorizontalScrollbar(TextField *field)
{
    if(field->showing_horizontal_scrollbar)
    {
        field->bbox.height = field->bbox.height - SCROLLBAR_THICKNESS - FIELD_TOP_MARGIN;

        Recti pseudo_text_rect = field->bbox;
        pseudo_text_rect.x = field->bbox.x;
        pseudo_text_rect.width = field->bbox.width - SCROLLBAR_THICKNESS;
        PositionScrollbar(&field->horizontal_scrollbar, pseudo_text_rect);

        UpdateHorizontalScrollbarThumb(field);
    }
    else
    {
        field->bbox.height = field->bbox.height;
    }
}

inline FractionalNumber
GetCharacterWidth(TextField *field, Cursor pos_in_line, u32 line, ShapedText *shaped_line = 0)
{
    TIMED_BLOCK;
    FractionalNumber result = EmWidth(field->render_text->font);

    if(shaped_line == 0)
    {
        shaped_line = GetShapedLine(field->render_text, line, &platform.frame_arena);
    }
    
    ShapedTextRun *run = GetRun(shaped_line, pos_in_line); // 144 cy
    if(run)
    {
        u32 pos_in_run = pos_in_line - run->chars_before_run;
        u32 one_past_cluster_index = run->character_to_cluster[pos_in_run];

        result = GetClusterWidth(run, one_past_cluster_index); // 332 cy
    }
    
    return result;
}

internal u32
GetColumnFromTextRelativeX(TextField *field, u32 line, i32 text_relative_x)
{
    // results in the behaviour of RenderText GetColumnFromX
    // except that the last line has a valid one past last char spot
    
    u32 result;
    
    ShapedText *shaped_line = GetShapedLine(field->render_text, line, &platform.frame_arena);
    FractionalNumber max_x = GetLineWidth(shaped_line);
    u32 max_column = GetOnePastLastLineChar(field->render_text->buffer, line) - LinePos(field->render_text->buffer, line);
    
    if(text_relative_x < 0)
    {
        result = 0;
    }
    else if(text_relative_x >= RoundFractional(max_x).integer_part)
    {
        result = max_column;
    }
    else
    {
        u32 one_past_column = TextRelativeXToOnePastColumn(shaped_line, text_relative_x);
        Assert(one_past_column);

        result = Min(one_past_column - 1, max_column);
    }

    return result;
}

internal Recti
GetCharacterRectangle(TextField *field, u32 cursor, u32 line, MemoryArena *temp_arena, ShapedText *shaped_line = 0)
{
    Recti result = {};
    
    RenderText *render_text = field->render_text;
    if(!shaped_line)
    {
        shaped_line = GetShapedLine(render_text, line, temp_arena);
    }

    Cursor line_start = LinePos(render_text->buffer, line);

    u32 x1;
    u32 x2;

    if(cursor < field->render_text->buffer->total_characters)
    {
        Cursor char_pos_in_line = cursor - line_start;
        FractionalNumber char_x = GetCharacterX(shaped_line, char_pos_in_line);
        FractionalNumber char_width = GetCharacterWidth(shaped_line, char_pos_in_line);

        x1 = RoundFractional(char_x).integer_part;
        x2 = RoundFractional(char_x + char_width).integer_part;
    }
    else
    {
        FractionalNumber shaped_text_width = GetShapedTextWidth(shaped_line);
        FractionalNumber em = EmWidth(field->render_text->font);
        
        x1 = RoundFractional(shaped_text_width).integer_part;
        x2 = RoundFractional(shaped_text_width + em).integer_part;
    }

    // TODO: Consider making render text not take any transform and return untransformed values?
    RenderTextTransform transform = GetRenderTextTransform(field);

    i32 y1 = GetLineY(render_text, line, transform);
    i32 y2 = GetLineY(field->render_text, line + 1, transform);

    // TODO remove
    // Cursor one_past_last_line_char = GetOnePastLastLineChar(field->render_text->buffer, line);
    // (cursor >= one_past_last_line_char) ? RoundFractional(GetCharacterWidth(field, cursor - line_start, line, shaped_line)).integer_part : x2 - x1;

    result.x = x1;
    result.y = y1;
    result.width = x2 - x1;
    result.height = y2 - y1;

    return result;
}

inline Recti
GetLineRect(TextField *field, u32 line, MemoryArena *temp_arena)
{
    Recti result = {};

    ShapedText *shaped_line = GetShapedLine(field->render_text, line, temp_arena);
    RenderTextTransform transform = GetRenderTextTransform(field); 
    
    result.x = field->bbox.x;
    result.y = GetLineY(field->render_text, line, transform);
    result.width = RoundFractional(GetLineWidth(shaped_line) + EmWidth(field->render_text->font)).integer_part;
    result.height = GetLineY(field->render_text, line + 1, transform) - result.y;

    return result;
}

internal void
DrawSelection(TextField *field)
{
    if(field->mark_activated)
    {
        u32 min_line = Min(field->mark_line, field->line);
        u32 max_line = Max(field->mark_line, field->line);

        u32 min_cursor = Min(field->mark, field->cursor);
        u32 max_cursor = Max(field->mark, field->cursor);
        
        u32 min_char_pos_on_line = min_cursor - LinePos(field->render_text->buffer, min_line);
        u32 max_char_pos_on_line = max_cursor - LinePos(field->render_text->buffer, max_line);
        
        RenderTextTransform transform = GetRenderTextTransform(field);
        
        // all the lines between the start and end runs
        // should be completely filled in
        u32 first_draw_line = Max(min_line + 1, field->top_pixel);
        u32 last_draw_line = Min(max_line, field->top_pixel + GetNumVisibleLines(field->render_text, transform) + 1);
        for(u32 line = first_draw_line;
            line < last_draw_line;
            line++)
        {
            Recti line_rect = {};
            line_rect = GetLineRect(field, line, &platform.frame_arena);
    
            DrawRectangle(line_rect, CLR_TEXT_SELECTION_REGION, field->framebuffer, GetClipRect(field));
        }

        ShapedText *min_shaped_line = GetShapedLine(field->render_text, min_line, &platform.frame_arena);
        ShapedText *max_shaped_line = GetShapedLine(field->render_text, max_line, &platform.frame_arena);

        ShapedTextRun *min_run = GetRun(min_shaped_line, min_char_pos_on_line);
        ShapedTextRun *max_run = GetRun(max_shaped_line, max_char_pos_on_line);
        u32 min_run_index = min_run - min_shaped_line->runs;
        u32 max_run_index = (max_run) ? max_run - max_shaped_line->runs : max_shaped_line->num_runs;

        
        // fill in all the runs after the min cursor run to the end of the line
        // or the max cursor run, whichever is less
        u32 max_fill_run_index = (min_line == max_line && max_run) ? max_run_index : min_shaped_line->num_runs;

        for(u32 i = min_run_index + 1;
            i < max_fill_run_index;
            i++)
        {
            ShapedTextRun *run = min_shaped_line->runs + i;

            i32 max_x = field->bbox.x + RoundFractional(GetWidthBeforeRun(min_shaped_line, run) + run->total_width).integer_part;
            
            Recti run_rect = {};
            run_rect.x = field->bbox.x + RoundFractional(GetWidthBeforeRun(min_shaped_line, run)).integer_part;
            run_rect.y = GetLineY(field->render_text, min_line, transform);
            run_rect.width = max_x - run_rect.x;
            run_rect.height = GetLineY(field->render_text, min_line + 1, transform) - run_rect.y;

            DrawRectangle(run_rect, CLR_TEXT_SELECTION_REGION, field->framebuffer);
        }
        
        // if the max cursor run is on another line,
        // fill in all the runs logcically before the max cursor run
        if(min_line != max_line)
        {
            // we are selecting two different lines,
            // then the end newline characters on the min line
            // is completely selected, so show that
            f32 min_line_pixel_width = (field->render_text, min_line);

            Recti newline_chars_rect = {};
            newline_chars_rect.x = field->bbox.x + min_line_pixel_width;
            newline_chars_rect.y = GetLineY(field->render_text, min_line, transform);
            newline_chars_rect.width = RoundFractional(EmWidth(field->render_text->font)).integer_part;
            newline_chars_rect.height = GetLineY(field->render_text, min_line + 1, transform) - newline_chars_rect.y;

            DrawRectangle(newline_chars_rect, CLR_TEXT_SELECTION_REGION, field->framebuffer, GetClipRect(field));

            for(u32 i = 0;
                i < max_run_index;
                i++)
            {
                ShapedTextRun *run = max_shaped_line->runs + i;

                i32 max_x = field->bbox.x + RoundFractional(GetWidthBeforeRun(max_shaped_line, run) + run->total_width).integer_part;
                
                Recti run_rect = {};
                run_rect.x = field->bbox.x + RoundFractional(GetWidthBeforeRun(max_shaped_line, run)).integer_part;
                run_rect.y = GetLineY(field->render_text, max_line, transform);
                run_rect.width = max_x - run_rect.x;
                run_rect.height = GetLineY(field->render_text, max_line + 1, transform) - run_rect.y;

                DrawRectangle(run_rect, CLR_TEXT_SELECTION_REGION, field->framebuffer, GetClipRect(field));
            }
        }
        
        //draw the min cursor run
        {
            FractionalNumber width_before_run = GetWidthBeforeRun(min_shaped_line, min_run);

            u32 highest_run_char;
            if(max_line == min_line)
            {
                highest_run_char = Min(max_char_pos_on_line - min_run->chars_before_run, min_run->num_chars - 1);
            }
            else
            {
                highest_run_char = min_run->num_chars - 1;
            }

            FractionalNumber x1;
            u32 min_cursor_in_run = min_char_pos_on_line - min_run->chars_before_run;
            if(min_cursor_in_run == 0 && min_run->RTL)
            {
                x1 = min_run->total_width;
            }
            else
            {
                x1 = GetCharacterXInShapedRun(min_run, min_char_pos_on_line - min_run->chars_before_run);
            }
            
            FractionalNumber x2 = GetCharacterXInShapedRun(min_run, highest_run_char);

            if(!min_run->RTL)
            {
                // enclose the highest character
                x2 += GetCharacterWidth(field, min_run->chars_before_run + highest_run_char, min_line);
            }
            
            FractionalNumber min_x = Min(x1, x2);
            FractionalNumber max_x = Max(x1, x2);
            
            Recti min_run_rect = {};
            min_run_rect.x = field->bbox.x + RoundFractional(min_x + width_before_run).integer_part;
            min_run_rect.y = GetLineY(field->render_text, min_line, transform);
            min_run_rect.width = field->bbox.x + RoundFractional(max_x + width_before_run).integer_part - min_run_rect.x;
            min_run_rect.height = GetLineY(field->render_text, min_line + 1, transform) - min_run_rect.y;

            DrawRectangle(min_run_rect, CLR_TEXT_SELECTION_REGION, field->framebuffer, GetClipRect(field));
        }

        // draw the max cursor run
        if(max_run &&
           min_run != max_run)
        {
            FractionalNumber width_before_run = GetWidthBeforeRun(max_shaped_line, max_run);

            FractionalNumber x1 = GetCharacterXInShapedRun(max_run, max_char_pos_on_line - max_run->chars_before_run);
            FractionalNumber x2 = max_run->RTL ? max_run->total_width : FractionalZero();

            if(!max_run->RTL)
            {
                // enclose the highest character
                x1 += GetCharacterWidth(field, max_char_pos_on_line, max_line);
            }
            
            FractionalNumber min_x = Min(x1, x2);
            FractionalNumber max_x = Max(x1, x2);
            
            Recti max_run_rect = {};
            max_run_rect.x = field->bbox.x + RoundFractional(min_x + width_before_run).integer_part;
            max_run_rect.y = GetLineY(field->render_text, max_line, transform);
            max_run_rect.width = field->bbox.x + RoundFractional(max_x + width_before_run).integer_part - max_run_rect.x;
            max_run_rect.height = GetLineY(field->render_text, max_line + 1, transform) - max_run_rect.y;;

            DrawRectangle(max_run_rect, CLR_TEXT_SELECTION_REGION, field->framebuffer, GetClipRect(field));
        }
    }
}
#if INTERNAL
internal DebugDrawCount *
GetDebugDrawCount(TextField *field, Cursor char_pos)
{
    DebugDrawCount *result = 0;
    for(u32 i = 0; i < field->num_draw_count; i++)
    {
        if(field->draw_count[i].char_pos == char_pos)
        {
            result = field->draw_count + i;
            break;
        }
    }
    
    return result;
}
#endif


internal void
RedrawTextFieldCharacter(TextField *field, u32 char_pos, u32 line, MemoryArena *temp_arena,
                         Recti *char_rect = 0, ShapedText *shaped_line = 0, b32 *should_render_glyph = 0)
{
    TIMED_BLOCK;

#if INTERNAL
#define SHOW_TEXTFIELD_REDRAW 1
#if SHOW_TEXTFIELD_REDRAW == 0
    field->show_redraw_visual = false;
#endif
    if(field->show_redraw_visual)
    {
        field->redraw_occurred = true;
    }
#endif

    RenderTextTransform transform = GetRenderTextTransform(field);

    if(char_rect == 0)
    {
        TIMED_BLOCK;
        char_rect = PushType(temp_arena, Recti);
        *char_rect = GetCharacterRectangle(field, char_pos, line, temp_arena);
    }
    
    u32 min_marked_char = Min(field->cursor, field->mark);
    u32 max_marked_char = Max(field->cursor, field->mark);

#if INTERNAL
    DebugDrawCount *draw_count = GetDebugDrawCount(field, char_pos);
#endif

    Color color = field->back_color;

    if(field->mark_activated &&
       char_pos >= min_marked_char &&
       char_pos <= max_marked_char)
    {
        color = CLR_TEXT_SELECTION_REGION;

#if INTERNAL
#define DebugGetColor(color_line, color_normal)  \
        color = !field->show_redraw_visual ? color : \
        (draw_count && draw_count->count >= 1) ? \
            CLR_YELLOW : (field->redraw_visual_line ? color_line : color_normal);
        
        DebugGetColor(rgb(0.2f, 0.7f, 0.2f), rgb(0.8f, 0.2f, 0.2f));
#endif
    }
    else if(char_pos == field->cursor)
    {
        color = field->is_active ? CLR_FOCUSED_POINT_BACKGROUND : CLR_POINT_BACKGROUND;
        
#if INTERNAL
        DebugGetColor(rgb(0.2f, 0.7f, 0.2f), rgb(0.8f, 0.2f, 0.2f));
#endif
    }
    else
    {
#if INTERNAL
        DebugGetColor(CLR_GREEN, CLR_RED);
#endif
        
    }

    {
        TIMED_BLOCK;
        DrawRectangle(*char_rect, color, field->framebuffer, GetClipRect(field));
    }

    Cursor one_past_last_char;

    b32 should_render;
    if(should_render_glyph)
    {
        should_render = *should_render_glyph;
    }
    else
    {
        UTF32 codepoint = GetCodepoint(field->render_text->buffer, char_pos);
        should_render = (codepoint != '\n' && codepoint != '\r') && char_pos < field->render_text->buffer->total_characters;
    }
    
    if(should_render)
    {
        DrawRenderTextCharacter_(field->render_text, char_pos, line, transform, temp_arena, field->framebuffer, shaped_line);
    }

#if INTERNAL
    if(field->show_redraw_visual)
    {
        if(draw_count)
        {
            draw_count->count++;
        }
        else
        {
            Assert(field->num_draw_count < MAX_DEBUG_DRAW_COUNT);
            field->draw_count[field->num_draw_count].char_pos = char_pos;
            field->draw_count[field->num_draw_count].count = 1;
            
            field->num_draw_count++;
        }
    }
#endif
}

internal void
RedrawTextFieldLine(TextField *field, u32 line, MemoryArena *temp_arena)
{
    TIMED_BLOCK;

    // TODO: optimization: don't always start at the 0th character in the line
    Cursor first_line_char = LinePos(field->render_text->buffer, line);
    Cursor start = first_line_char;
    Cursor last_valid_caret_pos = GetOnePastLastLineChar(field->render_text->buffer, line);

    Cursor last_buffer_char = last_valid_caret_pos < field->render_text->buffer->total_characters ?
        last_valid_caret_pos : field->render_text->buffer->total_characters - 1;


#if INTERNAL
    field->redraw_visual_line = false;
#endif


    ShapedText *shaped_line = GetShapedLine(field->render_text, line, temp_arena);
#if 1
    
    FractionalNumber char_x = {};
    if(start <= last_buffer_char)
    {
        // There are characters to be rendered on this line
        char_x = GetCharacterX(shaped_line, start - first_line_char);
    }

    RenderTextTransform transform = GetRenderTextTransform(field);
    i32 y1 = GetLineY(field->render_text, line, transform);
    i32 y2 = GetLineY(field->render_text, line + 1, transform);

    Recti char_rect = {};
    char_rect.y = y1;
    char_rect.height = y2 - y1;
    
    for(Cursor cursor = start;
        cursor <= last_buffer_char;
        cursor++)
    {
        TIMED_BLOCK;
        // FractionalNumber char_width = GetCharacterWidth(field, cursor - first_line_char, line, shaped_line);
        // i32 next_char_x = RoundFractional(char_x + char_width).integer_part;

        // char_rect.x = RoundFractional(char_x).integer_part - field->left_pixel + field->bbox.x;
        // char_rect.width = next_char_x - char_rect.x;
        
        // TODO: fix this
        //       plus, try to think about optimization: finding the range of characters that will be in frame
        Recti standard_rect = GetCharacterRectangle(field, cursor, line, temp_arena, shaped_line);
#if 0
        Assert(char_rect.x == standard_rect.x);
        Assert(char_rect.width == standard_rect.width);
        Assert(char_rect.y == standard_rect.y);
        Assert(char_rect.height == standard_rect.height);
#endif
        if(standard_rect.x > field->bbox.x + field->bbox.width)
        {
            break;
        }

        b32 should_render_glyph = cursor < last_valid_caret_pos;
        RedrawTextFieldCharacter(field, cursor, line, temp_arena, &standard_rect, shaped_line, &should_render_glyph);

        // char_x += char_width;
    }

    if(last_buffer_char != last_valid_caret_pos)
    {
        // Draw the last valid caret pos (one past the max number of chars in the buffer)
        char_rect = GetCharacterRectangle(field, last_valid_caret_pos, line, temp_arena);
        b32 should_render_glyph = false;
        RedrawTextFieldCharacter(field, last_valid_caret_pos, line, temp_arena, &char_rect, shaped_line, &should_render_glyph);
    }
#if INTERNAL
    field->redraw_visual_line = false;
#endif
#endif
}

struct RedrawLineWorkData
{
    TextField *field;
    u32 line;
};

internal void
DoRedrawLineWork(void *param, MemoryArena *thread_arena)
{
    TIMED_BLOCK;

    RedrawLineWorkData *redraw_data = (RedrawLineWorkData *)param;
    
    //char buf[1024];
    //sprintf(buf, "RedrawIng line %i\n", redraw_data->line);
    //platform.Info(buf);

    RedrawTextFieldLine(redraw_data->field, redraw_data->line, thread_arena);
}


internal void
RedrawTextField(TextField *field)
{
    TIMED_BLOCK;
    if(field->render_text)
    {
        RenderTextTransform transform = GetRenderTextTransform(field);
        u32 max_visible_lines = GetNumVisibleLines(field->render_text, transform);

        u32 line_height = GetLinePixelHeight(field->render_text);
        u32 first_line = (u32)transform.top_pixel / line_height;
        u32 one_past_max_draw_line = Min(first_line + max_visible_lines + 1, field->render_text->buffer->num_lines);

        {
            TIMED_BLOCK;
            
            // Get full 16-byte alignment for maximum efficiency
            Recti bbox = transform.bbox;
            bbox.width = field->framebuffer->width;

            DrawRectangle(bbox, field->back_color, field->framebuffer);
        }

#if 1
        for(u32 line = first_line;
            line < one_past_max_draw_line;
            line++)
        {
            RedrawTextFieldLine(field, line, &platform.frame_arena);
        }
#else
        EnableTicketLock(&field->render_text->ticket_lock);
        
        PlatformWorkQueue *work_queue = PushType(&platform.frame_arena, PlatformWorkQueue);

        u32 num_lines = one_past_max_draw_line - first_line;
        PlatformWorkEntry *job = PushCount(&platform.frame_arena, PlatformWorkEntry, num_lines);
        RedrawLineWorkData *data = PushCount(&platform.frame_arena, RedrawLineWorkData, num_lines);
        for(u32 i = 0;
            i < num_lines;
            i++)
        {
            data[i].field = field;
            data[i].line = first_line + i;
            
            job[i].function = DoRedrawLineWork;
            job[i].data = &data[i];

            platform.AddWorkQueueEntry(work_queue, &job[i]);
        }
        platform.CompleteAllWork(work_queue);

        DisableTicketLock(&field->render_text->ticket_lock);
#endif
    }

}

internal void
RedrawRange(TextField *field, CursorRange range)
{
    MemoryArena *temp_arena = &platform.frame_arena;
    
	u32 start_line = GetLineContainingCharPos(field->render_text->buffer, range.min);
	u32 end_line = GetLineContainingCharPos(field->render_text->buffer, range.max);
	
	Cursor at = range.min;
	for(u32 line = start_line; line <= end_line; line++)
	{
		Cursor last_line_char = GetRawLastLineChar(field->render_text->buffer, line);
		ShapedText *shaped_line = GetShapedLine(field->render_text, line, temp_arena);
		
		Cursor last = Min(range.max, last_line_char);
		
		for(;at <= last; at++)
		{
			// TODO: This is repeated code perhaps we should move this into 
			//       RedrawTextFieldCharacter
			Recti char_rect = GetCharacterRectangle(field, at, line, temp_arena);
			if(char_rect.x > field->bbox.x + field->bbox.width)
			{
				at = last + 1;
				break;
			}
            
            RedrawTextFieldCharacter(field, at, line, temp_arena, &char_rect, shaped_line);
		}
	}
}

internal void
SetMark(TextField *field)
{
    field->mark = field->cursor;
    field->mark_line = field->line;
}

internal void
StartSelectionIfNoneExist(TextField *field)
{
    if(!field->mark_activated)
    {
        SetMark(field);
        field->mark_activated = true;
    }
}

internal void
DefocusTextfield(void *param)
{
    TextField *field = (TextField *) param;

    field->is_active = false;
    RedrawTextFieldCharacter(field, field->cursor, field->line, &platform.frame_arena);
}

internal f32
GetTopLine(TextField *field)
{
    f32 result = 0.0f;

    f32 line_height = GetLinePixelHeight(field->render_text);
    result = field->top_pixel_dest / line_height;

    return result;
}

internal b32
ScrollFieldToCharPos(TextField *field, Cursor char_pos, u32 line)
{
    b32 scrolled = false;
    f32 field_visible_lines = GetNumVisibleLines(field->render_text, GetRenderTextTransform(field));
    f32 field_top_line = GetTopLine(field);

    if(field_top_line > line)
    {
        // we need to scroll upward to the curr line index
        ScrollTextVertically(field, line - field_top_line);
        scrolled = true;
    }
    else if(field_top_line + field_visible_lines < line + 1)
    {
        // we need to scroll downward to the curr line index
        FontMetrics *metrics = field->render_text->font->metrics;
        f32 delta_to_top_of_field = line - field_top_line;
        f32 delta_to_after_field = delta_to_top_of_field - field_visible_lines;
        f32 delta = delta_to_after_field + 1.0f;
        ScrollTextVertically(field, delta);
        scrolled = true;
    }

    RenderTextTransform transform = {};

   // Vec2i char_position = GetCharacterPixelPosition(field->render_text, field->cursor, field->line, transform);
    Recti char_rect = GetCharacterRectangle(field, field->cursor, field->line, &platform.frame_arena);


    u32 line_start = LinePos(field->render_text->buffer, line);
    f32 field_right_side = field->left_pixel_dest + field->bbox.width;

    if(char_rect.x < field->left_pixel_dest)
    {
        f32 delta = char_rect.x - field->left_pixel_dest;
        ScrollTextHorizontally(field, delta);
        scrolled = true;
    }
    else if(char_rect.x + char_rect.width > field_right_side)
    {
        f32 delta = char_rect.x + char_rect.width - field_right_side;
        ScrollTextHorizontally(field, delta);
        scrolled = true;
    }

    u32 column = GetCursorColumn(field->render_text, field->line, field->cursor);

    u32 line_height = GetLinePixelHeight(field->render_text);
    b32 scrolling = (line * line_height < field->top_pixel ||
                     line * line_height >= field->top_pixel + field->bbox.height ||
                     char_rect.x + char_rect.width < (i32)field->left_pixel ||
                     char_rect.x + char_rect.width >= field->left_pixel + field->bbox.width);

    return scrolled || scrolling;
}

inline b32
ScrollFieldToCursor(TextField *field)
{
    return ScrollFieldToCharPos(field, field->cursor, field->line);
}

inline void
MoveToVisualEndOfRun(TextField *field, ShapedText *shaped_text, ShapedTextRun *run)
{
    if(run->RTL)
    {
        field->cursor -= GetNumCharsInRTLSequenceBackward(shaped_text, run);
    }
    else
    {
        field->cursor--;
    }
}

internal void
MoveCursorToPreviousCharacter(TextField *field)
{
    ShapedText *shaped_text = GetShapedLine(field->render_text, field->line, &platform.frame_arena);
    
    u32 char_in_line = field->cursor - LinePos(field->render_text->buffer, field->line);
    ShapedTextRun *cursor_run = GetRun(shaped_text, char_in_line);

    if(!cursor_run)
    {
        if(shaped_text->num_runs > 0)
        {
            ShapedTextRun *last_run = shaped_text->runs + shaped_text->num_runs - 1;
            MoveToVisualEndOfRun(field, shaped_text, last_run);
        }
        else
        {
            if (field->line > 0)
            {
                field->line--;
                field->cursor = GetOnePastLastLineChar(field->render_text->buffer, field->line);
            }
        }
    }
    else if(cursor_run->RTL)
    {
        Cursor cursor_pos_in_line = field->cursor - LinePos(field->render_text->buffer, field->line);
        if(cursor_pos_in_line == cursor_run->chars_before_run + cursor_run->num_chars - 1)
        {
            ShapedTextRun *next_logical_run = 0;

            u32 run_index = cursor_run - shaped_text->runs;
            if(run_index + 1 < shaped_text->num_runs)
            {
                next_logical_run = shaped_text->runs + run_index + 1;
            }

            if(next_logical_run && next_logical_run->RTL)
            {
                field->cursor++;
            }
            else
            {
                u32 backward_chars = GetNumCharsInRTLSequenceBackward(shaped_text, cursor_run);

               if(backward_chars <= cursor_pos_in_line)
                {
                    field->cursor -= backward_chars;
                }
                else
                {
                    if(field->line > 0)
                    {
                        field->line--;
                        field->cursor = GetOnePastLastLineChar(field->render_text->buffer, field->line);
                    }
                }
            }
        }
        else
        {
            field->cursor++;
        }

    }
    else
    {
        Cursor cursor_pos_in_line = field->cursor - LinePos(field->render_text->buffer, field->line);
        if(cursor_pos_in_line == cursor_run->chars_before_run)
        {
            u32 run_index = cursor_run - shaped_text->runs;
            if(run_index > 0)
            {
                ShapedTextRun *previous_run = shaped_text->runs + run_index - 1;
                MoveToVisualEndOfRun(field, shaped_text, previous_run);
            }
            else
            {
                if(field->line > 0)
                {
                    field->line--;
                    field->cursor = GetOnePastLastLineChar(field->render_text->buffer, field->line);
                }
            }
        }
        else
        {
            field->cursor--;
        }
    }
    // }


    // TODO is there any way to set field->saved_cursor_x without setting it everywhere?
    //     we might forget if we manually change field->cursor
    field->one_past_saved_cursor_x = 0;
}

internal void
MoveCursorToNextCharacter(TextField *field)
{
    // TODO: Apply Unicode grapheme boundary rules
    u32 one_past_last_pos = GetOnePastLastLineChar(field->render_text->buffer, field->line);
    if(field->cursor == one_past_last_pos)
    {
        if(field->line + 1 < field->render_text->buffer->num_lines)
        {
            field->line++;
            field->cursor = LinePos(field->render_text->buffer, field->line);
        } 
    }
    else
    {
        ShapedText *shaped_text = GetShapedLine(field->render_text, field->line, &platform.frame_arena);

        u32 char_in_line = field->cursor - LinePos(field->render_text->buffer, field->line);
        ShapedTextRun *cursor_run = GetRun(shaped_text, char_in_line);
        ShapedTextRun *next_run = GetRun(shaped_text, char_in_line + 1);

        if(cursor_run->RTL)
        {
            if(char_in_line == cursor_run->chars_before_run)
            {
                ShapedTextRun *previous_run = 0;

                u32 run_index = cursor_run - shaped_text->runs;
                if(run_index > 0)
                {
                    previous_run = shaped_text->runs + run_index - 1;
                }

                if(previous_run && previous_run->RTL)
                {
                    field->cursor--;
                }
                else
                {
                    field->cursor += GetNumCharsInRTLSequence(shaped_text, cursor_run);
                }
            }
            else
            {
                field->cursor--;
            }
        }
        else
        {
            if(next_run && next_run->RTL)
            {
                field->cursor += GetNumCharsInRTLSequence(shaped_text, next_run);
            }
            else
            {
                field->cursor++;
            }
        }
    }

    field->one_past_saved_cursor_x = 0;
}

internal void
SaveCursorXIfNotSaved(TextField *field)
{
    if(field->one_past_saved_cursor_x == 0)
    {
        ShapedText *shaped_line = GetShapedLine(field->render_text, field->line, &platform.frame_arena);

        u32 char_pos_in_line = field->cursor - LinePos(field->render_text->buffer, field->line);
        FractionalNumber cursor_char_x = GetCharacterX(shaped_line, char_pos_in_line);

        field->one_past_saved_cursor_x = RoundFractional(cursor_char_x).integer_part + 1;
    }
}

internal void
MoveCursorToLine(TextField *field, u32 line)
{
    SaveCursorXIfNotSaved(field);
    Assert(field->one_past_saved_cursor_x);

    field->line = line;

    u32 column = GetColumnFromTextRelativeX(field, line, field->one_past_saved_cursor_x - 1);
    field->cursor = LinePos(field->render_text->buffer, line) + column;
}

internal void
JumpToPreviousBlankLine(TextField *field)
{
    if(field->line > 0)
    {
        u32 line = field->line - 1;
        for(;;)
        {
            if(LineIsBlank(field->render_text->buffer, line))
            {
                MoveCursorToLine(field, line);
                ScrollFieldToCursor(field);

                break;
            }

            if (line == 0)
            {
                MoveCursorToLine(field, line);
                break;
            }
            line--;
        }
    }

}

internal void
JumpToNextBlankLine(TextField *field)
{
    u32 line = field->line + 1;
    for(;;)
    {
        if(line >= field->render_text->buffer->num_lines)
        {
            MoveCursorToLine(field, line - 1);
            break;
        }

        if (LineIsBlank(field->render_text->buffer, line))
        {
            MoveCursorToLine(field, line);

            break;
        }

        if (field->line == field->render_text->buffer->num_lines - 1)
        {
            MoveCursorToLine(field, line);

            break;
        }

        line++;
    }
}

internal void
MoveCursorToLineStart(TextField *field)
{
    field->mark_activated = false;

    field->cursor = LinePos(field->render_text->buffer, field->line);
    field->one_past_saved_cursor_x = 0;
}

internal void
MoveCursorToLineEnd(TextField *field)
{
    field->mark_activated = false;

    if(field->line + 1 < field->render_text->buffer->num_lines)
    {
        field->cursor = GetLastLineChar(field->render_text->buffer, field->line) + 1;
    }
    else
    {
        field->cursor = field->render_text->buffer->total_characters;
    }
    
    field->one_past_saved_cursor_x = 0;
}

internal void
MoveCursorUp(TextField *field)
{
    if(field->line > 0)
    {
        MoveCursorToLine(field, field->line - 1);
    }
}

internal void
MoveCursorDown(TextField *field)
{
    if(field->line + 1 < field->render_text->buffer->num_lines)
    {
        MoveCursorToLine(field, field->line + 1);
    }
}

internal void
MoveCursorToPreviousWord(TextField *field)
{
    if(field->cursor == LinePos(field->render_text->buffer, field->line))
    {
        if(field->line > 0)
        {
            field->line--;
            field->cursor = GetOnePastLastLineChar(field->render_text->buffer, field->line);
        }
    }
    else
    {
        u32 first_char = LinePos(field->render_text->buffer, field->line);
        field->cursor--;
        while(field->cursor > first_char)
        {
            UTF32 codepoint = GetCodepoint(field->render_text->buffer, field->cursor);
            if(IsAlphanumeric(codepoint)) {
                if(field->cursor > first_char)
                {
                    Cursor cursor_prev = field->cursor - 1;
                    while(cursor_prev >= first_char) {
                        codepoint = GetCodepoint(field->render_text->buffer, cursor_prev);
                        if(!IsAlphanumeric(codepoint)) {
                            field->cursor = cursor_prev + 1;
                            break;
                        }
                        if(cursor_prev > 0)
                        {
                            cursor_prev--;
                        }
                        else
                        {
                            field->cursor = 0;
                            return;
                        }
                    }
                    field->cursor = cursor_prev + 1;
                    break;
                    
                }
                else
                {
                    // field->cursor is now at 0
                    break;
                }
            }

            field->cursor--;
        }
    }
    
    field->one_past_saved_cursor_x = 0;
}

internal b32
IsWordBreakPoint(UTF32 codepoint1, UTF32 codepoint2)
{
    // See https://unicode.org/reports/tr29/#Word_Boundary_Rules

    return true;
}

internal void
MoveCursorToNextWord(TextField *field)    
{
    u32 last_char_on_line = GetOnePastLastLineChar(field->render_text->buffer, field->line);
    if(field->cursor >= last_char_on_line)
    {
        // TODO: does this ever occur?
        // we are the last character on the line
        if(field->line + 1 < field->render_text->buffer->num_lines)
        {
            field->line++;
            field->cursor = LinePos(field->render_text->buffer, field->line);
        }
    }
    else
    {
        field->cursor++;
        while(field->cursor < last_char_on_line)
        {
            UTF32 codepoint = GetCodepoint(field->render_text->buffer, field->cursor);
            UTF32 next_codepoint = GetCodepoint(field->render_text->buffer, field->cursor);
            if(IsWordBreakPoint(codepoint, next_codepoint))
            {
                break;
            }
#if 0
            if(IsAlphanumeric(codepoint))
            {
                do
                {
                    if(!IsAlphanumeric(codepoint))
                    {
                        break;
                    }
                    field->cursor++;
                    codepoint = GetCodepoint(field->render_text->buffer, field->cursor);
                } while(field->cursor < last_char_on_line);
                break;
            }
#endif
            field->cursor++;
        }
    }
    
    field->one_past_saved_cursor_x = 0;
}

inline void
MoveCursorToTextStart(TextField *field)
{
    field->cursor = 0;
    field->line = 0;
    field->one_past_saved_cursor_x = 0;
}

inline void
MoveCursorToTextEnd(TextField *field)
{
    u32 new_line_index = field->render_text->buffer->num_lines - 1;
    field->cursor = GetLastLineChar(field->render_text->buffer, new_line_index) + 1;
    field->line = new_line_index;
    field->one_past_saved_cursor_x = 0;
}

internal void
DeleteText(TextField *field, Cursor cursor, u32 line, u32 chars_to_delete)
{
    u32 lines_deleted = DeleteText(field->render_text->buffer, cursor, line, chars_to_delete);

	CursorRange range = {};
    if(field->delete_text_hook)
    {
        field->delete_text_hook(cursor, line, chars_to_delete, lines_deleted, field->delete_text_hook_params);
	}

    for(u32 i = 0; 
        i < Max(1, lines_deleted);
        ++i)
    {
        InvalidateShapedLine(field->render_text, line + i);
    }

    // platform.Info("shift distance of %u\n", lines_deleted);
    ShiftCachedLinesUpward(field->render_text, line + 1, lines_deleted);

    field->full_redraw_needed = true;
}

internal void
KillLine(TextField *field)
{
    u32 chars_to_end_of_line = field->cursor - GetRawOnePastLastLineChar(field->render_text->buffer, field->line);
    DeleteText(field, field->cursor, field->line, chars_to_end_of_line);
}


inline SelectionRange
GetSelectionRange(TextField *field)
{
    SelectionRange result = {};
    
    result.min_line = Min(field->line, field->mark_line);
    result.max_line = Max(field->line, field->mark_line);
    
    result.min_cursor = Min(field->cursor, field->mark);
    result.max_cursor = Max(field->cursor, field->mark);

    // if the mark is activated, the selection includes the
    // min and max cursor
    // otherwise the selection excludes the max cursor
    if(!field->mark_activated &&
       result.max_cursor > 0 &&
        result.min_cursor != result.max_cursor)
    {
        result.max_cursor--;
    }

    return result;
}

internal void
DeleteSelectedText(TextField *field)
{
    SelectionRange range = GetSelectionRange(field);
    
    u32 max_valid_char_pos = GetOnePastLastLineChar(field->render_text->buffer, range.max_line);
    if(range.max_cursor == max_valid_char_pos)
    {
        range.max_cursor = GetRawLastLineChar(field->render_text->buffer, range.max_line);
    }

    u32 num_chars = range.max_cursor - range.min_cursor + 1;
    u32 num_bytes = GetNumBytesBetween(field->render_text->buffer, range.min_cursor, range.max_cursor);

    UTF8 *saved_text = PushCount(&platform.frame_arena, UTF8, num_bytes);
    u32 bytes_copied = CopyBufferChars(field->render_text->buffer, saved_text, num_bytes, range.min_cursor, num_chars);
    Assert(num_bytes == bytes_copied);

    u32 original_cursor = field->cursor;
    u32 original_line = field->line;

    DeleteText(field, range.min_cursor, range.min_line, num_chars);

    TextOperation text_operation = {};
    text_operation.type = TextOp_delete;
    text_operation.cursor_before = original_cursor;
    text_operation.line_before = original_line;
    text_operation.cursor_after = field->cursor;
    text_operation.line_after = field->line;
    text_operation.start = range.min_cursor;
    text_operation.start_line = range.min_line;
    text_operation.num_bytes = num_bytes;
    text_operation.num_chars = num_chars;
    text_operation.num_newlines = range.max_line - range.min_line;
    text_operation.text = saved_text;

    if(field->mark_activated)
    {
        if(field->mark < field->cursor)
        {
            text_operation.flags |= TextOp_mark_activated_at_start;
        }
        else
        {
            text_operation.flags |= TextOp_mark_activated_at_end;
        }
    }

    RecordTextOperation(field->undo_storage, text_operation);
    
    
    field->mark_activated = false;
    field->cursor = range.min_cursor;
    field->line = range.min_line;

    field->mark_line = Min(field->render_text->buffer->num_lines, field->line);
    field->mark = Min(field->render_text->buffer->total_characters, field->cursor);
}

internal void
CharTyped(TextField *field, UTF32 codepoint)
{
    TextOperation text_operation = {};

    UTF8 utf8_bytes[4];
    u32 num_bytes = CodepointToUTF8(codepoint, utf8_bytes);
    
    if(field->mark_activated)
    {
        DeleteSelectedText(field);
        field->full_redraw_needed = true;
        text_operation.flags |= TextOp_append;
    }
    

    // we need to reshape the changed line
    InvalidateShapedLine(field->render_text->shape_cache, field->line);

    u32 original_cursor = field->cursor;
    u32 original_line = field->line;
    
    InsertCharacter(field->render_text->buffer, field->cursor, field->line, codepoint);

	// TODO: Should we call insert_text_hook *once* in textfield.cpp?
	if(field->insert_text_hook)
    {
        field->insert_text_hook(field->cursor, 1, 0, field->insert_text_hook_params);
    }
    
    ScrollFieldToCursor(field);
    MoveCursorToNextCharacter(field);
    ScrollFieldToCursor(field);
    
    field->full_redraw_needed = true;

    text_operation.type = TextOp_insert;
    text_operation.cursor_before = original_cursor;
    text_operation.line_before = original_line;
    text_operation.cursor_after = field->cursor;
    text_operation.line_after = field->line;
    text_operation.start = original_cursor;
    text_operation.start_line = original_line;
    text_operation.num_bytes = num_bytes;
    text_operation.num_chars = 1;
    text_operation.text = (UTF8 *)utf8_bytes;
    text_operation.num_newlines = 0;
    RecordTextOperation(field->undo_storage, text_operation);
}

internal DeleteCharacterInfo
GetDeleteCharacterInfo(TextField *field)
{
    DeleteCharacterInfo result = {};

    // TODO: Introduce grapheme boundaries
    result.delete_text = PushCount(&platform.frame_arena, UTF8, 2);
    UTF32 codepoint = GetCodepoint(field->render_text->buffer, field->cursor);
    UTF32 next_codepoint = GetCodepoint(field->render_text->buffer, field->cursor + 1);
    if(codepoint == '\r')
    {
        result.lines_changed = true;

        result.delete_text[0] = '\r';

        if(next_codepoint == '\n')
        {
            result.delete_text[1] = '\n';
            result.num_chars = 2;
        }
        else
        {
            result.num_chars = 1;
        }
    }
    else if(codepoint == '\n')
    {
        result.delete_text[0] = '\n';
        result.lines_changed = true;

        result.num_chars = 1;
    }
    else if(codepoint != UTF32_PLACEHOLDER_CHARACTER)
    {
        result.delete_text[0] = codepoint;
        result.lines_changed = false;

        result.num_chars = 1;
    }

    return result;
}

internal DeleteCharacterInfo
ForwardDeleteCharacter(TextField *field)
{
    DeleteCharacterInfo delete_info = GetDeleteCharacterInfo(field);
    if(delete_info.num_chars > 0)
    {

        u32 original_cursor = field->cursor;
        u32 original_line = field->line;
            
        DeleteText(field, field->cursor, field->line, delete_info.num_chars);

        TextOperation operation = {};
        operation.type = TextOp_delete;
        operation.cursor_before = original_cursor;
        operation.line_before = original_line;
        operation.cursor_after = field->cursor;
        operation.line_after = field->line;
        operation.start = field->cursor;
        operation.start_line = field->line;
        operation.num_bytes = delete_info.num_chars;
        operation.text = delete_info.delete_text;
        operation.num_newlines = delete_info.lines_changed ? 1 : 0;
        RecordTextOperation(field->undo_storage, operation);

    }
    return delete_info;
}

internal DeleteCharacterInfo
BackwardDeleteCharacter(TextField *field)
{
    DeleteCharacterInfo result = {};

    if(field->cursor > 0)
    {
        Cursor original_cursor = field->cursor;
        u32 original_line = field->line;
        
        // TODO: Grapheme boundaries
        MoveCursorToPreviousCharacter(field);
        
        result = GetDeleteCharacterInfo(field);
        if(result.num_chars > 0)
        {
            DeleteText(field, field->cursor, field->line, result.num_chars);
            ScrollFieldToCursor(field);

            TextOperation operation = {};
            operation.type = TextOp_delete;
            operation.cursor_before = original_cursor;
            operation.line_before = original_line;
            operation.cursor_after = field->cursor;
            operation.line_after = field->line;
            operation.start = field->cursor;
            operation.start_line = field->line;
            operation.num_chars = result.num_chars;
            operation.num_bytes = result.num_chars;
            operation.num_newlines = result.lines_changed ? 1 : 0;
            operation.text = result.delete_text;
            RecordTextOperation(field->undo_storage, operation);

        }
    }

    return result;
}

internal void
DeleteTextAndRecord(TextField *field, Cursor start_pos, u32 start_line, Cursor end_pos, u32 end_line,
                    Cursor original_cursor, u32 original_line)
{
    // includes first and last characters
    u32 num_chars = end_pos - start_pos + 1; 
    u32 num_bytes = GetNumBytesBetween(field->render_text->buffer, start_pos, end_pos);
    
    UTF8 *saved_text = PushCount(&platform.frame_arena, UTF8, num_bytes);
    u32 bytes_copied = CopyBufferChars(field->render_text->buffer, saved_text, num_bytes, start_pos, num_chars);
    Assert(num_bytes == bytes_copied);
    
    DeleteText(field, start_pos, start_line, num_chars);

    ScrollFieldToCursor(field);

    TextOperation text_operation = {};
    text_operation.type = TextOp_delete;
    text_operation.cursor_before = original_cursor;
    text_operation.line_before = original_line;
    text_operation.cursor_after = field->cursor;
    text_operation.line_after = field->line;
    text_operation.start = start_pos;
    text_operation.start_line = end_pos;
    text_operation.num_bytes = num_bytes;
    text_operation.num_chars = num_chars;
    text_operation.text = saved_text;
    RecordTextOperation(field->undo_storage, text_operation);
}

internal void
BackwardDeleteWord(TextField *field)
{
    Cursor original_cursor = field->cursor;
    u32 original_line = field->line;
    
    MoveCursorToPreviousWord(field);
    
    Cursor start_pos = field->cursor;
    Cursor end_pos = original_cursor - 1;

    DeleteTextAndRecord(field, start_pos, field->line, end_pos, field->line, original_cursor, original_line);
}

struct ForwardDeleteWordResult
{
    b32 lines_changed;
};

internal ForwardDeleteWordResult
ForwardDeleteWord(TextField *field)
{
    ForwardDeleteWordResult result = {};

    u32 last_valid_pos = GetOnePastLastLineChar(field->render_text->buffer, field->line);
    if(field->cursor == last_valid_pos)
    {
        // HACK:: if we're in the last valid position
        //        then treat it as a forward delete character
        DeleteCharacterInfo delete_info = ForwardDeleteCharacter(field);
        result.lines_changed = delete_info.lines_changed;
    }
    else
    {
        Cursor original_cursor = field->cursor;
        u32 original_line = field->line;
    
        MoveCursorToNextWord(field);
    
        Cursor start_pos = original_cursor;
        Cursor end_pos = field->cursor - 1;
        DeleteTextAndRecord(field, start_pos, original_line, end_pos, field->line, original_cursor, original_line);

        result.lines_changed = original_line != field->line;

        field->cursor = original_cursor;
        field->line = original_line;

    }

    return result;
}

internal InsertTextResult
InsertText(TextField *field, String content, Cursor cursor, u32 line)
{
    InsertTextResult result;

    result = InsertText(field->render_text->buffer, cursor, line,
                        (UTF8 *)content.ptr, content.len);
    
    ShiftCachedLinesDownward(field->render_text, line + result.num_newlines, result.num_newlines);
    
    for(u32 i = 0;
        i < Max(1, result.num_newlines);
        ++i)
    {
        InvalidateShapedLine(field->render_text, line + i);
    }

    // TODO remove
    for(u32 i = 0;
        i < result.num_newlines;
        i++)
    {
        u32 invalidate_line = line + result.num_newlines + 1;
        InvalidateShapedLine(field->render_text, invalidate_line);
    }
	
	if(field->insert_text_hook)
    {
        field->insert_text_hook(cursor, result.num_chars, result.num_newlines, field->insert_text_hook_params);
    
	}
	
    field->full_redraw_needed = true;

    field->cursor += result.num_chars;
    field->line += result.num_newlines;
    ScrollFieldToCursor(field);

    result.num_chars = result.num_chars;

    return result;
}

internal void
PasteText(TextField *field, String content)
{
    TextOperation text_operation = {};

    if(field->mark_activated)
    {
        DeleteSelectedText(field);
        text_operation.flags |= TextOp_append;
    }

    u32 original_cursor = field->cursor;
    u32 original_line = field->line;
    
    InsertTextResult insert_text = InsertText(field, content, field->cursor, field->line);

    text_operation.type = TextOp_insert;
    text_operation.cursor_before = original_cursor;
    text_operation.line_before = original_line;
    text_operation.cursor_after = field->cursor;
    text_operation.line_after = field->line;
    text_operation.start = original_cursor;
    text_operation.start_line = original_line;
    text_operation.num_bytes = content.len;
    text_operation.num_chars = insert_text.num_chars;
    text_operation.num_newlines = insert_text.num_newlines;
    text_operation.text = content.ptr;
    RecordTextOperation(field->undo_storage, text_operation);

    field->full_redraw_needed = true;
}

internal void
Redo(TextField *field)
{
    for(;;)
    {
        TextOperation operation = GetRedoOperation(field->undo_storage, GetTextOperation_remove);

        if(operation.type != TextOp_invalid)
        {
            switch(operation.type)
            {
                case TextOp_insert:
                {
                    String string = MakeString(operation.text, operation.num_bytes);
                    InsertText(field, string, operation.start, operation.start_line);
                } break;

                case TextOp_delete:
                {
                    DeleteText(field, operation.start, operation.start_line, operation.num_chars);
                } break;

                if(operation.flags & TextOp_mark_activated_at_start)
                {
                    field->mark_activated = true;
                    field->mark = operation.start;
                    field->mark = operation.start_line;
                }
                else if(operation.flags & TextOp_mark_activated_at_end)
                {
                    field->mark_activated = true;
                    field->mark = operation.start + operation.num_chars - 1;
                    field->mark_line = operation.start_line + operation.num_newlines;
                }
                else
                {
                    field->mark_activated = false;
                }
            }

            field->cursor = operation.cursor_after;
            field->line = operation.line_after;
        }

        TextOperation next_operation = GetUndoOperation(field->undo_storage, 0);

        if(!(next_operation.flags & TextOp_append) ||
            next_operation.type == TextOp_invalid)
        {
            break;
        }
    }
    field->full_redraw_needed = true;
}

internal void
Undo(TextField *field)
{
    for(;;)
    {
        TextOperation operation = GetUndoOperation(field->undo_storage, GetTextOperation_remove);
        if(operation.type != TextOp_invalid)
        {
            switch(operation.type)
            {
                case TextOp_insert:
                {
                    DeleteText(field, operation.start, operation.start_line, operation.num_chars);
                } break;

                case TextOp_delete:
                {
                    String string = MakeString(operation.text, operation.num_bytes);
                    InsertText(field, string, operation.start, operation.start_line);
                } break;
            }

            if(operation.flags & TextOp_mark_activated_at_start)
            {
                field->mark_activated = true;
                field->mark = operation.start;
                field->mark_line = operation.start_line;
            }
            else if(operation.flags & TextOp_mark_activated_at_end)
            {
                field->mark_activated = true;
                field->mark = operation.start + operation.num_chars - 1;
                field->mark_line = operation.start_line + operation.num_newlines;
            }
            else
            {
                field->mark_activated = false;
            }

            field->cursor = operation.cursor_before;
            field->line = operation.line_before;
        }

        if(!(operation.flags & TextOp_append) ||
           operation.type == TextOp_invalid)
        {
            break;
        }
    }

    field->full_redraw_needed = true;
}

internal void
InsertNewline(TextField *field)
{
    TextOperation text_operation = {};

    if(field->mark_activated)
    {
        DeleteSelectedText(field);
        field->full_redraw_needed = true;
        text_operation.flags |= TextOp_append;
    }
    
    u32 num_chars = 0;
    UTF8 *insert_text = PushCount(&platform.frame_arena, UTF8, 2);
    switch (field->line_endings)
    {
        case LineEnding_LF:
        {
            num_chars = 1;
            insert_text[0] = '\n';
        } break;

        case LineEnding_CR:
        {
            num_chars = 1;
            insert_text[0]= '\r';
        } break;


        case LineEnding_CRLF:
        {
            num_chars = 2;
            insert_text[0] = '\r';
            insert_text[1] = '\n';
        } break;

        // Unknown line ending
        InvalidDefaultCase;
    }

    String string = MakeString(insert_text, num_chars);

    u32 original_cursor = field->cursor;
    u32 original_line = field->line;

    InsertText(field, string, field->cursor, field->line);

    text_operation.type = TextOp_insert;
    text_operation.cursor_before = original_cursor;
    text_operation.line_before = original_line;
    text_operation.start = original_cursor;
    text_operation.start_line = original_line;
    text_operation.cursor_after = field->cursor;
    text_operation.line_after = field->line;
    
    text_operation.num_chars = field->cursor - original_cursor;
    text_operation.num_bytes = text_operation.num_chars;
    text_operation.text = insert_text;
    RecordTextOperation(field->undo_storage, text_operation);

    UpdateVerticalScrollbarThumb(field);
}


internal String
GetSelectedText(TextField *field)
{
    SelectionRange range = GetSelectionRange(field);
    
    String selected_text = GetTemporaryBufferString(field->render_text->buffer,
                                                    range.min_cursor, range.min_line, range.max_cursor, range.max_line);
    return selected_text;
}

internal void
CutText(TextField *field)
{
    String contents = GetSelectedText(field);
    platform.CopyToClipboard(contents);
    
    DeleteSelectedText(field);
    field->full_redraw_needed = true;
}

enum DeactivateSelectionOption
{
    DeactivateSelection_leave_cursor_location = 0,
    DeactivateSelection_put_cursor_to_start = 1,
    DeactivateSelection_put_cursor_to_end = 2,
};

internal void
DeactivateExistingSelection(TextField *field, DeactivateSelectionOption deactivate_option = DeactivateSelection_leave_cursor_location)
{
    if(field->mark_activated)
    {
        field->mark_activated = false;

        if(deactivate_option == DeactivateSelection_put_cursor_to_start)
        {
            field->cursor = Min(field->mark, field->cursor);
            field->line = Min(field->mark_line, field->line);
        }
        else if(deactivate_option == DeactivateSelection_put_cursor_to_end)
        {
            field->cursor = Max(field->mark, field->cursor);
            field->line = Max(field->mark_line, field->line);
        }
    }
}

inline void
RedrawDeleteCharacter(TextField *field, b32 lines_changed, Recti old_last_char_rect)
{
    field->full_redraw_needed = true;
    // RenderTextTransform transform = GetRenderTextTransform(field);
 
    // if(lines_changed)
    // {
    //     u32 max_visible_lines = GetNumVisibleLines(field->render_text, transform);
    //     u32 one_past_max_draw_line = Min((u32)transform.min_line + RoundUp(max_visible_lines) + 1,
    //                                      field->render_text->buffer->num_lines);

    //     Recti next_line_rect = {};
    //     {
    //         Recti first_line_rect = {};
    //         first_line_rect.x = field->bbox.x;
    //         first_line_rect.y = Round(GetLineY(field->render_text, field->line + 1, transform));

    //         u32 line_start = LinePos(field->render_text->buffer, field->line);
    //         u32 one_past_last_line_char = GetOnePastLastLineChar(field->render_text->buffer, field->line);

    //         f32 width = 0;
    //         for(Cursor cursor = field->cursor;
    //             cursor < one_past_last_line_char;
    //             cursor++)
    //         {
    //             u32 pos_in_line = cursor - line_start;
    //             width += GetCharacterWidth(field, pos_in_line, field->line);
    //         }
    //         first_line_rect.width = Round(width);
    //         first_line_rect.height = (Round(GetLineY(field->render_text, field->line + 2, transform)) -
    //                                   first_line_rect.y);

    //         if(field->line + 1 < one_past_max_draw_line)
    //         {
    //             next_line_rect = GetLineRect(field, field->line + 1);
    //             next_line_rect.y = Round(GetLineY(field->render_text, field->line + 1, transform));
    //             next_line_rect.height = Round(GetLineY(field->render_text, field->line + 2, transform));

    //             Recti clear_rect = {};
    //             clear_rect.x = next_line_rect.x + next_line_rect.width;
    //             clear_rect.width = (first_line_rect.x + first_line_rect.width) - clear_rect.x;
    //             clear_rect.y = first_line_rect.y;
    //             clear_rect.height = first_line_rect.height;

    //             // DrawRectOutline(first_line_rect, 1, CLR_LBLUE);
    //             // DrawRectOutline(clear_rect, 1, CLR_GREEN);
    //             DrawRectangle(clear_rect, field->back_color);
    //             RedrawTextFieldLine(field, field->line + 1);
    //         }
    //     }

    //     Recti last_line_rect = next_line_rect;
    //     for(u32 line = field->line + 2;
    //         line < one_past_max_draw_line;
    //         line++)
    //     {
    //         Recti line_rect = last_line_rect;
    //         line_rect.y = Round(GetLineY(field->render_text, line, transform));
    //         line_rect.height = Round(GetLineY(field->render_text, line + 1, transform)) - line_rect.y;

    //         next_line_rect = GetLineRect(field, line);
                            
    //         Recti clear_rect = {};
    //         clear_rect.x = next_line_rect.x + next_line_rect.width;
    //         clear_rect.width = (line_rect.x + line_rect.width) - clear_rect.x;
    //         clear_rect.y = line_rect.y;
    //         clear_rect.height = line_rect.height;

    //         last_line_rect = next_line_rect;
    //         // DrawRectOutline(line_rect, 1, CLR_ORANGE);
    //         // DrawRectOutline(clear_rect, 1, CLR_GREEN);

    //         DrawRectangle(clear_rect, field->back_color);
    //         RedrawTextFieldLine(field, line);
    //     }
    // }
    // else
    // {
    //     u32 last_char = GetLastLineChar(field->render_text->buffer, field->line);

    //     Recti new_last_char_rect = GetCharacterRectangle(field, last_char, field->line);
    //     Recti clear_rect = {};
    //     clear_rect.x = new_last_char_rect.x + new_last_char_rect.width;
    //     clear_rect.width = old_last_char_rect.x + old_last_char_rect.width - clear_rect.x;
    //     clear_rect.y = old_last_char_rect.y;
    //     clear_rect.height = old_last_char_rect.height;
    //     DrawRectangle(clear_rect, field->back_color);
    // }


    // RedrawTextFieldLine(field, field->line);
}

inline void
RedrawCursorOrSelectionChange(TextField *field, u32 old_cursor, u32 old_line, u32 old_mark, u32 old_mark_line, b32 mark_was_activated)
{
    MemoryArena *temp_arena = &platform.frame_arena;
    if(field->cursor != old_cursor ||
       field->line != old_line ||
       field->mark_activated != mark_was_activated)
    {
        if(field->mark_activated)
        {
            if(mark_was_activated)
            {
                // TODO redraw all the line in the selected region
                //      that aren't in the previous selected region


                // current (bad) strategy 
                u32 min_old_line = Min(old_line, old_mark_line);
                u32 max_old_line = Max(old_line, old_mark_line);

                u32 min_new_line = Min(field->line, field->mark_line);
                u32 max_new_line = Max(field->line, field->mark_line);

                u32 min_line = Min(min_old_line, min_new_line);
                u32 max_line = Max(max_old_line, max_new_line);

                for(u32 line = min_line;
                    line <= max_line;
                    line++)
                {
                    RedrawTextFieldLine(field, line, temp_arena);
                }
                    
            }
            else
            {
                RedrawTextFieldCharacter(field, old_cursor, old_line, temp_arena);

                u32 min_line = Min(field->line, field->mark_line);
                u32 max_line = Max(field->line, field->mark_line);

                for(u32 line = min_line;
                    line <= max_line;
                    line++)
                {
                    RedrawTextFieldLine(field, line, temp_arena);
                }
            }
        }
        else
        {
            if(mark_was_activated)
            {
                // TODO optimization:: dont redraw the entire line?
                u32 min_line = Min(old_line, old_mark_line);
                u32 max_line = Max(old_line, old_mark_line);
                
                for(u32 line = min_line;
                    line <= max_line;
                    line++)
                {
                    RedrawTextFieldLine(field, line, temp_arena);
                }

                RedrawTextFieldCharacter(field, field->cursor, field->line, temp_arena);
            }
            else
            {
                RedrawTextFieldCharacter(field, old_cursor, old_line, temp_arena);
                RedrawTextFieldCharacter(field, field->cursor, field->line, temp_arena);
            }
                
        }
    }
}

internal void
InitTextField(EditorState *editor, TextField *field, TextBuffer *buffer, Font *font, Recti bbox, u32 z_index)
{
	field->render_text = CreateBufferRenderText(buffer, font, &field->arena);
    
	field->bbox = bbox;
    field->render_text->text_color = CLR_NORMAL_TEXT;
    field->back_color = CLR_BACK;
    field->z_index = 2;

    field->full_redraw_needed = true;
}


internal void
UpdateAndRenderTextField(EditorState *editor, TextField *field, Input *input)
{
    if(!field->framebuffer || 
		field->framebuffer->width < field->bbox.width || 
		field->framebuffer->height != field->bbox.height)
	{
		if(field->layer)
		{
			RemoveCompositionLayer(editor->compositor, field->layer);
		}
		
		if(field->framebuffer)
		{
			platform.FreeMemory(field->framebuffer);
		}
		
		u32 width = Align16(field->bbox.width);
		u32 height = field->bbox.height;
		u32 stride = Align16(field->bbox.width * COMPOSITOR_FRAMEBUFFER_BYTESPP);
		u32 max_dirty_rects = 50;
		u8 *data = (u8 *) platform.AllocateMemory(Align16(sizeof(Framebuffer)) + (stride * height) + sizeof(Recti)*max_dirty_rects);
		
		field->framebuffer = (Framebuffer *) data;
		field->framebuffer->width = width;
		field->framebuffer->height = height;
		field->framebuffer->stride = stride;
		field->framebuffer->pixels = data + Align16(sizeof(Framebuffer));
		field->framebuffer->bytespp = 4;
		
		field->framebuffer->max_dirty_rects = max_dirty_rects;
		field->framebuffer->dirty =  (Recti *)((u8 *) field->framebuffer->pixels + (stride * height) + max_dirty_rects);
		
		field->layer = AddCompositonLayer(editor->compositor, field->z_index, field->framebuffer);

		field->layer->pos.x = field->bbox.x;
		field->layer->pos.y = field->bbox.y;
		
		field->full_redraw_needed = true;
	}
	
	if(field->render_text)
    {
        RenderTextTransform transform = GetRenderTextTransform(field);

        u32 old_cursor = field->cursor;
        u32 old_line = field->line;
        u32 old_mark = field->cursor;
        u32 old_mark_line = field->mark_line;

        b32 mark_was_activated = field->mark_activated;

        b32 mouse_hovering_field = PosInRecti(input->mouse_pos, field->bbox);
        if(mouse_hovering_field)
        {
            if(input->ctrl_down)
            {
                // TODO implement the zoomage, by view!
                if(input->wheel_delta > 0)
                {
                    // zoom in
                    i32 zoom_steps = input->wheel_delta / 120;
                    
                }
                else if(input->wheel_delta < 0)
                {
                    // zoom out
                    i32 zoom_steps = -input->wheel_delta / 120;
                    
                }
            }
            else 
            {
                if(input->shift_down)
                {
                    ScrollTextHorizontally(field, -input->wheel_delta / 120 * EM_WIDTHS_PER_MOUSE_SCROLL);
                }
                else
                {
                    ScrollTextVertically(field, -input->wheel_delta / 120 * LINES_PER_MOUSE_SCROLL);
                }

            }
        }
        
#if INTERNAL
        if(KeyPressed(input->keys[Key_f4]))
        {
            field->show_redraw_visual = !field->show_redraw_visual;
        }

        if(field->redraw_occurred)
        {
            field->redraw_occurred = false;

            memcpy(field->last_draw_count, field->draw_count, sizeof(DebugDrawCount) * MAX_DEBUG_DRAW_COUNT);
            field->last_num_draw_count = field->num_draw_count;
        }
        memset(field->draw_count, 0, sizeof(field->draw_count));
        field->num_draw_count = 0;
#endif
        if((TextField *)editor->focused_ui_item == field)
        {
            for(u32 i = 0;
                i < input->chars_typed;
                i++)
            {
                CharTyped(field, input->typed_char[i]);

                Recti line_rect = GetLineRect(field, field->line, &platform.frame_arena);
                DrawRectangle(line_rect, field->back_color, field->framebuffer);
                RedrawTextFieldLine(field, field->line, &platform.frame_arena);

                old_cursor = field->cursor;
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_next_character))
            {
                DeactivateExistingSelection(field, DeactivateSelection_put_cursor_to_end);
                MoveCursorToNextCharacter(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_newline))
            {
                Recti old_line_rect = GetLineRect(field, old_line, &platform.frame_arena);

                InsertNewline(field);
                ScrollFieldToCursor(field);

                Recti char_rect = GetCharacterRectangle(field, old_cursor, old_line, &platform.frame_arena);

                Recti clear_rect = {};
                clear_rect.x = char_rect.x;
                clear_rect.y = old_line_rect.y;
                clear_rect.height = old_line_rect.height;
                clear_rect.width = old_line_rect.width - (clear_rect.x - old_line_rect.x);

                DrawRectangle(clear_rect, CLR_BACK, field->framebuffer);
				
                u32 max_visible_lines = GetNumVisibleLines(field->render_text, transform);
                u32 first_line = transform.top_pixel / GetLinePixelHeight(field->render_text);
                u32 one_past_max_draw_line = Min(first_line + 1, field->render_text->buffer->num_lines);
                
                for(u32 line = field->line;
                    line < one_past_max_draw_line;
                    line++)
                {
                    Recti line_rect = GetLineRect(field, line, &platform.frame_arena);
                    line_rect.width = field->bbox.width;
                    DrawRectangle(line_rect, field->back_color, field->framebuffer);
                    RedrawTextFieldLine(field, line, &platform.frame_arena);
                }
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_next_character_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorToNextCharacter(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_next_word))
            {
                DeactivateExistingSelection(field, DeactivateSelection_put_cursor_to_end);
                MoveCursorToNextWord(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_next_word_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorToNextWord(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_previous_word))
            {
                DeactivateExistingSelection(field, DeactivateSelection_put_cursor_to_start);
                MoveCursorToPreviousWord(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_previous_word_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorToPreviousWord(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_previous_character))
            {
                DeactivateExistingSelection(field, DeactivateSelection_put_cursor_to_start);
                MoveCursorToPreviousCharacter(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_previous_character_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorToPreviousCharacter(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_previous_line))
            {
                DeactivateExistingSelection(field, DeactivateSelection_put_cursor_to_start);
                MoveCursorUp(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_previous_line_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorUp(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_next_line))
            {
                DeactivateExistingSelection(field, DeactivateSelection_put_cursor_to_end);
                MoveCursorDown(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_next_line_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorDown(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_line_start))
            {
                DeactivateExistingSelection(field);
                MoveCursorToLineStart(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_line_start_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorToLineStart(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_line_end))
            {
                DeactivateExistingSelection(field);
                MoveCursorToLineEnd(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_line_end_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorToLineEnd(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_text_start))
            {
                DeactivateExistingSelection(field);
                MoveCursorToTextStart(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_text_end_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorToTextEnd(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_text_end_extend))
            {
                StartSelectionIfNoneExist(field);
                MoveCursorToTextEnd(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_paste))
            {
                // TODO is there a bug if more than one paste occurs in a single frame?
                PlatformClipboardPasteResult paste_result = platform.PasteFromClipboard();
                if(paste_result.is_valid)
                {
                    PasteText(field, paste_result.string);
                }
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_undo))
            {
                Undo(field);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_redo))
            {
                Redo(field);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_cut))
            {
                CutText(field);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_copy))
            {
                String contents = GetSelectedText(field);
                platform.CopyToClipboard(contents);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_forward_delete_character))
            {
                if(field->mark_activated)
                {
                    DeleteSelectedText(field);
                    field->full_redraw_needed = true;
                }
                else
                {
                    Cursor one_past_last_line_char = GetOnePastLastLineChar(field->render_text->buffer, field->line);
                    Cursor last_char = GetLastLineChar(field->render_text->buffer, field->line);
                    Recti old_last_char_rect = GetCharacterRectangle(field, one_past_last_line_char, field->line, &platform.frame_arena);
                    
                    DeleteCharacterInfo delete_info = ForwardDeleteCharacter(field);
                    b32 scrolled = ScrollFieldToCursor(field);

                    if(!scrolled)
                    {
                        RedrawDeleteCharacter(field, delete_info.lines_changed, old_last_char_rect);
                    }
                }

            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_backward_delete_character))
            {
                if(field->mark_activated)
                {
                    DeleteSelectedText(field);
                    field->full_redraw_needed = true;
                }
                else
                {
                    Cursor one_past_last_line_char = GetOnePastLastLineChar(field->render_text->buffer, field->line);
                    Cursor last_char = GetLastLineChar(field->render_text->buffer, field->line);
                    Recti old_last_char_rect = GetCharacterRectangle(field, one_past_last_line_char, field->line, &platform.frame_arena);

                    DeleteCharacterInfo delete_info = BackwardDeleteCharacter(field);

                    b32 scrolled = ScrollFieldToCursor(field);

                    if(!scrolled)
                    {
                        RedrawDeleteCharacter(field, delete_info.lines_changed, old_last_char_rect);
                    }
                }
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_forward_delete_word))
            {
                if(field->mark_activated)
                {
                    DeleteSelectedText(field);
                }
                else
                {
                    Cursor one_past_last_line_char = GetOnePastLastLineChar(field->render_text->buffer, field->line);
                    Recti old_last_char_rect = GetCharacterRectangle(field, one_past_last_line_char, field->line, &platform.frame_arena);

                    u32 original_line = field->line;
                    ForwardDeleteWordResult delete_result = ForwardDeleteWord(field);
                    
                    b32 scrolled = ScrollFieldToCursor(field);
                    if(!scrolled)
                    {
                        RedrawDeleteCharacter(field, delete_result.lines_changed, old_last_char_rect);
                    }
                }
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_backward_delete_word))
            {
                if(field->mark_activated)
                {
                    DeleteSelectedText(field);
                }
                else
                {
                    Cursor one_past_last_line_char = GetOnePastLastLineChar(field->render_text->buffer, field->line);
                    Recti old_last_char_rect = GetCharacterRectangle(field, one_past_last_line_char, field->line, &platform.frame_arena);

                    u32 original_line = field->line;
                    BackwardDeleteWord(field);
                    
                    b32 scrolled = ScrollFieldToCursor(field);
                    if(!scrolled)
                    {
                        RedrawDeleteCharacter(field, original_line != field->line, old_last_char_rect);
                    }
                }
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_set_mark))
            {
                SetMark(field);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_newline))
            {
                InsertNewline(field);
                ScrollFieldToCursor(field);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_previous_empty_line))
            {
                field->mark_activated = false;
                JumpToPreviousBlankLine(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_previous_empty_line_extend))
            {
                StartSelectionIfNoneExist(field);
                JumpToPreviousBlankLine(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_next_empty_line))
            {
                field->mark_activated = false;
                JumpToNextBlankLine(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
            while(CommandIsTriggered(editor, input, TextFieldCommand_next_empty_line_extend))
            {
                StartSelectionIfNoneExist(field);
                JumpToNextBlankLine(field);
                ScrollFieldToCursor(field);
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_page_up))
            {
                u32 old_top_pixel_dest = field->top_pixel_dest;

                f32 num_scroll_lines = Round(GetNumVisibleLines(field->render_text, GetRenderTextTransform(field))) + 1;
                ScrollTextVertically(field, -num_scroll_lines);

                if(field->top_pixel_dest != old_top_pixel_dest)
                {
                    u32 new_cursor_line = (field->line > num_scroll_lines) ? field->line - num_scroll_lines : 0;
                    MoveCursorToLine(field, new_cursor_line);
                }
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_page_down))
            {
                u32 old_top_pixel_dest = field->top_pixel_dest;

                f32 num_scroll_lines = Round(GetNumVisibleLines(field->render_text, GetRenderTextTransform(field))) + 1;
                ScrollTextVertically(field, num_scroll_lines);

                if(field->top_pixel_dest != old_top_pixel_dest)
                {
                    u32 new_cursor_line = ((num_scroll_lines < field->render_text->buffer->num_lines - field->line) ?
                                           field->line + num_scroll_lines :
                                           field->render_text->buffer->num_lines - 1);
                    MoveCursorToLine(field, new_cursor_line);
                }
            }

            while(CommandIsTriggered(editor, input, TextFieldCommand_kill_line))
            {
                Assert(false);
                KillLine(field);
                ScrollFieldToCursor(field);
            }
        }
		
        if(KeyPressed(input->mouse_buttons[MouseButton_left]) &&
           mouse_hovering_field)
        {
            if(field->z_index >= editor->focused_z_index)
            {
                Editor_CaptureMouse(editor, field);
                field->is_active = true;
                Editor_SetFocus(editor, field, field->z_index, DefocusTextfield);
                RedrawTextFieldCharacter(field, field->cursor, field->line, &platform.frame_arena);
                
                i32 field_relative_x = input->mouse_pos.x - field->bbox.x;
                i32 field_relative_y = input->mouse_pos.y - field->bbox.y;
                u32 cursor_line = GetLineFromY(field->render_text, field_relative_y, transform);
                u32 cursor_column = GetColumnFromTextRelativeX(field, cursor_line, field_relative_x);

                field->cursor = LinePos(field->render_text->buffer, cursor_line) + cursor_column;
                field->line = cursor_line;
                field->mark_activated = false;

                SetMark(field);
    
                field->one_past_saved_cursor_x = 0;
                // platform.Info("mark deactivated on mouse down!\n");
                RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
            }
        }
        else if(input->mouse_buttons[MouseButton_left].down &&
                editor->mouse_capture == field)
        {
            i32 field_relative_x = input->mouse_pos.x - field->bbox.x;
            i32 field_relative_y = input->mouse_pos.y - field->bbox.y;
            
            u32 cursor_line = GetLineFromY(field->render_text, field_relative_y, transform);
            u32 cursor_column = GetColumnFromTextRelativeX(field, cursor_line, field_relative_x);
            
            field->cursor = LinePos(field->render_text->buffer, cursor_line) + cursor_column;
            field->line = cursor_line;

            platform.Info("The column is %u\n", cursor_column);

            field->mark_activated = (field->mark != field->cursor);
            RedrawCursorOrSelectionChange(field, old_cursor, old_line, old_mark, old_mark_line, mark_was_activated);
        }

        
        if(KeyReleased(input->mouse_buttons[MouseButton_left]))
        {
            Editor_ReleaseMouse(editor);
        }
        

        
        // u32 cursor_line = GetLineFromY(field->render_text, input->mouse_pos.y, transform);
        // u32 field_relative_x = DisplayXToTextRelativeX(field->render_text, input->mouse_pos.x, transform);
        // u32 document_right_side = field->bbox.x - field->left_pixel + GetLinePixelWidth(field->render_text, cursor_line);
        // if(field_relative_x < 0 || field_relative_x > document_right_side)

        if(mouse_hovering_field)
        {
            // platform.SetCursorIcon(CursorIcon_ibeam);
        }
        else
        {
            // platform.SetCursorIcon(CursorIcon_arrow);
        }


        if(field->top_pixel != field->top_pixel_dest)
        {
            field->top_pixel_t += input->dt;
            if (field->top_pixel_t > SCROLL_TWEEN_TIME)
            {
                field->top_pixel_t = SCROLL_TWEEN_TIME;
            }
            f32 t = field->top_pixel_t / SCROLL_TWEEN_TIME;
            field->top_pixel = Lerp(field->top_pixel_from, field->top_pixel_dest, t);

            UpdateVerticalScrollbarThumb(field);

            field->full_redraw_needed = true;
        }


        // TODO this function repeats the function above like a lot
        //     how can we code this for maximum mental clarity and something that makes more sense
        if(field->left_pixel != field->left_pixel_dest)
        {
            field->left_pixel_t += input->dt;
            if (field->left_pixel_t > SCROLL_TWEEN_TIME)
            {
                field->left_pixel_t = SCROLL_TWEEN_TIME;
            }
            f32 t = field->left_pixel_t / SCROLL_TWEEN_TIME;
            field->left_pixel = Lerp(field->left_pixel_from, field->left_pixel_dest, t);


            UpdateHorizontalScrollbarThumb(field);

            field->full_redraw_needed = true;
        }

        if(field->cursor != old_cursor||
            field->line != old_line)
        {
            if(!mark_was_activated)
            {
            }
            else
            {
            }

        }
    }

    if(KeyPressed(input->keys[Key_f5]))
    {
        field->full_redraw_needed = true;
    }
    
    if(field->full_redraw_needed)
    {
        platform.Info("complete redraw!\n");
        RedrawTextField(field);
        field->full_redraw_needed = false;
    }

#if INTERNAL
    if(field->redraw_occurred || KeyPressed(input->keys[Key_f3]))
    {
        if(field->show_redraw_visual)
        {
            field->show_redraw_visual = false;
        
            for(int i = 0; i < field->last_num_draw_count; i++)
            {
                if(field->last_draw_count[i].count > 0)
                {
                    u32 char_pos = field->last_draw_count[i].char_pos;
                    DebugDrawCount *draw_count = GetDebugDrawCount(field, char_pos);         
                    if(!draw_count)
                    {
                        u32 line = GetLineContainingCharPos(field->render_text->buffer, char_pos);
                        RedrawTextFieldCharacter(field, char_pos, line, &platform.frame_arena);    
                    }
                }
            }
            field->show_redraw_visual = true;
        }        
    }
#endif
}
