#define SubpixelGranularity 64

struct FractionalNumber
{
    i32 integer_part;
    i32 fractional_part;
};

inline FractionalNumber I32ToFractional(i32 num);
inline FractionalNumber ImproperI32ToFractional(i32 improper);
inline FractionalNumber FractionalZero();

inline FractionalNumber operator+ (FractionalNumber left, FractionalNumber right);
inline FractionalNumber operator- (FractionalNumber left, FractionalNumber right);
inline FractionalNumber operator+= (FractionalNumber &left, FractionalNumber right);
inline FractionalNumber operator-= (FractionalNumber &left, FractionalNumber right);
inline b32 operator< (FractionalNumber left, FractionalNumber right);
inline b32 operator> (FractionalNumber left, FractionalNumber right);
inline FractionalNumber RoundFractional(FractionalNumber num);

struct GlyphOffset
{
    FractionalNumber x;
    FractionalNumber y;
};

inline Vec2i IntegerPartOnly(GlyphOffset offset);
inline Vec2i FractionalPartOnly(GlyphOffset offset);

struct ShapedCharacterMetrics
{
    FractionalNumber x;
    FractionalNumber width;
};

struct ClusterInfo
{
    u32 start_glyph_index;
    u32 num_glyphs; // redundant information
};

struct GlyphInfo
{
    u16 glyph_id;
    FractionalNumber advance;
    GlyphOffset offset;
};

struct ShapedTextRun
{
    b32 RTL;
    u32 *character_to_cluster;

    GlyphInfo *glyph;
    u32 num_glyphs;

    ClusterInfo *cluster;
    u32 num_clusters;

    FractionalNumber total_width;

    u32 chars_before_run;
    u32 num_chars;
};

struct ShapedText
{
    u32 *visual_to_logical;
    u32 *logical_to_visual;
    
    u32 num_runs;
    ShapedTextRun *runs;

    u32 num_chars;

    // TODO should we move this else where?
    // this is just here so that we can access cached
    // data of the script items

    // TODO think about alternate places this can be
    //      cached instead of in the ShapedTextCache
    ScriptItems *script_items;
};

struct EngineShapedRunData;
struct ShapeEngineFont;

