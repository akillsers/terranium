// TODO is this value optimal?
#define MAX_TOKENS_TO_REPARSE 512

internal void
ReparseEntireLF(LoadedFile *lf)
{
    platform.Info("platform.Info:: doing a reparse of the WHOLE LF!\n");
    lf->total_tokens = 0;
    
    TokenStore *first_token_store = lf->token_store_sentinel.next;
    for(TokenStore *token_store = first_token_store, *next_token_store = 0;
        token_store != &lf->token_store_sentinel;
        token_store = next_token_store)
    {
        next_token_store = token_store->next;
        platform.FreeMemory(token_store);
    }
    DL_INIT_SENTINEL(&lf->token_store_sentinel);
    
    ParseContext parse_context = {};
    parse_context.start_pos = 0;
    parse_context.end_pos = lf->text->total_characters - 1;
    parse_context.last_token = 0;
    parse_context.last_last_token = 0;
    parse_context.parsed_tokens = PushCount(&platform.frame_arena, CPPToken, MAX_TOKENS_TO_REPARSE);
    parse_context.max_tokens_to_parse = MAX_TOKENS_TO_REPARSE;
    
    u32 total_tokens_reparsed = 0;
    b32 done_parsing = false;
    
    while(!done_parsing)
    {
        done_parsing = ParseCPP(&parse_context, lf);
        
        PushBackTokens(lf, (Token *)parse_context.parsed_tokens, parse_context.num_tokens_parsed);
        
        total_tokens_reparsed += parse_context.num_tokens_parsed;
        
        TokenStore *last_store = GetLastTokenStore(lf);
        if(last_store)
        {
            parse_context.last_token = last_store->tokens + last_store->num_tokens - 1;
            parse_context.last_last_token = last_store->tokens + last_store->num_tokens - 2;
        }
        else
        {
            parse_context.last_token = 0;
            parse_context.last_last_token = 0;
        }
        
        parse_context.start_pos = parse_context.parse_finished_at;
    }
    
}

internal u32
ReparseCharacters(LoadedFile *lf, Cursor char_pos, u32 num_chars)
{
    if(num_chars == 0) return 0;

    Token *first_token = GetTokenAtCursorOrNearestBackward(lf, char_pos);
    u32 first_token_index = first_token ? GetTokenIndex(lf, first_token) : 0;

    // TODO: shouldn't this technically be char_pos + num_chars - 1?
    Cursor end_char_pos = char_pos + num_chars - 1;
    
    ParseContext parse_context = {};
    parse_context.start_pos = char_pos;
    parse_context.end_pos = end_char_pos;
    parse_context.parsed_tokens = PushCount(&platform.frame_arena, CPPToken, MAX_TOKENS_TO_REPARSE);
    parse_context.max_tokens_to_parse = MAX_TOKENS_TO_REPARSE;
    
    u32 total_tokens_reparsed = 0;
    b32 done_parsing = false;
    while(!done_parsing)
    {
        DEBUG_VerifyTokenStoreIntegrity(lf);
        if(first_token_index + total_tokens_reparsed > 0)
        {
            // TODO this seems wrong, what if we reparsed new things!!!!
            u32 last_token_index = first_token_index + total_tokens_reparsed - 1;
            parse_context.last_token = GetTokenByIndex(lf, last_token_index);
        }
        else
        {
            parse_context.last_token = 0;
        }
        
        if(first_token_index + total_tokens_reparsed > 1)
        {
            u32 last_last_token_index = first_token_index + total_tokens_reparsed - 2;
            parse_context.last_last_token = GetTokenByIndex(lf, last_last_token_index);
        } else {
            parse_context.last_last_token = 0;
        }
        
        done_parsing = ParseCPP(&parse_context, lf);
        
        Token *highest_parsed = 0;
        Cursor delete_from;
        Cursor delete_to;
        if(parse_context.num_tokens_parsed > 0)
        {
            highest_parsed = parse_context.parsed_tokens + parse_context.num_tokens_parsed - 1;
            delete_from = parse_context.parsed_tokens->pos;
            delete_to = Max(highest_parsed->pos + highest_parsed->len - 1, parse_context.end_pos);
        }
        else
        {
            delete_from = parse_context.start_pos;
            delete_to = parse_context.end_pos;
        }
            
        Token *first_token_to_delete = GetTokenAtCursorOrNearestForward(lf, parse_context.start_pos);
        Token *token_before_first_delete_token = 0;
        if(first_token_to_delete)
        {
            token_before_first_delete_token = GetTokenXBehind(lf, first_token_to_delete, 1);   
        }
        Token *last_token_to_delete = GetTokenAtCursorOrNearestBackward(lf, delete_to);
            
        DEBUG_VerifyTokenStoreIntegrity(lf);
        if(first_token_to_delete &&
            last_token_to_delete &&
            first_token_to_delete->pos <= last_token_to_delete->pos)
        {
            DEBUG_VerifyTokenStoreIntegrity(lf);
            u32 num_delete_tokens = CountTokensBetween(lf, first_token_to_delete, last_token_to_delete) + 1;
            Assert(num_delete_tokens > 0);
            DEBUG_VerifyTokenStoreIntegrity(lf);

            if(done_parsing && highest_parsed)
            {
                if(TokenLastCharacter(highest_parsed) == last_token_to_delete->pos &&
                    highest_parsed->pos != last_token_to_delete->pos &&
                    last_token_to_delete->type == highest_parsed->type &&
                    (last_token_to_delete->type == CPPToken_char_literal ||
                     last_token_to_delete->type == CPPToken_string)) // don't chain if its ourselves
                {
                    // An old string/char-literal needs to be reparsed since it is no longer
                    // a string/char-literal

                    // Since the highest parsed token is a string, the next string token after that
                    // should be deleted and reparsed as normal text

                    // Char literals and string tokens are the only types of tokens that should chain,
                    // in addition, they should chain with the same type (char-char and string-string chain)

                    // Reparse starting from the character after the first character of the string/char literal
                    // (which is the last_token_to_delete) to the end of the string token

                    // TODO: create example

                    u32 parse_start = last_token_to_delete->pos + 1;
                    u32 parse_len = last_token_to_delete->len - 1;
                    if(parse_len > 0)
                    {
                        // TODO: Consider unrolling this recursion? Since recursion is no fun to debug
                        //       and to eliminate arbitrary stack depth bounds
                        ReparseCharacters(lf, parse_start, parse_len);

                        // we don't have to delete the last token anymore!
                        // it was reparsed and deleted by ReparseCharacters above
                        num_delete_tokens--;

                    }

                    DEBUG_VerifyTokenStoreIntegrity(lf);
                }
                else if((highest_parsed->type == CPPToken_string || highest_parsed->type == CPPToken_char_literal) &&
                    (last_token_to_delete->type == CPPToken_char_literal ||
                        last_token_to_delete->type == CPPToken_string ||
                        last_token_to_delete->type == CPPToken_comment ||
                        last_token_to_delete->type == CPPToken_number) &&
                    highest_parsed->pos < last_token_to_delete->pos &&
                    TokenLastCharacter(highest_parsed) >= last_token_to_delete->pos &&
                    TokenLastCharacter(highest_parsed) <= TokenLastCharacter(last_token_to_delete)
                    )
                {
                    // The highest parsed token is a new string/char-literal ending in the middle of 
                    // the range of what used to be a string, char-literal, comment, or number literal 
                    // (e.g. 943'232).

                    // Reparse from the character after the end of this new string/char-literal 
                    // to the end of the old string, char-literal, comment, or number literal

                    // --Example--
                    // ''9'34
                    // e   rr
                    //   dddd

                    // e = edit: this character is removed
                    // r = reparse
                    // d = last_token_to_delete
                    u32 parse_start = highest_parsed->pos + highest_parsed->len;
                    u32 parse_len = last_token_to_delete->pos + last_token_to_delete->len - parse_start;
                    if(parse_len)
                    {
                        ReparseCharacters(lf, parse_start, parse_len);
                        // The last token has already been deleted by ReparseCharacters above
                        num_delete_tokens--;
                    }

                    DEBUG_VerifyTokenStoreIntegrity(lf);
                }
                else if(TokenLastCharacter(highest_parsed) < TokenLastCharacter(last_token_to_delete) &&
                    (last_token_to_delete->type == CPPToken_string ||
                        last_token_to_delete->type == CPPToken_char_literal))
                {
                    // While reparsing a token, we deleted a string or char literal 
                    // completely but have yet to reparse that yet (since the token ends 
                    // before the end of the string / char-literal)

                    // Reparse from the character after the highest parsed token to the end of the string / char-literal

                    // --Example--
                    // //"ake            line 1
                    // e
                    //   dddd
                    // -----------
                    // la"               line 2
                    // rrr
                    // ddd

                    // e = edit: this character is inserted
                    // r = reparse
                    // d = last_token_to_delete
                    u32 parse_start = TokenOnePastLastCharacter(highest_parsed);
                    u32 parse_len = TokenLastCharacter(last_token_to_delete) - parse_start + 1;
                    ReparseCharacters(lf, parse_start, parse_len);

                    // The last token (of type string/char literal) has already been deleted by ReparseCharacters
                    num_delete_tokens--;
                    DEBUG_VerifyTokenStoreIntegrity(lf);
                }
            }

            DeleteTokens(lf, first_token_to_delete, num_delete_tokens);

        }

        if(parse_context.num_tokens_parsed > 0)
        {
            Token *parsed_tokens = (Token *)parse_context.parsed_tokens;
            Token *insert_after_token = 0;
            if(token_before_first_delete_token)
            {
                insert_after_token = token_before_first_delete_token;
            }
            else
            {
                Token *last_token = GetLastToken(lf);
                if(last_token && TokenLastCharacter(last_token) < parsed_tokens[0].pos)
                {
                    insert_after_token = last_token;
                }
                else
                {
                    insert_after_token = 0;
                }
            }

            DEBUG_VerifyTokenStoreIntegrity(lf);
            InsertTokens(lf, insert_after_token, parsed_tokens, parse_context.num_tokens_parsed);
        }
            
        total_tokens_reparsed += parse_context.num_tokens_parsed;
        parse_context.start_pos = parse_context.parse_finished_at;
        DEBUG_VerifyTokenStoreIntegrity(lf);
    }
	
	u32 chars_parsed = parse_context.parse_finished_at - char_pos;
	return chars_parsed;
}

internal CursorRange
LFInsertText(Cursor cursor, u32 num_chars, u32 num_lines, void *params)
{
    LoadedFile *lf = (LoadedFile *)params;
    
    AnchorLink *first_anchor_link = lf->anchor_link_sentinel.next;
    for(AnchorLink *anchor_link = first_anchor_link;
        anchor_link != &lf->anchor_link_sentinel;
        anchor_link = anchor_link->next)
    {
        Anchor *anchor = &anchor_link->anchor;
        if(cursor <= *anchor->cursor)
        {
            platform.Info("shifting is happening!\n");
            anchor->cursor[0] += num_chars;
            anchor->line[0] += num_lines;
        }
    }
    
    // strategy: shift the tokens after the pasted text
    //           by num chars, changed the length of the
    //           partially enclosed tokens, then reparse
    //           from the first partially enclosed token
    //           or the insert cursor to last partially
    //           enclosed token or the insert cursor +
    //           num_chars
    
	// get the token that we are touching behind
	// TODO: Should we be more granular and only start from the 
	//       previous token when necessary?
    Token *start_token = GetTokenAtCursor(lf, cursor - 1);
    if(start_token)
    {
        // actually start at the the token before the one we are touching to handle
        // for example when we insert l to the end of the string "0x90l"
        u32 start_token_index = GetTokenIndex(lf, start_token);
        if(start_token_index > 0)
        {
            start_token = GetTokenByIndex(lf, start_token_index - 1);
        }
    }

    Token *end_token = GetTokenAtCursor(lf, cursor);
    if(!end_token)
    {
        end_token = GetTokenAtCursor(lf, cursor + 1);
    }
    
    Cursor start_pos = (start_token) ? (start_token->pos) : (cursor);
    Cursor end_pos = (end_token) ? (TokenLastCharacter(end_token) + num_chars) : (cursor + num_chars);
    
    // end_pos = lf->text->total_characters;
    
    platform.Info("start position is %u, end position is %u\n", start_pos, end_pos);
    
    ShiftTokensAheadCursor(lf, cursor, num_chars);
    u32 chars_changed = ReparseCharacters(lf, start_pos, end_pos - start_pos + 1);

	CursorRange range = {};
	range.min = start_pos;
	range.max = start_pos + chars_changed - 1;
	
	return range;
}

internal void
LFInsertCharacter(Cursor cursor, void *params)
{
    LoadedFile *lf = (LoadedFile *)params;
    
    AnchorLink *first_anchor_link = lf->anchor_link_sentinel.next;
    for(AnchorLink *anchor_link = first_anchor_link;
        anchor_link != &lf->anchor_link_sentinel;
        anchor_link = anchor_link->next)
    { 
        Anchor *anchor = &anchor_link->anchor;
        if(cursor <= *anchor->cursor)
        {
            anchor->cursor[0]++;
            
            Cursor next_line_boundary = GetLastLineChar(lf->text, anchor->line[0]);
            if(anchor->cursor[0] > next_line_boundary)
            {
                anchor->line[0]++;
            }
        }
    }
    
    // we need to reparse any tokens that this new character is touching + ourselves
    // if it aint touching anything, parse just this token and add it to the token array
    
    // if we are on a token, set that token to cursor_token
    Token *cursor_token = GetTokenAtCursor(lf, cursor);
    Token *touching_behind_token = 0; 
    // it is not possible to have a touching ahead token since the token
    // that would be the touching ahead token is actually located at where the cursor is
    // since the token list isn't reparsed at this point
    
    if(!cursor_token || cursor == cursor_token->pos)
    {
        Token *previous_token = GetPreviousTokenOrNearestBackward(lf, cursor);
        if(previous_token &&
           previous_token->pos + previous_token->len == cursor)
        {
            touching_behind_token = previous_token;
        }
    }
    
    Token *first_reparse_token = 0;
    Token *last_reparse_token = 0;
    
    // account for newly inserted character
    if(touching_behind_token) touching_behind_token->len++;
    else if(cursor_token) cursor_token->len++;
    
    if(touching_behind_token)
    {
        first_reparse_token = touching_behind_token;
        last_reparse_token = (cursor_token) ? (cursor_token) : touching_behind_token;
        
    }
    else if(cursor_token)
    {
        first_reparse_token = cursor_token;
        last_reparse_token = cursor_token;
    }
    
    if(first_reparse_token)
    {
        u32 num_reparse_tokens = CountTokensBetween(lf, first_reparse_token, last_reparse_token) + 1;
        
        ShiftTokensAheadCursor(lf, cursor, 1);
        ReparseCharacters(lf, first_reparse_token->pos, first_reparse_token->len);
    }
    else
    {
        // no need to reparse anything just add the new token
        ShiftTokensAheadCursor(lf, cursor, 1);
        
        ReparseCharacters(lf, cursor, 1);
    }
}


internal void
LoadCP1252(LoadedFile *lf, String content)
{
    Assert(lf->text->section_sentinel.prev == &lf->text->section_sentinel &&
           lf->text->section_sentinel.next == &lf->text->section_sentinel);
    
    AllocateAndPushBackSection(lf->text);
    
    UTF32 last_codepoint = 0;
    for(u32 i = 0; i < content.len; i++)
    {
        UTF32 codepoint = GetCP1252Codepoint(*content.ptr++);
        
        UTF8 utf8_bytes[4];
        u32 utf8_bytes_used = CodepointToUTF8(codepoint, utf8_bytes);
        PushUTF8Character(lf->text, utf8_bytes, utf8_bytes_used);
    }
}

// TODO get a test case to test this code
inline b32
IsFullRangeContinuationByte(UTF8 byte)
{
    b32 result;
    result = byte >= 0x80 && byte <= 0xBF;
    return result;
}

inline UTF8 *
TryGetOffset(String string, u64 offset)
{
    UTF8 *result = 0;
    
    if(offset < string.len)
    {
        result = string.ptr + offset;
    }
    
    return result;
}

internal u64
CheckUTF8Validity(String string)

{
    u64 offset = 0;
    while(offset < string.len)
    {
        UTF8 *first = string.ptr + offset;
        if(*first <= 0x7F)
        {
            offset++;
        }
        else if(*first >= 0xC2 && *first <= 0xDF)
        {
            if(offset + 1 < string.len)
            {
                
                UTF8 *second = string.ptr + offset + 1;
                if(IsFullRangeContinuationByte(*second))
                {
                    offset += 2;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else if(*first == 0xE0)
        {
            if(offset + 1 < string.len)
            {
                UTF8 *second = string.ptr + offset + 1;
                if(*second >= 0xA0 && *second <= 0xBF)
                {
                    if(offset + 2 < string.len)
                    {
                        UTF8 *third = string.ptr + offset + 2;
                        if(IsFullRangeContinuationByte(*third))
                        {
                            offset += 3;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else if(*first >= 0xE1 && *first <= 0xEC)
        {
            UTF8 *second = TryGetOffset(string, offset + 1);
            if(second)
            {
                if(IsFullRangeContinuationByte(*second))
                {
                    UTF8 *third = TryGetOffset(string, offset + 2);
                    
                    if(IsFullRangeContinuationByte(*third))
                    {
                        offset += 3;
                    }
                    else
                    {
                        break;
                    }
                    
                    if(third)
                    {
                        
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
            
        }
        else if(*first == 0xED)
        {
            UTF8 *second = TryGetOffset(string, offset + 1);
            if(second)
            {
                if(*second >= 0x80 && *second <= 0x9F)
                {
                    UTF8 *third = TryGetOffset(string, offset + 2);
                    
                    if(third &&
                       IsFullRangeContinuationByte(*third))
                    {
                        offset += 3;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else if(*first >= 0xEE && *first <= 0xEF)
        {
            UTF8 *second = TryGetOffset(string, offset + 1);
            if(second &&
               IsFullRangeContinuationByte(*second))
            {
                UTF8 *third = TryGetOffset(string, offset + 2);
                
                if(third &&
                   IsFullRangeContinuationByte(*third))
                {
                    offset += 3;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else if(*first == 0xF0)
        {
            UTF8 *second = TryGetOffset(string, offset + 1);
            if(second &&
               *second >= 0x90 && *second <= 0xBF)
            {
                UTF8 *third = TryGetOffset(string, offset + 2);
                
                if(third &&
                   IsFullRangeContinuationByte(*third))
                {
                    UTF8 *fourth = TryGetOffset(string, offset + 3);
                    
                    if(fourth &&
                       IsFullRangeContinuationByte(*fourth))
                    {
                        offset += 4;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else if(*first >= 0xF1 && *first <= 0xF3)
        {
            UTF8 *second = TryGetOffset(string, offset + 1);
            
            if(second &&
               IsFullRangeContinuationByte(*second))
            {
                UTF8 *third = TryGetOffset(string, offset + 2);
                if(third &&
                   IsFullRangeContinuationByte(*third))
                {
                    UTF8 *fourth = TryGetOffset(string, offset + 3);
                    
                    if(fourth &&
                       IsFullRangeContinuationByte(*fourth))
                    {
                        offset += 4;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else if(*first == 0xF4)
        {
            UTF8 *second = TryGetOffset(string, offset + 1);
            
            if(second &&
               IsFullRangeContinuationByte(*second))
            {
                UTF8 *third = TryGetOffset(string, offset + 2);
                
                if(third &&
                   IsFullRangeContinuationByte(*third))
                {
                    UTF8 *fourth = TryGetOffset(string, offset + 3);
                    
                    if(fourth &&
                       IsFullRangeContinuationByte(*fourth))
                    {
                        offset += 4;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else
        {
            break;
        }
        
    }
    
    return offset;
}


// this allocated the loadedfile for you
// TODO use freelist instead of malloc allocation?
enum FileLoadError {
    FileLoadError_none,
    FileLoadError_unsupported_encoding,
};

struct LoadFileResult
{
    FileLoadError error;
    LoadedFile *lf;
};

internal TextEncoding
GuessBufferEncoding(String content)
{
    TextEncoding result = {};
    
    u64 error_at = CheckUTF8Validity(content);
    if(error_at == content.len)
    {
        result.type = Encoding_UTF8;
        platform.Info("Guessed encoding UTF8\n");
        
        if(content.len >= 3)
        {
            u8 *first_byte = content.ptr;
            u8 *second_byte = content.ptr + 1;
            u8 *third_byte = content.ptr + 2;
            
            if(*first_byte == 0xEF && *second_byte == 0xBB && *third_byte == 0xBF)
            {
                result.flags |= TEXT_ENCODING_HAS_UNICODE_BOM;
            }
        }
    }
    else
    {
        result.type = Encoding_ASCII;
        platform.Info("Guessed encoding ASCII\n");
    }
    
    return result;
}

internal void
LoadASCII(LoadedFile *lf, String content)
{
    AllocateAndPushBackSection(lf->text);
    
    for(u32 i = 0;
        i < content.len;
        i++)
    {
        u8 *character = content.ptr + i;
        if(*character <= 127)
        {
            PushUTF8Character(lf->text, content.ptr + i, 1);
        }
        else
        {
            PushCodepoint(lf->text, 0xFFFD);
        }
    }
}

internal void
LoadUTF8(LoadedFile *lf, String content)
{
    u32 start = 0;
    if(lf->encoding.flags & TEXT_ENCODING_HAS_UNICODE_BOM)
    {
        start = 3;
    }
    
    AllocateAndPushBackSection(lf->text);
    
    for(u32 i = start; i < content.len;)
    {
        u32 multibyte_length = GetMultibyteLength(content.ptr[i]);
        PushUTF8Character(lf->text, content.ptr + i, multibyte_length);
        i += multibyte_length;
    }
}

internal void
FreeLoadedFile(LoadedFile *lf)
{
    FreeAllBlocks(&lf->arena);
}


// Table 3-7 in Unicode Standard 12.0
// Code Points First Byte Second Byte Third Byte Fourth Byte
// U+0000..U+007F 00..7F
// U+0080..U+07FF C2..DF 80..BF
// U+0800..U+0FFF E0 A0..BF 80..BF
// U+1000..U+CFFF E1..EC 80..BF 80..BF
// U+D000..U+D7FF ED 80..9F 80..BF
// U+E000..U+FFFF EE..EF 80..BF 80..BF

// U+10000..U+3FFFF F0 90..BF 80..BF 80..BF
// U+40000..U+FFFFF F1..F3 80..BF 80..BF 80..BF
// U+100000..U+10FFFF F4 80..8F 80..BF 80..BF

internal LoadFileResult
LoadFile(EditorState *editor,
         String file_path, String file_name,
         String content, MemoryArena *temp)
{
    LoadFileResult result = {};
    
    // THIS IS A MEGATODO TODO!!! we are allocating a 64k chunk of mem for EACH loaded file!!
    LoadedFile *lf = BootstrapPushStruct(LoadedFile, offsetof(LoadedFile, arena));
    *lf = {};
    
    lf->text = PushType(&lf->arena, TextBuffer);
    lf->undo_storage = PushType(&lf->arena, TextUndoStorage);
    InitDefaultTextBuffer(lf->text);
    InitTextUndoStorage(lf->undo_storage);
    
    lf->file_path = PushString(&lf->arena, file_path.len);
    CopyStringData(lf->file_path, file_path);
    lf->file_name = PushString(&lf->arena, file_name.len);
    CopyStringData(lf->file_name, file_name);

    DL_INIT_SENTINEL(&lf->anchor_link_sentinel);
    DL_INIT_SENTINEL(&lf->token_store_sentinel);
    
    lf->encoding = GuessBufferEncoding(content);
    
    switch(lf->encoding.type)
    {
        case Encoding_ASCII:
        {
            LoadASCII(lf, content);
        } break;
        
        // case Encoding_CP1252:
        // {
        //     LoadCP1252(lf, content);
        // } break;
        
        case Encoding_UTF8:
        {
            LoadUTF8(lf, content);
        } break;
        
        // case Encoding_UTF16:
        // {
        //     LoadUTF16(lf, content);
        // } break;
        
        // case Encoding_UTF32:
        // {
        //     LoadUTF32(lf, content);
        // } break;
        
        InvalidDefaultCase;
        
    }
    
    if(lf->encoding.type == Encoding_unknown)
    {
        platform.Error("Unsupported encoding!", "Terranium doesn't know what encoding that is!");
    }
    else
    {
        LinesCount lines_count = PushAllLinesToLineArray(lf->text);
        
        // TODO optimization:: we will have to do way less section trasversal if we parse the cpp
        //                    before splitting it into sections
        // TODO what if there are more tokens then what can be fit into temp memory (MAX_TOKENS)???
#define MAX_TOKENS 100000
        ParseContext parse_context = {};
        parse_context.start_pos = 0;
        parse_context.end_pos = lf->text->total_characters - 1;
        parse_context.last_token = 0;
        parse_context.last_last_token = 0;
        parse_context.parsed_tokens = PushCount(&platform.frame_arena, CPPToken, MAX_TOKENS);
        parse_context.max_tokens_to_parse = MAX_TOKENS;
        ParseCPP(&parse_context, lf);
        PushBackTokens(lf, (Token *)parse_context.parsed_tokens, parse_context.num_tokens_parsed);
#undef MAX_TOKENS
        
        lf->file_type = FileType_CPP;
        
        if(DifferentLineEndingsExist(&lines_count) || NoLinesExist(&lines_count))
        {
            // TODO tell the user about multiple line endings?
            lf->line_endings = PLATFORM_NATIVE_LINE_ENDING;
        }
        else
        {
            lf->line_endings = HighestOccuringLineEnding(&lines_count);
        }
        
        platform.Info("the selected line ending type is %s \n", line_ending_strings[lf->line_endings]);
        
        result.lf = lf;
    }
    
    return result;
}

internal Anchor *
LFCreateAnchor(LoadedFile *lf, Cursor *cursor, u32 *line)
{
    AnchorLink *anchor_link = PushType(&lf->arena, AnchorLink);
    Anchor *anchor = &anchor_link->anchor;
    
    anchor->activated = false;
    anchor->cursor = cursor;
    anchor->line = line;
    
    DL_INSERT_AFTER(lf->anchor_link_sentinel.prev, anchor_link);
    
    return anchor;
}

internal CursorRange
LFDeleteText(Cursor cursor, u32 line, u32 chars_removed, u32 lines_deleted, void *params)
{
    LoadedFile *lf = (LoadedFile *)params;
    AnchorLink *first_anchor_link = lf->anchor_link_sentinel.next;
    for(AnchorLink *anchor_link = first_anchor_link;
        anchor_link != &lf->anchor_link_sentinel;
        anchor_link = anchor_link->next)
    {
        Anchor *anchor = &anchor_link->anchor;
        
        if(*anchor->cursor > cursor && *anchor->cursor < cursor + chars_removed)
        {
            anchor->line[0] = line;
            anchor->cursor[0] = cursor;
        }
        else if(*anchor->cursor > cursor)
        {
            anchor->line[0] -= lines_deleted;
            anchor->cursor[0] -= chars_removed;
        }
    }
    
    // 1. any completely enclosed token gets deleted
    Token *cursor_token = GetTokenAtCursor(lf, cursor);
    Token *first_token;
    if(!cursor_token || cursor != cursor_token->pos)
    {
        first_token = GetNextTokenOrNearestForward(lf, cursor);
    }
    else
    {
        first_token = cursor_token;
    }
    
    Token *last_token = GetPreviousTokenOrNearestBackward(lf, cursor + chars_removed);
    
    if(first_token &&
       last_token &&
       first_token->pos <= last_token->pos)
    {
        u32 num_delete_tokens = CountTokensBetween(lf, first_token, last_token) + 1;
        DeleteTokens(lf, first_token, num_delete_tokens);
    }
    
    // 2. a) if there is a partially enclosed token at
    //       the start, its length gets trimmed by
    //       the number of removed characters that
    //       overlap the token
    
    Token *start_touching_token = GetTokenAtCursor(lf, cursor - 1);
    if(start_touching_token)
    {
        u32 num_overlap_chars = Min(cursor + chars_removed, TokenOnePastLastCharacter(start_touching_token)) - cursor;
        start_touching_token->len -= num_overlap_chars;
    }
    
    //    b) if there is a partially enclosed token at
    //       the end and it is not the same as the start token,
    //       it gets truncated so that its position moves up
    //       to the character the deleted character and its length is decreased
    
    Token *end_touching_token = GetTokenAtCursor(lf, cursor + chars_removed);
    if(end_touching_token &&
       start_touching_token != end_touching_token)
    {
        u32 new_pos = cursor + chars_removed;
        end_touching_token->len -= new_pos - end_touching_token->pos;
        end_touching_token->pos = new_pos;
    }
    
    // 3. everything after the old last deleted, or
    //    if there is no deleted tokens, after the start
    //    partially enclosed token, and if there is start
    //    partially enclosed token, after the start
    //    cursor, gets shifted up
    ShiftTokensAheadCursor(lf, cursor, -chars_removed);
    
    
    // 4. reparse the start partially enclosed token to the end of
    //    the last partially enclosed token, if there 
    //    is none, reparse the partially enclosed token
    //    at the end in its new shifted position, if there
    //    is none, don't reparse anything except when there
    //    is a line change, in which case reparse the previous token.
    //    (this is done because we MIGHT have backspaced into a single
    //    line comment)
    // 
    //    NOTE that the start and end partially enclosed tokens are
    //         any touching tokens even if they don't get truncated
    
	// TODO: refactor this so that there is just one call to ReparseCharacters?
	CursorRange range = {};
	
    if(start_touching_token)
    {
        u32 end;
        if(end_touching_token)
        {
            end = end_touching_token->pos + end_touching_token->len;
        }
        else
        {
            end = start_touching_token->pos + start_touching_token->len;
        }
		
        range.min = start_touching_token->pos;
		u32 chars_changed = ReparseCharacters(lf, start_touching_token->pos, end - start_touching_token->pos);
		range.max = range.min + chars_changed - 1;

		// TODO: Do we need to uncomment these (buggy)lines for some performance gain?
		//       i.e., Is the minimimal amount of stuff being redrawn?
		// Assert(range.max >= chars_removed && range.max - chars_removed >= range.min);
		// range.max -= chars_removed;
	}
    else if(end_touching_token)
    {
        range.min = end_touching_token->pos;
		u32 chars_changed = ReparseCharacters(lf, end_touching_token->pos, end_touching_token->len);
		range.max = range.min + chars_changed - 1;
		
		// TODO: Do we need to uncomment these (buggy) lines for some performance gain?
		// Assert(range.max >= chars_removed && range.max - chars_removed >= range.min);
		// range.max -= chars_removed;
	}
    else if(lines_deleted)
    {
        Token *previous_token = GetPreviousTokenOrNearestBackward(lf, cursor);
		if (previous_token)
        {
            u32 chars_changed = ReparseCharacters(lf, previous_token->pos, previous_token->len);
            range.min = previous_token->pos;
			range.max = previous_token->pos + chars_changed - 1;
			
			// TODO: Do we need to uncomment these (buggy) lines for some performance gain?
			// Assert(range.max >= chars_removed && range.max - chars_removed >= range.min);
			// range.max -= chars_removed;
		}
    }
	
	return range;
}

// TODO: make this a typedef'ed function prototype in render_text.h?
internal Color
LF_GetSyntaxColor(Cursor cursor, void *params)
{
    TIMED_BLOCK;
    Color result;
    
    LoadedFile *lf = (LoadedFile *) params;

    Token *token = GetTokenAtCursor(lf, cursor);
    if(token)
    {
        if(lf->file_type == FileType_CPP)
        {
            CPPToken *cpp_token = (CPPToken *)token;
            CPPTokenType basic_type = (CPPTokenType)(cpp_token->type & CPP_TOKEN_BASIC_TYPE);
            switch(basic_type)
            {
                case CPPToken_identifier:
                {
                    if(cpp_token->type & CPPToken_is_preprocessing_directive_keyword)
                    {
                        result = rgb(0.2f, 0.8f, 0.3f);
                    }
                    else if (cpp_token->type & CPPToken_is_normal_keyword)
                    {
                        result = rgb(0.8f, 0.2f, 0.3f);
                    }
                    else
                    {
                        result = rgb(1.0f, 0.9f, 0.7f);
                    }
                } break;
                case CPPToken_number:
                result = rgb(0.5f, 1.0f, 1.0f);
                break;
                case CPPToken_char_literal:
                result = rgb(0.6f, 1.0f, 0.05f);
                break;
                case CPPToken_string:
                result = rgb(1.0f, 0.6f, 0.05f);
                break;
                case CPPToken_comment:
                result = CLR_GREY50;
                break;
                case CPPToken_open_paren:
                case CPPToken_close_paren:
                result = rgb(0.2, 1.0f, 0.4f);
                break;
                case CPPToken_preprocessing_start:
                result = rgb(0.8f, 0.6f, 1.0f);
                break;
                case CPPToken_unknown:
                result = rgb(1.0f, 0.0f, 0.0f);
                break;
                default:
                result = rgb(1.0f, 1.0f, 1.0f);
            }
        }
        else
        {
            // this a file type other than File_CPP
            result = CLR_WHITE;
        }
    }
    else
    {
        // there is no token here
        result = CLR_MAGENTA;
    }
    
    return result; 
}

