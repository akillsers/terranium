#include "common.h"
#include "storage.h"

#define TIMED_BLOCK

struct PlatformAPI
{
    void *(*AllocateMemory) (u64 size_required);
    void (*FreeMemory) (void *ptr);
    
    MemoryArena frame_arena;
};

PlatformAPI platform;

#include "storage.cpp"

#include "string.cpp"
#include "bidi.h"
#include "bidi.cpp"

#include <stdio.h>
#include <malloc.h>
#include <string.h> //for strcpy

#include <stdlib.h> //for strtol

internal void
DisplayUsage()
{
    printf("Tool for using the data in BidiCharacterTest.txt to test Terranium's bidi algorithm.\n");
    printf("BidiCharacterTest.txt is included in the Unicode Character Database.\n");
    printf("Usage: the first argument should be the path to BidiCharacterTest.txt.\n");
}

enum TokenType
{
    Token_unknown,
    Token_newline,
    Token_value,
    Token_semicolon,
    Token_EOF,
};

struct Token
{
    TokenType type;
    char *start;
    unsigned int len;
};

struct Lexer
{
    char *at;
};

Token
GetNextToken(Lexer *lexer)
{
    Token result = {};

    for(;;)
    {
        if(*lexer->at && *lexer->at == '#')
        {
            while(*lexer->at && *lexer->at != '\n')
            {
                lexer->at++;
            }
        }
        else if(*lexer->at == ' ' ||
                *lexer->at == '\t')
        {
            lexer->at++;
        }
        else
        {
            break;
        }
    }

    if(*lexer->at == 0)
    {
        result.type = Token_EOF;
    }
    else if(*lexer->at == ';')
    {
        result.type = Token_semicolon;
        lexer->at++;
    }
    else if(*lexer->at == '\r')
    {
        lexer->at++;
        if(*lexer->at == '\n')
        {
            result.type = Token_newline;
            lexer->at++;
        }
    }
    else if(*lexer->at == '\n')
    {
        result.type = Token_newline;
        lexer->at++;
    }
    else
    {
        result.type = Token_value;
        result.start = lexer->at;

        while(*lexer->at && 
              *lexer->at != ' ' &&
              *lexer->at != ';' &&
              *lexer->at != '\n')
        {
            lexer->at++;
        }

        result.len = lexer->at - result.start;
    }

    return result;
}

// internal i32
// StringToI32(char *start, u32 len, u32 base)
// {
//     i32 result;
//     TemporaryMemory temp_mem = BeginTemporaryMemory(&platform.frame_arena);
//     char *c_str = PushCount(&platform.frame_arena, char, len + 1 + 2);

//     strncpy(c_str + 2, start, len);
//     c_str[0] = '0';
//     c_str[1] = 'x';
//     c_str[len + 2] = 0;

//     result = strtol(c_str, 0, base)
//     EndTemporaryMemory(temp_mem);
//     return result;
// }

internal u32
StringToU32(char *start, u32 len, u32 base)
{
    u32 result = 0;

    char *at = start + len - 1;
    u32 power = 0;
    for(;;)
    {
        u32 digit = 0;
        if(*at >= '0' && *at <= '9')
        {
            digit = *at - '0';
        }
        else if(*at >= 'a' && *at <= 'f')
        {
            digit = *at - 'a' + 10;
        }
        else if(*at >= 'A' && *at <= 'F')
        {
            digit = *at - 'A' + 10;
        }
        else
        {
            Assert(false);
        }

        if(power == 0)
        {
            result += digit;
        }
        else
        {
            u32 digit_value = digit;

            for(int i = 0;
                i < power;
                i++)
            {
                digit_value *= base;
            }

            result += digit_value;
        }

        if(at == start)
        {
            break;
        }
        
        power++;
        at--;
    }

    return result;    
}

internal void
RunCharacterTests(char *test_data_path)
{
    FILE *test_data_file = fopen(test_data_path, "rb");
    if(test_data_file)
    {
        int file_size;

        fseek(test_data_file, 0, SEEK_END);
        file_size = ftell(test_data_file);
        rewind(test_data_file);

        char *test_data = (char *)malloc(file_size + 1);
        fread(test_data, file_size, 1, test_data_file);
        test_data[file_size] = 0;
        printf("Initiating BIDI character test...\n");

        Lexer lexer = {};
        lexer.at = test_data;

        Token token = {};

#define MAX_TEST_CASE_CODEPOINTS 256

        UTF32 test_case_codepoints[MAX_TEST_CASE_CODEPOINTS];
        u32 resolved_levels[MAX_TEST_CASE_CODEPOINTS];

        u32 num_codepoints_in_case = 0;
        u32 num_resolved_levels;
        
        u32 one_past_test_index = 0;
        u32 line_num = 1;
        
        for(;;)
        {
            token = GetNextToken(&lexer);

            if(token.type == Token_newline)
            {
                line_num++;
            }
            else if(token.type == Token_value)
            {
                one_past_test_index++;
                num_resolved_levels = 0;
                num_codepoints_in_case = 0;
                
                u32 error_encountered = false;
                
                while(token.type != Token_semicolon)
                {
                    if(num_codepoints_in_case < MAX_TEST_CASE_CODEPOINTS)
                    {
                        test_case_codepoints[num_codepoints_in_case] = StringToU32(token.start, token.len, 16);
                        num_codepoints_in_case++;
                    }
                    else
                    {
                        fprintf(stderr, "Error on line %u: the number of codepoints in this test case exceeds the maximum capacity!\n",
                                line_num);
                        fprintf(stderr, "       skipping to next case...\n");
                        
                        error_encountered = true;
                        break;
                    }
                    token = GetNextToken(&lexer);
                }

                if(error_encountered)
                {
                    continue;
                }
                
                token = GetNextToken(&lexer);
                if(token.type != Token_value)
                {
                    fprintf(stderr, "Error on line %u: expected a number in field 1 of test case denoting the paragraph embedding level!\n", line_num);
                    fprintf(stderr, "       *note that field numbers are zero-based\n\n");

                    fprintf(stderr, "       skipping to next case...\n");
                    continue;
                }
                
                u32 paragraph_embedding_level = StringToU32(token.start, token.len, 10);
                if(paragraph_embedding_level == 2)
                {
                    paragraph_embedding_level = BIDI_COMPUTE_PARAGRAPH_EMBEDDING;
                }

                token = GetNextToken(&lexer);
                if(token.type != Token_semicolon)
                {
                    fprintf(stderr, "Error on line %u: expected a semicolon after field 1 of test case!\n", line_num);
                    fprintf(stderr, "       *note that field numbers are zero-based\n\n");

                    fprintf(stderr, "       skipping to next case...\n");
                    continue;
                }

                u32 resolved_paragraph_embedding_level = 0;
                token = GetNextToken(&lexer);
                Assert(token.type == Token_value);

                resolved_paragraph_embedding_level = StringToU32(token.start, token.len, 10);
                token = GetNextToken(&lexer);
                Assert(token.type == Token_semicolon);
                                                                 
                token = GetNextToken(&lexer);
                while(token.type == Token_value)
                {
                    Assert(num_resolved_levels < MAX_TEST_CASE_CODEPOINTS);
                    if(*token.start == 'x')
                    {
                        resolved_levels[num_resolved_levels] = CHARACTER_REMOVED_IN_RULE_X9;
                    }
                    else
                    {
                        resolved_levels[num_resolved_levels] = StringToU32(token.start, token.len, 10);
                    }
                        
                    num_resolved_levels++;
                    token = GetNextToken(&lexer);
                }

                if(num_resolved_levels != num_codepoints_in_case)
                {
                    fprintf(stderr, "Error on line %u: the number of resolved levels is different from the codepoitns in the case!\n", line_num);
                    fprintf(stderr, "       *note that field numbers are zero-based\n\n");

                    fprintf(stderr, "       skipping to next case...\n");
                    continue;
                }
                
                UTF32_String test_string = {};
                test_string.ptr = test_case_codepoints;
                test_string.len = num_codepoints_in_case;
                    
                printf("testing case %u on line %u...\n", one_past_test_index, line_num);
                BIDICharacter_String char_infos = ProcessBIDI_UTF32(test_string, paragraph_embedding_level, &platform.frame_arena);

                if(num_codepoints_in_case != char_infos.len)
                {
                    fprintf(stderr, "Error on line %u: the number of input charcters does not match the number of characters the BIDI algorithm generated!\n", line_num);

                    fprintf(stderr, "       skipping to next case...\n");
                    continue;
                }

                b32 failed = false;

                for(int i = 0;
                    i < char_infos.len;
                    i++)
                {
                    u32 expected = resolved_levels[i];
                    u32 actual = char_infos.ptr[i].embedding_level;

                    if(expected != actual)
                    {
                        failed = true;
                        break;
                    }
                }
                
                if(failed)
                {
                    printf("CASE FAILED on line %u!\n", line_num);
                    printf("Expected | Actual\n");
                    for(int i = 0;
                        i < char_infos.len;
                        i++)
                    {
                        u32 expected = resolved_levels[i];
                        u32 actual = char_infos.ptr[i].embedding_level;

                        if(expected == CHARACTER_REMOVED_IN_RULE_X9)
                        {
                            printf("REMOVED  |  %u\n", actual);
                        }
                        else
                        {
                            printf("%u       |  %u\n", expected, actual);
                        }
                    }

                    Assert(false);
                }
                else
                {
                    printf("Case %u passed on line %u.\n", one_past_test_index, line_num);
                }

                // skip to the end of the line (skips field 4 of the test case
                token = GetNextToken(&lexer);
                while(token.type != Token_newline &&
                      token.type != Token_EOF)
                {
                    token = GetNextToken(&lexer);
                }

                if(token.type == Token_newline)
                {
                    line_num++;
                }
            }
            else if(token.type == Token_EOF)
            {
                printf("All tests done. \n");
                break;
            }
            else
            {
                fprintf(stderr, "Expected a newline or value token on line %u!\n", line_num);
            }
        }
    }
    else
    {
        fprintf(stderr, "Could not open the file %s\n", test_data_path);
    }
}

int
main(int arg_count, char *args[])
{
    platform.AllocateMemory = malloc;
    platform.FreeMemory = free;
    
    if(arg_count > 1)
    {
        RunCharacterTests(args[1]);
    }
    else
    {
        DisplayUsage();
    }
}
