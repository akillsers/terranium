internal void
_D3D11_ResizeStagingTexture(D3D11_Data *data, u32 width, u32 height)
{
    HRESULT hr;
    if(data->staging_texture)
    {
        // Recreate the staging texture to match the size of the backbuffer
        g_win32.d3d11->device_context->Unmap((ID3D11Resource *)data->staging_texture, 0);
        data->staging_texture->Release();
        data->staging_texture = 0;
    }

    g_win32.d3d11->device_context->IASetInputLayout(data->input_layout);
    D3D11_TEXTURE2D_DESC staging_texture_desc = {};
    staging_texture_desc.Width = width;
    staging_texture_desc.Height = height;
    // Is this how you "disable" mipmapping?
    staging_texture_desc.MipLevels = 1;
    staging_texture_desc.ArraySize = 1;
    // NOTE:: this should be the same as the swap chain buffer format
    staging_texture_desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    staging_texture_desc.SampleDesc = {1, 0};
    staging_texture_desc.Usage = D3D11_USAGE_STAGING;
    staging_texture_desc.BindFlags = 0;
    staging_texture_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
    
    hr = g_win32.d3d11->device->CreateTexture2D(&staging_texture_desc, 0, &data->staging_texture);
    Assert(SUCCEEDED(hr));
    // NOTE:: permanently map the stagin texture
    hr = g_win32.d3d11->device_context->Map(data->staging_texture, 0, D3D11_MAP_READ_WRITE, 0 /*MapFlags*/, &data->staging_texture_mapped);
    Assert(SUCCEEDED(hr));
}

internal void
D3D11_CreateSwapChain(WIN_WindowData *window)
{
    HRESULT hr;

    // TODO have proper version detection
    b32 support_flip_sequential = IsWindows8OrGreater();
    b32 support_frame_waitable_object = 1;
    b32 support_flip_discard = 0;

    b32 direct_composition = false;


    // TODO if we are creating and destroying a lot of windows, this
    //      lack of memory reclamation is a problem
    window->d3d11_data = PushType(&g_win32.permanent_arena, D3D11_Data);
    
	Recti client_rect = WIN_GetClientRect(window);
	
    window->d3d11_data->swap_chain_desc = {};
    window->d3d11_data->swap_chain_desc.Width = client_rect.width;
    window->d3d11_data->swap_chain_desc.Height = client_rect.height;
    window->d3d11_data->swap_chain_desc.SampleDesc = { 1,0 };
    window->d3d11_data->swap_chain_desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM; // TODO is this the right one??
    window->d3d11_data->swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_SHADER_INPUT; // TODO is the necessary?

    window->d3d11_data->swap_chain_desc.Stereo = false;
    
    if(support_flip_sequential)
    {
        window->d3d11_data->swap_chain_desc.BufferCount = 2; // TODO try with one for lower latency?
        window->d3d11_data->swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL; 
        window->d3d11_data->swap_chain_desc.Flags = (support_frame_waitable_object) ? DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT : 0;
        window->d3d11_data->swap_chain_desc.Scaling = DXGI_SCALING_NONE;
    }
    else
    {
        // flip is not supported in windows 7
        // TODO BUT it IS supported in d3d9 using FLIPEX
        window->d3d11_data->swap_chain_desc.BufferCount = 1; // TODO try with one for lower latency?
        window->d3d11_data->swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
        window->d3d11_data->swap_chain_desc.Flags = 0;
    }

    // TODO should we just delete this direct composition code?
    if(direct_composition)
    {
        window->d3d11_data->swap_chain_desc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
        
        hr = g_win32.d3d11->factory->CreateSwapChainForComposition(g_win32.d3d11->device,
                                                          &window->d3d11_data->swap_chain_desc,
                                                          0, //pRestrictToOutput
                                                          &window->d3d11_data->swap_chain);
        Assert(SUCCEEDED(hr));
    }
    else
    {
        window->d3d11_data->swap_chain_desc.AlphaMode = DXGI_ALPHA_MODE_IGNORE;

    
        hr = g_win32.d3d11->factory->CreateSwapChainForHwnd(g_win32.d3d11->device,
                                                   window->hwnd,
                                                   &window->d3d11_data->swap_chain_desc,
                                                   0, // pFullscreenDesc
                                                   0, // pRestrictToOutput
                                                   &window->d3d11_data->swap_chain);
        Assert(SUCCEEDED(hr));
    }

    IDXGISwapChain2 *dxgi_swap_chain2;
    if(window->d3d11_data->swap_chain_desc.Flags & DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT &&
       SUCCEEDED(window->d3d11_data->swap_chain->QueryInterface(__uuidof(IDXGISwapChain2), (void **)&dxgi_swap_chain2)))
    {
        g_win32.d3d11->frame_latency_waitable_object = dxgi_swap_chain2->GetFrameLatencyWaitableObject();
        dxgi_swap_chain2->SetMaximumFrameLatency(1);
        dxgi_swap_chain2->Release();
    }
    
    Assert(SUCCEEDED(hr));

    // hr = window->d3d11_data->swap_chain->GetContainingOutput(&g_win32.d3d11->output);
    // Assert(SUCCEEDED(hr));
    
    hr = window->d3d11_data->swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void **)&window->d3d11_data->backbuffer);
    Assert(SUCCEEDED(hr));
    
    hr = g_win32.d3d11->device->CreateRenderTargetView(window->d3d11_data->backbuffer, 0, &window->d3d11_data->backbuffer_view);
    Assert(SUCCEEDED(hr));

    g_win32.d3d11->device->CreateVertexShader(g_vs_main, sizeof(g_vs_main), 0, &window->d3d11_data->vertex_shader);
    D3D11_INPUT_ELEMENT_DESC il_xyz_uv_rgba[] = {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0 /*offset*/, D3D11_INPUT_PER_VERTEX_DATA, 0}, // position
        {"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0}, // uv
        {"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0}, // uv
    };
    
    g_win32.d3d11->device->CreateInputLayout(il_xyz_uv_rgba, ArrayCount(il_xyz_uv_rgba), g_vs_main, sizeof(g_vs_main), &window->d3d11_data->input_layout);

    g_win32.d3d11->device->CreatePixelShader(g_ps_main, sizeof(g_ps_main), 0, &window->d3d11_data->pixel_shader);

    D3D11_BUFFER_DESC vertex_buffer_desc = {};
    f32 vertex_data[] = {
        // clockwise starting top left
        // position,            texcoord,                 color
        -1.0f, -1.0f, 0.5f,     0.0f, 1.0f,               1.0f, 1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 0.5f,      0.0f, 0.0f,               1.0f, 1.0f, 1.0f, 1.0f,
        1.0f,  -1.0f, 0.5f,     1.0f, 1.0f,               1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 0.5f,       1.0f, 0.0f,               1.0f, 1.0f, 1.0f, 1.0f,
    };
    
    vertex_buffer_desc.ByteWidth = sizeof(vertex_data);
    vertex_buffer_desc.Usage = D3D11_USAGE_IMMUTABLE;
    vertex_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

    D3D11_SUBRESOURCE_DATA vertex_initial_data = {vertex_data};
    g_win32.d3d11->device->CreateBuffer(&vertex_buffer_desc, &vertex_initial_data, &window->d3d11_data->vertex_buffer);

    D3D11_TEXTURE2D_DESC texture_desc = {};

	texture_desc.Width = client_rect.width;
    texture_desc.Height = client_rect.height;
    // Is this how you "disable" mipmapping?
    texture_desc.MipLevels = 1;
    texture_desc.ArraySize = 1;
    // NOTE:: this should be the same as the swap chain buffer format
    texture_desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    texture_desc.SampleDesc = {1, 0};
    // TODO is this the right usage?
    texture_desc.Usage = D3D11_USAGE_DEFAULT;
    texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    texture_desc.CPUAccessFlags = 0;

    hr = g_win32.d3d11->device->CreateTexture2D(&texture_desc, 0, &window->d3d11_data->draw_texture);
    Assert(SUCCEEDED(hr));

    hr = g_win32.d3d11->device->CreateShaderResourceView(window->d3d11_data->draw_texture, 0, &window->d3d11_data->texture_view);
    Assert(SUCCEEDED(hr));

    // we set the viewport y to the top of the window rect
    // because the non client area y is 0 because of the removal of the titlebar
    // window->d3d11_data->viewport = {100.0f, 100.0f, (FLOAT)window->d3d11_data->width, (FLOAT)window->d3d11_data->height - 100.0f, 0.0f, 1.0f};
    window->d3d11_data->viewport = {0.0f, 0.0f, (FLOAT)client_rect.width, (FLOAT)client_rect.height, 0.0f, 1.0f};
    
    D3D11_SAMPLER_DESC sampler_desc = {};
    sampler_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampler_desc.MinLOD = 0;
    sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;
    sampler_desc.ComparisonFunc = D3D11_COMPARISON_NEVER;

    hr = g_win32.d3d11->device->CreateSamplerState(&sampler_desc, &window->d3d11_data->sampler_state);
    Assert(SUCCEEDED(hr));

    D3D11_BLEND_DESC blend_desc = {};
    blend_desc.AlphaToCoverageEnable = false;
    blend_desc.IndependentBlendEnable = false;

    blend_desc.RenderTarget[0].BlendEnable = true; 
    blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    blend_desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
    blend_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

    // blend_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    // blend_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blend_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

    
    hr = g_win32.d3d11->device->CreateBlendState(&blend_desc, &window->d3d11_data->blend_state);
    Assert(SUCCEEDED(hr));
    
    _D3D11_ResizeStagingTexture(window->d3d11_data, client_rect.width, client_rect.height);
}

internal void
D3D11_UpdateFramebuffer(WIN_WindowData *window)
{
    HRESULT hr;

    // TODO there probably is an easier way to just blit a texture onto the swapchain's backbuffer and present it
    //     when I find what that method is I will facepalm because right now we're going thru SOOO much trouble
    //     (writing shaders...etc. just to blit it to the screen)

    WIN_UpdateFramebuffer(window,
                          window->d3d11_data->staging_texture_mapped.pData,
                          window->d3d11_data->staging_texture_mapped.RowPitch,
                          PixelFormat_B8G8R8A8);

    // intenionally left commented:
    // our device context is left permanently mapped
    // g_win32.d3d11->device_context->Unmap(window->d3d11_data->staging_texture, 0);

    // TODO only update a portion of the draw texture
    g_win32.d3d11->device_context->CopyResource(window->d3d11_data->draw_texture, window->d3d11_data->staging_texture);
    g_win32.d3d11->device_context->PSSetShaderResources(0, 1, &window->d3d11_data->texture_view);
    
    // TODO research if deffered drawing will make this more efficient?
    // TODO I don't think we need to clear it if we're going to be drawing the same exact thing?
    f32 background_color[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    g_win32.d3d11->device_context->ClearRenderTargetView(window->d3d11_data->backbuffer_view, background_color);
    g_win32.d3d11->device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
    g_win32.d3d11->device_context->Draw(4, 0);
    g_win32.d3d11->device_context->Flush();
}

internal void
D3D11_ResizeSwapChain(WIN_WindowData *window)
{
    // TODO check if these values ever get triggered
    if(!g_win32.d3d11->device)
        return;
    
	Recti client_rect = WIN_GetClientRect(window);
	
	if(client_rect.width == 0 || client_rect.height == 0)
        return;

    HRESULT hr;

    // NOTE:: this should be the same as when we created the swap chain

    if(window->d3d11_data->backbuffer)
    {
        window->d3d11_data->backbuffer->Release();
        window->d3d11_data->backbuffer_view->Release();
        // g_win32.d3d11->device_context->OMSetRenderTargets(0, 0, 0);
        
        window->d3d11_data->backbuffer = 0;
    }
    UINT flags = 0;
    if(g_win32.d3d11->frame_latency_waitable_object)
    {
        flags |= DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT;
    }
    hr = window->d3d11_data->swap_chain->ResizeBuffers(0/*BufferCount*/,
                                         0 /*width*/,
                                         0/*height*/,
                                         DXGI_FORMAT_UNKNOWN,
                                         flags /*flags*/);
    IDXGISwapChain2 *dxgi_swap_chain2;
    if(window->d3d11_data->swap_chain_desc.Flags & DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT &&
       SUCCEEDED(window->d3d11_data->swap_chain->QueryInterface(__uuidof(IDXGISwapChain2), (void **)&dxgi_swap_chain2)))
    {
        g_win32.d3d11->frame_latency_waitable_object = dxgi_swap_chain2->GetFrameLatencyWaitableObject();
        dxgi_swap_chain2->SetMaximumFrameLatency(1);
        dxgi_swap_chain2->Release();
    }
    Assert(SUCCEEDED(hr));
    hr = window->d3d11_data->swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void **)&window->d3d11_data->backbuffer);
    Assert(SUCCEEDED(hr));
    hr = g_win32.d3d11->device->CreateRenderTargetView(window->d3d11_data->backbuffer, 0, &window->d3d11_data->backbuffer_view);
    Assert(SUCCEEDED(hr));

    // We need to recreate the draw texture since the draw texture dimensions
    // no longer match the backbuffer dimensions
    if(window->d3d11_data->draw_texture)
    {
        window->d3d11_data->draw_texture->Release();
        window->d3d11_data->texture_view->Release();
        window->d3d11_data->draw_texture = 0;
    }

    D3D11_TEXTURE2D_DESC texture_desc = {};
    texture_desc.Width = client_rect.width;
    texture_desc.Height = client_rect.height;
    // Is this how you "disable" mipmapping?
    texture_desc.MipLevels = 1;
    texture_desc.ArraySize = 1;
    // NOTE:: this should be the same as the swap chain buffer format
    texture_desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    texture_desc.SampleDesc = {1, 0};
    // TODO is this the right usage?
    texture_desc.Usage = D3D11_USAGE_DEFAULT;
    texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    texture_desc.CPUAccessFlags = 0;

    hr = g_win32.d3d11->device->CreateTexture2D(&texture_desc, 0, &window->d3d11_data->draw_texture);
    Assert(SUCCEEDED(hr));

    hr = g_win32.d3d11->device->CreateShaderResourceView(window->d3d11_data->draw_texture, 0, &window->d3d11_data->texture_view);
    Assert(SUCCEEDED(hr));
    
    _D3D11_ResizeStagingTexture(window->d3d11_data, client_rect.width, client_rect.height);

    window->d3d11_data->viewport.Width = client_rect.width;
    window->d3d11_data->viewport.Height = client_rect.height;
    
    // TODO COM garbage collection is making it so that when you resize
    //     the relevant stuff getting released doesn't immeidately get released and reflected in the
    //     memory window of taskmgr

    // NOTE:: fixes bug with texture recreation on intel graphics 4400
    g_win32.d3d11->device_context->Flush();
}

internal void
D3D11_CreateResizeSwapChain(WIN_WindowData *window)
{
	if(!window->d3d11_data)
	{
		D3D11_CreateSwapChain(window);
	}
	else
	{
		D3D11_ResizeSwapChain(window);
	}
}

internal void
D3D11_Present(WIN_WindowData *window)
{
    g_win32.animating = (window->framebuffer->dirty_rects_used > 0);

    if(g_win32.animating)
    {
        if(g_win32.d3d11->frame_latency_waitable_object)
        {
            DWORD wait_result = WaitForSingleObjectEx(g_win32.d3d11->frame_latency_waitable_object, 10000, true);
            if(wait_result != WAIT_OBJECT_0)
            {
                // TODO logging
            
            }
        }

        u32 stride = 9 * sizeof(f32);
        u32 offset = 0;
        g_win32.d3d11->device_context->IASetInputLayout(window->d3d11_data->input_layout);
        g_win32.d3d11->device_context->IASetVertexBuffers(0, 1, &window->d3d11_data->vertex_buffer, &stride, &offset /*offset*/);
        g_win32.d3d11->device_context->RSSetViewports(1, &window->d3d11_data->viewport);
        g_win32.d3d11->device_context->PSSetShader(window->d3d11_data->pixel_shader, 0, 0);
        g_win32.d3d11->device_context->PSSetShaderResources(0, 1, &window->d3d11_data->texture_view);
        g_win32.d3d11->device_context->PSSetSamplers(0, 1, &window->d3d11_data->sampler_state);
        g_win32.d3d11->device_context->VSSetShader(window->d3d11_data->vertex_shader, 0, 0);
        g_win32.d3d11->device_context->OMSetRenderTargets(1, &window->d3d11_data->backbuffer_view, 0);
        g_win32.d3d11->device_context->OMSetBlendState(window->d3d11_data->blend_state, 0, 0xffffffff);

        PERF_START(update_framebuffer);
        D3D11_UpdateFramebuffer(window);
        PERF_END(update_framebuffer);

        PERF_START(present);

        window->d3d11_data->swap_chain->Present(1, 0 /*flags*/);

        // The following is necessary only for sequential presentation models
        g_win32.d3d11->device_context->OMSetRenderTargets(1, &window->d3d11_data->backbuffer_view, 0);

        PERF_END(present);

        // TODO ugly and awkward out of place
        if(g_win32.editor_memory.debug_info)
        {
            g_win32.editor_memory.debug_info->seconds_for_copy_framebuffer = (f64)PERF_update_framebuffer / g_win32.perf_freq;
            g_win32.editor_memory.debug_info->seconds_for_present = (f64)PERF_present/ g_win32.perf_freq;
        }

        window->framebuffer->dirty_rects_used = 0;
    }
}

internal void
D3D11_Init()
{
    HRESULT hr;

    Assert(g_win32.d3d11 == 0);
    Assert(g_win32.swap_chain_backend == WIN_SwapChain_null);

    g_win32.CreateResizeSwapChain = D3D11_CreateResizeSwapChain;
    g_win32.Present = D3D11_Present;
    
    g_win32.d3d11 = PushType(&g_win32.permanent_arena, D3D11_DeviceData);
    
    hr = CreateDXGIFactory(__uuidof(IDXGIFactory2), (void **)&g_win32.d3d11->factory);
    Assert(SUCCEEDED(hr));

    // D3D_FEATURE_LEVEL feature_level = D3D_FEATURE_LEVEL_9_1;
    hr = D3D11CreateDevice(NULL, // pAdapter
                           D3D_DRIVER_TYPE_HARDWARE,
                           NULL, // Software
                           D3D11_CREATE_DEVICE_DEBUG, // flags
                           0,
                           0,
                           D3D11_SDK_VERSION,
                           &g_win32.d3d11->device,
                           0, // returned feature level
                           &g_win32.d3d11->device_context);

    Assert(SUCCEEDED(hr));
}

