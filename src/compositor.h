#define COMPOSITOR_FRAMEBUFFER_BYTESPP 4
struct CompositionLayer
{
    i32 z_index;
    Framebuffer *framebuffer;

    Vec2i pos;
    
    CompositionLayer *next;
    CompositionLayer *prev;
};

struct Compositor
{
    MemoryArena arena;
    CompositionLayer *sentinel_layer;
};

