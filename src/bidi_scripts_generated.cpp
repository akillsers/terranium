BIDIScript
GetCodepointBIDIScript(UTF32 codepoint)
{
BIDIScript result = BIDIScript_unknown;

if(codepoint >= 0x0000 && codepoint <= 0x001F ||
codepoint == 0x0020 ||
codepoint >= 0x0021 && codepoint <= 0x0023 ||
codepoint == 0x0024 ||
codepoint >= 0x0025 && codepoint <= 0x0027 ||
codepoint == 0x0028 ||
codepoint == 0x0029 ||
codepoint == 0x002A ||
codepoint == 0x002B ||
codepoint == 0x002C ||
codepoint == 0x002D ||
codepoint >= 0x002E && codepoint <= 0x002F ||
codepoint >= 0x0030 && codepoint <= 0x0039 ||
codepoint >= 0x003A && codepoint <= 0x003B ||
codepoint >= 0x003C && codepoint <= 0x003E ||
codepoint >= 0x003F && codepoint <= 0x0040 ||
codepoint == 0x005B ||
codepoint == 0x005C ||
codepoint == 0x005D ||
codepoint == 0x005E ||
codepoint == 0x005F ||
codepoint == 0x0060 ||
codepoint == 0x007B ||
codepoint == 0x007C ||
codepoint == 0x007D ||
codepoint == 0x007E ||
codepoint >= 0x007F && codepoint <= 0x009F ||
codepoint == 0x00A0 ||
codepoint == 0x00A1 ||
codepoint >= 0x00A2 && codepoint <= 0x00A5 ||
codepoint == 0x00A6 ||
codepoint == 0x00A7 ||
codepoint == 0x00A8 ||
codepoint == 0x00A9 ||
codepoint == 0x00AB ||
codepoint == 0x00AC ||
codepoint == 0x00AD ||
codepoint == 0x00AE ||
codepoint == 0x00AF ||
codepoint == 0x00B0 ||
codepoint == 0x00B1 ||
codepoint >= 0x00B2 && codepoint <= 0x00B3 ||
codepoint == 0x00B4 ||
codepoint == 0x00B5 ||
codepoint >= 0x00B6 && codepoint <= 0x00B7 ||
codepoint == 0x00B8 ||
codepoint == 0x00B9 ||
codepoint == 0x00BB ||
codepoint >= 0x00BC && codepoint <= 0x00BE ||
codepoint == 0x00BF ||
codepoint == 0x00D7 ||
codepoint == 0x00F7 ||
codepoint >= 0x02B9 && codepoint <= 0x02C1 ||
codepoint >= 0x02C2 && codepoint <= 0x02C5 ||
codepoint >= 0x02C6 && codepoint <= 0x02D1 ||
codepoint >= 0x02D2 && codepoint <= 0x02DF ||
codepoint >= 0x02E5 && codepoint <= 0x02E9 ||
codepoint == 0x02EC ||
codepoint == 0x02ED ||
codepoint == 0x02EE ||
codepoint >= 0x02EF && codepoint <= 0x02FF ||
codepoint == 0x0374 ||
codepoint == 0x037E ||
codepoint == 0x0385 ||
codepoint == 0x0387 ||
codepoint == 0x0589 ||
codepoint == 0x0605 ||
codepoint == 0x060C ||
codepoint == 0x061B ||
codepoint == 0x061F ||
codepoint == 0x0640 ||
codepoint == 0x06DD ||
codepoint == 0x08E2 ||
codepoint >= 0x0964 && codepoint <= 0x0965 ||
codepoint == 0x0E3F ||
codepoint >= 0x0FD5 && codepoint <= 0x0FD8 ||
codepoint == 0x10FB ||
codepoint >= 0x16EB && codepoint <= 0x16ED ||
codepoint >= 0x1735 && codepoint <= 0x1736 ||
codepoint >= 0x1802 && codepoint <= 0x1803 ||
codepoint == 0x1805 ||
codepoint == 0x1CD3 ||
codepoint == 0x1CE1 ||
codepoint >= 0x1CE9 && codepoint <= 0x1CEC ||
codepoint >= 0x1CEE && codepoint <= 0x1CF3 ||
codepoint >= 0x1CF5 && codepoint <= 0x1CF6 ||
codepoint == 0x1CF7 ||
codepoint == 0x1CFA ||
codepoint >= 0x2000 && codepoint <= 0x200A ||
codepoint == 0x200B ||
codepoint >= 0x200E && codepoint <= 0x200F ||
codepoint >= 0x2010 && codepoint <= 0x2015 ||
codepoint >= 0x2016 && codepoint <= 0x2017 ||
codepoint == 0x2018 ||
codepoint == 0x2019 ||
codepoint == 0x201A ||
codepoint >= 0x201B && codepoint <= 0x201C ||
codepoint == 0x201D ||
codepoint == 0x201E ||
codepoint == 0x201F ||
codepoint >= 0x2020 && codepoint <= 0x2027 ||
codepoint == 0x2028 ||
codepoint == 0x2029 ||
codepoint >= 0x202A && codepoint <= 0x202E ||
codepoint == 0x202F ||
codepoint >= 0x2030 && codepoint <= 0x2038 ||
codepoint == 0x2039 ||
codepoint == 0x203A ||
codepoint >= 0x203B && codepoint <= 0x203E ||
codepoint >= 0x203F && codepoint <= 0x2040 ||
codepoint >= 0x2041 && codepoint <= 0x2043 ||
codepoint == 0x2044 ||
codepoint == 0x2045 ||
codepoint == 0x2046 ||
codepoint >= 0x2047 && codepoint <= 0x2051 ||
codepoint == 0x2052 ||
codepoint == 0x2053 ||
codepoint == 0x2054 ||
codepoint >= 0x2055 && codepoint <= 0x205E ||
codepoint == 0x205F ||
codepoint >= 0x2060 && codepoint <= 0x2064 ||
codepoint >= 0x2066 && codepoint <= 0x206F ||
codepoint == 0x2070 ||
codepoint >= 0x2074 && codepoint <= 0x2079 ||
codepoint >= 0x207A && codepoint <= 0x207C ||
codepoint == 0x207D ||
codepoint == 0x207E ||
codepoint >= 0x2080 && codepoint <= 0x2089 ||
codepoint >= 0x208A && codepoint <= 0x208C ||
codepoint == 0x208D ||
codepoint == 0x208E ||
codepoint >= 0x20A0 && codepoint <= 0x20BF ||
codepoint >= 0x2100 && codepoint <= 0x2101 ||
codepoint == 0x2102 ||
codepoint >= 0x2103 && codepoint <= 0x2106 ||
codepoint == 0x2107 ||
codepoint >= 0x2108 && codepoint <= 0x2109 ||
codepoint >= 0x210A && codepoint <= 0x2113 ||
codepoint == 0x2114 ||
codepoint == 0x2115 ||
codepoint >= 0x2116 && codepoint <= 0x2117 ||
codepoint == 0x2118 ||
codepoint >= 0x2119 && codepoint <= 0x211D ||
codepoint >= 0x211E && codepoint <= 0x2123 ||
codepoint == 0x2124 ||
codepoint == 0x2125 ||
codepoint == 0x2127 ||
codepoint == 0x2128 ||
codepoint == 0x2129 ||
codepoint >= 0x212C && codepoint <= 0x212D ||
codepoint == 0x212E ||
codepoint >= 0x212F && codepoint <= 0x2131 ||
codepoint >= 0x2133 && codepoint <= 0x2134 ||
codepoint >= 0x2135 && codepoint <= 0x2138 ||
codepoint == 0x2139 ||
codepoint >= 0x213A && codepoint <= 0x213B ||
codepoint >= 0x213C && codepoint <= 0x213F ||
codepoint >= 0x2140 && codepoint <= 0x2144 ||
codepoint >= 0x2145 && codepoint <= 0x2149 ||
codepoint == 0x214A ||
codepoint == 0x214B ||
codepoint >= 0x214C && codepoint <= 0x214D ||
codepoint == 0x214F ||
codepoint >= 0x2150 && codepoint <= 0x215F ||
codepoint == 0x2189 ||
codepoint >= 0x218A && codepoint <= 0x218B ||
codepoint >= 0x2190 && codepoint <= 0x2194 ||
codepoint >= 0x2195 && codepoint <= 0x2199 ||
codepoint >= 0x219A && codepoint <= 0x219B ||
codepoint >= 0x219C && codepoint <= 0x219F ||
codepoint == 0x21A0 ||
codepoint >= 0x21A1 && codepoint <= 0x21A2 ||
codepoint == 0x21A3 ||
codepoint >= 0x21A4 && codepoint <= 0x21A5 ||
codepoint == 0x21A6 ||
codepoint >= 0x21A7 && codepoint <= 0x21AD ||
codepoint == 0x21AE ||
codepoint >= 0x21AF && codepoint <= 0x21CD ||
codepoint >= 0x21CE && codepoint <= 0x21CF ||
codepoint >= 0x21D0 && codepoint <= 0x21D1 ||
codepoint == 0x21D2 ||
codepoint == 0x21D3 ||
codepoint == 0x21D4 ||
codepoint >= 0x21D5 && codepoint <= 0x21F3 ||
codepoint >= 0x21F4 && codepoint <= 0x22FF ||
codepoint >= 0x2300 && codepoint <= 0x2307 ||
codepoint == 0x2308 ||
codepoint == 0x2309 ||
codepoint == 0x230A ||
codepoint == 0x230B ||
codepoint >= 0x230C && codepoint <= 0x231F ||
codepoint >= 0x2320 && codepoint <= 0x2321 ||
codepoint >= 0x2322 && codepoint <= 0x2328 ||
codepoint == 0x2329 ||
codepoint == 0x232A ||
codepoint >= 0x232B && codepoint <= 0x237B ||
codepoint == 0x237C ||
codepoint >= 0x237D && codepoint <= 0x239A ||
codepoint >= 0x239B && codepoint <= 0x23B3 ||
codepoint >= 0x23B4 && codepoint <= 0x23DB ||
codepoint >= 0x23DC && codepoint <= 0x23E1 ||
codepoint >= 0x23E2 && codepoint <= 0x2426 ||
codepoint >= 0x2440 && codepoint <= 0x244A ||
codepoint >= 0x2460 && codepoint <= 0x249B ||
codepoint >= 0x249C && codepoint <= 0x24E9 ||
codepoint >= 0x24EA && codepoint <= 0x24FF ||
codepoint >= 0x2500 && codepoint <= 0x25B6 ||
codepoint == 0x25B7 ||
codepoint >= 0x25B8 && codepoint <= 0x25C0 ||
codepoint == 0x25C1 ||
codepoint >= 0x25C2 && codepoint <= 0x25F7 ||
codepoint >= 0x25F8 && codepoint <= 0x25FF ||
codepoint >= 0x2600 && codepoint <= 0x266E ||
codepoint == 0x266F ||
codepoint >= 0x2670 && codepoint <= 0x2767 ||
codepoint == 0x2768 ||
codepoint == 0x2769 ||
codepoint == 0x276A ||
codepoint == 0x276B ||
codepoint == 0x276C ||
codepoint == 0x276D ||
codepoint == 0x276E ||
codepoint == 0x276F ||
codepoint == 0x2770 ||
codepoint == 0x2771 ||
codepoint == 0x2772 ||
codepoint == 0x2773 ||
codepoint == 0x2774 ||
codepoint == 0x2775 ||
codepoint >= 0x2776 && codepoint <= 0x2793 ||
codepoint >= 0x2794 && codepoint <= 0x27BF ||
codepoint >= 0x27C0 && codepoint <= 0x27C4 ||
codepoint == 0x27C5 ||
codepoint == 0x27C6 ||
codepoint >= 0x27C7 && codepoint <= 0x27E5 ||
codepoint == 0x27E6 ||
codepoint == 0x27E7 ||
codepoint == 0x27E8 ||
codepoint == 0x27E9 ||
codepoint == 0x27EA ||
codepoint == 0x27EB ||
codepoint == 0x27EC ||
codepoint == 0x27ED ||
codepoint == 0x27EE ||
codepoint == 0x27EF ||
codepoint >= 0x27F0 && codepoint <= 0x27FF ||
codepoint >= 0x2900 && codepoint <= 0x2982 ||
codepoint == 0x2983 ||
codepoint == 0x2984 ||
codepoint == 0x2985 ||
codepoint == 0x2986 ||
codepoint == 0x2987 ||
codepoint == 0x2988 ||
codepoint == 0x2989 ||
codepoint == 0x298A ||
codepoint == 0x298B ||
codepoint == 0x298C ||
codepoint == 0x298D ||
codepoint == 0x298E ||
codepoint == 0x298F ||
codepoint == 0x2990 ||
codepoint == 0x2991 ||
codepoint == 0x2992 ||
codepoint == 0x2993 ||
codepoint == 0x2994 ||
codepoint == 0x2995 ||
codepoint == 0x2996 ||
codepoint == 0x2997 ||
codepoint == 0x2998 ||
codepoint >= 0x2999 && codepoint <= 0x29D7 ||
codepoint == 0x29D8 ||
codepoint == 0x29D9 ||
codepoint == 0x29DA ||
codepoint == 0x29DB ||
codepoint >= 0x29DC && codepoint <= 0x29FB ||
codepoint == 0x29FC ||
codepoint == 0x29FD ||
codepoint >= 0x29FE && codepoint <= 0x2AFF ||
codepoint >= 0x2B00 && codepoint <= 0x2B2F ||
codepoint >= 0x2B30 && codepoint <= 0x2B44 ||
codepoint >= 0x2B45 && codepoint <= 0x2B46 ||
codepoint >= 0x2B47 && codepoint <= 0x2B4C ||
codepoint >= 0x2B4D && codepoint <= 0x2B73 ||
codepoint >= 0x2B76 && codepoint <= 0x2B95 ||
codepoint >= 0x2B98 && codepoint <= 0x2BFF ||
codepoint >= 0x2E00 && codepoint <= 0x2E01 ||
codepoint == 0x2E02 ||
codepoint == 0x2E03 ||
codepoint == 0x2E04 ||
codepoint == 0x2E05 ||
codepoint >= 0x2E06 && codepoint <= 0x2E08 ||
codepoint == 0x2E09 ||
codepoint == 0x2E0A ||
codepoint == 0x2E0B ||
codepoint == 0x2E0C ||
codepoint == 0x2E0D ||
codepoint >= 0x2E0E && codepoint <= 0x2E16 ||
codepoint == 0x2E17 ||
codepoint >= 0x2E18 && codepoint <= 0x2E19 ||
codepoint == 0x2E1A ||
codepoint == 0x2E1B ||
codepoint == 0x2E1C ||
codepoint == 0x2E1D ||
codepoint >= 0x2E1E && codepoint <= 0x2E1F ||
codepoint == 0x2E20 ||
codepoint == 0x2E21 ||
codepoint == 0x2E22 ||
codepoint == 0x2E23 ||
codepoint == 0x2E24 ||
codepoint == 0x2E25 ||
codepoint == 0x2E26 ||
codepoint == 0x2E27 ||
codepoint == 0x2E28 ||
codepoint == 0x2E29 ||
codepoint >= 0x2E2A && codepoint <= 0x2E2E ||
codepoint == 0x2E2F ||
codepoint >= 0x2E30 && codepoint <= 0x2E39 ||
codepoint >= 0x2E3A && codepoint <= 0x2E3B ||
codepoint >= 0x2E3C && codepoint <= 0x2E3F ||
codepoint == 0x2E40 ||
codepoint == 0x2E41 ||
codepoint == 0x2E42 ||
codepoint >= 0x2E43 && codepoint <= 0x2E4F ||
codepoint >= 0x2FF0 && codepoint <= 0x2FFB ||
codepoint == 0x3000 ||
codepoint >= 0x3001 && codepoint <= 0x3003 ||
codepoint == 0x3004 ||
codepoint == 0x3006 ||
codepoint == 0x3008 ||
codepoint == 0x3009 ||
codepoint == 0x300A ||
codepoint == 0x300B ||
codepoint == 0x300C ||
codepoint == 0x300D ||
codepoint == 0x300E ||
codepoint == 0x300F ||
codepoint == 0x3010 ||
codepoint == 0x3011 ||
codepoint >= 0x3012 && codepoint <= 0x3013 ||
codepoint == 0x3014 ||
codepoint == 0x3015 ||
codepoint == 0x3016 ||
codepoint == 0x3017 ||
codepoint == 0x3018 ||
codepoint == 0x3019 ||
codepoint == 0x301A ||
codepoint == 0x301B ||
codepoint == 0x301C ||
codepoint == 0x301D ||
codepoint >= 0x301E && codepoint <= 0x301F ||
codepoint == 0x3020 ||
codepoint == 0x3030 ||
codepoint >= 0x3031 && codepoint <= 0x3035 ||
codepoint >= 0x3036 && codepoint <= 0x3037 ||
codepoint == 0x303C ||
codepoint == 0x303D ||
codepoint >= 0x303E && codepoint <= 0x303F ||
codepoint >= 0x309B && codepoint <= 0x309C ||
codepoint == 0x30A0 ||
codepoint == 0x30FB ||
codepoint == 0x30FC ||
codepoint >= 0x3190 && codepoint <= 0x3191 ||
codepoint >= 0x3192 && codepoint <= 0x3195 ||
codepoint >= 0x3196 && codepoint <= 0x319F ||
codepoint >= 0x31C0 && codepoint <= 0x31E3 ||
codepoint >= 0x3220 && codepoint <= 0x3229 ||
codepoint >= 0x322A && codepoint <= 0x3247 ||
codepoint >= 0x3248 && codepoint <= 0x324F ||
codepoint == 0x3250 ||
codepoint >= 0x3251 && codepoint <= 0x325F ||
codepoint == 0x327F ||
codepoint >= 0x3280 && codepoint <= 0x3289 ||
codepoint >= 0x328A && codepoint <= 0x32B0 ||
codepoint >= 0x32B1 && codepoint <= 0x32BF ||
codepoint >= 0x32C0 && codepoint <= 0x32CF ||
codepoint >= 0x3358 && codepoint <= 0x33FF ||
codepoint >= 0x4DC0 && codepoint <= 0x4DFF ||
codepoint >= 0xA700 && codepoint <= 0xA716 ||
codepoint >= 0xA717 && codepoint <= 0xA71F ||
codepoint >= 0xA720 && codepoint <= 0xA721 ||
codepoint == 0xA788 ||
codepoint >= 0xA789 && codepoint <= 0xA78A ||
codepoint >= 0xA830 && codepoint <= 0xA835 ||
codepoint >= 0xA836 && codepoint <= 0xA837 ||
codepoint == 0xA838 ||
codepoint == 0xA839 ||
codepoint == 0xA92E ||
codepoint == 0xA9CF ||
codepoint == 0xAB5B ||
codepoint == 0xFD3E ||
codepoint == 0xFD3F ||
codepoint >= 0xFE10 && codepoint <= 0xFE16 ||
codepoint == 0xFE17 ||
codepoint == 0xFE18 ||
codepoint == 0xFE19 ||
codepoint == 0xFE30 ||
codepoint >= 0xFE31 && codepoint <= 0xFE32 ||
codepoint >= 0xFE33 && codepoint <= 0xFE34 ||
codepoint == 0xFE35 ||
codepoint == 0xFE36 ||
codepoint == 0xFE37 ||
codepoint == 0xFE38 ||
codepoint == 0xFE39 ||
codepoint == 0xFE3A ||
codepoint == 0xFE3B ||
codepoint == 0xFE3C ||
codepoint == 0xFE3D ||
codepoint == 0xFE3E ||
codepoint == 0xFE3F ||
codepoint == 0xFE40 ||
codepoint == 0xFE41 ||
codepoint == 0xFE42 ||
codepoint == 0xFE43 ||
codepoint == 0xFE44 ||
codepoint >= 0xFE45 && codepoint <= 0xFE46 ||
codepoint == 0xFE47 ||
codepoint == 0xFE48 ||
codepoint >= 0xFE49 && codepoint <= 0xFE4C ||
codepoint >= 0xFE4D && codepoint <= 0xFE4F ||
codepoint >= 0xFE50 && codepoint <= 0xFE52 ||
codepoint >= 0xFE54 && codepoint <= 0xFE57 ||
codepoint == 0xFE58 ||
codepoint == 0xFE59 ||
codepoint == 0xFE5A ||
codepoint == 0xFE5B ||
codepoint == 0xFE5C ||
codepoint == 0xFE5D ||
codepoint == 0xFE5E ||
codepoint >= 0xFE5F && codepoint <= 0xFE61 ||
codepoint == 0xFE62 ||
codepoint == 0xFE63 ||
codepoint >= 0xFE64 && codepoint <= 0xFE66 ||
codepoint == 0xFE68 ||
codepoint == 0xFE69 ||
codepoint >= 0xFE6A && codepoint <= 0xFE6B ||
codepoint == 0xFEFF ||
codepoint >= 0xFF01 && codepoint <= 0xFF03 ||
codepoint == 0xFF04 ||
codepoint >= 0xFF05 && codepoint <= 0xFF07 ||
codepoint == 0xFF08 ||
codepoint == 0xFF09 ||
codepoint == 0xFF0A ||
codepoint == 0xFF0B ||
codepoint == 0xFF0C ||
codepoint == 0xFF0D ||
codepoint >= 0xFF0E && codepoint <= 0xFF0F ||
codepoint >= 0xFF10 && codepoint <= 0xFF19 ||
codepoint >= 0xFF1A && codepoint <= 0xFF1B ||
codepoint >= 0xFF1C && codepoint <= 0xFF1E ||
codepoint >= 0xFF1F && codepoint <= 0xFF20 ||
codepoint == 0xFF3B ||
codepoint == 0xFF3C ||
codepoint == 0xFF3D ||
codepoint == 0xFF3E ||
codepoint == 0xFF3F ||
codepoint == 0xFF40 ||
codepoint == 0xFF5B ||
codepoint == 0xFF5C ||
codepoint == 0xFF5D ||
codepoint == 0xFF5E ||
codepoint == 0xFF5F ||
codepoint == 0xFF60 ||
codepoint == 0xFF61 ||
codepoint == 0xFF62 ||
codepoint == 0xFF63 ||
codepoint >= 0xFF64 && codepoint <= 0xFF65 ||
codepoint == 0xFF70 ||
codepoint >= 0xFF9E && codepoint <= 0xFF9F ||
codepoint >= 0xFFE0 && codepoint <= 0xFFE1 ||
codepoint == 0xFFE2 ||
codepoint == 0xFFE3 ||
codepoint == 0xFFE4 ||
codepoint >= 0xFFE5 && codepoint <= 0xFFE6 ||
codepoint == 0xFFE8 ||
codepoint >= 0xFFE9 && codepoint <= 0xFFEC ||
codepoint >= 0xFFED && codepoint <= 0xFFEE ||
codepoint >= 0xFFF9 && codepoint <= 0xFFFB ||
codepoint >= 0xFFFC && codepoint <= 0xFFFD ||
codepoint >= 0x10100 && codepoint <= 0x10102 ||
codepoint >= 0x10107 && codepoint <= 0x10133 ||
codepoint >= 0x10137 && codepoint <= 0x1013F ||
codepoint >= 0x10190 && codepoint <= 0x1019B ||
codepoint >= 0x101D0 && codepoint <= 0x101FC ||
codepoint >= 0x102E1 && codepoint <= 0x102FB ||
codepoint == 0x16FE2 ||
codepoint == 0x16FE3 ||
codepoint >= 0x1BCA0 && codepoint <= 0x1BCA3 ||
codepoint >= 0x1D000 && codepoint <= 0x1D0F5 ||
codepoint >= 0x1D100 && codepoint <= 0x1D126 ||
codepoint >= 0x1D129 && codepoint <= 0x1D164 ||
codepoint >= 0x1D165 && codepoint <= 0x1D166 ||
codepoint >= 0x1D16A && codepoint <= 0x1D16C ||
codepoint >= 0x1D16D && codepoint <= 0x1D172 ||
codepoint >= 0x1D173 && codepoint <= 0x1D17A ||
codepoint >= 0x1D183 && codepoint <= 0x1D184 ||
codepoint >= 0x1D18C && codepoint <= 0x1D1A9 ||
codepoint >= 0x1D1AE && codepoint <= 0x1D1E8 ||
codepoint >= 0x1D2E0 && codepoint <= 0x1D2F3 ||
codepoint >= 0x1D300 && codepoint <= 0x1D356 ||
codepoint >= 0x1D360 && codepoint <= 0x1D378 ||
codepoint >= 0x1D400 && codepoint <= 0x1D454 ||
codepoint >= 0x1D456 && codepoint <= 0x1D49C ||
codepoint >= 0x1D49E && codepoint <= 0x1D49F ||
codepoint == 0x1D4A2 ||
codepoint >= 0x1D4A5 && codepoint <= 0x1D4A6 ||
codepoint >= 0x1D4A9 && codepoint <= 0x1D4AC ||
codepoint >= 0x1D4AE && codepoint <= 0x1D4B9 ||
codepoint == 0x1D4BB ||
codepoint >= 0x1D4BD && codepoint <= 0x1D4C3 ||
codepoint >= 0x1D4C5 && codepoint <= 0x1D505 ||
codepoint >= 0x1D507 && codepoint <= 0x1D50A ||
codepoint >= 0x1D50D && codepoint <= 0x1D514 ||
codepoint >= 0x1D516 && codepoint <= 0x1D51C ||
codepoint >= 0x1D51E && codepoint <= 0x1D539 ||
codepoint >= 0x1D53B && codepoint <= 0x1D53E ||
codepoint >= 0x1D540 && codepoint <= 0x1D544 ||
codepoint == 0x1D546 ||
codepoint >= 0x1D54A && codepoint <= 0x1D550 ||
codepoint >= 0x1D552 && codepoint <= 0x1D6A5 ||
codepoint >= 0x1D6A8 && codepoint <= 0x1D6C0 ||
codepoint == 0x1D6C1 ||
codepoint >= 0x1D6C2 && codepoint <= 0x1D6DA ||
codepoint == 0x1D6DB ||
codepoint >= 0x1D6DC && codepoint <= 0x1D6FA ||
codepoint == 0x1D6FB ||
codepoint >= 0x1D6FC && codepoint <= 0x1D714 ||
codepoint == 0x1D715 ||
codepoint >= 0x1D716 && codepoint <= 0x1D734 ||
codepoint == 0x1D735 ||
codepoint >= 0x1D736 && codepoint <= 0x1D74E ||
codepoint == 0x1D74F ||
codepoint >= 0x1D750 && codepoint <= 0x1D76E ||
codepoint == 0x1D76F ||
codepoint >= 0x1D770 && codepoint <= 0x1D788 ||
codepoint == 0x1D789 ||
codepoint >= 0x1D78A && codepoint <= 0x1D7A8 ||
codepoint == 0x1D7A9 ||
codepoint >= 0x1D7AA && codepoint <= 0x1D7C2 ||
codepoint == 0x1D7C3 ||
codepoint >= 0x1D7C4 && codepoint <= 0x1D7CB ||
codepoint >= 0x1D7CE && codepoint <= 0x1D7FF ||
codepoint >= 0x1EC71 && codepoint <= 0x1ECAB ||
codepoint == 0x1ECAC ||
codepoint >= 0x1ECAD && codepoint <= 0x1ECAF ||
codepoint == 0x1ECB0 ||
codepoint >= 0x1ECB1 && codepoint <= 0x1ECB4 ||
codepoint >= 0x1ED01 && codepoint <= 0x1ED2D ||
codepoint == 0x1ED2E ||
codepoint >= 0x1ED2F && codepoint <= 0x1ED3D ||
codepoint >= 0x1F000 && codepoint <= 0x1F02B ||
codepoint >= 0x1F030 && codepoint <= 0x1F093 ||
codepoint >= 0x1F0A0 && codepoint <= 0x1F0AE ||
codepoint >= 0x1F0B1 && codepoint <= 0x1F0BF ||
codepoint >= 0x1F0C1 && codepoint <= 0x1F0CF ||
codepoint >= 0x1F0D1 && codepoint <= 0x1F0F5 ||
codepoint >= 0x1F100 && codepoint <= 0x1F10C ||
codepoint >= 0x1F110 && codepoint <= 0x1F16C ||
codepoint >= 0x1F170 && codepoint <= 0x1F1AC ||
codepoint >= 0x1F1E6 && codepoint <= 0x1F1FF ||
codepoint >= 0x1F201 && codepoint <= 0x1F202 ||
codepoint >= 0x1F210 && codepoint <= 0x1F23B ||
codepoint >= 0x1F240 && codepoint <= 0x1F248 ||
codepoint >= 0x1F250 && codepoint <= 0x1F251 ||
codepoint >= 0x1F260 && codepoint <= 0x1F265 ||
codepoint >= 0x1F300 && codepoint <= 0x1F3FA ||
codepoint >= 0x1F3FB && codepoint <= 0x1F3FF ||
codepoint >= 0x1F400 && codepoint <= 0x1F6D5 ||
codepoint >= 0x1F6E0 && codepoint <= 0x1F6EC ||
codepoint >= 0x1F6F0 && codepoint <= 0x1F6FA ||
codepoint >= 0x1F700 && codepoint <= 0x1F773 ||
codepoint >= 0x1F780 && codepoint <= 0x1F7D8 ||
codepoint >= 0x1F7E0 && codepoint <= 0x1F7EB ||
codepoint >= 0x1F800 && codepoint <= 0x1F80B ||
codepoint >= 0x1F810 && codepoint <= 0x1F847 ||
codepoint >= 0x1F850 && codepoint <= 0x1F859 ||
codepoint >= 0x1F860 && codepoint <= 0x1F887 ||
codepoint >= 0x1F890 && codepoint <= 0x1F8AD ||
codepoint >= 0x1F900 && codepoint <= 0x1F90B ||
codepoint >= 0x1F90D && codepoint <= 0x1F971 ||
codepoint >= 0x1F973 && codepoint <= 0x1F976 ||
codepoint >= 0x1F97A && codepoint <= 0x1F9A2 ||
codepoint >= 0x1F9A5 && codepoint <= 0x1F9AA ||
codepoint >= 0x1F9AE && codepoint <= 0x1F9CA ||
codepoint >= 0x1F9CD && codepoint <= 0x1FA53 ||
codepoint >= 0x1FA60 && codepoint <= 0x1FA6D ||
codepoint >= 0x1FA70 && codepoint <= 0x1FA73 ||
codepoint >= 0x1FA78 && codepoint <= 0x1FA7A ||
codepoint >= 0x1FA80 && codepoint <= 0x1FA82 ||
codepoint >= 0x1FA90 && codepoint <= 0x1FA95 ||
codepoint == 0xE0001 ||
codepoint >= 0xE0020 && codepoint <= 0xE007F)
{
result = BIDIScript_Common;
}
else if(codepoint >= 0x0041 && codepoint <= 0x005A ||
codepoint >= 0x0061 && codepoint <= 0x007A ||
codepoint == 0x00AA ||
codepoint == 0x00BA ||
codepoint >= 0x00C0 && codepoint <= 0x00D6 ||
codepoint >= 0x00D8 && codepoint <= 0x00F6 ||
codepoint >= 0x00F8 && codepoint <= 0x01BA ||
codepoint == 0x01BB ||
codepoint >= 0x01BC && codepoint <= 0x01BF ||
codepoint >= 0x01C0 && codepoint <= 0x01C3 ||
codepoint >= 0x01C4 && codepoint <= 0x0293 ||
codepoint == 0x0294 ||
codepoint >= 0x0295 && codepoint <= 0x02AF ||
codepoint >= 0x02B0 && codepoint <= 0x02B8 ||
codepoint >= 0x02E0 && codepoint <= 0x02E4 ||
codepoint >= 0x1D00 && codepoint <= 0x1D25 ||
codepoint >= 0x1D2C && codepoint <= 0x1D5C ||
codepoint >= 0x1D62 && codepoint <= 0x1D65 ||
codepoint >= 0x1D6B && codepoint <= 0x1D77 ||
codepoint >= 0x1D79 && codepoint <= 0x1D9A ||
codepoint >= 0x1D9B && codepoint <= 0x1DBE ||
codepoint >= 0x1E00 && codepoint <= 0x1EFF ||
codepoint == 0x2071 ||
codepoint == 0x207F ||
codepoint >= 0x2090 && codepoint <= 0x209C ||
codepoint >= 0x212A && codepoint <= 0x212B ||
codepoint == 0x2132 ||
codepoint == 0x214E ||
codepoint >= 0x2160 && codepoint <= 0x2182 ||
codepoint >= 0x2183 && codepoint <= 0x2184 ||
codepoint >= 0x2185 && codepoint <= 0x2188 ||
codepoint >= 0x2C60 && codepoint <= 0x2C7B ||
codepoint >= 0x2C7C && codepoint <= 0x2C7D ||
codepoint >= 0x2C7E && codepoint <= 0x2C7F ||
codepoint >= 0xA722 && codepoint <= 0xA76F ||
codepoint == 0xA770 ||
codepoint >= 0xA771 && codepoint <= 0xA787 ||
codepoint >= 0xA78B && codepoint <= 0xA78E ||
codepoint == 0xA78F ||
codepoint >= 0xA790 && codepoint <= 0xA7BF ||
codepoint >= 0xA7C2 && codepoint <= 0xA7C6 ||
codepoint == 0xA7F7 ||
codepoint >= 0xA7F8 && codepoint <= 0xA7F9 ||
codepoint == 0xA7FA ||
codepoint >= 0xA7FB && codepoint <= 0xA7FF ||
codepoint >= 0xAB30 && codepoint <= 0xAB5A ||
codepoint >= 0xAB5C && codepoint <= 0xAB5F ||
codepoint >= 0xAB60 && codepoint <= 0xAB64 ||
codepoint >= 0xAB66 && codepoint <= 0xAB67 ||
codepoint >= 0xFB00 && codepoint <= 0xFB06 ||
codepoint >= 0xFF21 && codepoint <= 0xFF3A ||
codepoint >= 0xFF41 && codepoint <= 0xFF5A)
{
result = BIDIScript_Latin;
}
else if(codepoint >= 0x0370 && codepoint <= 0x0373 ||
codepoint == 0x0375 ||
codepoint >= 0x0376 && codepoint <= 0x0377 ||
codepoint == 0x037A ||
codepoint >= 0x037B && codepoint <= 0x037D ||
codepoint == 0x037F ||
codepoint == 0x0384 ||
codepoint == 0x0386 ||
codepoint >= 0x0388 && codepoint <= 0x038A ||
codepoint == 0x038C ||
codepoint >= 0x038E && codepoint <= 0x03A1 ||
codepoint >= 0x03A3 && codepoint <= 0x03E1 ||
codepoint >= 0x03F0 && codepoint <= 0x03F5 ||
codepoint == 0x03F6 ||
codepoint >= 0x03F7 && codepoint <= 0x03FF ||
codepoint >= 0x1D26 && codepoint <= 0x1D2A ||
codepoint >= 0x1D5D && codepoint <= 0x1D61 ||
codepoint >= 0x1D66 && codepoint <= 0x1D6A ||
codepoint == 0x1DBF ||
codepoint >= 0x1F00 && codepoint <= 0x1F15 ||
codepoint >= 0x1F18 && codepoint <= 0x1F1D ||
codepoint >= 0x1F20 && codepoint <= 0x1F45 ||
codepoint >= 0x1F48 && codepoint <= 0x1F4D ||
codepoint >= 0x1F50 && codepoint <= 0x1F57 ||
codepoint == 0x1F59 ||
codepoint == 0x1F5B ||
codepoint == 0x1F5D ||
codepoint >= 0x1F5F && codepoint <= 0x1F7D ||
codepoint >= 0x1F80 && codepoint <= 0x1FB4 ||
codepoint >= 0x1FB6 && codepoint <= 0x1FBC ||
codepoint == 0x1FBD ||
codepoint == 0x1FBE ||
codepoint >= 0x1FBF && codepoint <= 0x1FC1 ||
codepoint >= 0x1FC2 && codepoint <= 0x1FC4 ||
codepoint >= 0x1FC6 && codepoint <= 0x1FCC ||
codepoint >= 0x1FCD && codepoint <= 0x1FCF ||
codepoint >= 0x1FD0 && codepoint <= 0x1FD3 ||
codepoint >= 0x1FD6 && codepoint <= 0x1FDB ||
codepoint >= 0x1FDD && codepoint <= 0x1FDF ||
codepoint >= 0x1FE0 && codepoint <= 0x1FEC ||
codepoint >= 0x1FED && codepoint <= 0x1FEF ||
codepoint >= 0x1FF2 && codepoint <= 0x1FF4 ||
codepoint >= 0x1FF6 && codepoint <= 0x1FFC ||
codepoint >= 0x1FFD && codepoint <= 0x1FFE ||
codepoint == 0x2126 ||
codepoint == 0xAB65 ||
codepoint >= 0x10140 && codepoint <= 0x10174 ||
codepoint >= 0x10175 && codepoint <= 0x10178 ||
codepoint >= 0x10179 && codepoint <= 0x10189 ||
codepoint >= 0x1018A && codepoint <= 0x1018B ||
codepoint >= 0x1018C && codepoint <= 0x1018E ||
codepoint == 0x101A0 ||
codepoint >= 0x1D200 && codepoint <= 0x1D241 ||
codepoint >= 0x1D242 && codepoint <= 0x1D244 ||
codepoint == 0x1D245)
{
result = BIDIScript_Greek;
}
else if(codepoint >= 0x0400 && codepoint <= 0x0481 ||
codepoint == 0x0482 ||
codepoint >= 0x0483 && codepoint <= 0x0484 ||
codepoint == 0x0487 ||
codepoint >= 0x0488 && codepoint <= 0x0489 ||
codepoint >= 0x048A && codepoint <= 0x052F ||
codepoint >= 0x1C80 && codepoint <= 0x1C88 ||
codepoint == 0x1D2B ||
codepoint == 0x1D78 ||
codepoint >= 0x2DE0 && codepoint <= 0x2DFF ||
codepoint >= 0xA640 && codepoint <= 0xA66D ||
codepoint == 0xA66E ||
codepoint == 0xA66F ||
codepoint >= 0xA670 && codepoint <= 0xA672 ||
codepoint == 0xA673 ||
codepoint >= 0xA674 && codepoint <= 0xA67D ||
codepoint == 0xA67E ||
codepoint == 0xA67F ||
codepoint >= 0xA680 && codepoint <= 0xA69B ||
codepoint >= 0xA69C && codepoint <= 0xA69D ||
codepoint >= 0xA69E && codepoint <= 0xA69F ||
codepoint >= 0xFE2E && codepoint <= 0xFE2F)
{
result = BIDIScript_Cyrillic;
}
else if(codepoint >= 0x0531 && codepoint <= 0x0556 ||
codepoint == 0x0559 ||
codepoint >= 0x055A && codepoint <= 0x055F ||
codepoint >= 0x0560 && codepoint <= 0x0588 ||
codepoint == 0x058A ||
codepoint >= 0x058D && codepoint <= 0x058E ||
codepoint == 0x058F ||
codepoint >= 0xFB13 && codepoint <= 0xFB17)
{
result = BIDIScript_Armenian;
}
else if(codepoint >= 0x0591 && codepoint <= 0x05BD ||
codepoint == 0x05BE ||
codepoint == 0x05BF ||
codepoint == 0x05C0 ||
codepoint >= 0x05C1 && codepoint <= 0x05C2 ||
codepoint == 0x05C3 ||
codepoint >= 0x05C4 && codepoint <= 0x05C5 ||
codepoint == 0x05C6 ||
codepoint == 0x05C7 ||
codepoint >= 0x05D0 && codepoint <= 0x05EA ||
codepoint >= 0x05EF && codepoint <= 0x05F2 ||
codepoint >= 0x05F3 && codepoint <= 0x05F4 ||
codepoint == 0xFB1D ||
codepoint == 0xFB1E ||
codepoint >= 0xFB1F && codepoint <= 0xFB28 ||
codepoint == 0xFB29 ||
codepoint >= 0xFB2A && codepoint <= 0xFB36 ||
codepoint >= 0xFB38 && codepoint <= 0xFB3C ||
codepoint == 0xFB3E ||
codepoint >= 0xFB40 && codepoint <= 0xFB41 ||
codepoint >= 0xFB43 && codepoint <= 0xFB44 ||
codepoint >= 0xFB46 && codepoint <= 0xFB4F)
{
result = BIDIScript_Hebrew;
}
else if(codepoint >= 0x0600 && codepoint <= 0x0604 ||
codepoint >= 0x0606 && codepoint <= 0x0608 ||
codepoint >= 0x0609 && codepoint <= 0x060A ||
codepoint == 0x060B ||
codepoint == 0x060D ||
codepoint >= 0x060E && codepoint <= 0x060F ||
codepoint >= 0x0610 && codepoint <= 0x061A ||
codepoint == 0x061C ||
codepoint == 0x061E ||
codepoint >= 0x0620 && codepoint <= 0x063F ||
codepoint >= 0x0641 && codepoint <= 0x064A ||
codepoint >= 0x0656 && codepoint <= 0x065F ||
codepoint >= 0x0660 && codepoint <= 0x0669 ||
codepoint >= 0x066A && codepoint <= 0x066D ||
codepoint >= 0x066E && codepoint <= 0x066F ||
codepoint >= 0x0671 && codepoint <= 0x06D3 ||
codepoint == 0x06D4 ||
codepoint == 0x06D5 ||
codepoint >= 0x06D6 && codepoint <= 0x06DC ||
codepoint == 0x06DE ||
codepoint >= 0x06DF && codepoint <= 0x06E4 ||
codepoint >= 0x06E5 && codepoint <= 0x06E6 ||
codepoint >= 0x06E7 && codepoint <= 0x06E8 ||
codepoint == 0x06E9 ||
codepoint >= 0x06EA && codepoint <= 0x06ED ||
codepoint >= 0x06EE && codepoint <= 0x06EF ||
codepoint >= 0x06F0 && codepoint <= 0x06F9 ||
codepoint >= 0x06FA && codepoint <= 0x06FC ||
codepoint >= 0x06FD && codepoint <= 0x06FE ||
codepoint == 0x06FF ||
codepoint >= 0x0750 && codepoint <= 0x077F ||
codepoint >= 0x08A0 && codepoint <= 0x08B4 ||
codepoint >= 0x08B6 && codepoint <= 0x08BD ||
codepoint >= 0x08D3 && codepoint <= 0x08E1 ||
codepoint >= 0x08E3 && codepoint <= 0x08FF ||
codepoint >= 0xFB50 && codepoint <= 0xFBB1 ||
codepoint >= 0xFBB2 && codepoint <= 0xFBC1 ||
codepoint >= 0xFBD3 && codepoint <= 0xFD3D ||
codepoint >= 0xFD50 && codepoint <= 0xFD8F ||
codepoint >= 0xFD92 && codepoint <= 0xFDC7 ||
codepoint >= 0xFDF0 && codepoint <= 0xFDFB ||
codepoint == 0xFDFC ||
codepoint == 0xFDFD ||
codepoint >= 0xFE70 && codepoint <= 0xFE74 ||
codepoint >= 0xFE76 && codepoint <= 0xFEFC ||
codepoint >= 0x10E60 && codepoint <= 0x10E7E ||
codepoint >= 0x1EE00 && codepoint <= 0x1EE03 ||
codepoint >= 0x1EE05 && codepoint <= 0x1EE1F ||
codepoint >= 0x1EE21 && codepoint <= 0x1EE22 ||
codepoint == 0x1EE24 ||
codepoint == 0x1EE27 ||
codepoint >= 0x1EE29 && codepoint <= 0x1EE32 ||
codepoint >= 0x1EE34 && codepoint <= 0x1EE37 ||
codepoint == 0x1EE39 ||
codepoint == 0x1EE3B ||
codepoint == 0x1EE42 ||
codepoint == 0x1EE47 ||
codepoint == 0x1EE49 ||
codepoint == 0x1EE4B ||
codepoint >= 0x1EE4D && codepoint <= 0x1EE4F ||
codepoint >= 0x1EE51 && codepoint <= 0x1EE52 ||
codepoint == 0x1EE54 ||
codepoint == 0x1EE57 ||
codepoint == 0x1EE59 ||
codepoint == 0x1EE5B ||
codepoint == 0x1EE5D ||
codepoint == 0x1EE5F ||
codepoint >= 0x1EE61 && codepoint <= 0x1EE62 ||
codepoint == 0x1EE64 ||
codepoint >= 0x1EE67 && codepoint <= 0x1EE6A ||
codepoint >= 0x1EE6C && codepoint <= 0x1EE72 ||
codepoint >= 0x1EE74 && codepoint <= 0x1EE77 ||
codepoint >= 0x1EE79 && codepoint <= 0x1EE7C ||
codepoint == 0x1EE7E ||
codepoint >= 0x1EE80 && codepoint <= 0x1EE89 ||
codepoint >= 0x1EE8B && codepoint <= 0x1EE9B ||
codepoint >= 0x1EEA1 && codepoint <= 0x1EEA3 ||
codepoint >= 0x1EEA5 && codepoint <= 0x1EEA9 ||
codepoint >= 0x1EEAB && codepoint <= 0x1EEBB ||
codepoint >= 0x1EEF0 && codepoint <= 0x1EEF1)
{
result = BIDIScript_Arabic;
}
else if(codepoint >= 0x0700 && codepoint <= 0x070D ||
codepoint == 0x070F ||
codepoint == 0x0710 ||
codepoint == 0x0711 ||
codepoint >= 0x0712 && codepoint <= 0x072F ||
codepoint >= 0x0730 && codepoint <= 0x074A ||
codepoint >= 0x074D && codepoint <= 0x074F ||
codepoint >= 0x0860 && codepoint <= 0x086A)
{
result = BIDIScript_Syriac;
}
else if(codepoint >= 0x0780 && codepoint <= 0x07A5 ||
codepoint >= 0x07A6 && codepoint <= 0x07B0 ||
codepoint == 0x07B1)
{
result = BIDIScript_Thaana;
}
else if(codepoint >= 0x0900 && codepoint <= 0x0902 ||
codepoint == 0x0903 ||
codepoint >= 0x0904 && codepoint <= 0x0939 ||
codepoint == 0x093A ||
codepoint == 0x093B ||
codepoint == 0x093C ||
codepoint == 0x093D ||
codepoint >= 0x093E && codepoint <= 0x0940 ||
codepoint >= 0x0941 && codepoint <= 0x0948 ||
codepoint >= 0x0949 && codepoint <= 0x094C ||
codepoint == 0x094D ||
codepoint >= 0x094E && codepoint <= 0x094F ||
codepoint == 0x0950 ||
codepoint >= 0x0955 && codepoint <= 0x0957 ||
codepoint >= 0x0958 && codepoint <= 0x0961 ||
codepoint >= 0x0962 && codepoint <= 0x0963 ||
codepoint >= 0x0966 && codepoint <= 0x096F ||
codepoint == 0x0970 ||
codepoint == 0x0971 ||
codepoint >= 0x0972 && codepoint <= 0x097F ||
codepoint >= 0xA8E0 && codepoint <= 0xA8F1 ||
codepoint >= 0xA8F2 && codepoint <= 0xA8F7 ||
codepoint >= 0xA8F8 && codepoint <= 0xA8FA ||
codepoint == 0xA8FB ||
codepoint == 0xA8FC ||
codepoint >= 0xA8FD && codepoint <= 0xA8FE ||
codepoint == 0xA8FF)
{
result = BIDIScript_Devanagari;
}
else if(codepoint == 0x0980 ||
codepoint == 0x0981 ||
codepoint >= 0x0982 && codepoint <= 0x0983 ||
codepoint >= 0x0985 && codepoint <= 0x098C ||
codepoint >= 0x098F && codepoint <= 0x0990 ||
codepoint >= 0x0993 && codepoint <= 0x09A8 ||
codepoint >= 0x09AA && codepoint <= 0x09B0 ||
codepoint == 0x09B2 ||
codepoint >= 0x09B6 && codepoint <= 0x09B9 ||
codepoint == 0x09BC ||
codepoint == 0x09BD ||
codepoint >= 0x09BE && codepoint <= 0x09C0 ||
codepoint >= 0x09C1 && codepoint <= 0x09C4 ||
codepoint >= 0x09C7 && codepoint <= 0x09C8 ||
codepoint >= 0x09CB && codepoint <= 0x09CC ||
codepoint == 0x09CD ||
codepoint == 0x09CE ||
codepoint == 0x09D7 ||
codepoint >= 0x09DC && codepoint <= 0x09DD ||
codepoint >= 0x09DF && codepoint <= 0x09E1 ||
codepoint >= 0x09E2 && codepoint <= 0x09E3 ||
codepoint >= 0x09E6 && codepoint <= 0x09EF ||
codepoint >= 0x09F0 && codepoint <= 0x09F1 ||
codepoint >= 0x09F2 && codepoint <= 0x09F3 ||
codepoint >= 0x09F4 && codepoint <= 0x09F9 ||
codepoint == 0x09FA ||
codepoint == 0x09FB ||
codepoint == 0x09FC ||
codepoint == 0x09FD ||
codepoint == 0x09FE)
{
result = BIDIScript_Bengali;
}
else if(codepoint >= 0x0A01 && codepoint <= 0x0A02 ||
codepoint == 0x0A03 ||
codepoint >= 0x0A05 && codepoint <= 0x0A0A ||
codepoint >= 0x0A0F && codepoint <= 0x0A10 ||
codepoint >= 0x0A13 && codepoint <= 0x0A28 ||
codepoint >= 0x0A2A && codepoint <= 0x0A30 ||
codepoint >= 0x0A32 && codepoint <= 0x0A33 ||
codepoint >= 0x0A35 && codepoint <= 0x0A36 ||
codepoint >= 0x0A38 && codepoint <= 0x0A39 ||
codepoint == 0x0A3C ||
codepoint >= 0x0A3E && codepoint <= 0x0A40 ||
codepoint >= 0x0A41 && codepoint <= 0x0A42 ||
codepoint >= 0x0A47 && codepoint <= 0x0A48 ||
codepoint >= 0x0A4B && codepoint <= 0x0A4D ||
codepoint == 0x0A51 ||
codepoint >= 0x0A59 && codepoint <= 0x0A5C ||
codepoint == 0x0A5E ||
codepoint >= 0x0A66 && codepoint <= 0x0A6F ||
codepoint >= 0x0A70 && codepoint <= 0x0A71 ||
codepoint >= 0x0A72 && codepoint <= 0x0A74 ||
codepoint == 0x0A75 ||
codepoint == 0x0A76)
{
result = BIDIScript_Gurmukhi;
}
else if(codepoint >= 0x0A81 && codepoint <= 0x0A82 ||
codepoint == 0x0A83 ||
codepoint >= 0x0A85 && codepoint <= 0x0A8D ||
codepoint >= 0x0A8F && codepoint <= 0x0A91 ||
codepoint >= 0x0A93 && codepoint <= 0x0AA8 ||
codepoint >= 0x0AAA && codepoint <= 0x0AB0 ||
codepoint >= 0x0AB2 && codepoint <= 0x0AB3 ||
codepoint >= 0x0AB5 && codepoint <= 0x0AB9 ||
codepoint == 0x0ABC ||
codepoint == 0x0ABD ||
codepoint >= 0x0ABE && codepoint <= 0x0AC0 ||
codepoint >= 0x0AC1 && codepoint <= 0x0AC5 ||
codepoint >= 0x0AC7 && codepoint <= 0x0AC8 ||
codepoint == 0x0AC9 ||
codepoint >= 0x0ACB && codepoint <= 0x0ACC ||
codepoint == 0x0ACD ||
codepoint == 0x0AD0 ||
codepoint >= 0x0AE0 && codepoint <= 0x0AE1 ||
codepoint >= 0x0AE2 && codepoint <= 0x0AE3 ||
codepoint >= 0x0AE6 && codepoint <= 0x0AEF ||
codepoint == 0x0AF0 ||
codepoint == 0x0AF1 ||
codepoint == 0x0AF9 ||
codepoint >= 0x0AFA && codepoint <= 0x0AFF)
{
result = BIDIScript_Gujarati;
}
else if(codepoint == 0x0B01 ||
codepoint >= 0x0B02 && codepoint <= 0x0B03 ||
codepoint >= 0x0B05 && codepoint <= 0x0B0C ||
codepoint >= 0x0B0F && codepoint <= 0x0B10 ||
codepoint >= 0x0B13 && codepoint <= 0x0B28 ||
codepoint >= 0x0B2A && codepoint <= 0x0B30 ||
codepoint >= 0x0B32 && codepoint <= 0x0B33 ||
codepoint >= 0x0B35 && codepoint <= 0x0B39 ||
codepoint == 0x0B3C ||
codepoint == 0x0B3D ||
codepoint == 0x0B3E ||
codepoint == 0x0B3F ||
codepoint == 0x0B40 ||
codepoint >= 0x0B41 && codepoint <= 0x0B44 ||
codepoint >= 0x0B47 && codepoint <= 0x0B48 ||
codepoint >= 0x0B4B && codepoint <= 0x0B4C ||
codepoint == 0x0B4D ||
codepoint == 0x0B56 ||
codepoint == 0x0B57 ||
codepoint >= 0x0B5C && codepoint <= 0x0B5D ||
codepoint >= 0x0B5F && codepoint <= 0x0B61 ||
codepoint >= 0x0B62 && codepoint <= 0x0B63 ||
codepoint >= 0x0B66 && codepoint <= 0x0B6F ||
codepoint == 0x0B70 ||
codepoint == 0x0B71 ||
codepoint >= 0x0B72 && codepoint <= 0x0B77)
{
result = BIDIScript_Oriya;
}
else if(codepoint == 0x0B82 ||
codepoint == 0x0B83 ||
codepoint >= 0x0B85 && codepoint <= 0x0B8A ||
codepoint >= 0x0B8E && codepoint <= 0x0B90 ||
codepoint >= 0x0B92 && codepoint <= 0x0B95 ||
codepoint >= 0x0B99 && codepoint <= 0x0B9A ||
codepoint == 0x0B9C ||
codepoint >= 0x0B9E && codepoint <= 0x0B9F ||
codepoint >= 0x0BA3 && codepoint <= 0x0BA4 ||
codepoint >= 0x0BA8 && codepoint <= 0x0BAA ||
codepoint >= 0x0BAE && codepoint <= 0x0BB9 ||
codepoint >= 0x0BBE && codepoint <= 0x0BBF ||
codepoint == 0x0BC0 ||
codepoint >= 0x0BC1 && codepoint <= 0x0BC2 ||
codepoint >= 0x0BC6 && codepoint <= 0x0BC8 ||
codepoint >= 0x0BCA && codepoint <= 0x0BCC ||
codepoint == 0x0BCD ||
codepoint == 0x0BD0 ||
codepoint == 0x0BD7 ||
codepoint >= 0x0BE6 && codepoint <= 0x0BEF ||
codepoint >= 0x0BF0 && codepoint <= 0x0BF2 ||
codepoint >= 0x0BF3 && codepoint <= 0x0BF8 ||
codepoint == 0x0BF9 ||
codepoint == 0x0BFA ||
codepoint >= 0x11FC0 && codepoint <= 0x11FD4 ||
codepoint >= 0x11FD5 && codepoint <= 0x11FDC ||
codepoint >= 0x11FDD && codepoint <= 0x11FE0 ||
codepoint >= 0x11FE1 && codepoint <= 0x11FF1 ||
codepoint == 0x11FFF)
{
result = BIDIScript_Tamil;
}
else if(codepoint == 0x0C00 ||
codepoint >= 0x0C01 && codepoint <= 0x0C03 ||
codepoint == 0x0C04 ||
codepoint >= 0x0C05 && codepoint <= 0x0C0C ||
codepoint >= 0x0C0E && codepoint <= 0x0C10 ||
codepoint >= 0x0C12 && codepoint <= 0x0C28 ||
codepoint >= 0x0C2A && codepoint <= 0x0C39 ||
codepoint == 0x0C3D ||
codepoint >= 0x0C3E && codepoint <= 0x0C40 ||
codepoint >= 0x0C41 && codepoint <= 0x0C44 ||
codepoint >= 0x0C46 && codepoint <= 0x0C48 ||
codepoint >= 0x0C4A && codepoint <= 0x0C4D ||
codepoint >= 0x0C55 && codepoint <= 0x0C56 ||
codepoint >= 0x0C58 && codepoint <= 0x0C5A ||
codepoint >= 0x0C60 && codepoint <= 0x0C61 ||
codepoint >= 0x0C62 && codepoint <= 0x0C63 ||
codepoint >= 0x0C66 && codepoint <= 0x0C6F ||
codepoint == 0x0C77 ||
codepoint >= 0x0C78 && codepoint <= 0x0C7E ||
codepoint == 0x0C7F)
{
result = BIDIScript_Telugu;
}
else if(codepoint == 0x0C80 ||
codepoint == 0x0C81 ||
codepoint >= 0x0C82 && codepoint <= 0x0C83 ||
codepoint == 0x0C84 ||
codepoint >= 0x0C85 && codepoint <= 0x0C8C ||
codepoint >= 0x0C8E && codepoint <= 0x0C90 ||
codepoint >= 0x0C92 && codepoint <= 0x0CA8 ||
codepoint >= 0x0CAA && codepoint <= 0x0CB3 ||
codepoint >= 0x0CB5 && codepoint <= 0x0CB9 ||
codepoint == 0x0CBC ||
codepoint == 0x0CBD ||
codepoint == 0x0CBE ||
codepoint == 0x0CBF ||
codepoint >= 0x0CC0 && codepoint <= 0x0CC4 ||
codepoint == 0x0CC6 ||
codepoint >= 0x0CC7 && codepoint <= 0x0CC8 ||
codepoint >= 0x0CCA && codepoint <= 0x0CCB ||
codepoint >= 0x0CCC && codepoint <= 0x0CCD ||
codepoint >= 0x0CD5 && codepoint <= 0x0CD6 ||
codepoint == 0x0CDE ||
codepoint >= 0x0CE0 && codepoint <= 0x0CE1 ||
codepoint >= 0x0CE2 && codepoint <= 0x0CE3 ||
codepoint >= 0x0CE6 && codepoint <= 0x0CEF ||
codepoint >= 0x0CF1 && codepoint <= 0x0CF2)
{
result = BIDIScript_Kannada;
}
else if(codepoint >= 0x0D00 && codepoint <= 0x0D01 ||
codepoint >= 0x0D02 && codepoint <= 0x0D03 ||
codepoint >= 0x0D05 && codepoint <= 0x0D0C ||
codepoint >= 0x0D0E && codepoint <= 0x0D10 ||
codepoint >= 0x0D12 && codepoint <= 0x0D3A ||
codepoint >= 0x0D3B && codepoint <= 0x0D3C ||
codepoint == 0x0D3D ||
codepoint >= 0x0D3E && codepoint <= 0x0D40 ||
codepoint >= 0x0D41 && codepoint <= 0x0D44 ||
codepoint >= 0x0D46 && codepoint <= 0x0D48 ||
codepoint >= 0x0D4A && codepoint <= 0x0D4C ||
codepoint == 0x0D4D ||
codepoint == 0x0D4E ||
codepoint == 0x0D4F ||
codepoint >= 0x0D54 && codepoint <= 0x0D56 ||
codepoint == 0x0D57 ||
codepoint >= 0x0D58 && codepoint <= 0x0D5E ||
codepoint >= 0x0D5F && codepoint <= 0x0D61 ||
codepoint >= 0x0D62 && codepoint <= 0x0D63 ||
codepoint >= 0x0D66 && codepoint <= 0x0D6F ||
codepoint >= 0x0D70 && codepoint <= 0x0D78 ||
codepoint == 0x0D79 ||
codepoint >= 0x0D7A && codepoint <= 0x0D7F)
{
result = BIDIScript_Malayalam;
}
else if(codepoint >= 0x0D82 && codepoint <= 0x0D83 ||
codepoint >= 0x0D85 && codepoint <= 0x0D96 ||
codepoint >= 0x0D9A && codepoint <= 0x0DB1 ||
codepoint >= 0x0DB3 && codepoint <= 0x0DBB ||
codepoint == 0x0DBD ||
codepoint >= 0x0DC0 && codepoint <= 0x0DC6 ||
codepoint == 0x0DCA ||
codepoint >= 0x0DCF && codepoint <= 0x0DD1 ||
codepoint >= 0x0DD2 && codepoint <= 0x0DD4 ||
codepoint == 0x0DD6 ||
codepoint >= 0x0DD8 && codepoint <= 0x0DDF ||
codepoint >= 0x0DE6 && codepoint <= 0x0DEF ||
codepoint >= 0x0DF2 && codepoint <= 0x0DF3 ||
codepoint == 0x0DF4 ||
codepoint >= 0x111E1 && codepoint <= 0x111F4)
{
result = BIDIScript_Sinhala;
}
else if(codepoint >= 0x0E01 && codepoint <= 0x0E30 ||
codepoint == 0x0E31 ||
codepoint >= 0x0E32 && codepoint <= 0x0E33 ||
codepoint >= 0x0E34 && codepoint <= 0x0E3A ||
codepoint >= 0x0E40 && codepoint <= 0x0E45 ||
codepoint == 0x0E46 ||
codepoint >= 0x0E47 && codepoint <= 0x0E4E ||
codepoint == 0x0E4F ||
codepoint >= 0x0E50 && codepoint <= 0x0E59 ||
codepoint >= 0x0E5A && codepoint <= 0x0E5B)
{
result = BIDIScript_Thai;
}
else if(codepoint >= 0x0E81 && codepoint <= 0x0E82 ||
codepoint == 0x0E84 ||
codepoint >= 0x0E86 && codepoint <= 0x0E8A ||
codepoint >= 0x0E8C && codepoint <= 0x0EA3 ||
codepoint == 0x0EA5 ||
codepoint >= 0x0EA7 && codepoint <= 0x0EB0 ||
codepoint == 0x0EB1 ||
codepoint >= 0x0EB2 && codepoint <= 0x0EB3 ||
codepoint >= 0x0EB4 && codepoint <= 0x0EBC ||
codepoint == 0x0EBD ||
codepoint >= 0x0EC0 && codepoint <= 0x0EC4 ||
codepoint == 0x0EC6 ||
codepoint >= 0x0EC8 && codepoint <= 0x0ECD ||
codepoint >= 0x0ED0 && codepoint <= 0x0ED9 ||
codepoint >= 0x0EDC && codepoint <= 0x0EDF)
{
result = BIDIScript_Lao;
}
else if(codepoint == 0x0F00 ||
codepoint >= 0x0F01 && codepoint <= 0x0F03 ||
codepoint >= 0x0F04 && codepoint <= 0x0F12 ||
codepoint == 0x0F13 ||
codepoint == 0x0F14 ||
codepoint >= 0x0F15 && codepoint <= 0x0F17 ||
codepoint >= 0x0F18 && codepoint <= 0x0F19 ||
codepoint >= 0x0F1A && codepoint <= 0x0F1F ||
codepoint >= 0x0F20 && codepoint <= 0x0F29 ||
codepoint >= 0x0F2A && codepoint <= 0x0F33 ||
codepoint == 0x0F34 ||
codepoint == 0x0F35 ||
codepoint == 0x0F36 ||
codepoint == 0x0F37 ||
codepoint == 0x0F38 ||
codepoint == 0x0F39 ||
codepoint == 0x0F3A ||
codepoint == 0x0F3B ||
codepoint == 0x0F3C ||
codepoint == 0x0F3D ||
codepoint >= 0x0F3E && codepoint <= 0x0F3F ||
codepoint >= 0x0F40 && codepoint <= 0x0F47 ||
codepoint >= 0x0F49 && codepoint <= 0x0F6C ||
codepoint >= 0x0F71 && codepoint <= 0x0F7E ||
codepoint == 0x0F7F ||
codepoint >= 0x0F80 && codepoint <= 0x0F84 ||
codepoint == 0x0F85 ||
codepoint >= 0x0F86 && codepoint <= 0x0F87 ||
codepoint >= 0x0F88 && codepoint <= 0x0F8C ||
codepoint >= 0x0F8D && codepoint <= 0x0F97 ||
codepoint >= 0x0F99 && codepoint <= 0x0FBC ||
codepoint >= 0x0FBE && codepoint <= 0x0FC5 ||
codepoint == 0x0FC6 ||
codepoint >= 0x0FC7 && codepoint <= 0x0FCC ||
codepoint >= 0x0FCE && codepoint <= 0x0FCF ||
codepoint >= 0x0FD0 && codepoint <= 0x0FD4 ||
codepoint >= 0x0FD9 && codepoint <= 0x0FDA)
{
result = BIDIScript_Tibetan;
}
else if(codepoint >= 0x1000 && codepoint <= 0x102A ||
codepoint >= 0x102B && codepoint <= 0x102C ||
codepoint >= 0x102D && codepoint <= 0x1030 ||
codepoint == 0x1031 ||
codepoint >= 0x1032 && codepoint <= 0x1037 ||
codepoint == 0x1038 ||
codepoint >= 0x1039 && codepoint <= 0x103A ||
codepoint >= 0x103B && codepoint <= 0x103C ||
codepoint >= 0x103D && codepoint <= 0x103E ||
codepoint == 0x103F ||
codepoint >= 0x1040 && codepoint <= 0x1049 ||
codepoint >= 0x104A && codepoint <= 0x104F ||
codepoint >= 0x1050 && codepoint <= 0x1055 ||
codepoint >= 0x1056 && codepoint <= 0x1057 ||
codepoint >= 0x1058 && codepoint <= 0x1059 ||
codepoint >= 0x105A && codepoint <= 0x105D ||
codepoint >= 0x105E && codepoint <= 0x1060 ||
codepoint == 0x1061 ||
codepoint >= 0x1062 && codepoint <= 0x1064 ||
codepoint >= 0x1065 && codepoint <= 0x1066 ||
codepoint >= 0x1067 && codepoint <= 0x106D ||
codepoint >= 0x106E && codepoint <= 0x1070 ||
codepoint >= 0x1071 && codepoint <= 0x1074 ||
codepoint >= 0x1075 && codepoint <= 0x1081 ||
codepoint == 0x1082 ||
codepoint >= 0x1083 && codepoint <= 0x1084 ||
codepoint >= 0x1085 && codepoint <= 0x1086 ||
codepoint >= 0x1087 && codepoint <= 0x108C ||
codepoint == 0x108D ||
codepoint == 0x108E ||
codepoint == 0x108F ||
codepoint >= 0x1090 && codepoint <= 0x1099 ||
codepoint >= 0x109A && codepoint <= 0x109C ||
codepoint == 0x109D ||
codepoint >= 0x109E && codepoint <= 0x109F ||
codepoint >= 0xA9E0 && codepoint <= 0xA9E4 ||
codepoint == 0xA9E5 ||
codepoint == 0xA9E6 ||
codepoint >= 0xA9E7 && codepoint <= 0xA9EF ||
codepoint >= 0xA9F0 && codepoint <= 0xA9F9 ||
codepoint >= 0xA9FA && codepoint <= 0xA9FE ||
codepoint >= 0xAA60 && codepoint <= 0xAA6F ||
codepoint == 0xAA70 ||
codepoint >= 0xAA71 && codepoint <= 0xAA76 ||
codepoint >= 0xAA77 && codepoint <= 0xAA79 ||
codepoint == 0xAA7A ||
codepoint == 0xAA7B ||
codepoint == 0xAA7C ||
codepoint == 0xAA7D ||
codepoint >= 0xAA7E && codepoint <= 0xAA7F)
{
result = BIDIScript_Myanmar;
}
else if(codepoint >= 0x10A0 && codepoint <= 0x10C5 ||
codepoint == 0x10C7 ||
codepoint == 0x10CD ||
codepoint >= 0x10D0 && codepoint <= 0x10FA ||
codepoint == 0x10FC ||
codepoint >= 0x10FD && codepoint <= 0x10FF ||
codepoint >= 0x1C90 && codepoint <= 0x1CBA ||
codepoint >= 0x1CBD && codepoint <= 0x1CBF ||
codepoint >= 0x2D00 && codepoint <= 0x2D25 ||
codepoint == 0x2D27 ||
codepoint == 0x2D2D)
{
result = BIDIScript_Georgian;
}
else if(codepoint >= 0x1100 && codepoint <= 0x11FF ||
codepoint >= 0x302E && codepoint <= 0x302F ||
codepoint >= 0x3131 && codepoint <= 0x318E ||
codepoint >= 0x3200 && codepoint <= 0x321E ||
codepoint >= 0x3260 && codepoint <= 0x327E ||
codepoint >= 0xA960 && codepoint <= 0xA97C ||
codepoint >= 0xAC00 && codepoint <= 0xD7A3 ||
codepoint >= 0xD7B0 && codepoint <= 0xD7C6 ||
codepoint >= 0xD7CB && codepoint <= 0xD7FB ||
codepoint >= 0xFFA0 && codepoint <= 0xFFBE ||
codepoint >= 0xFFC2 && codepoint <= 0xFFC7 ||
codepoint >= 0xFFCA && codepoint <= 0xFFCF ||
codepoint >= 0xFFD2 && codepoint <= 0xFFD7 ||
codepoint >= 0xFFDA && codepoint <= 0xFFDC)
{
result = BIDIScript_Hangul;
}
else if(codepoint >= 0x1200 && codepoint <= 0x1248 ||
codepoint >= 0x124A && codepoint <= 0x124D ||
codepoint >= 0x1250 && codepoint <= 0x1256 ||
codepoint == 0x1258 ||
codepoint >= 0x125A && codepoint <= 0x125D ||
codepoint >= 0x1260 && codepoint <= 0x1288 ||
codepoint >= 0x128A && codepoint <= 0x128D ||
codepoint >= 0x1290 && codepoint <= 0x12B0 ||
codepoint >= 0x12B2 && codepoint <= 0x12B5 ||
codepoint >= 0x12B8 && codepoint <= 0x12BE ||
codepoint == 0x12C0 ||
codepoint >= 0x12C2 && codepoint <= 0x12C5 ||
codepoint >= 0x12C8 && codepoint <= 0x12D6 ||
codepoint >= 0x12D8 && codepoint <= 0x1310 ||
codepoint >= 0x1312 && codepoint <= 0x1315 ||
codepoint >= 0x1318 && codepoint <= 0x135A ||
codepoint >= 0x135D && codepoint <= 0x135F ||
codepoint >= 0x1360 && codepoint <= 0x1368 ||
codepoint >= 0x1369 && codepoint <= 0x137C ||
codepoint >= 0x1380 && codepoint <= 0x138F ||
codepoint >= 0x1390 && codepoint <= 0x1399 ||
codepoint >= 0x2D80 && codepoint <= 0x2D96 ||
codepoint >= 0x2DA0 && codepoint <= 0x2DA6 ||
codepoint >= 0x2DA8 && codepoint <= 0x2DAE ||
codepoint >= 0x2DB0 && codepoint <= 0x2DB6 ||
codepoint >= 0x2DB8 && codepoint <= 0x2DBE ||
codepoint >= 0x2DC0 && codepoint <= 0x2DC6 ||
codepoint >= 0x2DC8 && codepoint <= 0x2DCE ||
codepoint >= 0x2DD0 && codepoint <= 0x2DD6 ||
codepoint >= 0x2DD8 && codepoint <= 0x2DDE ||
codepoint >= 0xAB01 && codepoint <= 0xAB06 ||
codepoint >= 0xAB09 && codepoint <= 0xAB0E ||
codepoint >= 0xAB11 && codepoint <= 0xAB16 ||
codepoint >= 0xAB20 && codepoint <= 0xAB26 ||
codepoint >= 0xAB28 && codepoint <= 0xAB2E)
{
result = BIDIScript_Ethiopic;
}
else if(codepoint >= 0x13A0 && codepoint <= 0x13F5 ||
codepoint >= 0x13F8 && codepoint <= 0x13FD ||
codepoint >= 0xAB70 && codepoint <= 0xABBF)
{
result = BIDIScript_Cherokee;
}
else if(codepoint == 0x1400 ||
codepoint >= 0x1401 && codepoint <= 0x166C ||
codepoint == 0x166D ||
codepoint == 0x166E ||
codepoint >= 0x166F && codepoint <= 0x167F ||
codepoint >= 0x18B0 && codepoint <= 0x18F5)
{
result = BIDIScript_Canadian_Aboriginal;
}
else if(codepoint == 0x1680 ||
codepoint >= 0x1681 && codepoint <= 0x169A ||
codepoint == 0x169B ||
codepoint == 0x169C)
{
result = BIDIScript_Ogham;
}
else if(codepoint >= 0x16A0 && codepoint <= 0x16EA ||
codepoint >= 0x16EE && codepoint <= 0x16F0 ||
codepoint >= 0x16F1 && codepoint <= 0x16F8)
{
result = BIDIScript_Runic;
}
else if(codepoint >= 0x1780 && codepoint <= 0x17B3 ||
codepoint >= 0x17B4 && codepoint <= 0x17B5 ||
codepoint == 0x17B6 ||
codepoint >= 0x17B7 && codepoint <= 0x17BD ||
codepoint >= 0x17BE && codepoint <= 0x17C5 ||
codepoint == 0x17C6 ||
codepoint >= 0x17C7 && codepoint <= 0x17C8 ||
codepoint >= 0x17C9 && codepoint <= 0x17D3 ||
codepoint >= 0x17D4 && codepoint <= 0x17D6 ||
codepoint == 0x17D7 ||
codepoint >= 0x17D8 && codepoint <= 0x17DA ||
codepoint == 0x17DB ||
codepoint == 0x17DC ||
codepoint == 0x17DD ||
codepoint >= 0x17E0 && codepoint <= 0x17E9 ||
codepoint >= 0x17F0 && codepoint <= 0x17F9 ||
codepoint >= 0x19E0 && codepoint <= 0x19FF)
{
result = BIDIScript_Khmer;
}
else if(codepoint >= 0x1800 && codepoint <= 0x1801 ||
codepoint == 0x1804 ||
codepoint == 0x1806 ||
codepoint >= 0x1807 && codepoint <= 0x180A ||
codepoint >= 0x180B && codepoint <= 0x180D ||
codepoint == 0x180E ||
codepoint >= 0x1810 && codepoint <= 0x1819 ||
codepoint >= 0x1820 && codepoint <= 0x1842 ||
codepoint == 0x1843 ||
codepoint >= 0x1844 && codepoint <= 0x1878 ||
codepoint >= 0x1880 && codepoint <= 0x1884 ||
codepoint >= 0x1885 && codepoint <= 0x1886 ||
codepoint >= 0x1887 && codepoint <= 0x18A8 ||
codepoint == 0x18A9 ||
codepoint == 0x18AA ||
codepoint >= 0x11660 && codepoint <= 0x1166C)
{
result = BIDIScript_Mongolian;
}
else if(codepoint >= 0x3041 && codepoint <= 0x3096 ||
codepoint >= 0x309D && codepoint <= 0x309E ||
codepoint == 0x309F ||
codepoint >= 0x1B001 && codepoint <= 0x1B11E ||
codepoint >= 0x1B150 && codepoint <= 0x1B152 ||
codepoint == 0x1F200)
{
result = BIDIScript_Hiragana;
}
else if(codepoint >= 0x30A1 && codepoint <= 0x30FA ||
codepoint >= 0x30FD && codepoint <= 0x30FE ||
codepoint == 0x30FF ||
codepoint >= 0x31F0 && codepoint <= 0x31FF ||
codepoint >= 0x32D0 && codepoint <= 0x32FE ||
codepoint >= 0x3300 && codepoint <= 0x3357 ||
codepoint >= 0xFF66 && codepoint <= 0xFF6F ||
codepoint >= 0xFF71 && codepoint <= 0xFF9D ||
codepoint == 0x1B000 ||
codepoint >= 0x1B164 && codepoint <= 0x1B167)
{
result = BIDIScript_Katakana;
}
else if(codepoint >= 0x02EA && codepoint <= 0x02EB ||
codepoint >= 0x3105 && codepoint <= 0x312F ||
codepoint >= 0x31A0 && codepoint <= 0x31BA)
{
result = BIDIScript_Bopomofo;
}
else if(codepoint >= 0x2E80 && codepoint <= 0x2E99 ||
codepoint >= 0x2E9B && codepoint <= 0x2EF3 ||
codepoint >= 0x2F00 && codepoint <= 0x2FD5 ||
codepoint == 0x3005 ||
codepoint == 0x3007 ||
codepoint >= 0x3021 && codepoint <= 0x3029 ||
codepoint >= 0x3038 && codepoint <= 0x303A ||
codepoint == 0x303B ||
codepoint >= 0x3400 && codepoint <= 0x4DB5 ||
codepoint >= 0x4E00 && codepoint <= 0x9FEF ||
codepoint >= 0xF900 && codepoint <= 0xFA6D ||
codepoint >= 0xFA70 && codepoint <= 0xFAD9 ||
codepoint >= 0x20000 && codepoint <= 0x2A6D6 ||
codepoint >= 0x2A700 && codepoint <= 0x2B734 ||
codepoint >= 0x2B740 && codepoint <= 0x2B81D ||
codepoint >= 0x2B820 && codepoint <= 0x2CEA1 ||
codepoint >= 0x2CEB0 && codepoint <= 0x2EBE0 ||
codepoint >= 0x2F800 && codepoint <= 0x2FA1D)
{
result = BIDIScript_Han;
}
else if(codepoint >= 0xA000 && codepoint <= 0xA014 ||
codepoint == 0xA015 ||
codepoint >= 0xA016 && codepoint <= 0xA48C ||
codepoint >= 0xA490 && codepoint <= 0xA4C6)
{
result = BIDIScript_Yi;
}
else if(codepoint >= 0x10300 && codepoint <= 0x1031F ||
codepoint >= 0x10320 && codepoint <= 0x10323 ||
codepoint >= 0x1032D && codepoint <= 0x1032F)
{
result = BIDIScript_Old_Italic;
}
else if(codepoint >= 0x10330 && codepoint <= 0x10340 ||
codepoint == 0x10341 ||
codepoint >= 0x10342 && codepoint <= 0x10349 ||
codepoint == 0x1034A)
{
result = BIDIScript_Gothic;
}
else if(codepoint >= 0x10400 && codepoint <= 0x1044F)
{
result = BIDIScript_Deseret;
}
else if(codepoint >= 0x0300 && codepoint <= 0x036F ||
codepoint >= 0x0485 && codepoint <= 0x0486 ||
codepoint >= 0x064B && codepoint <= 0x0655 ||
codepoint == 0x0670 ||
codepoint >= 0x0951 && codepoint <= 0x0954 ||
codepoint >= 0x1AB0 && codepoint <= 0x1ABD ||
codepoint == 0x1ABE ||
codepoint >= 0x1CD0 && codepoint <= 0x1CD2 ||
codepoint >= 0x1CD4 && codepoint <= 0x1CE0 ||
codepoint >= 0x1CE2 && codepoint <= 0x1CE8 ||
codepoint == 0x1CED ||
codepoint == 0x1CF4 ||
codepoint >= 0x1CF8 && codepoint <= 0x1CF9 ||
codepoint >= 0x1DC0 && codepoint <= 0x1DF9 ||
codepoint >= 0x1DFB && codepoint <= 0x1DFF ||
codepoint >= 0x200C && codepoint <= 0x200D ||
codepoint >= 0x20D0 && codepoint <= 0x20DC ||
codepoint >= 0x20DD && codepoint <= 0x20E0 ||
codepoint == 0x20E1 ||
codepoint >= 0x20E2 && codepoint <= 0x20E4 ||
codepoint >= 0x20E5 && codepoint <= 0x20F0 ||
codepoint >= 0x302A && codepoint <= 0x302D ||
codepoint >= 0x3099 && codepoint <= 0x309A ||
codepoint >= 0xFE00 && codepoint <= 0xFE0F ||
codepoint >= 0xFE20 && codepoint <= 0xFE2D ||
codepoint == 0x101FD ||
codepoint == 0x102E0 ||
codepoint == 0x1133B ||
codepoint >= 0x1D167 && codepoint <= 0x1D169 ||
codepoint >= 0x1D17B && codepoint <= 0x1D182 ||
codepoint >= 0x1D185 && codepoint <= 0x1D18B ||
codepoint >= 0x1D1AA && codepoint <= 0x1D1AD ||
codepoint >= 0xE0100 && codepoint <= 0xE01EF)
{
result = BIDIScript_Inherited;
}
else if(codepoint >= 0x1700 && codepoint <= 0x170C ||
codepoint >= 0x170E && codepoint <= 0x1711 ||
codepoint >= 0x1712 && codepoint <= 0x1714)
{
result = BIDIScript_Tagalog;
}
else if(codepoint >= 0x1720 && codepoint <= 0x1731 ||
codepoint >= 0x1732 && codepoint <= 0x1734)
{
result = BIDIScript_Hanunoo;
}
else if(codepoint >= 0x1740 && codepoint <= 0x1751 ||
codepoint >= 0x1752 && codepoint <= 0x1753)
{
result = BIDIScript_Buhid;
}
else if(codepoint >= 0x1760 && codepoint <= 0x176C ||
codepoint >= 0x176E && codepoint <= 0x1770 ||
codepoint >= 0x1772 && codepoint <= 0x1773)
{
result = BIDIScript_Tagbanwa;
}
else if(codepoint >= 0x1900 && codepoint <= 0x191E ||
codepoint >= 0x1920 && codepoint <= 0x1922 ||
codepoint >= 0x1923 && codepoint <= 0x1926 ||
codepoint >= 0x1927 && codepoint <= 0x1928 ||
codepoint >= 0x1929 && codepoint <= 0x192B ||
codepoint >= 0x1930 && codepoint <= 0x1931 ||
codepoint == 0x1932 ||
codepoint >= 0x1933 && codepoint <= 0x1938 ||
codepoint >= 0x1939 && codepoint <= 0x193B ||
codepoint == 0x1940 ||
codepoint >= 0x1944 && codepoint <= 0x1945 ||
codepoint >= 0x1946 && codepoint <= 0x194F)
{
result = BIDIScript_Limbu;
}
else if(codepoint >= 0x1950 && codepoint <= 0x196D ||
codepoint >= 0x1970 && codepoint <= 0x1974)
{
result = BIDIScript_Tai_Le;
}
else if(codepoint >= 0x10000 && codepoint <= 0x1000B ||
codepoint >= 0x1000D && codepoint <= 0x10026 ||
codepoint >= 0x10028 && codepoint <= 0x1003A ||
codepoint >= 0x1003C && codepoint <= 0x1003D ||
codepoint >= 0x1003F && codepoint <= 0x1004D ||
codepoint >= 0x10050 && codepoint <= 0x1005D ||
codepoint >= 0x10080 && codepoint <= 0x100FA)
{
result = BIDIScript_Linear_B;
}
else if(codepoint >= 0x10380 && codepoint <= 0x1039D ||
codepoint == 0x1039F)
{
result = BIDIScript_Ugaritic;
}
else if(codepoint >= 0x10450 && codepoint <= 0x1047F)
{
result = BIDIScript_Shavian;
}
else if(codepoint >= 0x10480 && codepoint <= 0x1049D ||
codepoint >= 0x104A0 && codepoint <= 0x104A9)
{
result = BIDIScript_Osmanya;
}
else if(codepoint >= 0x10800 && codepoint <= 0x10805 ||
codepoint == 0x10808 ||
codepoint >= 0x1080A && codepoint <= 0x10835 ||
codepoint >= 0x10837 && codepoint <= 0x10838 ||
codepoint == 0x1083C ||
codepoint == 0x1083F)
{
result = BIDIScript_Cypriot;
}
else if(codepoint >= 0x2800 && codepoint <= 0x28FF)
{
result = BIDIScript_Braille;
}
else if(codepoint >= 0x1A00 && codepoint <= 0x1A16 ||
codepoint >= 0x1A17 && codepoint <= 0x1A18 ||
codepoint >= 0x1A19 && codepoint <= 0x1A1A ||
codepoint == 0x1A1B ||
codepoint >= 0x1A1E && codepoint <= 0x1A1F)
{
result = BIDIScript_Buginese;
}
else if(codepoint >= 0x03E2 && codepoint <= 0x03EF ||
codepoint >= 0x2C80 && codepoint <= 0x2CE4 ||
codepoint >= 0x2CE5 && codepoint <= 0x2CEA ||
codepoint >= 0x2CEB && codepoint <= 0x2CEE ||
codepoint >= 0x2CEF && codepoint <= 0x2CF1 ||
codepoint >= 0x2CF2 && codepoint <= 0x2CF3 ||
codepoint >= 0x2CF9 && codepoint <= 0x2CFC ||
codepoint == 0x2CFD ||
codepoint >= 0x2CFE && codepoint <= 0x2CFF)
{
result = BIDIScript_Coptic;
}
else if(codepoint >= 0x1980 && codepoint <= 0x19AB ||
codepoint >= 0x19B0 && codepoint <= 0x19C9 ||
codepoint >= 0x19D0 && codepoint <= 0x19D9 ||
codepoint == 0x19DA ||
codepoint >= 0x19DE && codepoint <= 0x19DF)
{
result = BIDIScript_New_Tai_Lue;
}
else if(codepoint >= 0x2C00 && codepoint <= 0x2C2E ||
codepoint >= 0x2C30 && codepoint <= 0x2C5E ||
codepoint >= 0x1E000 && codepoint <= 0x1E006 ||
codepoint >= 0x1E008 && codepoint <= 0x1E018 ||
codepoint >= 0x1E01B && codepoint <= 0x1E021 ||
codepoint >= 0x1E023 && codepoint <= 0x1E024 ||
codepoint >= 0x1E026 && codepoint <= 0x1E02A)
{
result = BIDIScript_Glagolitic;
}
else if(codepoint >= 0x2D30 && codepoint <= 0x2D67 ||
codepoint == 0x2D6F ||
codepoint == 0x2D70 ||
codepoint == 0x2D7F)
{
result = BIDIScript_Tifinagh;
}
else if(codepoint >= 0xA800 && codepoint <= 0xA801 ||
codepoint == 0xA802 ||
codepoint >= 0xA803 && codepoint <= 0xA805 ||
codepoint == 0xA806 ||
codepoint >= 0xA807 && codepoint <= 0xA80A ||
codepoint == 0xA80B ||
codepoint >= 0xA80C && codepoint <= 0xA822 ||
codepoint >= 0xA823 && codepoint <= 0xA824 ||
codepoint >= 0xA825 && codepoint <= 0xA826 ||
codepoint == 0xA827 ||
codepoint >= 0xA828 && codepoint <= 0xA82B)
{
result = BIDIScript_Syloti_Nagri;
}
else if(codepoint >= 0x103A0 && codepoint <= 0x103C3 ||
codepoint >= 0x103C8 && codepoint <= 0x103CF ||
codepoint == 0x103D0 ||
codepoint >= 0x103D1 && codepoint <= 0x103D5)
{
result = BIDIScript_Old_Persian;
}
else if(codepoint == 0x10A00 ||
codepoint >= 0x10A01 && codepoint <= 0x10A03 ||
codepoint >= 0x10A05 && codepoint <= 0x10A06 ||
codepoint >= 0x10A0C && codepoint <= 0x10A0F ||
codepoint >= 0x10A10 && codepoint <= 0x10A13 ||
codepoint >= 0x10A15 && codepoint <= 0x10A17 ||
codepoint >= 0x10A19 && codepoint <= 0x10A35 ||
codepoint >= 0x10A38 && codepoint <= 0x10A3A ||
codepoint == 0x10A3F ||
codepoint >= 0x10A40 && codepoint <= 0x10A48 ||
codepoint >= 0x10A50 && codepoint <= 0x10A58)
{
result = BIDIScript_Kharoshthi;
}
else if(codepoint >= 0x1B00 && codepoint <= 0x1B03 ||
codepoint == 0x1B04 ||
codepoint >= 0x1B05 && codepoint <= 0x1B33 ||
codepoint == 0x1B34 ||
codepoint == 0x1B35 ||
codepoint >= 0x1B36 && codepoint <= 0x1B3A ||
codepoint == 0x1B3B ||
codepoint == 0x1B3C ||
codepoint >= 0x1B3D && codepoint <= 0x1B41 ||
codepoint == 0x1B42 ||
codepoint >= 0x1B43 && codepoint <= 0x1B44 ||
codepoint >= 0x1B45 && codepoint <= 0x1B4B ||
codepoint >= 0x1B50 && codepoint <= 0x1B59 ||
codepoint >= 0x1B5A && codepoint <= 0x1B60 ||
codepoint >= 0x1B61 && codepoint <= 0x1B6A ||
codepoint >= 0x1B6B && codepoint <= 0x1B73 ||
codepoint >= 0x1B74 && codepoint <= 0x1B7C)
{
result = BIDIScript_Balinese;
}
else if(codepoint >= 0x12000 && codepoint <= 0x12399 ||
codepoint >= 0x12400 && codepoint <= 0x1246E ||
codepoint >= 0x12470 && codepoint <= 0x12474 ||
codepoint >= 0x12480 && codepoint <= 0x12543)
{
result = BIDIScript_Cuneiform;
}
else if(codepoint >= 0x10900 && codepoint <= 0x10915 ||
codepoint >= 0x10916 && codepoint <= 0x1091B ||
codepoint == 0x1091F)
{
result = BIDIScript_Phoenician;
}
else if(codepoint >= 0xA840 && codepoint <= 0xA873 ||
codepoint >= 0xA874 && codepoint <= 0xA877)
{
result = BIDIScript_Phags_Pa;
}
else if(codepoint >= 0x07C0 && codepoint <= 0x07C9 ||
codepoint >= 0x07CA && codepoint <= 0x07EA ||
codepoint >= 0x07EB && codepoint <= 0x07F3 ||
codepoint >= 0x07F4 && codepoint <= 0x07F5 ||
codepoint == 0x07F6 ||
codepoint >= 0x07F7 && codepoint <= 0x07F9 ||
codepoint == 0x07FA ||
codepoint == 0x07FD ||
codepoint >= 0x07FE && codepoint <= 0x07FF)
{
result = BIDIScript_Nko;
}
else if(codepoint >= 0x1B80 && codepoint <= 0x1B81 ||
codepoint == 0x1B82 ||
codepoint >= 0x1B83 && codepoint <= 0x1BA0 ||
codepoint == 0x1BA1 ||
codepoint >= 0x1BA2 && codepoint <= 0x1BA5 ||
codepoint >= 0x1BA6 && codepoint <= 0x1BA7 ||
codepoint >= 0x1BA8 && codepoint <= 0x1BA9 ||
codepoint == 0x1BAA ||
codepoint >= 0x1BAB && codepoint <= 0x1BAD ||
codepoint >= 0x1BAE && codepoint <= 0x1BAF ||
codepoint >= 0x1BB0 && codepoint <= 0x1BB9 ||
codepoint >= 0x1BBA && codepoint <= 0x1BBF ||
codepoint >= 0x1CC0 && codepoint <= 0x1CC7)
{
result = BIDIScript_Sundanese;
}
else if(codepoint >= 0x1C00 && codepoint <= 0x1C23 ||
codepoint >= 0x1C24 && codepoint <= 0x1C2B ||
codepoint >= 0x1C2C && codepoint <= 0x1C33 ||
codepoint >= 0x1C34 && codepoint <= 0x1C35 ||
codepoint >= 0x1C36 && codepoint <= 0x1C37 ||
codepoint >= 0x1C3B && codepoint <= 0x1C3F ||
codepoint >= 0x1C40 && codepoint <= 0x1C49 ||
codepoint >= 0x1C4D && codepoint <= 0x1C4F)
{
result = BIDIScript_Lepcha;
}
else if(codepoint >= 0x1C50 && codepoint <= 0x1C59 ||
codepoint >= 0x1C5A && codepoint <= 0x1C77 ||
codepoint >= 0x1C78 && codepoint <= 0x1C7D ||
codepoint >= 0x1C7E && codepoint <= 0x1C7F)
{
result = BIDIScript_Ol_Chiki;
}
else if(codepoint >= 0xA500 && codepoint <= 0xA60B ||
codepoint == 0xA60C ||
codepoint >= 0xA60D && codepoint <= 0xA60F ||
codepoint >= 0xA610 && codepoint <= 0xA61F ||
codepoint >= 0xA620 && codepoint <= 0xA629 ||
codepoint >= 0xA62A && codepoint <= 0xA62B)
{
result = BIDIScript_Vai;
}
else if(codepoint >= 0xA880 && codepoint <= 0xA881 ||
codepoint >= 0xA882 && codepoint <= 0xA8B3 ||
codepoint >= 0xA8B4 && codepoint <= 0xA8C3 ||
codepoint >= 0xA8C4 && codepoint <= 0xA8C5 ||
codepoint >= 0xA8CE && codepoint <= 0xA8CF ||
codepoint >= 0xA8D0 && codepoint <= 0xA8D9)
{
result = BIDIScript_Saurashtra;
}
else if(codepoint >= 0xA900 && codepoint <= 0xA909 ||
codepoint >= 0xA90A && codepoint <= 0xA925 ||
codepoint >= 0xA926 && codepoint <= 0xA92D ||
codepoint == 0xA92F)
{
result = BIDIScript_Kayah_Li;
}
else if(codepoint >= 0xA930 && codepoint <= 0xA946 ||
codepoint >= 0xA947 && codepoint <= 0xA951 ||
codepoint >= 0xA952 && codepoint <= 0xA953 ||
codepoint == 0xA95F)
{
result = BIDIScript_Rejang;
}
else if(codepoint >= 0x10280 && codepoint <= 0x1029C)
{
result = BIDIScript_Lycian;
}
else if(codepoint >= 0x102A0 && codepoint <= 0x102D0)
{
result = BIDIScript_Carian;
}
else if(codepoint >= 0x10920 && codepoint <= 0x10939 ||
codepoint == 0x1093F)
{
result = BIDIScript_Lydian;
}
else if(codepoint >= 0xAA00 && codepoint <= 0xAA28 ||
codepoint >= 0xAA29 && codepoint <= 0xAA2E ||
codepoint >= 0xAA2F && codepoint <= 0xAA30 ||
codepoint >= 0xAA31 && codepoint <= 0xAA32 ||
codepoint >= 0xAA33 && codepoint <= 0xAA34 ||
codepoint >= 0xAA35 && codepoint <= 0xAA36 ||
codepoint >= 0xAA40 && codepoint <= 0xAA42 ||
codepoint == 0xAA43 ||
codepoint >= 0xAA44 && codepoint <= 0xAA4B ||
codepoint == 0xAA4C ||
codepoint == 0xAA4D ||
codepoint >= 0xAA50 && codepoint <= 0xAA59 ||
codepoint >= 0xAA5C && codepoint <= 0xAA5F)
{
result = BIDIScript_Cham;
}
else if(codepoint >= 0x1A20 && codepoint <= 0x1A54 ||
codepoint == 0x1A55 ||
codepoint == 0x1A56 ||
codepoint == 0x1A57 ||
codepoint >= 0x1A58 && codepoint <= 0x1A5E ||
codepoint == 0x1A60 ||
codepoint == 0x1A61 ||
codepoint == 0x1A62 ||
codepoint >= 0x1A63 && codepoint <= 0x1A64 ||
codepoint >= 0x1A65 && codepoint <= 0x1A6C ||
codepoint >= 0x1A6D && codepoint <= 0x1A72 ||
codepoint >= 0x1A73 && codepoint <= 0x1A7C ||
codepoint == 0x1A7F ||
codepoint >= 0x1A80 && codepoint <= 0x1A89 ||
codepoint >= 0x1A90 && codepoint <= 0x1A99 ||
codepoint >= 0x1AA0 && codepoint <= 0x1AA6 ||
codepoint == 0x1AA7 ||
codepoint >= 0x1AA8 && codepoint <= 0x1AAD)
{
result = BIDIScript_Tai_Tham;
}
else if(codepoint >= 0xAA80 && codepoint <= 0xAAAF ||
codepoint == 0xAAB0 ||
codepoint == 0xAAB1 ||
codepoint >= 0xAAB2 && codepoint <= 0xAAB4 ||
codepoint >= 0xAAB5 && codepoint <= 0xAAB6 ||
codepoint >= 0xAAB7 && codepoint <= 0xAAB8 ||
codepoint >= 0xAAB9 && codepoint <= 0xAABD ||
codepoint >= 0xAABE && codepoint <= 0xAABF ||
codepoint == 0xAAC0 ||
codepoint == 0xAAC1 ||
codepoint == 0xAAC2 ||
codepoint >= 0xAADB && codepoint <= 0xAADC ||
codepoint == 0xAADD ||
codepoint >= 0xAADE && codepoint <= 0xAADF)
{
result = BIDIScript_Tai_Viet;
}
else if(codepoint >= 0x10B00 && codepoint <= 0x10B35 ||
codepoint >= 0x10B39 && codepoint <= 0x10B3F)
{
result = BIDIScript_Avestan;
}
else if(codepoint >= 0x13000 && codepoint <= 0x1342E ||
codepoint >= 0x13430 && codepoint <= 0x13438)
{
result = BIDIScript_Egyptian_Hieroglyphs;
}
else if(codepoint >= 0x0800 && codepoint <= 0x0815 ||
codepoint >= 0x0816 && codepoint <= 0x0819 ||
codepoint == 0x081A ||
codepoint >= 0x081B && codepoint <= 0x0823 ||
codepoint == 0x0824 ||
codepoint >= 0x0825 && codepoint <= 0x0827 ||
codepoint == 0x0828 ||
codepoint >= 0x0829 && codepoint <= 0x082D ||
codepoint >= 0x0830 && codepoint <= 0x083E)
{
result = BIDIScript_Samaritan;
}
else if(codepoint >= 0xA4D0 && codepoint <= 0xA4F7 ||
codepoint >= 0xA4F8 && codepoint <= 0xA4FD ||
codepoint >= 0xA4FE && codepoint <= 0xA4FF)
{
result = BIDIScript_Lisu;
}
else if(codepoint >= 0xA6A0 && codepoint <= 0xA6E5 ||
codepoint >= 0xA6E6 && codepoint <= 0xA6EF ||
codepoint >= 0xA6F0 && codepoint <= 0xA6F1 ||
codepoint >= 0xA6F2 && codepoint <= 0xA6F7 ||
codepoint >= 0x16800 && codepoint <= 0x16A38)
{
result = BIDIScript_Bamum;
}
else if(codepoint >= 0xA980 && codepoint <= 0xA982 ||
codepoint == 0xA983 ||
codepoint >= 0xA984 && codepoint <= 0xA9B2 ||
codepoint == 0xA9B3 ||
codepoint >= 0xA9B4 && codepoint <= 0xA9B5 ||
codepoint >= 0xA9B6 && codepoint <= 0xA9B9 ||
codepoint >= 0xA9BA && codepoint <= 0xA9BB ||
codepoint >= 0xA9BC && codepoint <= 0xA9BD ||
codepoint >= 0xA9BE && codepoint <= 0xA9C0 ||
codepoint >= 0xA9C1 && codepoint <= 0xA9CD ||
codepoint >= 0xA9D0 && codepoint <= 0xA9D9 ||
codepoint >= 0xA9DE && codepoint <= 0xA9DF)
{
result = BIDIScript_Javanese;
}
else if(codepoint >= 0xAAE0 && codepoint <= 0xAAEA ||
codepoint == 0xAAEB ||
codepoint >= 0xAAEC && codepoint <= 0xAAED ||
codepoint >= 0xAAEE && codepoint <= 0xAAEF ||
codepoint >= 0xAAF0 && codepoint <= 0xAAF1 ||
codepoint == 0xAAF2 ||
codepoint >= 0xAAF3 && codepoint <= 0xAAF4 ||
codepoint == 0xAAF5 ||
codepoint == 0xAAF6 ||
codepoint >= 0xABC0 && codepoint <= 0xABE2 ||
codepoint >= 0xABE3 && codepoint <= 0xABE4 ||
codepoint == 0xABE5 ||
codepoint >= 0xABE6 && codepoint <= 0xABE7 ||
codepoint == 0xABE8 ||
codepoint >= 0xABE9 && codepoint <= 0xABEA ||
codepoint == 0xABEB ||
codepoint == 0xABEC ||
codepoint == 0xABED ||
codepoint >= 0xABF0 && codepoint <= 0xABF9)
{
result = BIDIScript_Meetei_Mayek;
}
else if(codepoint >= 0x10840 && codepoint <= 0x10855 ||
codepoint == 0x10857 ||
codepoint >= 0x10858 && codepoint <= 0x1085F)
{
result = BIDIScript_Imperial_Aramaic;
}
else if(codepoint >= 0x10A60 && codepoint <= 0x10A7C ||
codepoint >= 0x10A7D && codepoint <= 0x10A7E ||
codepoint == 0x10A7F)
{
result = BIDIScript_Old_South_Arabian;
}
else if(codepoint >= 0x10B40 && codepoint <= 0x10B55 ||
codepoint >= 0x10B58 && codepoint <= 0x10B5F)
{
result = BIDIScript_Inscriptional_Parthian;
}
else if(codepoint >= 0x10B60 && codepoint <= 0x10B72 ||
codepoint >= 0x10B78 && codepoint <= 0x10B7F)
{
result = BIDIScript_Inscriptional_Pahlavi;
}
else if(codepoint >= 0x10C00 && codepoint <= 0x10C48)
{
result = BIDIScript_Old_Turkic;
}
else if(codepoint >= 0x11080 && codepoint <= 0x11081 ||
codepoint == 0x11082 ||
codepoint >= 0x11083 && codepoint <= 0x110AF ||
codepoint >= 0x110B0 && codepoint <= 0x110B2 ||
codepoint >= 0x110B3 && codepoint <= 0x110B6 ||
codepoint >= 0x110B7 && codepoint <= 0x110B8 ||
codepoint >= 0x110B9 && codepoint <= 0x110BA ||
codepoint >= 0x110BB && codepoint <= 0x110BC ||
codepoint == 0x110BD ||
codepoint >= 0x110BE && codepoint <= 0x110C1 ||
codepoint == 0x110CD)
{
result = BIDIScript_Kaithi;
}
else if(codepoint >= 0x1BC0 && codepoint <= 0x1BE5 ||
codepoint == 0x1BE6 ||
codepoint == 0x1BE7 ||
codepoint >= 0x1BE8 && codepoint <= 0x1BE9 ||
codepoint >= 0x1BEA && codepoint <= 0x1BEC ||
codepoint == 0x1BED ||
codepoint == 0x1BEE ||
codepoint >= 0x1BEF && codepoint <= 0x1BF1 ||
codepoint >= 0x1BF2 && codepoint <= 0x1BF3 ||
codepoint >= 0x1BFC && codepoint <= 0x1BFF)
{
result = BIDIScript_Batak;
}
else if(codepoint == 0x11000 ||
codepoint == 0x11001 ||
codepoint == 0x11002 ||
codepoint >= 0x11003 && codepoint <= 0x11037 ||
codepoint >= 0x11038 && codepoint <= 0x11046 ||
codepoint >= 0x11047 && codepoint <= 0x1104D ||
codepoint >= 0x11052 && codepoint <= 0x11065 ||
codepoint >= 0x11066 && codepoint <= 0x1106F ||
codepoint == 0x1107F)
{
result = BIDIScript_Brahmi;
}
else if(codepoint >= 0x0840 && codepoint <= 0x0858 ||
codepoint >= 0x0859 && codepoint <= 0x085B ||
codepoint == 0x085E)
{
result = BIDIScript_Mandaic;
}
else if(codepoint >= 0x11100 && codepoint <= 0x11102 ||
codepoint >= 0x11103 && codepoint <= 0x11126 ||
codepoint >= 0x11127 && codepoint <= 0x1112B ||
codepoint == 0x1112C ||
codepoint >= 0x1112D && codepoint <= 0x11134 ||
codepoint >= 0x11136 && codepoint <= 0x1113F ||
codepoint >= 0x11140 && codepoint <= 0x11143 ||
codepoint == 0x11144 ||
codepoint >= 0x11145 && codepoint <= 0x11146)
{
result = BIDIScript_Chakma;
}
else if(codepoint >= 0x109A0 && codepoint <= 0x109B7 ||
codepoint >= 0x109BC && codepoint <= 0x109BD ||
codepoint >= 0x109BE && codepoint <= 0x109BF ||
codepoint >= 0x109C0 && codepoint <= 0x109CF ||
codepoint >= 0x109D2 && codepoint <= 0x109FF)
{
result = BIDIScript_Meroitic_Cursive;
}
else if(codepoint >= 0x10980 && codepoint <= 0x1099F)
{
result = BIDIScript_Meroitic_Hieroglyphs;
}
else if(codepoint >= 0x16F00 && codepoint <= 0x16F4A ||
codepoint == 0x16F4F ||
codepoint == 0x16F50 ||
codepoint >= 0x16F51 && codepoint <= 0x16F87 ||
codepoint >= 0x16F8F && codepoint <= 0x16F92 ||
codepoint >= 0x16F93 && codepoint <= 0x16F9F)
{
result = BIDIScript_Miao;
}
else if(codepoint >= 0x11180 && codepoint <= 0x11181 ||
codepoint == 0x11182 ||
codepoint >= 0x11183 && codepoint <= 0x111B2 ||
codepoint >= 0x111B3 && codepoint <= 0x111B5 ||
codepoint >= 0x111B6 && codepoint <= 0x111BE ||
codepoint >= 0x111BF && codepoint <= 0x111C0 ||
codepoint >= 0x111C1 && codepoint <= 0x111C4 ||
codepoint >= 0x111C5 && codepoint <= 0x111C8 ||
codepoint >= 0x111C9 && codepoint <= 0x111CC ||
codepoint == 0x111CD ||
codepoint >= 0x111D0 && codepoint <= 0x111D9 ||
codepoint == 0x111DA ||
codepoint == 0x111DB ||
codepoint == 0x111DC ||
codepoint >= 0x111DD && codepoint <= 0x111DF)
{
result = BIDIScript_Sharada;
}
else if(codepoint >= 0x110D0 && codepoint <= 0x110E8 ||
codepoint >= 0x110F0 && codepoint <= 0x110F9)
{
result = BIDIScript_Sora_Sompeng;
}
else if(codepoint >= 0x11680 && codepoint <= 0x116AA ||
codepoint == 0x116AB ||
codepoint == 0x116AC ||
codepoint == 0x116AD ||
codepoint >= 0x116AE && codepoint <= 0x116AF ||
codepoint >= 0x116B0 && codepoint <= 0x116B5 ||
codepoint == 0x116B6 ||
codepoint == 0x116B7 ||
codepoint == 0x116B8 ||
codepoint >= 0x116C0 && codepoint <= 0x116C9)
{
result = BIDIScript_Takri;
}
else if(codepoint >= 0x10530 && codepoint <= 0x10563 ||
codepoint == 0x1056F)
{
result = BIDIScript_Caucasian_Albanian;
}
else if(codepoint >= 0x16AD0 && codepoint <= 0x16AED ||
codepoint >= 0x16AF0 && codepoint <= 0x16AF4 ||
codepoint == 0x16AF5)
{
result = BIDIScript_Bassa_Vah;
}
else if(codepoint >= 0x1BC00 && codepoint <= 0x1BC6A ||
codepoint >= 0x1BC70 && codepoint <= 0x1BC7C ||
codepoint >= 0x1BC80 && codepoint <= 0x1BC88 ||
codepoint >= 0x1BC90 && codepoint <= 0x1BC99 ||
codepoint == 0x1BC9C ||
codepoint >= 0x1BC9D && codepoint <= 0x1BC9E ||
codepoint == 0x1BC9F)
{
result = BIDIScript_Duployan;
}
else if(codepoint >= 0x10500 && codepoint <= 0x10527)
{
result = BIDIScript_Elbasan;
}
else if(codepoint >= 0x11300 && codepoint <= 0x11301 ||
codepoint >= 0x11302 && codepoint <= 0x11303 ||
codepoint >= 0x11305 && codepoint <= 0x1130C ||
codepoint >= 0x1130F && codepoint <= 0x11310 ||
codepoint >= 0x11313 && codepoint <= 0x11328 ||
codepoint >= 0x1132A && codepoint <= 0x11330 ||
codepoint >= 0x11332 && codepoint <= 0x11333 ||
codepoint >= 0x11335 && codepoint <= 0x11339 ||
codepoint == 0x1133C ||
codepoint == 0x1133D ||
codepoint >= 0x1133E && codepoint <= 0x1133F ||
codepoint == 0x11340 ||
codepoint >= 0x11341 && codepoint <= 0x11344 ||
codepoint >= 0x11347 && codepoint <= 0x11348 ||
codepoint >= 0x1134B && codepoint <= 0x1134D ||
codepoint == 0x11350 ||
codepoint == 0x11357 ||
codepoint >= 0x1135D && codepoint <= 0x11361 ||
codepoint >= 0x11362 && codepoint <= 0x11363 ||
codepoint >= 0x11366 && codepoint <= 0x1136C ||
codepoint >= 0x11370 && codepoint <= 0x11374)
{
result = BIDIScript_Grantha;
}
else if(codepoint >= 0x16B00 && codepoint <= 0x16B2F ||
codepoint >= 0x16B30 && codepoint <= 0x16B36 ||
codepoint >= 0x16B37 && codepoint <= 0x16B3B ||
codepoint >= 0x16B3C && codepoint <= 0x16B3F ||
codepoint >= 0x16B40 && codepoint <= 0x16B43 ||
codepoint == 0x16B44 ||
codepoint == 0x16B45 ||
codepoint >= 0x16B50 && codepoint <= 0x16B59 ||
codepoint >= 0x16B5B && codepoint <= 0x16B61 ||
codepoint >= 0x16B63 && codepoint <= 0x16B77 ||
codepoint >= 0x16B7D && codepoint <= 0x16B8F)
{
result = BIDIScript_Pahawh_Hmong;
}
else if(codepoint >= 0x11200 && codepoint <= 0x11211 ||
codepoint >= 0x11213 && codepoint <= 0x1122B ||
codepoint >= 0x1122C && codepoint <= 0x1122E ||
codepoint >= 0x1122F && codepoint <= 0x11231 ||
codepoint >= 0x11232 && codepoint <= 0x11233 ||
codepoint == 0x11234 ||
codepoint == 0x11235 ||
codepoint >= 0x11236 && codepoint <= 0x11237 ||
codepoint >= 0x11238 && codepoint <= 0x1123D ||
codepoint == 0x1123E)
{
result = BIDIScript_Khojki;
}
else if(codepoint >= 0x10600 && codepoint <= 0x10736 ||
codepoint >= 0x10740 && codepoint <= 0x10755 ||
codepoint >= 0x10760 && codepoint <= 0x10767)
{
result = BIDIScript_Linear_A;
}
else if(codepoint >= 0x11150 && codepoint <= 0x11172 ||
codepoint == 0x11173 ||
codepoint >= 0x11174 && codepoint <= 0x11175 ||
codepoint == 0x11176)
{
result = BIDIScript_Mahajani;
}
else if(codepoint >= 0x10AC0 && codepoint <= 0x10AC7 ||
codepoint == 0x10AC8 ||
codepoint >= 0x10AC9 && codepoint <= 0x10AE4 ||
codepoint >= 0x10AE5 && codepoint <= 0x10AE6 ||
codepoint >= 0x10AEB && codepoint <= 0x10AEF ||
codepoint >= 0x10AF0 && codepoint <= 0x10AF6)
{
result = BIDIScript_Manichaean;
}
else if(codepoint >= 0x1E800 && codepoint <= 0x1E8C4 ||
codepoint >= 0x1E8C7 && codepoint <= 0x1E8CF ||
codepoint >= 0x1E8D0 && codepoint <= 0x1E8D6)
{
result = BIDIScript_Mende_Kikakui;
}
else if(codepoint >= 0x11600 && codepoint <= 0x1162F ||
codepoint >= 0x11630 && codepoint <= 0x11632 ||
codepoint >= 0x11633 && codepoint <= 0x1163A ||
codepoint >= 0x1163B && codepoint <= 0x1163C ||
codepoint == 0x1163D ||
codepoint == 0x1163E ||
codepoint >= 0x1163F && codepoint <= 0x11640 ||
codepoint >= 0x11641 && codepoint <= 0x11643 ||
codepoint == 0x11644 ||
codepoint >= 0x11650 && codepoint <= 0x11659)
{
result = BIDIScript_Modi;
}
else if(codepoint >= 0x16A40 && codepoint <= 0x16A5E ||
codepoint >= 0x16A60 && codepoint <= 0x16A69 ||
codepoint >= 0x16A6E && codepoint <= 0x16A6F)
{
result = BIDIScript_Mro;
}
else if(codepoint >= 0x10A80 && codepoint <= 0x10A9C ||
codepoint >= 0x10A9D && codepoint <= 0x10A9F)
{
result = BIDIScript_Old_North_Arabian;
}
else if(codepoint >= 0x10880 && codepoint <= 0x1089E ||
codepoint >= 0x108A7 && codepoint <= 0x108AF)
{
result = BIDIScript_Nabataean;
}
else if(codepoint >= 0x10860 && codepoint <= 0x10876 ||
codepoint >= 0x10877 && codepoint <= 0x10878 ||
codepoint >= 0x10879 && codepoint <= 0x1087F)
{
result = BIDIScript_Palmyrene;
}
else if(codepoint >= 0x11AC0 && codepoint <= 0x11AF8)
{
result = BIDIScript_Pau_Cin_Hau;
}
else if(codepoint >= 0x10350 && codepoint <= 0x10375 ||
codepoint >= 0x10376 && codepoint <= 0x1037A)
{
result = BIDIScript_Old_Permic;
}
else if(codepoint >= 0x10B80 && codepoint <= 0x10B91 ||
codepoint >= 0x10B99 && codepoint <= 0x10B9C ||
codepoint >= 0x10BA9 && codepoint <= 0x10BAF)
{
result = BIDIScript_Psalter_Pahlavi;
}
else if(codepoint >= 0x11580 && codepoint <= 0x115AE ||
codepoint >= 0x115AF && codepoint <= 0x115B1 ||
codepoint >= 0x115B2 && codepoint <= 0x115B5 ||
codepoint >= 0x115B8 && codepoint <= 0x115BB ||
codepoint >= 0x115BC && codepoint <= 0x115BD ||
codepoint == 0x115BE ||
codepoint >= 0x115BF && codepoint <= 0x115C0 ||
codepoint >= 0x115C1 && codepoint <= 0x115D7 ||
codepoint >= 0x115D8 && codepoint <= 0x115DB ||
codepoint >= 0x115DC && codepoint <= 0x115DD)
{
result = BIDIScript_Siddham;
}

// this split gets rid of compiler limit: blocks nested too deeply

if(codepoint >= 0x112B0 && codepoint <= 0x112DE ||
codepoint == 0x112DF ||
codepoint >= 0x112E0 && codepoint <= 0x112E2 ||
codepoint >= 0x112E3 && codepoint <= 0x112EA ||
codepoint >= 0x112F0 && codepoint <= 0x112F9)
{
result = BIDIScript_Khudawadi;
}
else if(codepoint >= 0x11480 && codepoint <= 0x114AF ||
codepoint >= 0x114B0 && codepoint <= 0x114B2 ||
codepoint >= 0x114B3 && codepoint <= 0x114B8 ||
codepoint == 0x114B9 ||
codepoint == 0x114BA ||
codepoint >= 0x114BB && codepoint <= 0x114BE ||
codepoint >= 0x114BF && codepoint <= 0x114C0 ||
codepoint == 0x114C1 ||
codepoint >= 0x114C2 && codepoint <= 0x114C3 ||
codepoint >= 0x114C4 && codepoint <= 0x114C5 ||
codepoint == 0x114C6 ||
codepoint == 0x114C7 ||
codepoint >= 0x114D0 && codepoint <= 0x114D9)
{
result = BIDIScript_Tirhuta;
}
else if(codepoint >= 0x118A0 && codepoint <= 0x118DF ||
codepoint >= 0x118E0 && codepoint <= 0x118E9 ||
codepoint >= 0x118EA && codepoint <= 0x118F2 ||
codepoint == 0x118FF)
{
result = BIDIScript_Warang_Citi;
}
else if(codepoint >= 0x11700 && codepoint <= 0x1171A ||
codepoint >= 0x1171D && codepoint <= 0x1171F ||
codepoint >= 0x11720 && codepoint <= 0x11721 ||
codepoint >= 0x11722 && codepoint <= 0x11725 ||
codepoint == 0x11726 ||
codepoint >= 0x11727 && codepoint <= 0x1172B ||
codepoint >= 0x11730 && codepoint <= 0x11739 ||
codepoint >= 0x1173A && codepoint <= 0x1173B ||
codepoint >= 0x1173C && codepoint <= 0x1173E ||
codepoint == 0x1173F)
{
result = BIDIScript_Ahom;
}
else if(codepoint >= 0x14400 && codepoint <= 0x14646)
{
result = BIDIScript_Anatolian_Hieroglyphs;
}
else if(codepoint >= 0x108E0 && codepoint <= 0x108F2 ||
codepoint >= 0x108F4 && codepoint <= 0x108F5 ||
codepoint >= 0x108FB && codepoint <= 0x108FF)
{
result = BIDIScript_Hatran;
}
else if(codepoint >= 0x11280 && codepoint <= 0x11286 ||
codepoint == 0x11288 ||
codepoint >= 0x1128A && codepoint <= 0x1128D ||
codepoint >= 0x1128F && codepoint <= 0x1129D ||
codepoint >= 0x1129F && codepoint <= 0x112A8 ||
codepoint == 0x112A9)
{
result = BIDIScript_Multani;
}
else if(codepoint >= 0x10C80 && codepoint <= 0x10CB2 ||
codepoint >= 0x10CC0 && codepoint <= 0x10CF2 ||
codepoint >= 0x10CFA && codepoint <= 0x10CFF)
{
result = BIDIScript_Old_Hungarian;
}
else if(codepoint >= 0x1D800 && codepoint <= 0x1D9FF ||
codepoint >= 0x1DA00 && codepoint <= 0x1DA36 ||
codepoint >= 0x1DA37 && codepoint <= 0x1DA3A ||
codepoint >= 0x1DA3B && codepoint <= 0x1DA6C ||
codepoint >= 0x1DA6D && codepoint <= 0x1DA74 ||
codepoint == 0x1DA75 ||
codepoint >= 0x1DA76 && codepoint <= 0x1DA83 ||
codepoint == 0x1DA84 ||
codepoint >= 0x1DA85 && codepoint <= 0x1DA86 ||
codepoint >= 0x1DA87 && codepoint <= 0x1DA8B ||
codepoint >= 0x1DA9B && codepoint <= 0x1DA9F ||
codepoint >= 0x1DAA1 && codepoint <= 0x1DAAF)
{
result = BIDIScript_SignWriting;
}
else if(codepoint >= 0x1E900 && codepoint <= 0x1E943 ||
codepoint >= 0x1E944 && codepoint <= 0x1E94A ||
codepoint == 0x1E94B ||
codepoint >= 0x1E950 && codepoint <= 0x1E959 ||
codepoint >= 0x1E95E && codepoint <= 0x1E95F)
{
result = BIDIScript_Adlam;
}
else if(codepoint >= 0x11C00 && codepoint <= 0x11C08 ||
codepoint >= 0x11C0A && codepoint <= 0x11C2E ||
codepoint == 0x11C2F ||
codepoint >= 0x11C30 && codepoint <= 0x11C36 ||
codepoint >= 0x11C38 && codepoint <= 0x11C3D ||
codepoint == 0x11C3E ||
codepoint == 0x11C3F ||
codepoint == 0x11C40 ||
codepoint >= 0x11C41 && codepoint <= 0x11C45 ||
codepoint >= 0x11C50 && codepoint <= 0x11C59 ||
codepoint >= 0x11C5A && codepoint <= 0x11C6C)
{
result = BIDIScript_Bhaiksuki;
}
else if(codepoint >= 0x11C70 && codepoint <= 0x11C71 ||
codepoint >= 0x11C72 && codepoint <= 0x11C8F ||
codepoint >= 0x11C92 && codepoint <= 0x11CA7 ||
codepoint == 0x11CA9 ||
codepoint >= 0x11CAA && codepoint <= 0x11CB0 ||
codepoint == 0x11CB1 ||
codepoint >= 0x11CB2 && codepoint <= 0x11CB3 ||
codepoint == 0x11CB4 ||
codepoint >= 0x11CB5 && codepoint <= 0x11CB6)
{
result = BIDIScript_Marchen;
}
else if(codepoint >= 0x11400 && codepoint <= 0x11434 ||
codepoint >= 0x11435 && codepoint <= 0x11437 ||
codepoint >= 0x11438 && codepoint <= 0x1143F ||
codepoint >= 0x11440 && codepoint <= 0x11441 ||
codepoint >= 0x11442 && codepoint <= 0x11444 ||
codepoint == 0x11445 ||
codepoint == 0x11446 ||
codepoint >= 0x11447 && codepoint <= 0x1144A ||
codepoint >= 0x1144B && codepoint <= 0x1144F ||
codepoint >= 0x11450 && codepoint <= 0x11459 ||
codepoint == 0x1145B ||
codepoint == 0x1145D ||
codepoint == 0x1145E ||
codepoint == 0x1145F)
{
result = BIDIScript_Newa;
}
else if(codepoint >= 0x104B0 && codepoint <= 0x104D3 ||
codepoint >= 0x104D8 && codepoint <= 0x104FB)
{
result = BIDIScript_Osage;
}
else if(codepoint == 0x16FE0 ||
codepoint >= 0x17000 && codepoint <= 0x187F7 ||
codepoint >= 0x18800 && codepoint <= 0x18AF2)
{
result = BIDIScript_Tangut;
}
else if(codepoint >= 0x11D00 && codepoint <= 0x11D06 ||
codepoint >= 0x11D08 && codepoint <= 0x11D09 ||
codepoint >= 0x11D0B && codepoint <= 0x11D30 ||
codepoint >= 0x11D31 && codepoint <= 0x11D36 ||
codepoint == 0x11D3A ||
codepoint >= 0x11D3C && codepoint <= 0x11D3D ||
codepoint >= 0x11D3F && codepoint <= 0x11D45 ||
codepoint == 0x11D46 ||
codepoint == 0x11D47 ||
codepoint >= 0x11D50 && codepoint <= 0x11D59)
{
result = BIDIScript_Masaram_Gondi;
}
else if(codepoint == 0x16FE1 ||
codepoint >= 0x1B170 && codepoint <= 0x1B2FB)
{
result = BIDIScript_Nushu;
}
else if(codepoint == 0x11A50 ||
codepoint >= 0x11A51 && codepoint <= 0x11A56 ||
codepoint >= 0x11A57 && codepoint <= 0x11A58 ||
codepoint >= 0x11A59 && codepoint <= 0x11A5B ||
codepoint >= 0x11A5C && codepoint <= 0x11A89 ||
codepoint >= 0x11A8A && codepoint <= 0x11A96 ||
codepoint == 0x11A97 ||
codepoint >= 0x11A98 && codepoint <= 0x11A99 ||
codepoint >= 0x11A9A && codepoint <= 0x11A9C ||
codepoint == 0x11A9D ||
codepoint >= 0x11A9E && codepoint <= 0x11AA2)
{
result = BIDIScript_Soyombo;
}
else if(codepoint == 0x11A00 ||
codepoint >= 0x11A01 && codepoint <= 0x11A0A ||
codepoint >= 0x11A0B && codepoint <= 0x11A32 ||
codepoint >= 0x11A33 && codepoint <= 0x11A38 ||
codepoint == 0x11A39 ||
codepoint == 0x11A3A ||
codepoint >= 0x11A3B && codepoint <= 0x11A3E ||
codepoint >= 0x11A3F && codepoint <= 0x11A46 ||
codepoint == 0x11A47)
{
result = BIDIScript_Zanabazar_Square;
}
else if(codepoint >= 0x11800 && codepoint <= 0x1182B ||
codepoint >= 0x1182C && codepoint <= 0x1182E ||
codepoint >= 0x1182F && codepoint <= 0x11837 ||
codepoint == 0x11838 ||
codepoint >= 0x11839 && codepoint <= 0x1183A ||
codepoint == 0x1183B)
{
result = BIDIScript_Dogra;
}
else if(codepoint >= 0x11D60 && codepoint <= 0x11D65 ||
codepoint >= 0x11D67 && codepoint <= 0x11D68 ||
codepoint >= 0x11D6A && codepoint <= 0x11D89 ||
codepoint >= 0x11D8A && codepoint <= 0x11D8E ||
codepoint >= 0x11D90 && codepoint <= 0x11D91 ||
codepoint >= 0x11D93 && codepoint <= 0x11D94 ||
codepoint == 0x11D95 ||
codepoint == 0x11D96 ||
codepoint == 0x11D97 ||
codepoint == 0x11D98 ||
codepoint >= 0x11DA0 && codepoint <= 0x11DA9)
{
result = BIDIScript_Gunjala_Gondi;
}
else if(codepoint >= 0x11EE0 && codepoint <= 0x11EF2 ||
codepoint >= 0x11EF3 && codepoint <= 0x11EF4 ||
codepoint >= 0x11EF5 && codepoint <= 0x11EF6 ||
codepoint >= 0x11EF7 && codepoint <= 0x11EF8)
{
result = BIDIScript_Makasar;
}
else if(codepoint >= 0x16E40 && codepoint <= 0x16E7F ||
codepoint >= 0x16E80 && codepoint <= 0x16E96 ||
codepoint >= 0x16E97 && codepoint <= 0x16E9A)
{
result = BIDIScript_Medefaidrin;
}
else if(codepoint >= 0x10D00 && codepoint <= 0x10D23 ||
codepoint >= 0x10D24 && codepoint <= 0x10D27 ||
codepoint >= 0x10D30 && codepoint <= 0x10D39)
{
result = BIDIScript_Hanifi_Rohingya;
}
else if(codepoint >= 0x10F30 && codepoint <= 0x10F45 ||
codepoint >= 0x10F46 && codepoint <= 0x10F50 ||
codepoint >= 0x10F51 && codepoint <= 0x10F54 ||
codepoint >= 0x10F55 && codepoint <= 0x10F59)
{
result = BIDIScript_Sogdian;
}
else if(codepoint >= 0x10F00 && codepoint <= 0x10F1C ||
codepoint >= 0x10F1D && codepoint <= 0x10F26 ||
codepoint == 0x10F27)
{
result = BIDIScript_Old_Sogdian;
}
else if(codepoint >= 0x10FE0 && codepoint <= 0x10FF6)
{
result = BIDIScript_Elymaic;
}
else if(codepoint >= 0x119A0 && codepoint <= 0x119A7 ||
codepoint >= 0x119AA && codepoint <= 0x119D0 ||
codepoint >= 0x119D1 && codepoint <= 0x119D3 ||
codepoint >= 0x119D4 && codepoint <= 0x119D7 ||
codepoint >= 0x119DA && codepoint <= 0x119DB ||
codepoint >= 0x119DC && codepoint <= 0x119DF ||
codepoint == 0x119E0 ||
codepoint == 0x119E1 ||
codepoint == 0x119E2 ||
codepoint == 0x119E3 ||
codepoint == 0x119E4)
{
result = BIDIScript_Nandinagari;
}
else if(codepoint >= 0x1E100 && codepoint <= 0x1E12C ||
codepoint >= 0x1E130 && codepoint <= 0x1E136 ||
codepoint >= 0x1E137 && codepoint <= 0x1E13D ||
codepoint >= 0x1E140 && codepoint <= 0x1E149 ||
codepoint == 0x1E14E ||
codepoint == 0x1E14F)
{
result = BIDIScript_Nyiakeng_Puachue_Hmong;
}
else if(codepoint >= 0x1E2C0 && codepoint <= 0x1E2EB ||
codepoint >= 0x1E2EC && codepoint <= 0x1E2EF ||
codepoint >= 0x1E2F0 && codepoint <= 0x1E2F9 ||
codepoint == 0x1E2FF)
{
result = BIDIScript_Wancho;
}
return result;

}
