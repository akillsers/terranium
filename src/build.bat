@echo off
IF NOT EXIST ..\build mkdir "..\build"

REM Useful flags
REM /Bt for the compiler
REM /time for the linker 
set CommonCompilerFlags= -fp:fast -nologo -Zi -MD -source-charset:utf-8 -WX -DFULL_WIN_SDK=1 -DSLOW=1 -DINTERNAL=1 -Od
set ExternalLibs= "kernel32.lib" "user32.lib" "usp10.lib" "gdi32.lib" "dwrite.lib" "DXGI.lib" "d3d11.lib" "Comdlg32.lib" "Ole32.lib" "Rpcrt4.lib" "Shell32.lib" "Comctl32.lib" "dwmapi.lib" "uuid.lib" "advapi32.lib" "d3d9.lib"

set ThirdPartyIncludes=-I"..\third party\harfbuzz\include"
set ThirdPartyLibs="..\third party\harfbuzz\harfbuzz.lib"

pushd "..\build"

cl %CommonCompilerFlags% -Fe:terranium -Fo:terranium "..\src\terranium_windows.cpp" %ThirdPartyIncludes% /link %ExternalLibs% %ThirdPartyLibs% "..\data\resource.res" -incremental:no

REM TODO
REM is this the only way to handle the two pdbs that visual studio generates (one from the linker and one from the compiler)
DEL vc140.pdb
REM cl %CommonCompilerFlags% "..\src\bidi_tester.cpp"

popd "..\build"
