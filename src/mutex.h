inline u32 AtomicIncrement(volatile u32 *value)
{
    // TOOO: MSVC specific

    // TODO: is this cast safe?
    u32 result = _InterlockedIncrement((volatile long *)value);

    // NOTE: We return the new value
    return result;
}

inline u32 AtomicAdd(volatile u32 *value, u32 addend)
{
    // TODO: MSVC specific

    // TODO: is this cast safe?
    u32 result = _InterlockedExchangeAdd((volatile long *)value, addend);

    // NOTE: We return the original value
    return result;
}

struct TicketLock
{
    volatile u32 ticket_served;
    volatile u32 ticket_finished;
    b32 enabled;
};

internal void WIN_Info(char *message, ...);
inline void
AcquireTicketLock(TicketLock *lock)
{
    if(lock->enabled)
    {
        u32 ticket = AtomicIncrement(&lock->ticket_served);

        u32 finished = lock->ticket_finished;
        while(ticket != finished) { finished = lock->ticket_finished; }

        //WIN_Info("Lock acquired! Ticket # %u Since finished %u \n", ticket, finished);
    }
}

inline void
ReleaseTicketLock(TicketLock *lock)
{
    u32 now_serving_old = AtomicIncrement(&lock->ticket_finished);

    //WIN_Info("Lock released! Next to serve %u.\n", now_serving_old + 1);
}

inline void
EnableTicketLock(TicketLock *lock)
{
    lock->ticket_served = 0;
    lock->ticket_finished = 1;
    lock->enabled = true;
}

inline void
DisableTicketLock(TicketLock *lock)
{
    lock->enabled = false;
}