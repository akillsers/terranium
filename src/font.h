#define MAX_FONT_CODEPOINT 0x10FFFF

enum GlyphTextureType
{
    GlyphTextureType_grayscale,
    GlyphTextureType_subpixel,
};

struct RenderedGlyphLink
{
    RenderedGlyphLink *prev; 
    RenderedGlyphLink *next;
};

struct RenderedGlyph
{
    // NOTE: link must be the first item of the struct (at offset 0)
    RenderedGlyphLink link; // for hash table and free list
    RenderedGlyphLink lru_link;

    // TODO: how much of this info do we actually need here??
    
    // TODO: is there a way to ensure these two values are always 0 outside dwrite??
    i16 xoff;
    i16 yoff;

    u16 bboxw;
    u16 bboxh;
    
    u8 *pixels;

    // TODO: consider storing this into a struct?
    u16 glyph_id;
    Vec2i subpixel_position;
    GlyphTextureType texture_type;

    // Note:: We store an acquire count so that we can tell each thread what position they are in in acquiring
    //        this rendered glyph.
    //        The first thread that acquires this is designated to do the rendering.
    volatile b32 acquire_count;

    // Is the RenderedGlyph rendered?
    volatile b32 is_valid;
};


struct GlyphRenderInfo
{
    void *sys;
    
    i16 xoff;
    i16 yoff;
    u16 bboxw;
    u16 bboxh;
};

struct FontMetrics
{
    f32 point_size;
    f32 logical_size;
    f32 pixel_size;

    // TOODO: should this really be an f32 rather than an i32??
    // TODO: we should consider removing a bunch of this stuff!
    f32 y_advance;
    f32 ascent;
    f32 descent;
    f32 height_scaling;
};

struct GlyphCache
{
    u8 *base;
    
    u32 max_elem_width;
    u32 max_elem_height;
    
    RenderedGlyphLink *free;
    RenderedGlyphLink *lru;

    RenderedGlyphLink *table[4021];

    TicketLock ticket_lock;
};

struct FontDriverData;
struct ShapeEngineFont;
struct Font
{
    MemoryArena arena;
    String family_name;

    u16 num_glyphs;
    
    // codepoint to glyph mapping
    u16 codepoint[MAX_FONT_CODEPOINT];

    FontMetrics *metrics;

    FontDriverData *driver_data;

    GlyphCache *cache;

    // used to hold the font data in the form for the
    // shaping engine
    // e.g. hb_face_t, hb_font_t
    //
    // TODO: remove this when Terranium shapes its own
    // text
    ShapeEngineFont *shape_engine_font;
};

struct FontDescriptor
{
    f32 point_size;
    String name;
};

enum FontBackendID
{
    FontBackend_none,
    FontBackend_DWrite,
    FontBackend_stb,
    FontBackend_freetype
};

struct FontBackend
{
    FontBackendID font_backend_id;
    Vec2i (*GetMaxGlyphDimensions) (Font *font);
    
    // NOTE: The following two functions must be called together and in order
    GlyphRenderInfo (*CreateGlyphRenderInfo) (Font *font, u16 glyph_id, Vec2i subpixel_position, GlyphTextureType texture_type, MemoryArena *temp_arena);    
    void (*RenderGlyphByID) (GlyphRenderInfo *render_info, u8 *base); // Also frees GlyphRenderInfo for DWRITE

    Font *(*CreateSystemFont) (String name, f32 ptsize, f32 dpi_scaling);
    void (*FreeFont) (Font *font);
    
    union
    {
        // TODO consider just having a call to get the tables that harfbuzz requires,
        //      since that is really all that we need

        // only available for DWRITE
        void *(*GetDWriteFontFace) (Font *font);
    };
};

global FontBackend font_backend;

inline FractionalNumber
EmWidth(Font *font)
{
    FractionalNumber result = {};
    result.integer_part = font->metrics->pixel_size;

    return result;
}