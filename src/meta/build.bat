@echo off

IF NOT EXIST ..\..\meta mkdir "..\..\meta"

pushd "..\..\meta"

REM TODO!!! how do I prevent the generation of program.obj files??

REM cl -nologo "..\src\meta\cpp_identifier_codepoints.cpp"

cl -nologo -FC "..\src\meta\bidi_gen_types.cpp" -Zi
REM cl -nologo -FC "..\src\meta\bidi_gen_brackets.cpp" -Zi
   
popd "..\meta"

