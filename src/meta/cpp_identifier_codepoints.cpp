#include <stdio.h>
#include <ctype.h>

void
DisplayUsage()
{
    printf("Internal tool to generate c++ if statements from list of Unicode codepoints.\n");
    printf("Expected input (each range should be a seperate cl argument):\n");
    printf("00A8, 00AA, 00AD, 00AF, 00B2-00B5, 00B7-00BA, 00BC-00BE, 00C0-00D6, 00D8-00F6, 00F8-00FF\n");
}

bool
RangeIsSingleItem(char *range_item)
{
    bool result = true;
    char *at = range_item;

    while(*at)
    {
        if(*at == '-')
        {
            result = false;
        }
        at++;
    }

    return result;
}

void
ParseCodepointRange(char *range_item)
{
    bool single = RangeIsSingleItem(range_item);

    if(single)
    {
        int num_print_chars = 0;

        char *at = range_item;
        while(*at && isxdigit(*at))
        {
            num_print_chars++;
            at++;
        }

        printf("codepoint == 0x%.*s", num_print_chars, range_item);
    } else {
        char *at = range_item;
        int lower_bound_chars = 0;

        while (*at &&
               *at != '-' &&
               isxdigit(*at))
        {
            lower_bound_chars++;
            at++;
        }

        while(*at &&
              (*at == ' ' || *at == '-'))
        {
            at++; 
        }

        char *upper_bound_start = at;
        int upper_bound_chars = 0;
        while(*at && isxdigit(*at))
        {
            upper_bound_chars++;
            at++;
        }

        printf("codepoint >= 0x%.*s && codepoint <= 0x%.*s", lower_bound_chars, range_item, upper_bound_chars, upper_bound_start);
    }
}

int
main(int arg_count, char *args[])
{
    if(arg_count > 1)
    {
        printf("if(");
        for(int i = 1;
            i < arg_count;
            i++)
        {
            ParseCodepointRange(args[i]);
            if(i + 1 < arg_count)
            {
                printf(" || \n");
            }
        }
        printf(")\n");
        printf("{\n");
        printf("}\n");
    } else {
        DisplayUsage();
    }
}
