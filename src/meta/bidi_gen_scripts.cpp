#include <stdio.h>
#include <malloc.h>

#include <assert.h>
#if 0
//resultant code

BIDIType
GetCodepointBidiType(UTF32 codepoint)
{
    BIDIType result = BIDIType_L;

    if(codepoint >= 0x41 && codepoint <= 005A ||
       codepoint)
    {
        result = BIDIType_L;
    } 
    else if(codepoint >= 0x34 & codepoint <= 0x809 ||
            codepoint >= 0x34 & codepoint <= 0x809 ||
            codepoint >= 0x34 & codepoint <= 0x809 ||
            codepoint >= 0x34 & codepoint <= 0x809 ||
            codepoint >= 0x34 & codepoint <= 0x809 ||
            codepoint >= 0x34 & codepoint <= 0x809)
    {
        result = BIDIType_R;
    }
    else if(codepoint >)
    
    return result;
}
#endif



void
DisplayUsage()
{
    printf("Internal tool to generate c++ functions that retrieve the BIDI types of a given codepoint.\n");
    printf("Usage: (program name)  path to DeriveBidiClass.txt from the Unicode Character Database.\n");
}

enum TokenType
{
    Token_unknown,
    Token_value,
    Token_semicolon,
    Token_EOF,
};

struct Token
{
    TokenType type;
    char *start;
    unsigned int len;
};

struct Lexer
{
    char *at;
};

Token
GetNextToken(Lexer *lexer)
{
    Token result = {};

    for(;;)
    {
        if(*lexer->at && *lexer->at == '#')
        {
            while(*lexer->at && *lexer->at != '\n')
            {
                lexer->at++;
            }

            lexer->at++;
        }
        else if(*lexer->at == ' ' ||
                *lexer->at == '\t' ||
                *lexer->at == '\n')
        {
            lexer->at++;
        }
        else
        {
            break;
        }
    }

    if(*lexer->at == 0)
    {
        result.type = Token_EOF;
    }
    else if(*lexer->at == ';')
    {
        result.type = Token_semicolon;
        lexer->at++;
    }
    else
    {
        result.type = Token_value;
        result.start = lexer->at;

        while(*lexer->at && 
              *lexer->at != ' ' &&
              *lexer->at != ';' &&
              *lexer->at != '\n')
        {
            lexer->at++;
        }

        result.len = lexer->at - result.start;
    }

    return result;
}

void
PrintCodepointRange(Token token)
{
    char *at = token.start;
    
    int second_clause_begin = 0;
    for(int i = 0; 
        i < token.len;
        i++)
    {
        char *token_char = token.start + i;
        if(*token_char == '.')
        {
            printf("codepoint >= 0x%.*s && ", i, token.start);
            second_clause_begin = i + 2;
            i++;
        }
    }

    if(second_clause_begin)
    {
        printf("codepoint <= 0x%.*s", token.len - second_clause_begin, token.start + second_clause_begin);
    }
    else
    {
        printf("codepoint == 0x%.*s", token.len, token.start);
    }

}

Token
RequireToken(Lexer *lexer, TokenType required_type)
{
    Token token = GetNextToken(lexer);
    
    if(token.type != required_type)
    {
        fprintf(stderr, "ERROR: The retrieved token type (%u) does not match the required token type (%u)!",
                token.type, required_type);
    }

    return token;
}

void
PrintPreamble()
{
    printf("BIDIScript\n");
    printf("GetCodepointBidiType(UTF32 codepoint)\n");
    printf("{\n");
    printf("BIDIType result = BIDIType_L;\n");
    printf("\n");
}

void
PrintCloser()
{
    printf("return result;\n");
    printf("\n");
    printf("}");
}


bool
TokenStringMatches(Token *first, Token *second)
{
    bool result;

    if(first->len != second->len)
    {
        result = false;
    }
    else
    {
        unsigned int offset = 0;
        
        for(int i = 0;
            i < first->len;
            i++)
        {
            char *first_at = first->start + offset;
            char *second_at = second->start + offset;
            
            if(*first_at != *second_at)
            {
                break;
            }

            offset++;
        }

        result = (offset == first->len);
    }

    return result;
}

void
PrintBidiGroupEnding(Token bidi_type_token)
{
    printf(")\n");
    printf("{\n");
    printf("result = BIDIType_%.*s;\n", bidi_type_token.len, bidi_type_token.start);
    printf("}\n");
}
    
void
Generate(char *database_path)
{
    FILE *bidi_class_database = fopen(database_path, "rb");
    if(bidi_class_database)
    {
        int file_size;

        fseek(bidi_class_database, 0, SEEK_END);
        file_size = ftell(bidi_class_database);
        rewind(bidi_class_database);

        char *database = (char *)malloc(file_size + 1);
        fread(database, file_size, 1, bidi_class_database);
        database[file_size] = 0;

        PrintPreamble();
        
        Lexer lexer = {};
        lexer.at = database;

        Token token = {};
        Token current_bidi_type = {};

        for(;;)
        {
            token = GetNextToken(&lexer);

            if(token.type == Token_EOF)
            {
                break;
            }
            
            assert(token.type == Token_value);

            Token codepoint_range_token = token;
            
            RequireToken(&lexer, Token_semicolon);
            Token bidi_script_token =  RequireToken(&lexer, Token_value);
            
            if(current_bidi_type.type == Token_unknown)
            {
                current_bidi_type = bidi_type_token;
                printf("if(");
                PrintCodepointRange(codepoint_range_token);
            } 
            else if(TokenStringMatches(&bidi_type_token, &current_bidi_type))
            {
                printf(" ||\n");
                PrintCodepointRange(codepoint_range_token);
            }
            else
            {
                PrintBidiGroupEnding(current_bidi_type);
                current_bidi_type = bidi_type_token;
                printf("else if(");
                PrintCodepointRange(codepoint_range_token);
            }
        }

        if(current_bidi_type.type != Token_unknown)
        {
            PrintBidiGroupEnding(current_bidi_type);
        }

        PrintCloser();
    }
    else
    {
        fprintf(stderr, "Could not open the file @ %s.", database_path);
    }
}

int
main(int arg_count, char *args[])
{
    if(arg_count > 1)
    {
        Generate(args[1]);
    }
    else
    {
        DisplayUsage();
    }
}
