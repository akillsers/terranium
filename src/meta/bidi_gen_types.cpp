#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include <string.h>

#define MAX_CODEPOINT_RANGES 900000
#define MAX_BIDI_TYPE_CHARS 5

struct BIDITypeRange
{
    char type[MAX_BIDI_TYPE_CHARS];
    unsigned long min;
    unsigned long max;
};

typedef unsigned long int b32;

BIDITypeRange bidi_type_range[MAX_CODEPOINT_RANGES];
unsigned int num_bidi_type_ranges;

#if 0
// resultant code
struct BIDITypeRange
{
    BIDIType type;

    UTF32 min;
    UTF32 max;
};

struct BIDITypeRange bidi_type_ranges[] = { 
    {BIDIType_L, 0x41, 005A},
    {BIDIType_R, 0x34, 0x809},
};

BIDIType
GetCodepointBidiType(UTF32 codepoint)
{
    BIDIType result = BIDIType_L;

    // TODO: Consider providing logging information when a character is not found
    //       in DerivedBidiClass.txt?

    u32 left = 0;
    u32 right = ArrayCount(bidi_type_ranges) - 1;
    
    while(left <= right)
    {
        u32 mid = left + (right - left) / 2;
        BIDITypeRange *bidi_type_range = &bidi_type_ranges[mid];

        if(bidi_type_range->max < codepoint)
        {
            left = mid + 1;
        }
        else if(bidi_type_range->min > codepoint)
        {
            right = mid - 1;
        }
        else
        {
            result = bidi_type_range->type;
            break;
        }
    }

    return result;
}
#endif

void
DisplayUsage()
{
    printf("Internal tool to generate c++ functions that retrieve the BIDI types of a given codepoint.\n");
    printf("Usage: (program name)  path to DerivedBidiClass.txt from the Unicode Character Database.\n");
}

enum TokenType
{
    Token_unknown,
    Token_value,
    Token_semicolon,
    Token_EOF,
};

struct Token
{
    TokenType type;
    char *start;
    unsigned int len;
};

struct Lexer
{
    char *at;
};

Token
GetNextToken(Lexer *lexer)
{
    Token result = {};

    for(;;)
    {
        if(*lexer->at && *lexer->at == '#')
        {
            while(*lexer->at && *lexer->at != '\n')
            {
                lexer->at++;
            }

            lexer->at++;
        }
        else if(*lexer->at == ' ' ||
                *lexer->at == '\t' ||
                *lexer->at == '\r' ||
                *lexer->at == '\n')
        {
            lexer->at++;
        }
        else
        {
            break;
        }
    }

    if(*lexer->at == 0)
    {
        result.type = Token_EOF;
    }
    else if(*lexer->at == ';')
    {
        result.type = Token_semicolon;
        lexer->at++;
    }
    else
    {
        result.type = Token_value;
        result.start = lexer->at;

        while(*lexer->at && 
              *lexer->at != ' ' &&
              *lexer->at != ';' &&
              *lexer->at != '\n')
        {
            lexer->at++;
        }

        result.len = lexer->at - result.start;
    }

    return result;
}

Token
RequireToken(Lexer *lexer, TokenType required_type)
{
    Token token = GetNextToken(lexer);
    
    if(token.type != required_type)
    {
        fprintf(stderr, "ERROR: The retrieved token type (%u) does not match the required token type (%u)!\n",
                token.type, required_type);
    }

    return token;
}

bool
TokenStringMatches(Token *first, Token *second)
{
    bool result;

    if(first->len != second->len)
    {
        result = false;
    }
    else
    {
        unsigned int offset = 0;
        
        for(int i = 0;
            i < first->len;
            i++)
        {
            char *first_at = first->start + offset;
            char *second_at = second->start + offset;
            
            if(*first_at != *second_at)
            {
                break;
            }

            offset++;
        }

        result = (offset == first->len);
    }

    return result;
}

void
AddRange(Token bidi_type_token, Token codepoint_range_token)
{
    unsigned int array_index = num_bidi_type_ranges++;

    strncpy(bidi_type_range[array_index].type, bidi_type_token.start, bidi_type_token.len);

#define TEMP_BUF_SIZE 10
    char min_str[TEMP_BUF_SIZE] = {};
    char max_str[TEMP_BUF_SIZE] = {};


    b32 copying_min_str = true;
    b32 copying_max_str = false;
    unsigned int min_at = 0;
    unsigned int max_at = 0;

    for(int at = 0;
        at < codepoint_range_token.len;
        at++)
    {
        char character = codepoint_range_token.start[at];

        if(character == '.')
        {
            copying_min_str = false;
        }
        else
        {
            if(copying_min_str == false) { copying_max_str = true; }

            if(copying_min_str)
            {
                assert(min_at < TEMP_BUF_SIZE);
                min_str[min_at++] = character;
            }
            else if(copying_max_str)
            {
                assert(max_at < TEMP_BUF_SIZE);
                max_str[max_at++] = character;
            }
        }
    }

    assert(min_at < TEMP_BUF_SIZE);
    min_str[min_at] = '\0';

    if(copying_min_str)
    {
        // We were still copying the min string (we didn't hit a dot)
        // So max_str is the same as min_str
        strcpy(max_str, min_str);
    }
    else
    {
        assert(max_at < TEMP_BUF_SIZE);
        max_str[max_at] = '\0';
    }
#undef TEMP_BUF_SIZE

    unsigned long min = strtoul(min_str, NULL, 16);
    unsigned long max = strtoul(max_str, NULL, 16);
    
    assert(min <= max);

    bidi_type_range[array_index].min = min;
    bidi_type_range[array_index].max = max;
}

int CompareBidiTypeRanges(const void *left, const void *right)
{
    int result = 0;

    BIDITypeRange *left_range = (BIDITypeRange *)left;
    BIDITypeRange *right_range = (BIDITypeRange *)right;
    
    if(left_range->min < right_range->min)
    {
        assert(left_range->max < right_range->min);
        result = -1;
    }
    else
    {
        assert(left_range->min > right_range->max);
        result = 1;
    }

    return result;
}

void
Generate(char *database_path)
{
    FILE *bidi_class_database = fopen(database_path, "rb");
    if(bidi_class_database)
    {
        int file_size;

        fseek(bidi_class_database, 0, SEEK_END);
        file_size = ftell(bidi_class_database);
        rewind(bidi_class_database);

        char *database = (char *)malloc(file_size + 1);
        fread(database, file_size, 1, bidi_class_database);
        database[file_size] = 0;

        Lexer lexer = {};
        lexer.at = database;

        Token token = {};
        Token current_bidi_type = {};

        for(;;)
        {
            token = GetNextToken(&lexer);

            if(token.type == Token_EOF)
            {
                break;
            }
            
            assert(token.type == Token_value);

            Token codepoint_range_token = token;
            
            RequireToken(&lexer, Token_semicolon);
            Token bidi_type_token = RequireToken(&lexer, Token_value);
            
            AddRange(bidi_type_token, codepoint_range_token);
        }

        qsort(bidi_type_range, num_bidi_type_ranges, sizeof(BIDITypeRange), CompareBidiTypeRanges);
        

        // --Emit the required code--

        printf(R"delimiter(struct BIDITypeRange
{
    BIDIType type;

    UTF32 min;
    UTF32 max;
};

)delimiter");

        printf("struct BIDITypeRange bidi_type_ranges[] = {\n");

        for(int i = 0; i < num_bidi_type_ranges; i++)
        {
            BIDITypeRange *range = &bidi_type_range[i];
            printf("    {BIDIType_%s, 0x%x, 0x%x},\n", range->type, range->min, range->max);
        }

        printf("};\n\n");

        printf(R"delimiter(BIDIType
GetCodepointBidiType(UTF32 codepoint)
{
    BIDIType result = BIDIType_L;

    // TODO: Consider providing logging information when a character is not found
    //       in DerivedBidiClass.txt?

    u32 left = 0;
    u32 right = ArrayCount(bidi_type_ranges) - 1;
    
    while(left <= right)
    {
        u32 mid = left + (right - left) / 2;
        BIDITypeRange *bidi_type_range = &bidi_type_ranges[mid];

        if(bidi_type_range->max < codepoint)
        {
            left = mid + 1;
        }
        else if(bidi_type_range->min > codepoint)
        {
            right = mid - 1;
        }
        else
        {
            result = bidi_type_range->type;
            break;
        }
    }

    return result;
})delimiter");
    }
    else
    {
        fprintf(stderr, "Could not open the file @ %s.", database_path);
    }
}

int
main(int arg_count, char *args[])
{
    if(arg_count > 1)
    {
        Generate(args[1]);
    }
    else
    {
        DisplayUsage();
    }
}
