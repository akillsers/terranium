#include <stdio.h>
#include <malloc.h>

#include <assert.h>
#if 0
//resultant code
enum BIDIBracketType
{
    BIDIBracketType_none,
    BIDIBracketType_open,
    BIDIBracketType_close,
};

struct BIDIBracketProperty
{
    UTF32 codepoint;

    UTF32 bidi_paired_bracket;
    BIDIBracketType type;
};

#endif

void
DisplayUsage()
{
    printf("Internal tool to generate c++ functions that retrieve the BIDI_Paired_Bracket and BIDI_paired_bracket_type property of a given codepoint.\n");
    printf("Usage: (program name)  path to BidiBrackets.txt from the Unicode Character Database.\n");
}

enum TokenType
{
    Token_unknown,
    Token_value,
    Token_semicolon,
    Token_EOF,
};

struct Token
{
    TokenType type;
    char *start;
    unsigned int len;
};

struct Lexer
{
    char *at;
};

Token
GetNextToken(Lexer *lexer)
{
    Token result = {};

    for(;;)
    {
        if(*lexer->at && *lexer->at == '#')
        {
            while(*lexer->at && *lexer->at != '\n')
            {
                lexer->at++;
            }

            lexer->at++;
        }
        else if(*lexer->at == ' ' ||
                *lexer->at == '\t' ||
                *lexer->at == '\n')
        {
            lexer->at++;
        }
        else
        {
            break;
        }
    }

    if(*lexer->at == 0)
    {
        result.type = Token_EOF;
    }
    else if(*lexer->at == ';')
    {
        result.type = Token_semicolon;
        lexer->at++;
    }
    else
    {
        result.type = Token_value;
        result.start = lexer->at;

        while(*lexer->at && 
              *lexer->at != ' ' &&
              *lexer->at != ';' &&
              *lexer->at != '\n')
        {
            lexer->at++;
        }

        result.len = lexer->at - result.start;
    }

    return result;
}

Token
RequireToken(Lexer *lexer, TokenType required_type)
{
    Token token = GetNextToken(lexer);
    
    if(token.type != required_type)
    {
        fprintf(stderr, "ERROR: The retrieved token type (%u) does not match the required token type (%u)!",
                token.type, required_type);
    }

    return token;
}

void
PrintPreamble()
{
    printf("global BIDIBracketProperty bracket_properties[] = \n");
    printf("{\n");
}

void
PrintCloser()
{
    printf("};\n");
}

void
Generate(char *database_path)
{
    FILE *bidi_class_database = fopen(database_path, "rb");
    if(bidi_class_database)
    {
        int file_size;

        fseek(bidi_class_database, 0, SEEK_END);
        file_size = ftell(bidi_class_database);
        rewind(bidi_class_database);

        char *database = (char *)malloc(file_size + 1);
        fread(database, file_size, 1, bidi_class_database);
        database[file_size] = 0;

        PrintPreamble();
        
        Lexer lexer = {};
        lexer.at = database;

        Token token = {};
        Token current_bidi_type = {};

        for(;;)
        {
            token = GetNextToken(&lexer);

            if(token.type == Token_EOF)
            {
                break;
            }
            
            assert(token.type == Token_value);

            printf("{0x%.*s, ", token.len, token.start);
            
            token = GetNextToken(&lexer);
            assert(token.type == Token_semicolon);

            token = GetNextToken(&lexer);
            assert(token.type == Token_value);

            printf(" 0x%.*s, ", token.len, token.start);
            
            token = GetNextToken(&lexer);
            assert(token.type == Token_semicolon);

            token = GetNextToken(&lexer);
            assert(token.type == Token_value &&
                   token.len == 1);
            
            if(*token.start == 'o')
            {
                printf(" BIDIBracketType_open},\n");
            }
            else if(*token.start == 'c')
            {
                printf(" BIDIBracketType_close},\n");
            }
            else
            {
                printf(" BIDIBracketType_none},\n");
            }
        }

        PrintCloser();
    }
    else
    {
        fprintf(stderr, "Could not open the file @ %s.", database_path);
    }
}

int
main(int arg_count, char *args[])
{
    if(arg_count > 1)
    {
        Generate(args[1]);
    }
    else
    {
        DisplayUsage();
    }
}
