internal RawBufSection *
GetFirstSection(TextBuffer *buffer)
{
    return buffer->section_sentinel.next;
}

inline b32
SectionContainsBytePos(TextBuffer *buffer, RawBufSection *section, Cursor byte_pos)
{
    b32 result = (byte_pos >= section->bytes_before_section &&
                  byte_pos < section->bytes_before_section + section->num_bytes);
    
    return result;
}

internal RawBufSection *
GetSectionContainingBytePos(TextBuffer *buffer, Cursor byte_pos)
{
    RawBufSection *result = 0;
    
    if(buffer->cached_section)
    {
        // a lot of times we will be looking in the same section:
        if(SectionContainsBytePos(buffer, buffer->cached_section, byte_pos))
        {
            result = buffer->cached_section;
        }
    }
    
    if(!result)
    {
        for(RawBufSection *section = GetFirstSection(buffer);
            section != &buffer->section_sentinel;
            section = section->next)
        {
            if(byte_pos >= section->bytes_before_section + section->num_bytes)
            {
                continue;
            }
            
            Assert(SectionContainsBytePos(buffer, section, byte_pos));
            
            result = section;
            break;
        }
    }
    
    if(!result)
    {
        // TODO should we keep this HACK where we say that a
        //      section contains the byte position if it is
        //      one past the last byte?
        if(byte_pos == buffer->total_bytes)
        {
            result = buffer->section_sentinel.prev;
        }
    }
    
    buffer->cached_section = result;
    
    Assert(result != &buffer->section_sentinel);
    
    return result;
}

internal RawBufSection *
GetSectionContainingCharPos(TextBuffer *buffer, Cursor char_pos)
{
    RawBufSection *result = 0;
    RawBufSection *first_section = buffer->section_sentinel.next;
    
    for(RawBufSection *section = GetFirstSection(buffer);
        section != &buffer->section_sentinel;
        section = section->next)
    {
        if(char_pos >= section->chars_before_section + section->num_chars)
        {
            continue;
        }
        
        Assert(char_pos >= section->chars_before_section &&
               char_pos < section->chars_before_section + section->num_chars);
        
        result = section;
        break;
    }
    
    if(!result)
    {
        if(char_pos == buffer->total_characters)
        {
            result = buffer->section_sentinel.prev;
        }
    }
    
    Assert(result != &buffer->section_sentinel);
    
    return result;
}

// TODO check out how many of these functions are useda

internal u32
GetBytePosWithKnownSection(RawBufSection *section, UTF8 *byte)
{
    u32 byte_pos;
    if(byte < section->data + section->bytes_before_section)
    {
        return byte - section->data;
    }
    else
    {
        return byte - section->data - section->num_gap_bytes;
    }
    return byte_pos;
}

internal UTF8 *
GetByteInSection(RawBufSection *section, Cursor byte_pos_in_section)
{
    UTF8 *result = 0;
    if(byte_pos_in_section < section->bytes_before_gap)
    {
        result = section->data + byte_pos_in_section;
    } else {
        result = section->data + byte_pos_in_section + section->num_gap_bytes;
        
    }
    return result;
}

internal UTF8 *
GetBufferByte(RawBufSection *section, Cursor byte_pos)
{
    UTF8 *result;
    
    Cursor byte_pos_in_section = byte_pos - section->bytes_before_section;
    
    if(byte_pos_in_section < section->bytes_before_gap)
    {
        result = section->data + byte_pos_in_section;
    }
    else
    {
        result = section->data + byte_pos_in_section + section->num_gap_bytes;
    }
    
    return result;
}

internal UTF8 *
GetSectionByte(RawBufSection *section, Cursor byte_pos_in_section)
{
    UTF8 *result;
    
    if(byte_pos_in_section < section->bytes_before_gap)
    {
        result = section->data + byte_pos_in_section;
    }
    else
    {
        result =  section->data + byte_pos_in_section + section->num_gap_bytes;
    }
    
    return result;
}

internal b32
VerifySectionCacheIntegrity(RawBufSection *section)
{
    b32 result;
    
    result = (section->num_bytes == section->cached_pos_pair.check_total_bytes &&
              section->num_chars == section->cached_pos_pair.check_total_chars);
    
    return result;
}

// obtains the section-relative byte pos by counting characters forward
inline u32
GetSectionBytePosForward(RawBufSection *section,
                         Cursor known_section_char_pos,
                         u32 known_section_byte_pos,
                         u32 target_section_char_pos)
{
    u32 section_byte_pos = known_section_byte_pos;
    u32 section_char_pos = known_section_char_pos;
    
    while(section_char_pos < target_section_char_pos)
    {
        section_byte_pos++;
        
        UTF8 byte = *GetSectionByte(section, section_byte_pos);
        if(IsLeadingUTF8Byte(byte))
        {
            section_char_pos++;
        }
    }
    return section_byte_pos;
}

// obtains the section-relative byte pos by counting characters backward
inline u32
GetSectionBytePosBackward(RawBufSection *section,
                          Cursor known_section_char_pos,
                          u32 known_section_byte_pos,
                          Cursor target_section_char_pos)
{
    u32 section_char_pos = known_section_char_pos;
    u32 section_byte_pos = known_section_byte_pos;
    
    while(section_char_pos > target_section_char_pos)
    {
        section_byte_pos--;
        
        UTF8 byte = *GetSectionByte(section, section_byte_pos);
        if(IsLeadingUTF8Byte(byte))
        {
            section_char_pos--;
        }
    }
    return section_byte_pos;
}

// NOTE that this character pos refers to the global file
//      passing in a RawBufSection just skips the step of finding a section and
//      makes the function more performant
internal u32
CharacterPosToBytePos(RawBufSection *section, Cursor cursor, b32 cache_position = true)
{
    // makes sure that the cursor is in range!
    u32 section_byte_pos = 0;
    Assert(cursor >= section->chars_before_section &&
           cursor <= section->chars_before_section + section->num_chars);
    
    // there are 2-3 locations where we know the char pos and byte pos
    // we can count up from the START OF THE SECTION
    // we can count down from the END OF THE SECTION
    // we can count up or down from the cached CharPosBytePosPair
    
    u32 section_char_pos = cursor - section->chars_before_section;
    u32 distance_from_start = section_char_pos;
    u32 distance_from_end = section->num_chars - section_char_pos;
    u32 distance_from_cached_pos = 0;
    
    if(section->cached_pos_pair.is_valid)
    {
        if(VerifySectionCacheIntegrity(section))
        {
            if(section->cached_pos_pair.section_char_pos > section_char_pos)
            {
                distance_from_cached_pos = section->cached_pos_pair.section_char_pos - section_char_pos;
            }
            else
            {
                distance_from_cached_pos = section_char_pos - section->cached_pos_pair.section_char_pos;
            }
        }
        else
        {
            Assert(!"The section cache is likely not valid, but it said that it was");
            section->cached_pos_pair.is_valid = false;
        }
    }
    
    
    if(section->cached_pos_pair.is_valid &&
       distance_from_cached_pos < distance_from_start &&
       distance_from_cached_pos < distance_from_end)
    {
        if(section_char_pos < section->cached_pos_pair.section_char_pos)
        {
            u32 anchor_char_pos = section->cached_pos_pair.section_char_pos;
            u32 anchor_byte_pos = section->cached_pos_pair.section_byte_pos;
            section_byte_pos = GetSectionBytePosBackward(section, anchor_char_pos, anchor_byte_pos, section_char_pos);
        }
        else if(section_char_pos > section->cached_pos_pair.section_char_pos)
        {
            u32 anchor_char_pos = section->cached_pos_pair.section_char_pos;
            u32 anchor_byte_pos = section->cached_pos_pair.section_byte_pos;
            section_byte_pos = GetSectionBytePosForward(section, anchor_char_pos, anchor_byte_pos, section_char_pos);
        }
        else
        {
            section_byte_pos = section->cached_pos_pair.section_byte_pos;
        }
    }
    else if(distance_from_start < distance_from_end)
    {
        section_byte_pos = GetSectionBytePosForward(section, 0, 0, section_char_pos);
    }
    else
    {
        section_byte_pos = GetSectionBytePosBackward(section, section->num_chars, section->num_bytes, section_char_pos);
    }
    
    if(cache_position)
    {
        section->cached_pos_pair.section_char_pos = section_char_pos;
        section->cached_pos_pair.section_byte_pos = section_byte_pos;
        section->cached_pos_pair.is_valid = true;
        section->cached_pos_pair.check_total_chars = section->num_chars;
        section->cached_pos_pair.check_total_bytes = section->num_bytes;
    }
    return section->bytes_before_section + section_byte_pos;
}

internal void
MoveSectionGapToCursor(TextBuffer *buffer, RawBufSection *section, Cursor cursor)
{
    Assert(cursor >= section->chars_before_section &&
           cursor <= section->chars_before_section + section->num_chars);
    
    u32 target_byte_pos = CharacterPosToBytePos(section, cursor, false /*don't cache this position*/);
    u32 target_byte_pos_in_section = target_byte_pos - section->bytes_before_section;
    u32 target_char_pos_in_section = cursor - section->chars_before_section;
    if(target_char_pos_in_section > section->chars_before_gap)
    {
        // NOTE:: move the range from the end of the old gap
        //       to the (cursor + the gap size) over to the
        //       start of old gap location
        // TODO this actually makes no sense so I changed it feb 4 2019
        UTF8 *dest = section->data + section->bytes_before_gap;
        UTF8 *src = SECTION_BYTE_AFTER_GAP(section);
        u32 byte_size = (target_byte_pos_in_section - section->bytes_before_gap +
                         section->num_gap_bytes);
        memcpy(dest, src, byte_size);
        
        u32 char_count = target_char_pos_in_section - section->chars_before_gap;
        u32 byte_count = target_byte_pos_in_section - section->bytes_before_gap;
        section->bytes_before_gap += byte_count;
        section->chars_before_gap += char_count;
        
        section->cached_pos_pair.is_valid = false;
        
    }
    else if (target_char_pos_in_section < section->chars_before_gap)
    {
        // NOTE:: move the range from the cursor to the start of the old gap
        //       over to behind the new gap
        UTF8 *dest = section->data + target_byte_pos_in_section + section->num_gap_bytes;
        UTF8 *src = section->data + target_byte_pos_in_section;
        u32 byte_size = section->bytes_before_gap - target_byte_pos_in_section;
        memcpy(dest, src, byte_size);
        
        u32 char_count = section->chars_before_gap - target_char_pos_in_section;
        u32 byte_count = section->bytes_before_gap - target_byte_pos_in_section;
        section->bytes_before_gap -= byte_count;
        section->chars_before_gap -= char_count;
        
        section->cached_pos_pair.is_valid = false;
    }
}

internal RawBufSection *
AllocateSectionAndSplitContent(TextBuffer *buffer, RawBufSection *old_section)
{
    u64 alloc_size = Kilobytes(64);
    RawBufSection *new_section = (RawBufSection *) platform.AllocateMemory(alloc_size);
    
    u32 chars_in_old_section = old_section->num_chars / 2;
    u32 chars_in_new_section = old_section->num_chars - chars_in_old_section;
    Cursor transfer_byte_pos = CharacterPosToBytePos(old_section, old_section->chars_before_section + chars_in_old_section);
    
    u32 bytes_in_old_section = transfer_byte_pos - old_section->bytes_before_section;
    u32 bytes_in_new_section = old_section->num_bytes - bytes_in_old_section;
    
    new_section->chars_before_section = old_section->chars_before_section + chars_in_old_section;
    new_section->data = (UTF8 *)new_section + sizeof(RawBufSection);
    new_section->max_bytes = alloc_size - sizeof(RawBufSection);
    
    old_section->num_chars = chars_in_old_section;
    new_section->num_chars = chars_in_new_section;
    
    old_section->num_bytes = bytes_in_old_section;
    new_section->num_bytes = bytes_in_new_section;
    new_section->bytes_before_section = transfer_byte_pos;
    
    if(old_section->chars_before_gap > old_section->num_chars)
    {
        old_section->cached_pos_pair.is_valid = false;
        MoveSectionGapToCursor(buffer, old_section, old_section->num_chars);
    }
    
    memcpy(new_section->data,
           GetBufferByte(old_section, transfer_byte_pos),
           bytes_in_new_section);
    
    DL_INSERT_AFTER(old_section, new_section);
    
    new_section->cached_pos_pair.is_valid = false;
    old_section->cached_pos_pair.is_valid = false;
    
    return new_section;
}

internal b32
SectionSpaceAvailible(RawBufSection *section, u32 num_bytes)
{
    return section->num_bytes + num_bytes <= section->max_bytes;
}

internal RawBufSection *
AllocateAndPushBackSection(TextBuffer *buffer)
{
    // TODO 64k might be too high for this (consider 4k allocation...somehow??)
    // TODO we are conveniently using 64k since that is USUALLY the windows memory page granularity
    //     WHAT about other platforms?
    u64 alloc_size = Kilobytes(64);
    RawBufSection *section = (RawBufSection *) platform.AllocateMemory(alloc_size);
    
    section->chars_before_section = buffer->total_characters;
    section->bytes_before_section = buffer->total_bytes;
    section->data = (UTF8 *)section + sizeof(RawBufSection);
    section->max_bytes = alloc_size - sizeof(RawBufSection);
    section->num_chars = 0;
    section->num_gap_bytes = 0;
    section->num_bytes = 0;
    
    section->cached_pos_pair = {};
    
    DL_INSERT_AFTER(buffer->section_sentinel.prev, section);
    return section;
}

internal LineStore *
AllocateLineStoreAndSplitContent(TextBuffer *buffer, LineStore *old_line_store)
{
    u64 alloc_size = Kilobytes(64);
    LineStore *new_line_store = (LineStore *) platform.AllocateMemory(alloc_size);
    
    u32 lines_in_old_store = old_line_store->num_lines / 2;
    u32 lines_in_new_store = old_line_store->num_lines - lines_in_old_store;
    
    new_line_store->lines_before_store = old_line_store->lines_before_store + lines_in_old_store;
    new_line_store->line_array = (Cursor *)new_line_store + sizeof(LineStore);
    new_line_store->max_lines = (alloc_size - sizeof(LineStore) / sizeof(Cursor));
    
    Assert(lines_in_new_store < new_line_store->max_lines);
    old_line_store->num_lines = lines_in_old_store;
    new_line_store->num_lines = lines_in_new_store;
    
    memcpy(new_line_store->line_array,
           old_line_store->line_array + lines_in_old_store,
           lines_in_new_store * sizeof(Cursor));
    
    DL_INSERT_AFTER(old_line_store, new_line_store);
    return new_line_store;
}

internal UTF8 *
GetByteAddress_(TextBuffer *buffer, Cursor byte_pos)
{
    UTF8 *result = 0;
    RawBufSection *section = GetSectionContainingBytePos(buffer, byte_pos);
    if(section)
    {
        Cursor byte_pos_in_section = byte_pos - section->bytes_before_section;
        if(byte_pos_in_section < section->bytes_before_gap)
        {
            result = section->data + byte_pos_in_section;
        } else {
            result =  section->data + byte_pos_in_section + section->num_gap_bytes;
        }
    }
    
    return result;
}

internal LineStore *
GetLineStoreContainingLine(TextBuffer *buffer, u32 line_index)
{
    LineStore *result = 0;
    
    // TODO: this is the SLOWEST possible way to this (well fine, not really, but STILL!)
    LineStore *first_line_store = buffer->line_store_sentinel.next;
    for(LineStore *line_store = first_line_store;
        line_store != &buffer->line_store_sentinel;
        line_store = line_store->next)
    {
        if(line_index >= line_store->lines_before_store + line_store->num_lines)
        {
            continue;
        }
        
        Assert(line_index >= line_store->lines_before_store &&
               line_index < line_store->lines_before_store + line_store->num_lines);
        
        result = line_store;
        break;
    }
    return result;
}

internal Cursor
LinePos(TextBuffer *buffer, u32 line_index)
{
    TIMED_BLOCK;
    Cursor ret = 0;
    LineStore *line_store = GetLineStoreContainingLine(buffer, line_index);
    if (line_store)
    {
        ret = line_store->line_array[line_index - line_store->lines_before_store];
    }
    return ret;
}

internal Cursor *
GetLineArrayEntry(TextBuffer *buffer, u32 line_index)
{
    Cursor *ret = 0;
    LineStore *line_store = GetLineStoreContainingLine(buffer, line_index);
    
    if (line_store)
    {
        ret = line_store->line_array + (line_index - line_store->lines_before_store);
    }
    
    return ret;
}

internal LineStore *
GetLastNonEmptyLineStore(TextBuffer *buffer)
{
    LineStore *result = 0;
    LineStore *last_line_store = buffer->line_store_sentinel.prev;
    
    for(LineStore *line_store = last_line_store;
        line_store != &buffer->line_store_sentinel;
        line_store = line_store->prev)
    {
        result = line_store;
    }
    
    return result;
}

internal LineStore *
GetOrAllocateNextLineStoreWithAvailibleSpace(TextBuffer *buffer, u32 lines_required)
{
    LineStore *result = 0;
    LineStore *last_non_empty_line_store = GetLastNonEmptyLineStore(buffer);
    
    if(last_non_empty_line_store->num_lines + lines_required >= last_non_empty_line_store->max_lines)
    {
        u64 alloc_size = Kilobytes(64);
        
        LineStore *new_line_store = (LineStore *) platform.AllocateMemory(alloc_size);
        new_line_store->line_array = (Cursor *)((u8 *)new_line_store + sizeof(LineStore));
        new_line_store->lines_before_store = last_non_empty_line_store->lines_before_store + last_non_empty_line_store->num_lines;
        new_line_store->num_lines = 0;
        new_line_store->max_lines = (alloc_size - sizeof(LineStore)) / sizeof(Cursor);
        
        result = new_line_store;
    } else {
        result = last_non_empty_line_store;
    }
    return result;
}


// TODO should we rename cursor to charpos?
// NOTE:: assumed that (lf->total_characters == lf->total_bytes)
internal u32
CharacterPosToBytePos(TextBuffer *buffer, Cursor cursor)
{
    if(buffer->total_characters == buffer->total_bytes)
    {
        return cursor;
    }
    
    RawBufSection *section = GetSectionContainingCharPos(buffer, cursor);
    Assert(section);
    u32 byte = CharacterPosToBytePos(section, cursor);
    return byte;
    
}
internal UTF8
GetByte(TextBuffer *buffer, u32 byte_pos)
{
    UTF8 *byte_addr = GetByteAddress_(buffer, byte_pos);
    return *byte_addr;
}

struct UTF8ByteSequence
{
    u32 num_bytes;
    UTF8 bytes[4];
};

internal UTF8ByteSequence
GetBytes(TextBuffer *buffer, u32 cursor)
{
    UTF8ByteSequence result;
    
    Cursor byte_position = CharacterPosToBytePos(buffer, cursor);
    UTF8 *utf8_bytes = GetByteAddress_(buffer, byte_position);
    
    result.num_bytes = GetMultibyteLength(utf8_bytes[0]);
    
    result.bytes[0] = utf8_bytes[0];
    result.bytes[1] = utf8_bytes[1];
    result.bytes[2] = utf8_bytes[2];
    result.bytes[3] = utf8_bytes[3];
    
    return result;
}

internal UTF32
GetCodepoint(TextBuffer *buffer, Cursor cursor)
{
    TIMED_BLOCK;
    UTF32 result;
    
    if(cursor >= buffer->total_characters)
    {
        result = UTF32_PLACEHOLDER_CHARACTER;
    }
    else if(buffer->total_characters == buffer->total_bytes)
    {
        result = GetByte(buffer, cursor);
    }
    else
    {
        // we can just get the byte directly from the buffer
        // because we KNOW all the bytes for a character are
        // stored contiguously in a single section
        u32 byte_position = CharacterPosToBytePos(buffer, cursor);
        
        UTF8 *utf8_bytes = GetByteAddress_(buffer, byte_position);
        result = GetCodepointFromUTF8(utf8_bytes);
    }
    
    return result;
}

internal Cursor
GetRawOnePastLastLineChar(TextBuffer *buffer, u32 line)
{
    TIMED_BLOCK;
    Cursor cursor = 0;
    if(line + 1 < buffer->num_lines)
    {
        cursor = LinePos(buffer, line + 1);
    }
    else if(line + 1 == buffer->num_lines)
    {
        cursor = buffer->total_characters;
    }
    else
    {
        Assert(!"queried line is too high!");
    }
    return cursor;
}

internal Cursor
GetRawLastLineChar(TextBuffer *buffer, u32 line)
{
    Cursor cursor;
    
    if(line + 1 < buffer->num_lines)
    {
        cursor = LinePos(buffer, line + 1) - 1;
    }
    else
    {
        cursor = (buffer->total_characters > 0) ? (buffer->total_characters - 1) : 0;
    }
    
    return cursor;
}

// excludes newline characters('\r' or '\n')
// TODO support unicode line and paragraph seperators
internal Cursor
GetLastLineChar(TextBuffer *buffer, u32 line)
{
    Cursor cursor = GetRawLastLineChar(buffer, line);
    
    // TODO
    // this can be optimized with a raw read of the byte
    // because we are matching for an ascii char
    Cursor line_start = LinePos(buffer, line);
    char character = GetCodepoint(buffer, cursor);
    while((character == '\r' || character == '\n') && cursor > line_start)
    {
        cursor--;
        if(cursor > 0)
        {
            character = GetCodepoint(buffer, cursor);
        }
        else
        {
            break;
        }
    }
    
    return cursor;
}

internal Cursor
GetOnePastLastLineChar(TextBuffer *buffer, u32 line)
{
    TIMED_BLOCK;
    Cursor cursor = GetRawOnePastLastLineChar(buffer, line);
    
    Cursor line_start = LinePos(buffer, line);
    if(cursor > 0)
    {
        char character = GetCodepoint(buffer, cursor - 1);
        while((character == '\r' || character == '\n') && cursor > line_start)
        {
            cursor--;
            if(cursor > 0)
            {
                character = GetCodepoint(buffer, cursor - 1);
            }
            else
            {
                break;   
            }
        }
    }
    
    return cursor;
}

internal b32
LineIsBlank(TextBuffer *buffer, u32 line)
{
    b32 result;
    
    Cursor start = LinePos(buffer, line);
    Cursor one_past_line_end = GetOnePastLastLineChar(buffer, line);
    
    result = true;
    
    for(Cursor cursor = start;
        cursor < one_past_line_end;
        cursor++)
    {
        UTF32 codepoint = GetCodepoint(buffer, cursor);
        if(!IsWhitespace(codepoint))
        {
            result = false;
            break;
        }
    }
    
    return result;
}


internal u32
LineCharLength(TextBuffer *buffer, u32 line)
{
    u32 ret;
    Cursor one_past_last_char = GetOnePastLastLineChar(buffer, line);
    ret = one_past_last_char - LinePos(buffer, line); 
    return ret;
}


// TODO there may be a typo here, test this rigorously!!!!
u16 CP1252_mapping[32] = {0x20AC,
    0xFFFD,  // undefined in CP1252, probably not CP1252 at all if this char is seen
    0x201a,
    0x0192,
    0x201E,
    0x2026,
    0x2020,
    0x2021,
    0x02c6,
    0x2030,
    0x0160,
    0x2039,
    0x0152,
    0xFFFD,
    0x017D,
    0xFFFD,
    
    0xFFFD,
    0x2018,
    0x2019,
    0x201C,
    0x201D,
    0x2022,
    0x2013,
    0x2014,
    0x02dc,
    0x2122,
    0x0161,
    0x203a,
    0x0153,
    0xFFFD,
    0x017e,
    0x0178};
internal UTF32
GetCP1252Codepoint(UTF8 byte)
{
    UTF32 codepoint;
    if(byte >= 0x80 && byte <= 0x9F) {
        codepoint = CP1252_mapping[byte - 0x08];
    } else {
        codepoint = byte;
    }
    return codepoint;
}

internal LineStore *
AllocateAndPushBackLineStore(TextBuffer *buffer)
{
    // NOTE:: a 64 kilobyte line store can store ~2000 lines (64k / sizeof(Cursor))
    u64 alloc_size = Kilobytes(64);
    LineStore *line_store = (LineStore *) platform.AllocateMemory(alloc_size);
    
    line_store->lines_before_store = buffer->num_lines;
    line_store->line_array = (Cursor *)((u8 *)line_store + sizeof(LineStore));
    line_store->max_lines = (alloc_size - sizeof(LineStore)) / sizeof(Cursor);
    line_store->num_lines = 0;
    
    DL_INSERT_AFTER(buffer->line_store_sentinel.prev, line_store);
    return line_store;
}

internal void
PushUTF8Character(TextBuffer *buffer, UTF8 *byte, u32 num_bytes)
{
    RawBufSection *section = buffer->section_sentinel.prev;
    if(section->num_bytes + num_bytes > section->max_bytes)
    {
        section = AllocateAndPushBackSection(buffer);
    }
    
    UTF8 *dest = section->data + section->num_bytes;
    for(int i = 0;
        i < num_bytes;
        i++)
    {
        *dest++ = *byte++;
    }
    
    section->num_bytes += num_bytes;
    section->num_chars++;
    section->cached_pos_pair.is_valid = false;
    
    buffer->total_bytes += num_bytes;
    buffer->total_characters++;
}

internal void
PushCodepoint(TextBuffer *buffer, UTF32 codepoint)
{
    UTF8 utf8_bytes[4];
    u32 num_bytes = CodepointToUTF8(codepoint, utf8_bytes);
    PushUTF8Character(buffer, utf8_bytes, num_bytes);
}

internal void
PushLine(TextBuffer *buffer, u32 cursor)
{
    LineStore *line_store = buffer->line_store_sentinel.prev;
    if(line_store->num_lines + 1 >= line_store->max_lines)
    {
        line_store = AllocateAndPushBackLineStore(buffer);
    }
    
    buffer->num_lines++;
    line_store->line_array[line_store->num_lines++] = cursor;
}

internal LinesCount
PushAllLinesToLineArray(TextBuffer *buffer)
{
    LinesCount lines_count = {};
    AllocateAndPushBackLineStore(buffer);
    
    // NOTE:: all files have at least one line!
    PushLine(buffer, 0);
    
    Cursor cursor = 0;
    if (buffer->total_characters > 0)
    {
        while (cursor < buffer->total_characters - 1)
        {
            UTF32 this_codepoint = GetCodepoint(buffer, cursor);
            UTF32 next_codepoint = GetCodepoint(buffer, cursor + 1);
            
            if (this_codepoint == '\r' && next_codepoint == '\n') {
                cursor += 2;
                PushLine(buffer, cursor);
                lines_count.endings_of_type[LineEnding_CRLF] ++;
            }
            else
            {
                cursor++;
                if (this_codepoint == '\r')
                {
                    PushLine(buffer, cursor);
                    lines_count.endings_of_type[LineEnding_CR]++;
                }
                else if (this_codepoint == '\n')
                {
                    PushLine(buffer, cursor);
                    lines_count.endings_of_type[LineEnding_LF]++;
                }
                
            }
        }
    } 
    
    UTF32 last_codepoint = GetCodepoint(buffer, cursor);
    if(last_codepoint == '\r' ||
       last_codepoint == '\n')
    {
        PushLine(buffer, cursor);
    }
    return lines_count;
}


internal b32
DifferentLineEndingsExist(LinesCount *lines_count)
{
    b32 result;
    if(lines_count->endings_of_type[LineEnding_CR] == lines_count->endings_of_type[LineEnding_CRLF] &&
       lines_count->endings_of_type[LineEnding_CRLF] == lines_count->endings_of_type[LineEnding_LF])
    {
        result = false;
    }
    else
    {
        result = true;
    }
    
    return result;
}

internal b32
NoLinesExist(LinesCount *lines_count)
{
    b32 result;
    if(lines_count->endings_of_type[LineEnding_CR] == 0 &&
       lines_count->endings_of_type[LineEnding_LF] == 0 &&
       lines_count->endings_of_type[LineEnding_CRLF] == 0)
    {
        result = true;
    }
    else
    {
        result = false;
    }
    
    return result;
}

internal LineEndingType
HighestOccuringLineEnding(LinesCount *lines_count)
{
    LineEndingType result;
    if(lines_count->endings_of_type[LineEnding_LF] > lines_count->endings_of_type[LineEnding_CR] &&
       lines_count->endings_of_type[LineEnding_LF] > lines_count->endings_of_type[LineEnding_CRLF])
    {
        result = LineEnding_LF;
    }
    else
    {
        if(lines_count->endings_of_type[LineEnding_CR] > lines_count->endings_of_type[LineEnding_CRLF])
        {
            result = LineEnding_CR;
        }
        else
        {
            result = LineEnding_CRLF;
            
        }
    }
    return result;
}

internal void
InitDefaultTextBuffer(TextBuffer *buffer)
{
    DL_INIT_SENTINEL(&buffer->section_sentinel);
    DL_INIT_SENTINEL(&buffer->line_store_sentinel);
}

internal TextBuffer *
CreateTextBuffer(MemoryArena *arena)
{
    TextBuffer *buffer;
    buffer = PushType(arena, TextBuffer);
    
    InitDefaultTextBuffer(buffer);
    AllocateAndPushBackSection(buffer);
    PushLine(buffer, 0);
    
    return buffer;
}


internal String
GetTemporaryBufferString(TextBuffer *buffer, u32 min_cursor, u32 min_line, u32 max_cursor, u32 max_line)
{
    String result = {};
    
    u32 min_byte_pos = CharacterPosToBytePos(buffer, min_cursor);
    u32 max_byte_pos = CharacterPosToBytePos(buffer , max_cursor);
    
    u32 num_bytes = max_byte_pos - min_byte_pos + 1;
    result.ptr = PushCount(&platform.frame_arena, UTF8, num_bytes);
    result.len = num_bytes;
    
    u32 offset = 0;
    while(offset < num_bytes)
    {
        u32 byte_pos = min_byte_pos + offset;
        UTF8 *src = GetByteAddress_(buffer, byte_pos);
        UTF8 *dest = result.ptr + offset;
        
        *dest = *src;
        offset++;
    }
    
    return result;
}

internal void
RemoveLine(TextBuffer *buffer, u32 line_index)
{
    // NOTE:: this should always be true
    //       but if its not there is no need to do the shifting
    LineStore *line_store = GetLineStoreContainingLine(buffer, line_index);
    
    Assert(buffer->num_lines > 0);
    
    u32 line_index_in_store = line_index - line_store->lines_before_store;
    
    u32 num_lines = line_store->num_lines - (line_index_in_store + 1);
    memmove(line_store->line_array + line_index_in_store,
            line_store->line_array + line_index_in_store + 1,
            num_lines* sizeof(Cursor));
    
    buffer->num_lines--;
    line_store->num_lines--;
    
    LineStore *next_line_store = line_store->next;
    for(LineStore *line_store_to_shift = next_line_store;
        line_store_to_shift != &buffer->line_store_sentinel;
        line_store_to_shift = line_store_to_shift->next)
    {
        line_store_to_shift->lines_before_store--;
    }
}

internal void
RemoveCharacter_(TextBuffer *buffer, Cursor cursor, u32 line_index)
{
    RawBufSection *section = GetSectionContainingCharPos(buffer, cursor);
    Assert(section);
    
    u32 byte_pos = CharacterPosToBytePos(buffer, cursor);
    UTF8 *utf8_bytes = GetByteAddress_(buffer, byte_pos);
    u32 num_bytes = GetMultibyteLength(utf8_bytes[0]);
    UTF32 codepoint = GetCodepointFromUTF8(utf8_bytes, num_bytes);
    
    if(IsValid(codepoint))
    {
        u32 bytes_to_remove = num_bytes;
        
        MoveSectionGapToCursor(buffer, section, cursor);
        section->num_bytes -= bytes_to_remove;
        section->num_gap_bytes += bytes_to_remove;
        section->num_chars--;
        
        // move all the section anchors up
        for (RawBufSection *section_to_reposition = section->next;
             section_to_reposition != &buffer->section_sentinel;
             section_to_reposition = section_to_reposition->next)
        {
            section_to_reposition->chars_before_section--;
            section_to_reposition->bytes_before_section -= bytes_to_remove;
        }
        
        // move all lines after cursor down by the bytes removed"
        for (int i = line_index + 1; i < buffer->num_lines; i++)
        {
            (*GetLineArrayEntry(buffer, i))--;
        }
        buffer->total_characters--;
        buffer->total_bytes -= bytes_to_remove;
        
        section->cached_pos_pair.is_valid = false;
    }
}

struct BufferDeleteCharacterResult
{
    b32 lines_changed;
    u32 chars_removed;
};

// // deletes the character directly after the gap
// // NOTE:: CR LF is treated as one character
// // DeleteCharacter treats CR and LF as one character, and handles line deletion
// // RemoveCharacter__ treats every codepoint in the raw buffer as a character; it's meant
// //                   for internal use only or for users who are familiar with the inner
// //                   workings of Terranium
// // DeleteCharacter calls RemoveCharacter_
// // TODO make an efficient X version for DeleteCharacter
// internal BufferDeleteCharacterResult
// DeleteCharacter(TextBuffer *buffer, Cursor cursor, u32 line_index)
// {
//     BufferDeleteCharacterResult result = {};
//     result.lines_changed = true;
//     result.chars_removed;

//     if(cursor < buffer->total_characters)
//     {
//         UTF8 codepoint = GetCodepoint(buffer, cursor);

//         if(codepoint == '\r')
//         {
//             // TODO when we remove the line eventually we may want to shrink the max lines
//             //     to create space for other things on the primary storage unit (like
//             //     the raw lf)
//             RemoveLine(buffer, line_index+1);

//             // TODO likely bug: what if cursor + 1 is not of range
//             UTF8 next_codepoint = GetCodepoint(buffer, cursor + 1);
//             if(next_codepoint == '\n')
//             {
//                 RemoveCharacter_(buffer, cursor, line_index);
//                 RemoveCharacter_(buffer, cursor, line_index);
//                 result.chars_removed = 2;
//             }
//             else
//             {
//                 result.chars_removed = 1;
//                 RemoveCharacter_(buffer, cursor, line_index);
//             }
//         }
//         else if(codepoint == '\n')
//         {
//             result.chars_removed = 1;
//             RemoveCharacter_(buffer, cursor, line_index);
//             RemoveLine(buffer, line_index+1);
//         }
//         else
//         {
//             result.chars_removed = 1;
//             RemoveCharacter_(buffer, cursor, line_index);
//             result.lines_changed = false;
//         }
//     }
//     else
//     {
//         result.chars_removed = 0;
//         result.lines_changed = false;
//     }

//     return result;
// }

// TODO:: make insertcharacterx which precalculates gap expansion
// NOTE:: more optimized insertion of character without moving linearrays
inline void
InsertByteAtSectionGap_(RawBufSection *section, UTF8 utf8_byte)
{
    // there should be gap bytes available
    Assert(section->num_gap_bytes > 0);
    
    section->data[section->bytes_before_gap] = utf8_byte;
    section->bytes_before_gap++;
    section->num_gap_bytes--;
}

// expands the section gap if the section gap size is 0
// returns true if expansion is succesful or gap expansion is not needed
// returns false if there is no more space on the section to expand the gap
internal b32
ExpandGapIfLessThanSize(RawBufSection *section, u32 size_required)
{
    b32 result = true;
    if(section->num_gap_bytes < size_required)
    {
        // TODO gap expansion might be a waste of computation perhaps we can just say that the unused space in the
        //      section is the gap?
        // TODO should we still expand the gap is its less than GAP_BUFFER_ALLOC_SIZE?
        //      or maybe its not if the gap is big enough
        u32 num_expansion_bytes = Min(GAP_BUFFER_ALLOC_SIZE, section->max_bytes - section->num_bytes);
        
        if(num_expansion_bytes < size_required)
        {
            result = false;
        }
        
        Assert(num_expansion_bytes >= size_required);
        UTF8 *after_gap = SECTION_BYTE_AFTER_GAP(section);
        u32 bytes_to_move = SECTION_NUM_BYTES_AFTER_GAP(section);
        memmove(after_gap + num_expansion_bytes, after_gap, bytes_to_move);
        
        section->num_gap_bytes += num_expansion_bytes;
    }
    
    return result;
}

// TODO insert character is not efficient for inserting a chunk of text since it will reparse the LF for every chaaracter
internal void
InsertCharacter(TextBuffer *buffer, Cursor cursor, u32 line, UTF32 codepoint)
{
    RawBufSection *section = GetSectionContainingCharPos(buffer, cursor);
    Assert(section);
    
    UTF8 utf8_bytes[4];
    u32 bytes_used = CodepointToUTF8(codepoint, utf8_bytes);
    if(!SectionSpaceAvailible(section, bytes_used))
    {
        platform.Info("not enough section space availible dealing with that...\n");
        // TODO there is not enough space availible, so its time to create a new section
        // and split half the data here and half the data there
        // TODO splitting half the data here and half the data there may not be the most efficient way to prevent fragmentation
        //     should we try to search for availible free space on other sections or do a defrag first??
        
        RawBufSection *new_section = AllocateSectionAndSplitContent(buffer, section);
        
        if(cursor >= new_section->chars_before_section)
        {
            section = new_section;
        }
    }
    
    // TODO optimization:: don't always move the gap to view cursor
    MoveSectionGapToCursor(buffer, section, cursor);
    
    section->chars_before_gap++;
    
    section->cached_pos_pair.is_valid = false;
    
    // this should never be false since we already checked that enough space was availible
    Assert(ExpandGapIfLessThanSize(section, bytes_used));
    
    for(int i = 0; i < bytes_used; i++)
    {
        InsertByteAtSectionGap_(section, utf8_bytes[i]);
    }
    section->num_bytes += bytes_used;
    
    buffer->total_bytes += bytes_used;
    buffer->total_characters++;
    section->num_chars++;
    
    for(RawBufSection *scan_section = section->next;
        scan_section != &buffer->section_sentinel;
        scan_section = scan_section->next)
    {
        scan_section->chars_before_section++;
        scan_section->bytes_before_section += bytes_used;
    }
    
    // move all line anchors up by the chars inserted
    // TODO should we move by a bytes instead
    for(int i = line + 1; i < buffer->num_lines; i++)
    {
        *GetLineArrayEntry(buffer, i) += 1;
    }
}

internal void
InsertNewlineInLineArray(TextBuffer *buffer, u32 insert_after_index, Cursor cursor /*new line starts here*/)
{
    u32 insert_index = insert_after_index + 1;
    LineStore *line_store = GetLineStoreContainingLine(buffer, insert_index);
    if(!line_store)
    {
        // NOTE:: that this is the proper bug_free way of dealing with line-store like data structures
        //       and should be used for section and token array instead of "last section contains the byte
        //       pos" type of hack!!!
        if(insert_index == buffer->num_lines)
            line_store = GetOrAllocateNextLineStoreWithAvailibleSpace(buffer, 1);
        
        Assert(line_store);
    }
    
    if(line_store->num_lines + 1 >= line_store->max_lines) {
        // TODO dumbest possible alglorithm: create a new LineStore after this line store
        //     and split habuffer of the data over there
        LineStore *new_line_store = AllocateLineStoreAndSplitContent(buffer, line_store);
        if(insert_index > new_line_store->lines_before_store)
        {
            line_store = new_line_store;
        }
    }
    
    Assert(insert_index >= line_store->lines_before_store);
    u32 insert_index_in_store = insert_index - line_store->lines_before_store;
    
    memmove(line_store->line_array + insert_index_in_store + 1,
            line_store->line_array + insert_index_in_store,
            (line_store->num_lines - insert_index)*sizeof(Cursor));
    
    line_store->line_array[insert_index] = cursor;
    line_store->num_lines++;
    buffer->num_lines++;
}

// TODO consider removing out_chars and out_lines
internal InsertTextResult
InsertText(TextBuffer *buffer,
           Cursor cursor, u32 line, UTF8 *text, u32 text_len)
{
    InsertTextResult result = {};
    
    u32 index = 0;
    u32 current_line = line;
    while(index < text_len)
    {
        UTF8 *utf8_bytes = text + index;
        u32 num_utf8_bytes = GetMultibyteLength(utf8_bytes[0]);
        u32 codepoint = GetCodepointFromUTF8(utf8_bytes, num_utf8_bytes);
        if(codepoint == '\r')
        {
            if(index + 1 < text_len)
            {
                UTF8 *next_utf8_byte = text + index + 1;
                // NOTE:: this assumes that the num_utf8_bytes is actually accurate and not an error
                //       and will consume that many bytes forwards without bounds checking
                u32 next_num_utf8_bytes = GetMultibyteLength(utf8_bytes[0]);
                u32 next_codepoint = GetCodepointFromUTF8(next_utf8_byte, next_num_utf8_bytes);
                if(next_codepoint == '\n')
                {
                    // consume two bytes
                    // TODO this doesn't retain information about the type of newline
                    
                    InsertCharacter(buffer, cursor++, current_line, codepoint);
                    InsertCharacter(buffer, cursor++, current_line, next_codepoint);
                    
                    InsertNewlineInLineArray(buffer, current_line, cursor);
                    current_line++;
                    result.num_chars += 2;
                    index += next_num_utf8_bytes;
                    // at the end of the loop index is again incremented by num_utf8_bytes
                }
                else
                {
                    goto single_byte_new_line;
                }
            }
            else
            {
                goto single_byte_new_line;   
            }
        }
        else if (codepoint == '\n')
        {
            single_byte_new_line:
            InsertNewlineInLineArray(buffer, current_line, cursor);
            InsertCharacter(buffer, cursor++, current_line, codepoint);
            current_line++;
            result.num_chars += 1; // a newline is a character too
        }
        else
        {
            // TODO make a insert utf8 bytes that way we don't have to convert to codepoint and then back to utf8
            InsertCharacter(buffer, cursor++, current_line, codepoint);
            result.num_chars += 1;
        }
        
        index += num_utf8_bytes;
    }
    
    result.num_newlines = current_line - line;
    
    return result;
}

inline u32
GetNumBytesBetween(TextBuffer *buffer, Cursor start, Cursor end)
{
    u32 result;
    u32 end_byte_pos = CharacterPosToBytePos(buffer, end + 1);
    u32 start_byte_pos = CharacterPosToBytePos(buffer, start);
    result = end_byte_pos - start_byte_pos;
    
    return result;
}

internal u32
GetNumBytesInLine(TextBuffer *buffer, u32 line)
{
    TIMED_BLOCK;
    u32 result;
    
    Cursor start = LinePos(buffer, line);
    Cursor one_past_last_char = GetRawOnePastLastLineChar(buffer, line);
    
    result = CharacterPosToBytePos(buffer, one_past_last_char) - CharacterPosToBytePos(buffer, start);
    
    return result;
}

internal u32
GetCharsInLine(TextBuffer *buffer, u32 line)
{
    u32 result;
    
    Cursor start = LinePos(buffer, line);
    Cursor one_past_last_char = GetRawOnePastLastLineChar(buffer, line);
    
    result = one_past_last_char - start;
    
    return result;
}


internal u32
CopyBufferChars(TextBuffer *buffer, UTF8 *output, u32 max_output_size, Cursor start_pos, u32 num_chars)
{
    u32 bytes_copied = 0;
    u32 chars_copied = 0;

    for(;;)
    {
        Cursor cursor = start_pos + chars_copied;
        UTF8ByteSequence byte_sequence = GetBytes(buffer, cursor);
        
        if(bytes_copied + byte_sequence.num_bytes > max_output_size)
        {
            break;
        }
        else
        {

            memcpy(output + bytes_copied,
                   byte_sequence.bytes,
                   byte_sequence.num_bytes);

            bytes_copied += byte_sequence.num_bytes;
        }
        
        
        chars_copied++;
        
        if(chars_copied >= num_chars)
        {
            break;
        }
    }

    return bytes_copied;
}

inline u32
CopyLineContents(TextBuffer *buffer, UTF8 *output, u32 max_output_size, u32 line)
{
    TIMED_BLOCK;
    u32 bytes_copied = 0;
    
    u32 chars_in_line = GetCharsInLine(buffer, line);
    if(chars_in_line)
    {
        Cursor start_pos = LinePos(buffer, line);
        bytes_copied = CopyBufferChars(buffer, output, max_output_size, start_pos, chars_in_line);
    }
    
    return bytes_copied;
}

// TODO do range checking
internal u32
CopyEntireBuffer(TextBuffer *buffer, UTF8 *output, u32 output_size)
{
    u32 chars_outputted = 0;
    for(RawBufSection *section = GetFirstSection(buffer);
        section != &buffer->section_sentinel;
        section = section->next)
    {
        {   // copy the bytes before the gap
            UTF8 *dest = output + chars_outputted;
            UTF8 *src = section->data;
            u32 size = section->bytes_before_gap;
            memcpy(dest, src, size);
            chars_outputted += size;
        }
        {   // copy the bytes after the gap
            UTF8 *dest = output + chars_outputted;
            UTF8 *src = section->data + section->bytes_before_gap + section->num_gap_bytes;
            u32 size = section->num_bytes - section->bytes_before_gap;
            memcpy(dest, src, size);
            chars_outputted += size;
        }
    }
    return chars_outputted;
}

internal u32
GetLineContainingCharPos(TextBuffer *buffer, Cursor char_pos)
{
    // TODO make this a binary search
    Assert(char_pos < buffer->total_characters);
    
    u32 line = 0;
    if(buffer->num_lines > 0)
    {
        int i = 0;
        for(; i < buffer->num_lines; i++)
        {
            u32 line_start = LinePos(buffer, i);
            if(line_start > char_pos)
            {
                line = i - 1;
                break;
            }
        }
        if(i == buffer->num_lines)
        {
            line = buffer->num_lines - 1;
        }
    }
    
    // we return the last line index for
    // any char pos, no matter how far away from the last line
    return line;
}

internal u32
DeleteText(TextBuffer *buffer, Cursor from, u32 line, u32 chars_to_delete)
{
    Cursor to = from + chars_to_delete;
    // NOTE:: bytes_to_delete does not include gap bytes
    u32 bytes_to_delete = CharacterPosToBytePos(buffer, to) - CharacterPosToBytePos(buffer, from);
    
    u32 new_lines = 0;
    // count the new lines
    Cursor scan_cursor = from;
    while(scan_cursor < to)
    {
        UTF32 codepoint = GetCodepoint(buffer, scan_cursor);
        if(codepoint == '\r')
        {
            if(scan_cursor + 1 < to)
            {
                UTF32 next_codepoint = GetCodepoint(buffer, scan_cursor + 1);
                if(next_codepoint == '\n')
                {
                    new_lines++;
                    scan_cursor += 2;
                } else goto single_new_line;
            }
            else
            {
                single_new_line:
                new_lines++;
                scan_cursor++;
            }
        }
        else if(codepoint == '\n')
        {
            new_lines++;
            scan_cursor++;
        }
        else
        {
            scan_cursor++;
        }
    }
    
    
    RawBufSection *start_section = GetSectionContainingCharPos(buffer, from);
    u32 byte_pos = CharacterPosToBytePos(start_section, from);
    u32 char_pos = from;
    
    RawBufSection *section = start_section;
    u32 bytes_left = bytes_to_delete;
    u32 chars_left = chars_to_delete;
    while(bytes_left)
    {
        Assert(section != &buffer->section_sentinel);
        
        section->cached_pos_pair.is_valid = false;
        
        u32 char_pos_in_section = char_pos - section->chars_before_section;
        u32 byte_pos_in_section = byte_pos - section->chars_before_section;
        u32 bytes_to_delete_in_section = Min(bytes_left, section->num_bytes - byte_pos_in_section);
        u32 chars_to_delete_in_section = Min(chars_left, section->num_chars - char_pos_in_section);
        
        MoveSectionGapToCursor(buffer, section, char_pos);
        // effectively delete what we selected
        section->num_gap_bytes += bytes_to_delete_in_section;
        section->num_bytes -= bytes_to_delete_in_section;
        section->num_chars -= chars_to_delete_in_section;
        
        section->bytes_before_section -= bytes_to_delete - bytes_left;
        section->chars_before_section -= chars_to_delete - chars_left;
        
        bytes_left -= bytes_to_delete_in_section;
        chars_left -= chars_to_delete_in_section;
        
        char_pos += chars_to_delete_in_section;
        byte_pos += bytes_to_delete_in_section;
        section = section->next;
    }
    
    buffer->total_bytes -= bytes_to_delete;
    buffer->total_characters -= chars_to_delete;
    
    // remove all the new lines we that were in our selection
    // (so starting from the next line down after the line the selection starts)
    // this remove line is inefficient can we just kill the section
    // that needs to be killed?
    for(int i = 0; i < new_lines; i++)
    {
        RemoveLine(buffer, line + 1);
    }
    
    // shift all the remaining lines from this one(the start of our selection) onwards
    for(int i = line + 1; i < buffer->num_lines; i++)
    {
        *GetLineArrayEntry(buffer, i) -= chars_to_delete;
    }
    // shift the section anchors
    for(RawBufSection *scan_section = section;
        scan_section != &buffer->section_sentinel;
        scan_section = scan_section->next)
    {
        scan_section->chars_before_section -= chars_to_delete;
        scan_section->bytes_before_section -= bytes_to_delete;
    }
    
    return new_lines;
}

