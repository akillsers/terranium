internal void
InitTextUndoStorage(TextUndoStorage *undo_storage)
{
    if(undo_storage)
    {
        DL_INIT_SENTINEL(&undo_storage->sentinel_text_op_link);
        undo_storage->next_operation_link = &undo_storage->sentinel_text_op_link;
    }
}

internal void
PushTextOperation(TextUndoStorage *undo_storage, TextOperation operation)
{
    if(!undo_storage) return;
    
    TextOperationLink *text_operation_link = PushType(&undo_storage->arena, TextOperationLink);
    
    if(operation.num_bytes > 0)
    {
        UTF8 *text_storage = PushCount(&undo_storage->arena, UTF8, operation.num_bytes);

        CopyUTF8(text_storage, operation.text, operation.num_bytes);
        operation.text = text_storage;
    }
    
    text_operation_link->operation = operation;
    MemoryArenaSnapshot saved_arena = TakeSnapshot(&undo_storage->arena);
    text_operation_link->arena_snapshot = saved_arena;
    text_operation_link->prev = undo_storage->next_operation_link;
    text_operation_link->next = &undo_storage->sentinel_text_op_link;

    undo_storage->next_operation_link->next = text_operation_link;
    undo_storage->sentinel_text_op_link.prev = text_operation_link;
    undo_storage->next_operation_link = text_operation_link;
}

internal void
RecordTextOperation(TextUndoStorage *undo_storage, TextOperation operation)
{
    if(!undo_storage) return;

    UndoArenaToSnapshot(&undo_storage->arena, undo_storage->next_operation_link->arena_snapshot);

    TextOperationLink *previous_operation_link = undo_storage->next_operation_link;
    TextOperation previous_operation = previous_operation_link->operation;

    TextOperationLink *text_operation_link = 0;

    if(operation.type == TextOp_insert &&
       previous_operation.type == TextOp_insert &&
       operation.num_chars == 1 &&
       operation.start == previous_operation.start + previous_operation.num_chars &&
       operation.flags == previous_operation.flags &&
       operation.start_line == previous_operation.start_line)
    {
        TextOperation collated_operation = {};
        collated_operation.flags = operation.flags;
        collated_operation.type = operation.type;
        collated_operation.cursor_before = previous_operation.cursor_before;
        collated_operation.line_before = previous_operation.line_before;
        collated_operation.cursor_after = operation.cursor_after;
        collated_operation.line_after = operation.line_after;

        collated_operation.start = previous_operation.start;
        collated_operation.start_line = operation.start_line;
        collated_operation.num_chars = previous_operation.num_chars + operation.num_chars;
        collated_operation.num_newlines = 0;
        collated_operation.num_bytes = previous_operation.num_bytes + operation.num_bytes;

        collated_operation.text = PushCount(&platform.frame_arena, UTF8, collated_operation.num_bytes);
        CopyUTF8(collated_operation.text, previous_operation.text, previous_operation.num_bytes);
        CopyUTF8(collated_operation.text + previous_operation.num_bytes,
                 operation.text,
                 operation.num_bytes);

        undo_storage->next_operation_link = undo_storage->next_operation_link->prev;
        UndoArenaToSnapshot(&undo_storage->arena, undo_storage->next_operation_link->arena_snapshot);
        PushTextOperation(undo_storage, collated_operation);
    }
    else if(operation.type == TextOp_delete &&
            previous_operation.type == TextOp_delete &&
            operation.num_chars == 1 &&
            operation.start + operation.num_chars == previous_operation.start &&
            operation.flags == previous_operation.flags &&
            //operation.start_line == operation.start_line &&
            previous_operation.num_chars < 25)
    {
        TextOperation collated_operation = {};
        collated_operation.flags = operation.flags;
        collated_operation.type = operation.type;
        collated_operation.cursor_before = previous_operation.cursor_before;
        collated_operation.line_before = previous_operation.line_before;
        collated_operation.cursor_after = operation.cursor_after;
        collated_operation.line_after = operation.line_after;

        collated_operation.start = operation.start;
        collated_operation.start_line = operation.start_line;
        collated_operation.num_newlines = previous_operation.num_newlines + operation.num_newlines;
        collated_operation.num_bytes = previous_operation.num_bytes + operation.num_bytes;

        collated_operation.text = PushCount(&platform.frame_arena, UTF8, collated_operation.num_bytes);
        CopyUTF8(collated_operation.text, operation.text, operation.num_bytes);
        CopyUTF8(collated_operation.text + operation.num_bytes,
                 previous_operation.text,
                 previous_operation.num_bytes);

        
        undo_storage->next_operation_link = undo_storage->next_operation_link->prev;
        UndoArenaToSnapshot(&undo_storage->arena, undo_storage->next_operation_link->arena_snapshot);
        PushTextOperation(undo_storage, collated_operation);
    }
    else
    {
        PushTextOperation(undo_storage, operation);
    }


}

internal TextOperation
GetRedoOperation(TextUndoStorage *undo_storage, u64 flags)
{
    TextOperation result = {};

    if(undo_storage)
    {
        TextOperationLink *next_redo_operation_link = undo_storage->next_operation_link->next;
        if(next_redo_operation_link != &undo_storage->sentinel_text_op_link)
        {
            result = next_redo_operation_link->operation;

            if(flags & GetTextOperation_remove)
            {
                undo_storage->next_operation_link = next_redo_operation_link;
            }
        }
    }
    
    return result;
}

internal TextOperation
GetUndoOperation(TextUndoStorage *undo_storage, u64 flags)
{
    TextOperation result = {};
    
    if(undo_storage)
    {
        result = undo_storage->next_operation_link->operation;

        if(undo_storage->next_operation_link != &undo_storage->sentinel_text_op_link)
        {
            if(flags & GetTextOperation_remove)
            {
                undo_storage->next_operation_link = undo_storage->next_operation_link->prev;
            }
        }
    }
    
    return result;
}
